# CAMDTv1

The Climate Agriculture Modeling and Decision Tool (CAMDT) is a computer desktop tool designed to guide decision-makers in adopting appropriate crop and water management practices that can improve crop yields given a climatic condition.