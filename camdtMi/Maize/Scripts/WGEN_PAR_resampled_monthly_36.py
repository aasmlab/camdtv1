#Author: Eunjin Han
#Institute: IRI-Columbia University
#Date: 5/20/2019
#Purpose: Improve Richardson(1984)'s weather generator so as to
# 1) link with seasonal climate forecast provided in tercile format for each individual month (e.g., PAGASA's)
# 2) parameters of rainfall model and temperature/srad are estimated based on resampled "n" years of weather data
# 3) Monthly target values are estimated to reflect interannual variability (low-frequency)
#    Here, monthly target values (5%~95%) are extracted from the CDF curves of ""resampled weather realizations"""
#          not from historical observations. (5/24/2019)

import numpy as np
import math
import scipy
import pandas as pd
import calendar
from scipy import stats
from scipy.stats import kurtosis, skew
from numpy.linalg import inv
from scipy.stats import gamma
from scipy.optimize import curve_fit
import time
import matplotlib.pyplot as plt

start_time = time.clock()

#Fourier series with one harmonic
def func(x, Ubar, C, T):
    y=[]
    for i in range(x.shape[0]):
        #y.append(Ubar + C*math.cos(0.0172*x[i]-0.0172*T))
        y.append(Ubar + C*math.cos(0.0172*x[i]+T))
        y_array = np.asarray(y)
    return y_array
  #return C*math.cos(0.0172*(x-T))+Ubar
#=================================================================


# #================== MAIN PROGRAM ======================================
# #1) read observed weather *.WTD (skip 1st row - heading)
# #obs_file = r'C:\IRI\WGEN_Richardson\FResampler_Python\PILI.WTD'
# WTD_fname = r'C:\Users\Eunjin\IRI\PH_FAO\WGEN_PH_36\SANJ.WTD'
# data1 = np.loadtxt(WTD_fname,skiprows=1)
# #convert numpy array to dataframe
# WTD_df = pd.DataFrame({'YEAR':data1[:,0].astype(int)//1000,    #python 3.6: / --> //
#               'DOY':data1[:,0].astype(int)%1000,
#               'SRAD':data1[:,1],
#               'TMAX':data1[:,2],
#               'TMIN':data1[:,3],
#               'RAIN':data1[:,4]})

# #2) MONTHLY tercile probability forecasts for 12 months
# frst_fname = r'C:\Users\Eunjin\IRI\PH_FAO\WGEN_PH_36\SCF_input_monthly.csv'
# #read *.csv data into a dataframe
# df_scf = pd.read_csv(frst_fname, header = 0)

def WGEN_PAR (WTD_df,df_scf, wdir, station_name, ALAT, target_year):
    #===================================================
    m_doys_list = [1,32,60,91,121,152,182,213,244,274,305,335]  #starting date of each month for regular years
    m_doye_list = [31,59,90,120,151,181,212,243,273,304,334,365] #ending date of each month for regular years
    m_doys_list2 = [1,32,61,92,122,153,183,214,245,275,306,336]  #starting date of each month for leap years
    #m_doye_list2 = [31,60,91,121,152,182,213,244,274,305,335,366] #ending date of each month for leap years
    m_doye_list2 = [31,59,91,121,152,182,213,244,274,305,335,366] #ignore 29th of Feb in case of leap year
    numday_list = [31,28,31,30,31,30,31,31,30,31,30,31]
    #======================================================================

    #3) Do resampling for each month based on the given tercile SCF
    #3-1) Extract daily weather data for each month => i.e., matrix {(31 days * 30 years in case of January)
    # count how many years are recorded
    year_array = WTD_df.YEAR.unique()
    nyears = year_array.shape[0]
    nrealiz = 500 #===> Hard coded
    #wdir = "C:\\Users\\Eunjin\\IRI\\PH_FAO\\WGEN_PH_36\\"  #===> Hard coded

    ##rain_3D = np.empty((12,31,nyears,))*np.NAN  #12 months * [31 days * n years]
    ##tmax_3D = np.empty((12,31,nyears,))*np.NAN
    ##tmin_3D = np.empty((12,31,nyears,))*np.NAN
    ##srad_3D = np.empty((12,31,nyears,))*np.NAN

    rain_resampled = np.empty((nrealiz,365,))*np.NAN  #nyears *  365 days
    tmax_resampled = np.empty((nrealiz,365,))*np.NAN
    tmin_resampled = np.empty((nrealiz,365,))*np.NAN
    srad_resampled = np.empty((nrealiz,365,))*np.NAN
    
    #save monthly observed variabls into array and then return to use for bias correction later
    rain_WTD_m = np.empty((nyears,12,))*np.NAN  #nyears *  12 months
    tmin_WTD_m = np.empty((nyears,12,))*np.NAN  #nyears *  12 months
    tmax_WTD_m = np.empty((nyears,12,))*np.NAN  #nyears *  12 months
    srad_WTD_m = np.empty((nyears,12,))*np.NAN  #nyears *  12 months      



    print( '==Start WGEN_PAR rountine for resampling (in WGEN_PAR_resampled_monthly_36.py)')
    for i in range(0,12):  #12 months
        Rain_Month= np.empty((nyears,numday_list[i],))*np.NAN  #e.g., 30 years * 31 days for January
        Tmax_Month= np.empty((nyears,numday_list[i],))*np.NAN
        Tmin_Month= np.empty((nyears,numday_list[i],))*np.NAN
        SRad_Month= np.empty((nyears,numday_list[i],))*np.NAN

        for j in range(0,nyears): #loop for extracting a matrix [31 days * n years] for each month
            if (i == 10):
                print("printing Rain_month. Month i:{}, Day j:{}".format(i,j))
                print(Rain_Month[j,:])
            if calendar.isleap(year_array[j]):
                Rain_Month[j,:] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= m_doys_list2[i]) & (WTD_df["DOY"] <= m_doye_list2[i])].values
                Tmin_Month[j,:] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= m_doys_list2[i]) & (WTD_df["DOY"] <= m_doye_list2[i])].values
                Tmax_Month[j,:] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= m_doys_list2[i]) & (WTD_df["DOY"] <= m_doye_list2[i])].values
                SRad_Month[j,:] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= m_doys_list2[i]) & (WTD_df["DOY"] <= m_doye_list2[i])].values
            else: #normal year
                
                Rain_Month[j,:] = WTD_df.RAIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= m_doys_list[i]) & (WTD_df["DOY"] <= m_doye_list[i])].values
                Tmin_Month[j,:] = WTD_df.TMIN[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= m_doys_list[i]) & (WTD_df["DOY"] <= m_doye_list[i])].values
                Tmax_Month[j,:] = WTD_df.TMAX[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= m_doys_list[i]) & (WTD_df["DOY"] <= m_doye_list[i])].values
                SRad_Month[j,:] = WTD_df.SRAD[(WTD_df["YEAR"] == year_array[j])& (WTD_df["DOY"] >= m_doys_list[i]) & (WTD_df["DOY"] <= m_doye_list[i])].values
        #================================================================
        # MAKE 1) Monthly SUM, 2) SORT, 3) SAMPLE a block of weather BASED ON SCF
        #1)MAKE monthly SUM
        monthly_R_sum = np.sum(Rain_Month, axis=1) #row-wise sum
        index_sort = np.argsort(monthly_R_sum)  #Returns the indices that would sort an array.

        #save monthly variables into a matix for future use (bias correction & finding target values from theoretical CDF of rainfall based on SCF)
        rain_WTD_m[:, i] = np.sum(Rain_Month, axis = 1)
        tmin_WTD_m[:, i] = np.mean(Tmin_Month, axis = 1)
        tmax_WTD_m[:, i] = np.mean(Tmax_Month, axis = 1)
        srad_WTD_m[:, i] = np.mean(SRad_Month, axis = 1)

    ##    #just for debugging
    ##    out_fname1 = wdir+"monthly_rain_sum_"+str(i+1)+".csv"
    ##    out_fname2 = wdir+"monthly_rain_sort_index_"+str(i+1)+".csv"
    ##    np.savetxt(out_fname1, monthly_R_sum.transpose(),fmt='%5.2f', delimiter=",")
    ##    np.savetxt(out_fname2, index_sort.transpose(), fmt='%5.2f',elimiter=",")

        #3) random sampling
        BN = df_scf.iloc[0][i+1]  #e.g., 36%
        NN = df_scf.iloc[1][i+1]   #e.g., 33%
        AN = df_scf.iloc[2][i+1]     #e.g., 31%
        num_BN = int(nrealiz * (BN/100.0))
        num_AN = int(nrealiz * (AN/100.0))
        num_NN = nrealiz - num_BN - num_AN

        nsample_BN = monthly_R_sum.shape[0]//3  #python 3.x => / -> //
        nsample_AN = monthly_R_sum.shape[0]//3
        nsample_NN = monthly_R_sum.shape[0] - 2*nsample_BN

        BN_index = np.random.uniform(0,1,num_BN) * nsample_BN
        BN_index = BN_index.astype(int)  #0~9

        NN_index = nsample_BN + np.random.uniform(0,1,num_NN) * nsample_NN
        NN_index = NN_index.astype(int)  #10~19

        AN_index = nsample_BN + nsample_NN + np.random.uniform(0,1,num_AN) * nsample_AN
        AN_index = AN_index.astype(int)  #20~29 for 30 yrs of observations

        #================================================================
        #WRITE SAMPLES FOR BELOW NORMAL CATEGORY into the ONE big matrix
        for ii in range(0,num_BN):  #0~66
            temp_index = index_sort[BN_index[ii]] #***********
            rain_resampled[ii][m_doys_list[i]-1:m_doye_list[i]] = Rain_Month[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[ii][m_doys_list[i]-1:m_doye_list[i]] = Tmax_Month[temp_index,:]
            tmin_resampled[ii][m_doys_list[i]-1:m_doye_list[i]] = Tmin_Month[temp_index,:]
            srad_resampled[ii][m_doys_list[i]-1:m_doye_list[i]] = SRad_Month[temp_index,:]
        for ii in range(0,num_NN):   # 68
            temp_index = index_sort[NN_index[ii]]#***********
            rain_resampled[num_BN+ii][m_doys_list[i]-1:m_doye_list[i]] = Rain_Month[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+ii][m_doys_list[i]-1:m_doye_list[i]] = Tmax_Month[temp_index,:]
            tmin_resampled[num_BN+ii][m_doys_list[i]-1:m_doye_list[i]] = Tmin_Month[temp_index,:]
            srad_resampled[num_BN+ii][m_doys_list[i]-1:m_doye_list[i]] = SRad_Month[temp_index,:]
        for ii in range(0,num_AN):   #66 out of 200
            temp_index = index_sort[AN_index[ii]]  #***********
            rain_resampled[num_BN+num_NN+ii][m_doys_list[i]-1:m_doye_list[i]] = Rain_Month[temp_index,:]  #e.g., rain_resampled[0][0:31]
            tmax_resampled[num_BN+num_NN+ii][m_doys_list[i]-1:m_doye_list[i]] = Tmax_Month[temp_index,:]
            tmin_resampled[num_BN+num_NN+ii][m_doys_list[i]-1:m_doye_list[i]] = Tmin_Month[temp_index,:]
            srad_resampled[num_BN+num_NN+ii][m_doys_list[i]-1:m_doye_list[i]] = SRad_Month[temp_index,:]

    #=======End of Resampling => output: Sorted Matrix [ 200 years * 365 days]
    #===================================================
    #===================================================
    #====================================================================
    #2) Compute transition probability of Markov chain for each month
    #====================================================================
    #make an dataframe to save the estimated parameters for each month
    #rows for monthly rainfall parameters (p01,p11, alpha and beta), cols for months
    df = pd.DataFrame(np.zeros((4, 12)))
    df.columns = ['Jan','Feb', 'Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    #===============Separate Dry and Wet days
    P_threshold = 0.1 #mm/day
    rain= np.copy(rain_resampled)
    rain[rain_resampled < P_threshold] = 0.0 #dry day
    rain[rain_resampled >= P_threshold] = 1.0 #wet day

    p01_list = []
    p11_list = []

    ###===================================================
    ##m_doys_list = [1,32,60,91,121,152,182,213,244,274,305,335]  #starting date of each month for regular years
    ##m_doye_list = [31,59,90,120,151,181,212,243,273,304,334,365] #ending date of each month for regular years
    ###======================================================================
    for i in range(len(m_doys_list)):  #for 12 months
    #  rain_month = rain[:,m_doys_list[i]-1:m_doye_list[i]]  #[0:31]  #data[:, [1, 9]]
        target_rain = rain[:,m_doys_list[i]-1:m_doye_list[i]]  #[0:31]  #data[:, [1, 9]]
        # Estimate state-to-state transition probability
        # by counting sequnces
        #1) p(W/D), p01 = n01/(n00 + n01)
        p1 = np.array([0.0,0.0])
        p2 = np.array([0.0,1.0])
        p3 = np.array([1.0,0.0])
        p4 = np.array([1.0,1.0])
        n00_total = 0.0   #across all years
        n01_total = 0.0
        n10_total = 0.0
        n11_total = 0.0
        for ii in range(len(target_rain)):
            a = target_rain[ii]
            n00 = sum(1 for k in
                [a[j:j+len(p1)] for j in range(len(a) - len(p1) + 1)]
                if np.array_equal(k, p1))
            n01 = sum(1 for k in
                [a[j:j+len(p2)] for j in range(len(a) - len(p2) + 1)]
                if np.array_equal(k, p2))
            n10 = sum(1 for k in
                [a[j:j+len(p3)] for j in range(len(a) - len(p3) + 1)]
                if np.array_equal(k, p3))
            n11 = sum(1 for k in
                [a[j:j+len(p4)] for j in range(len(a) - len(p4) + 1)]
                if np.array_equal(k, p4))
            n00_total = n00_total + n00
            n01_total = n01_total + n01
            n10_total = n10_total + n10
            n11_total = n11_total + n11

        #transition probability
        p01 = n01_total/float(n00_total+n01_total)  #P(W/D)  => P(D/D) = 1- P(W/D)
        p11 = n11_total/float(n10_total+n11_total)  #p(W/W) => P(D/W) = 1- P(W/W)
        p01_list.append(p01)
        p11_list.append(p11)

    # now fill up the dataframe row by row
    df.loc[0] = np.asarray(p01_list)
    df.loc[1] = np.asarray(p11_list)
    ##print(df.iloc[[0]])

    # print (time.clock() - start_time, "- after computing Markov chain params")

    #====================================================================
    #3) Compute alpha (shape) and beba (scale paramter) of Gamma distribution rainfall model for each month
    #====================================================================
    #===============Separate Dry and Wet days
    #m_doy1_list = [1,32,60,91,121,152,182,213,244,274,305,335]
    #m_doy2_list = [31,59,90,120,151,181,212,243,273,304,334,365]
    #numday_list = [31,28,31,30,31,30,31,31,30,31,30,31]
    rain= np.copy(rain_resampled)
    rain[rain_resampled < P_threshold] = np.nan

    alpha_list = []
    beta_list = []

    for i in range(len(m_doys_list)): # 12 months loop
        rain_month = rain[:,m_doys_list[i]-1:m_doye_list[i]]  #[0:31]
        ##numday_list = [31,28,31,30,31,30,31,31,30,31,30,31]
        rain_month1=rain_month.reshape(1,numday_list[i]*nrealiz)     #(1,31*37)
        rain_month1 = rain_month1[~np.isnan(rain_month1)]

        # fit the distributions, get the PDF distribution using the parameters
        #scipy.stats uses maximum likelihood estimation for fitting so we need to pass the raw data
        shape1, loc1, scale1 = gamma.fit(rain_month1)
    ##    x = np.arange(0,100,1)
    ##    g1 = gamma.pdf(x=x, a=shape1, loc=loc1, scale=scale1)

        alpha_list.append(shape1)
        beta_list.append(scale1)
    ##
    ##    print( '====Month: {}'.format(i+1))
    ##    print( 'shape (alpha): {}'.format(shape1))
    ##    print( 'scale (beta): {}'.format(scale1))

    # now fill up the dataframe row by row
    df.loc[2] = np.asarray(alpha_list)
    df.loc[3] = np.asarray(beta_list)
    # #test print for  the first row and all column
    # print( 'alpha: {}'.format(df.loc[2,:]))
    # print( 'beta): {}'.format(df.loc[3,:]))

    ### plot the distributions and fits.  to lazy to do iteration today
    ##plt.hist(rain_month1, bins=40, normed=True)
    ##plt.plot(x, g1, 'r-', linewidth=6, alpha=.6)
    ###ax.annotate(s='shape = %.3f\nloc = %.3f\nscale = %.3f' %(shape1, loc1, scale1), xy=(6,.2))
    ###ax.set_title('gamma fit')
    ##plt.title('MOBA-January (37 yrs of obs)')
    ##
    ##plt.show()
    ##for x in np.arange(0, numberOfRows):
    ##    #loc or iloc both work here since the index is natural numbers
    ##    df.loc[x] = [np.random.randint(-1,1) for n in range(3)]
    #prinkt (time.clock() - start_time, "- after computing alpha and beta of rainfall model")
    #====================================================================
    #3) Compute Fourier coefficients for Tmax, Tmin and SRad for wet and dry days
    #====================================================================
    #make an dataframe to save the estimated parameters for each month
    # +++rows for Tmax(dry),Tmax(Wet),Tmin(Dry),Tmin(Wet),Srad(dry), Srad(wet),
    # +++and cols for Ubar(mean), C(amplitude), T(position) for MEAN and  Ubaar(mean), C(amplitude), T(position) for STDEV
    df2 = pd.DataFrame(np.zeros((6, 6)))
    df2.columns = ['mean_M','amp_M', 'position_M','mean_STD','amp_STD', 'position_STD']
    df2.index =  ['Tmax_D','Tmax_W', 'Tmin_D','Tmin_W','Srad_D', 'Srad_W']
    #Separate Dry and Wet days
    Tmin_dry = np.copy(tmin_resampled)
    Tmin_wet = np.copy(tmin_resampled)
    Tmin_wet[rain_resampled < P_threshold] = np.nan
    Tmin_dry[rain_resampled >= P_threshold] = np.nan

    Tmax_dry = np.copy(tmax_resampled)
    Tmax_wet = np.copy(tmax_resampled)
    Tmax_wet[rain_resampled < P_threshold] = np.nan
    Tmax_dry[rain_resampled >= P_threshold] = np.nan

    Srad_dry = np.copy(srad_resampled)
    Srad_wet = np.copy(srad_resampled)
    Srad_wet[rain_resampled < P_threshold] = np.nan
    Srad_dry[rain_resampled >= P_threshold] = np.nan

    #compute mean and stdev of daily weather data
    Tmin_D_avg=np.nanmean(Tmin_dry, axis=0)
    Tmin_W_avg=np.nanmean(Tmin_wet, axis=0)
    Tmin_D_std=np.nanstd(Tmin_dry, axis=0)
    Tmin_W_std=np.nanstd(Tmin_wet, axis=0)

    Tmax_D_avg=np.nanmean(Tmax_dry, axis=0)
    Tmax_W_avg=np.nanmean(Tmax_wet, axis=0)
    Tmax_D_std=np.nanstd(Tmax_dry, axis=0)
    Tmax_W_std=np.nanstd(Tmax_wet, axis=0)

    Srad_D_avg=np.nanmean(Srad_dry, axis=0)
    Srad_W_avg=np.nanmean(Srad_wet, axis=0)
    Srad_D_std=np.nanstd(Srad_dry, axis=0)
    Srad_W_std=np.nanstd(Srad_wet, axis=0)

    #=========================
    #---Check if ther is 'nan' because curve_fit does not allow 'nan'
    ##for i in range(len(Tmax_W_avg)):
    ##    if i == 0 and np.isnan(Tmax_W_avg[i]):
    ##        Tmax_W_avg[i]=Tmax_W_avg[i+1]
    ##        Tmax_W_std[i]=Tmax_W_std[i+1]
    ##        Tmin_W_avg[i]=Tmin_W_avg[i+1]
    ##        Tmin_W_std[i]=Tmin_W_std[i+1]
    ##        Srad_W_avg[i]=Srad_W_avg[i+1]
    ##        Srad_W_std[i]=Srad_W_std[i+1]
    ##    elif i == len(Tmax_W_avg)-1 and np.isnan(Tmax_W_avg[i]):
    ##        Tmax_W_avg[i]=Tmax_W_avg[i-1]
    ##        Tmax_W_std[i]=Tmax_W_std[i-1]
    ##        Tmin_W_avg[i]=Tmin_W_avg[i-1]
    ##        Tmin_W_std[i]=Tmin_W_std[i-1]
    ##        Srad_W_avg[i]=Srad_W_avg[i-1]
    ##        Srad_W_std[i]=Srad_W_std[i-1]
    ##    elif i < len(Tmax_W_avg)-1 and i >0:
    ##        if np.isnan(Tmax_W_avg[i]):
    ##            Tmax_W_avg[i]=0.5*(Tmax_W_avg[i+1] + Tmax_W_avg[i-1])
    ##            Tmax_W_std[i]=0.5*(Tmax_W_std[i+1] + Tmax_W_std[i-1])
    ##            Tmin_W_avg[i]=0.5*(Tmin_W_avg[i+1] + Tmin_W_avg[i-1])
    ##            Tmin_W_std[i]=0.5*(Tmin_W_std[i+1] + Tmin_W_std[i-1])
    ##            Srad_W_avg[i]=0.5*(Srad_W_avg[i+1] + Srad_W_avg[i-1])
    ##            Srad_W_std[i]=0.5*(Srad_W_std[i+1] + Srad_W_std[i-1])
    ##
    #Tmax: curve-fitting for average
    xdata = np.asarray(range(1,366))
    ydata_W = Tmax_W_avg[~np.isnan(Tmax_W_avg)]  #Tmax_W_avg
    xdata_W = xdata[~np.isnan(Tmax_W_avg)]  #For Jan and Feb, there are too little wet days and thus ther are many nan values in Tmax_W_avg
    popt_W, pcov_W = curve_fit(func, xdata_W, ydata_W)

    ydata_D = Tmax_D_avg[~np.isnan(Tmax_D_avg)]  #Tmax_D_avg
    xdata_D= xdata[~np.isnan(Tmax_D_avg)]  #just in case....
    popt, pcov = curve_fit(func, xdata_D, ydata_D)

    # print('Four coeff: Tmax-mean(dry): Ubar ={0:5.3f}, C = {1:5.3f}, T = {2:5.3f}'.format(popt[0],popt[1],popt[2]))
    # print('Four coeff: Tmax-mean(wet): Ubar ={0:5.3f}, C = {1:5.3f}, T = {2:5.3f}'.format(popt_W[0],popt_W[1],popt_W[2]))

    #curve fitting for standard dev
    ydata_W = Tmax_W_std[~np.isnan(Tmax_W_std)]   #Tmax_W_std
    xdata_W = xdata[~np.isnan(Tmax_W_std)]  #For Jan and Feb, there are too little wet days and thus ther are many nan values
    popt_Ws, pcov_Ws = curve_fit(func, xdata_W, ydata_W)

    ydata_D = Tmax_D_std[~np.isnan(Tmax_D_std)]  #Tmax_D_std
    xdata_D= xdata[~np.isnan(Tmax_D_std)]  #just in case...
    popts, pcovs = curve_fit(func, xdata_D, ydata_D)
    # print('Four coeff: Tmax-std(dry): Ubar ={0:5.3f}, C = {1:5.3f}, T = {2:5.3f}'.format(popts[0],popts[1],popts[2]))
    # print('Four coeff: Tmax-std(wet): Ubar ={0:5.3f}, C = {1:5.3f}, T = {2:5.3f}'.format(popt_Ws[0],popt_Ws[1],popt_Ws[2]))

    #Tmin: curve-fitting for average
    ydata_W = Tmin_W_avg[~np.isnan(Tmin_W_avg)]  #Tmin_W_avg
    xdata_W = xdata[~np.isnan(Tmin_W_avg)]  #For Jan and Feb, there
    popt_W2, pcov_W2 = curve_fit(func, xdata_W, ydata_W)
    #return C*math.cos(0.0172*(x-T))+Ubar
    ydata_D = Tmin_D_avg[~np.isnan(Tmin_D_avg)]   #Tmin_D_avg
    xdata_D= xdata[~np.isnan(Tmin_D_avg)]  #just in case...
    popt2, pcov2 = curve_fit(func, xdata_D, ydata_D)
    # #print Fourier coefficients
    # print('Four coeff: Tmin-mean(dry): Ubar ={0:5.3f}, C = {1:5.3f}, T = {2:5.3f}'.format(popt2[0],popt2[1],popt2[2]))
    # print('Four coeff: Tmin-mean(wet): Ubar ={0:5.3f}, C = {1:5.3f}, T = {2:5.3f}'.format(popt_W2[0],popt_W2[1],popt_W2[2]))

    #curve fitting for standard dev
    ydata_W = Tmin_W_std[~np.isnan(Tmin_W_std)]   #Tmin_W_std
    xdata_W = xdata[~np.isnan(Tmin_W_std)]  #For Jan and Feb, there
    popt_W3, pcov_W3 = curve_fit(func, xdata_W, ydata_W)

    ydata_D = Tmin_D_std[~np.isnan(Tmin_D_std)]    #Tmin_D_std
    xdata_D= xdata[~np.isnan(Tmin_D_std)]  #just in case...
    popt3, pcov3 = curve_fit(func, xdata_D, ydata_D)
    # #print Fourier coefficients
    # print('Four coeff: Tmin-std(dry): Ubar ={0:5.3f}, C = {1:5.3f}, T = {2:5.3f}'.format(popt3[0],popt3[1],popt3[2]))
    # print('Four coeff: Tmin-std(wet): Ubar ={0:5.3f}, C = {1:5.3f}, T = {2:5.3f}'.format(popt_W3[0],popt_W3[1],popt_W3[2]))

    #Srad: curve-fitting for average
    ydata_W = Srad_W_avg[~np.isnan(Srad_W_avg)]   #Srad_W_avg
    xdata_W = xdata[~np.isnan(Srad_W_avg)]  #For Jan and Feb, there
    popt_W4, pcov_W4 = curve_fit(func, xdata_W, ydata_W)

    ydata_D = Srad_D_avg[~np.isnan(Srad_D_avg)]   #Srad_D_avg
    xdata_D= xdata[~np.isnan(Srad_D_avg)]  #just in case...
    popt4, pcov4 = curve_fit(func, xdata_D, ydata_D)
    # print('Four coeff: Srad-mean(dry): Ubar ={0:5.3f}, C = {1:5.3f}, T = {2:5.3f}'.format(popt4[0],popt4[1],popt4[2]))
    # print('Four coeff: Srad-mean(wet): Ubar ={0:5.3f}, C = {1:5.3f}, T = {2:5.3f}'.format(popt_W4[0],popt_W4[1],popt_W4[2]))

    #curve fitting for standard dev
    ydata_W =Srad_W_std[~np.isnan(Srad_W_std)]
    xdata_W = xdata[~np.isnan(Srad_W_std)]  #For Jan and Feb, there
    popt_W5, pcov_W5 = curve_fit(func, xdata_W, ydata_W)

    ydata_D = Srad_D_std[~np.isnan(Srad_D_std)]  #Srad_D_std
    xdata_D = xdata[~np.isnan(Srad_D_std)]  #For Jan and Feb, there
    popt5, pcov5 = curve_fit(func, xdata_D, ydata_D)
    # print('Four coeff: Srad-std(dry): Ubar ={0:5.3f}, C = {1:5.3f}, T = {2:5.3f}'.format(popt5[0],popt5[1],popt5[2]))
    # print('Four coeff: Srad-std(wet): Ubar ={0:5.3f}, C = {1:5.3f}, T = {2:5.3f}'.format(popt_W5[0],popt_W5[1],popt_W5[2]))

    #allocate estimated parameters to dataframe
    #Tmax - dry
    df2.mean_M.iloc[[0]] = popt[0]
    df2.amp_M.iloc[[0]] = popt[1]
    df2.position_M.iloc[[0]] = popt[2]
    df2.mean_STD.iloc[[0]] = popts[0]
    df2.amp_STD.iloc[[0]] = popts[1]
    df2.position_STD.iloc[[0]] = popts[2]
    #Tmax - wet
    df2.mean_M.iloc[[1]] = popt_W[0]
    df2.amp_M.iloc[[1]] = popt_W[1]
    df2.position_M.iloc[[1]] = popt_W[2]
    df2.mean_STD.iloc[[1]] = popt_Ws[0]
    df2.amp_STD.iloc[[1]] = popt_Ws[1]
    df2.position_STD.iloc[[1]] = popt_Ws[2]

    #Tmin - dry
    df2.mean_M.iloc[[2]] = popt2[0]
    df2.amp_M.iloc[[2]] = popt2[1]
    df2.position_M.iloc[[2]] = popt2[2]
    df2.mean_STD.iloc[[2]] = popt3[0]
    df2.amp_STD.iloc[[2]] = popt3[1]
    df2.position_STD.iloc[[2]] = popt3[2]
    #Tmin - wet
    df2.mean_M.iloc[[3]] = popt_W2[0]
    df2.amp_M.iloc[[3]] = popt_W2[1]
    df2.position_M.iloc[[3]] = popt_W2[2]
    df2.mean_STD.iloc[[3]] = popt_W3[0]
    df2.amp_STD.iloc[[3]] = popt_W3[1]
    df2.position_STD.iloc[[3]] = popt_W3[2]

    #SRad - dry
    df2.mean_M.iloc[[4]] = popt4[0]
    df2.amp_M.iloc[[4]] = popt4[1]
    df2.position_M.iloc[[4]] = popt4[2]
    df2.mean_STD.iloc[[4]] = popt5[0]
    df2.amp_STD.iloc[[4]] = popt5[1]
    df2.position_STD.iloc[[4]] = popt5[2]
    #SRad - wet
    df2.mean_M.iloc[[5]] = popt_W4[0]
    df2.amp_M.iloc[[5]] = popt_W4[1]
    df2.position_M.iloc[[5]] = popt_W4[2]
    df2.mean_STD.iloc[[5]] = popt_W5[0]
    df2.amp_STD.iloc[[5]] = popt_W5[1]
    df2.position_STD.iloc[[5]] = popt_W5[2]

    # print (time.clock() - start_time, "- after computing Fourier coeffients")
    #====================================================================
    #4)Compute auto/cross correlation between RESIDUALS of Tmin, Tmax and SRad => [A] and [B] matrix
    #====================================================================
    #Separate Dry and Wet days
    srad_dry = np.copy(srad_resampled)  #Srad_array => row = num years, col = 365 days
    srad_wet = np.copy(srad_resampled)
    srad_wet[rain_resampled < P_threshold] = np.nan
    srad_dry[rain_resampled >= P_threshold] = np.nan
    Tmax_dry = np.copy(tmax_resampled)
    Tmax_wet = np.copy(tmax_resampled)
    Tmax_wet[rain_resampled < P_threshold] = np.nan
    Tmax_dry[rain_resampled >= P_threshold] = np.nan
    Tmin_dry = np.copy(tmin_resampled)
    Tmin_wet = np.copy(tmin_resampled)
    Tmin_wet[rain_resampled < P_threshold] = np.nan
    Tmin_dry[rain_resampled >= P_threshold] = np.nan

    #4-1) compute long-term mean and stdev of daily weather data separately for dry and wet days
    #====================================================================
    srad_D_avg=np.nanmean(srad_dry, axis=0)
    srad_W_avg=np.nanmean(srad_wet, axis=0)
    srad_D_std=np.nanstd(srad_dry, axis=0)
    srad_W_std=np.nanstd(srad_wet, axis=0)
    Tmax_D_avg=np.nanmean(Tmax_dry, axis=0)
    Tmax_W_avg=np.nanmean(Tmax_wet, axis=0)
    Tmax_D_std=np.nanstd(Tmax_dry, axis=0)
    Tmax_W_std=np.nanstd(Tmax_wet, axis=0)
    Tmin_D_avg=np.nanmean(Tmin_dry, axis=0)
    Tmin_W_avg=np.nanmean(Tmin_wet, axis=0)
    Tmin_D_std=np.nanstd(Tmin_dry, axis=0)
    Tmin_W_std=np.nanstd(Tmin_wet, axis=0)

    #4-2) compute daily residuals separately for dry and wet days
    #====================================================================
    temp = np.subtract(srad_resampled,srad_D_avg) #dry days
    srad_dry_res = np.divide(temp,srad_D_std)
    srad_dry_res[rain_resampled >= P_threshold] = np.nan
    temp = np.subtract(srad_resampled,srad_W_avg)#wet days
    srad_wet_res = np.divide(temp,srad_W_std)
    srad_wet_res[rain_resampled < P_threshold] = np.nan
    #combine two residual matrix from dry and wet
    srad_res = np.nan_to_num(srad_dry_res) + np.nan_to_num(srad_wet_res)
    #----Tmax
    temp = np.subtract(tmax_resampled,Tmax_D_avg) #dry days
    Tmax_dry_res = np.divide(temp,Tmax_D_std)
    Tmax_dry_res[rain_resampled >= P_threshold] = np.nan
    temp = np.subtract(tmax_resampled,Tmax_W_avg)#wet days
    Tmax_wet_res = np.divide(temp,Tmax_W_std)
    Tmax_wet_res[rain_resampled < P_threshold] = np.nan
    #combine two residual matrix from dry and wet
    Tmax_res = np.nan_to_num(Tmax_dry_res) + np.nan_to_num(Tmax_wet_res)
    #----tmin
    temp = np.subtract(tmin_resampled,Tmin_D_avg) #dry days
    Tmin_dry_res = np.divide(temp,Tmin_D_std)
    Tmin_dry_res[rain_resampled >= P_threshold] = np.nan
    temp = np.subtract(tmin_resampled,Tmin_W_avg)#wet days
    Tmin_wet_res = np.divide(temp,Tmin_W_std)
    Tmin_wet_res[rain_resampled < P_threshold] = np.nan
    #combine two residual matrix from dry and wet
    Tmin_res = np.nan_to_num(Tmin_dry_res) + np.nan_to_num(Tmin_wet_res)

    #4-3) Compute auto- and serial-correlations between variables
    #====================================================================
    #4-3-1) lag-0 cross-correlation
    #temp = np.concatenate((Tmax_res_1D, Tmin_res_1D), axis=0)
    #M_matrix = np.concatenate((temp, srad_res_1D), axis=0)
    Tmax_res_1D = Tmax_res.reshape(nrealiz*365)
    Tmin_res_1D = Tmin_res.reshape(nrealiz*365)
    srad_res_1D = srad_res.reshape(nrealiz*365)
    temp = np.vstack((Tmax_res_1D, Tmin_res_1D))
    M0_matrix = np.vstack((temp, srad_res_1D))
    #--**-- lag 0 cross-correation between variables
    M0 = np.corrcoef(M0_matrix) #Each row of x represents a variable, and each column a single observation of all those variables.

    #4-3-2) lag-1 serial correlation for variable j
    M1_matrix = np.empty((3,3))*np.nan #initialize
    temp = np.vstack((Tmax_res_1D[:-1], Tmax_res_1D[1:]))
    M1_matrix[0][0] = np.corrcoef(temp)[0][1]    #lag 1 day auto-correlation
    temp = np.vstack((Tmin_res_1D[:-1], Tmin_res_1D[1:]))
    M1_matrix[1][1] = np.corrcoef(temp)[0][1]  #lag 1 day auto-correlation
    temp = np.vstack((srad_res_1D[:-1], srad_res_1D[1:]))
    M1_matrix[2][2] = np.corrcoef(temp)[0][1]  #lag 1 day auto-correlation

    #4-3-3) lag-1 cross correlation for variable j
    # rho(j,k) is the cross correlation between variable j and k with variable k lagged 1 day with respect to variable j
    temp = np.vstack((Tmax_res_1D[1:], Tmin_res_1D[:-1]))
    M1_matrix[0][1] = np.corrcoef(temp)[0][1]
    temp = np.vstack((Tmax_res_1D[:-1], srad_res_1D[1:]))
    M1_matrix[0][2] = np.corrcoef(temp)[0][1]

    temp = np.vstack((Tmin_res_1D[1:], Tmax_res_1D[:-1]))
    M1_matrix[1][0] = np.corrcoef(temp)[0][1]
    temp = np.vstack((Tmin_res_1D[:-1], srad_res_1D[1:]))
    M1_matrix[1][2] = np.corrcoef(temp)[0][1]

    temp = np.vstack((srad_res_1D[1:], Tmax_res_1D[:-1]))
    M1_matrix[2][0] = np.corrcoef(temp)[0][1]
    temp = np.vstack((srad_res_1D[:-1], Tmin_res_1D[1:]))
    M1_matrix[2][1] = np.corrcoef(temp)[0][1]

    #4-4) Compute [A] and [B]  => see equation (4) and (5) of Richardson et al. 1984
    #====================================================================
    invM0=inv(M0)
    A=np.matmul(M1_matrix, invM0)

    temp = np.matmul(A, np.transpose(M1_matrix))
    BBT = M0-temp  #B*B.T
    # print( 'B*B_T: {}'.format(BBT))
    #test Cholesky decomposition
    L = np.linalg.cholesky(BBT)  #=> [B] matrix
    #print np.dot(L, L.T)  ## verify that L * L.T = A
    # print( 'L*L_T{}'.format(np.dot(L, L.T)))

    # print (time.clock() - start_time, "- after computing [A] [b] matrix")
    #====================================================================
    #====================================================================
    #====================================================================
    #====================================================================
    #write output dataframes and matrics to text file
    #====================================================================
    #====================================================================
    #fname = 'C:\\Users\\Eunjin\\IRI\\PH_FAO\\WGEN_PH_36\\WGEN_input_param_month_SANJ.txt'
   # fname = wdir + 'WGEN_input_param_month_' + station_name+ '.txt'
    fname = wdir.replace("/","\\") + '\\WGEN_input_param_month_' + station_name+ '.txt'
    f = open(fname,"w") #opens file
    f.write("* INPUT # 01 - STATION NAME *" + "\n")
    f.write(station_name + "\n")  #********
    f.write("* INPUT # 02 - target year, NUMBER OF YEARS/samples, GENERATION CODES, AND LATITUDE, temp corr factor, rain corr factor" + "\n")
  #  target_year=2018
    NYRS = 10  #YEARS OF DATA TO BE GENERATED *
    KGEN = 1 #GENERATION OPTION CODE * IF KGEN = 1,RAIN, MAX TEMP, MIN TEMP, AND SOLAR RADIATION WILL BE GENERATED *
  #  ALAT = 12.35  #STATION LATITUDE IN DEGREES *
    KTCF = 2 #TEMP. CORRECTION FACTOR OPTION CODE => if 2,  GENERATED MAX TEMP AND MIN TEMP WILL BE CORRECTED BASED ON OBSERVED MEAN MONTHLY MAX AND MIN TEMP *
    KRCF = 1 #RAIN CORRECTION FACTOR OPTION CODE => IF KRCF = 1 GENERATED RAIN WILL BE CORRECTED BASED ON OBSERVED MEAN MONTHLY RAIN *
    s1 = "%4d" % target_year   #********   100 FORMAT(I4, 2I5,F7.2,2I5)
    s2 = "%5d" % NYRS
    s3 = "%5d" % KGEN
    s4 = "%7.2f" % ALAT
    s5 = "%5d" % KTCF
    s6 = "%5d" % KRCF
    f.write(s1+s2+s3+s4+s5+s6 +"\n")
    f.write("* INPUT # 03 - PROBABILITY OF WET GIVEN WET (monthly values) *" + "\n")
    temp = df.values[0] #convert dataframe to array
    for i in range(temp.shape[0]):
        s_temp = "%7.3f" % df.values[1][i]  #df.loc[1] = np.asarray(p11_list)
        f.write(s_temp)
    f.write("\n"+"* INPUT # 04 - PROBABILITY OF WET GIVEN DRY (monthly values)"+ "\n")
    for i in range(temp.shape[0]):
        s_temp = "%7.3f" % df.values[0][i]  ###df.loc[0] = np.asarray(p01_list)
        f.write(s_temp)
    f.write("\n"+"* INPUT # 05 - GAMMA DISTRIBUTION SHAPE PARAMETER - alpha (monthly values)"+ "\n")
    for i in range(temp.shape[0]):
        s_temp = "%7.3f" % df.values[2][i]  #df.loc[2] = np.asarray(alpha_list)
        f.write(s_temp)
    f.write("\n"+"* INPUT # 06 - GAMMA DISTRIBUTION SCALE PARAMETER - beta (monthly values)"+ "\n")
    for i in range(temp.shape[0]):
        s_temp = "%7.3f" % df.values[3][i]  #df.loc[3] = np.asarray(beta_list)
        f.write(s_temp)


    f.write("\n"+"* INPUT # 07 - FOURIER COEFFICIENTS OF MAX TEMP ON 'DRY' DAYS (MEAN & STDEV)*"+ "\n")
    temp = df2.values[0] #convert dataframe to array
    for i in range(temp.shape[0]):
        s_temp = "%8.3f" % df2.values[0][i]  #df.loc[1] = np.asarray(p11_list)
        f.write(s_temp)
    f.write("\n"+"* INPUT # 08 - FOURIER COEFFICIENTS OF MAX TEMP ON 'WET' DAYS (MEAN & STDEV)*"+ "\n")
    for i in range(temp.shape[0]):
        s_temp = "%8.3f" % df2.values[1][i]  #df.loc[1] = np.asarray(p11_list)
        f.write(s_temp)
    f.write("\n"+"* INPUT # 09 - FOURIER COEFFICIENTS OF MIN TEM ON 'DRY' DAYS (MEAN & STDEV)*"+ "\n")
    for i in range(temp.shape[0]):
        s_temp = "%8.3f" % df2.values[2][i]  #df.loc[1] = np.asarray(p11_list)
        f.write(s_temp)
    f.write("\n"+"* INPUT # 10 - FOURIER COEFFICIENTS OF MIN TEM ON 'WET' DAYS (MEAN & STDEV)*"+ "\n")
    for i in range(temp.shape[0]):
        s_temp = "%8.3f" % df2.values[3][i]  #df.loc[1] = np.asarray(p11_list)
        f.write(s_temp)
    f.write("\n"+"* INPUT # 11 - FC OF SRAD ON 'DRY' DAYS (MEAN & STDEV): Rainy season *"+ "\n")
    for i in range(temp.shape[0]):
        s_temp = "%8.3f" % df2.values[4][i]  #df.loc[1] = np.asarray(p11_list)
        f.write(s_temp)
    f.write("\n"+"* INPUT # 12 - FC OF SRAD ON 'WET' DAYS (MEAN & STDEV): Rainy season *"+ "\n")
    for i in range(temp.shape[0]):
        s_temp = "%8.3f" % df2.values[5][i]  #df.loc[1] = np.asarray(p11_list)
        f.write(s_temp)

    #4-5) Write the elements of [A] and [B] into a text files which is an input of WGEN.
    #====================================================================
    f.write("\n"+"  ** [A][B] matrix for trivariate first-order autoregressive model  **" + "\n")
    s1="%7.3f" % M0[0][0]
    s2= "%7.3f" % M0[1][0]
    s3="%7.3f" % M0[2][0]
    s4= "%7.3f" % M0[0][1]
    s5="%7.3f" % M0[1][1]
    s6= "%7.3f" % M0[2][1]
    s7="%7.3f" % M0[0][2]
    s8= "%7.3f" % M0[1][2]
    s9= "%7.3f" % M0[2][2]
    f.write("M0: " +s1+s2+s3+s4+s5+s6+s7+s8+s9 +"\n")
    s1="%7.3f" % M1_matrix[0][0]
    s2= "%7.3f" % M1_matrix[1][0]
    s3="%7.3f" % M1_matrix[2][0]
    s4= "%7.3f" % M1_matrix[0][1]
    s5="%7.3f" % M1_matrix[1][1]
    s6= "%7.3f" % M1_matrix[2][1]
    s7="%7.3f" % M1_matrix[0][2]
    s8= "%7.3f" % M1_matrix[1][2]
    s9= "%7.3f" % M1_matrix[2][2]
    f.write("M1: " +s1+s2+s3+s4+s5+s6+s7+s8+s9 +"\n")
    s1="%7.3f" % A[0][0]
    s2= "%7.3f" % A[1][0]
    s3="%7.3f" % A[2][0]
    s4= "%7.3f" % A[0][1]
    s5="%7.3f" % A[1][1]
    s6= "%7.3f" % A[2][1]
    s7="%7.3f" % A[0][2]
    s8= "%7.3f" % A[1][2]
    s9= "%7.3f" % A[2][2]
    f.write("A: " +"\n")
    f.write(s1+s2+s3+s4+s5+s6+s7+s8+s9 +"\n")
    s1="%7.3f" % L[0][0]
    s2= "%7.3f" % L[1][0]
    s3="%7.3f" % L[2][0]
    s4= "%7.3f" % L[0][1]
    s5="%7.3f" % L[1][1]
    s6= "%7.3f" % L[2][1]
    s7="%7.3f" % L[0][2]
    s8= "%7.3f" % L[1][2]
    s9= "%7.3f" % L[2][2]
    f.write("B: " +"\n")
    f.write(s1+s2+s3+s4+s5+s6+s7+s8+s9 +"\n")
    ###f.close()
    ##f.write("\n")
    ##f.write("\n")
    ##f.write("  ** MONTHLY STATISTICS - both WET and DRY **" + "\n")
    ##f.write("    ========MEAN==============   ==========STDEV===========" + "\n")
    ##f.write("MTH TMAX    TMIN   SRAD   RAIN   TMAX    TMIN   SRAD   RAIN" + "\n")

    #====================================================================
    #Call function "Estimate_Monthly_target"
    # to compute monthly target rainfall, tmin, tmax and srad from 30 yrs of observations
    # by taking 5% - 95% of CDF curve
    # This step was for fixing the low-frequncy issue of the original WGEN ouput
    # The original WGEN output cannot reproduce CDF curves of monthly Tmin/tmax/srad of the 30yrs of observations
    #====================================================================
    print( '== WGEN_PAR => extract monthly target values')
    #1) compute monthly values from the resampled weather realizations
    rain_resampled_m = np.empty((nrealiz,12,))*np.NAN  #nyears *  365 days
    tmax_resampled_m = np.empty((nrealiz,12,))*np.NAN
    tmin_resampled_m = np.empty((nrealiz,12,))*np.NAN
    srad_resampled_m = np.empty((nrealiz,12,))*np.NAN
    n_rep = 10  #5% to 95% percentile => Hard coded
    target_rain = np.empty((n_rep,12,))* np.nan #initialize
    target_rain0 = np.empty((n_rep,12,))* np.nan # from the theoretical SCF curve
    target_tmax = np.empty((n_rep,12,))* np.nan 
    target_tmin = np.empty((n_rep,12,))* np.nan 
    target_srad = np.empty((n_rep,12,))* np.nan 

    target_rain_shuffl = np.empty((n_rep,12,))* np.nan #initialize
    target_rain_shuffl0 = np.empty((n_rep,12,))* np.nan # from the theoretical SCF curve
    target_tmax_shuffl = np.empty((n_rep,12,))* np.nan 
    target_tmin_shuffl = np.empty((n_rep,12,))* np.nan 
    target_srad_shuffl = np.empty((n_rep,12,))* np.nan 

    #NOTE (EJ 5/24/2019): take target 10 values of rainfall from theoretical curve rather than 500 resampled weather
    for i in range(0,12):  #12 months
        rain_temp = rain_WTD_m[:, i]
        sorted_rain0 = np.sort(rain_temp) #, axis=0)

        BN = df_scf.iloc[0][i+1]  #e.g., 36%
        NN = df_scf.iloc[1][i+1]   #e.g., 33%
        AN = df_scf.iloc[2][i+1]     #e.g., 31%
        fx_theo = np.empty(nyears)*np.nan #initialize
        Fx_theo = np.empty(nyears)*np.nan  #initialize
        #calculate number of years for each 1/3 section (i.e., 33-34-33 for 30 yrs of obs)
        num_13 = math.floor(nyears/3) #This method returns largest integer not greater than x.
        num_1 = float(num_13)
        num_2 = float(num_13)
        num_3 = float(num_13)
        if nyears % 3 == 1:
            num_2+1.0
        elif nyears % 3 == 2:
            num_1+1.0
            num_3+1.0

        for ii in range(0,nyears):
            #compute PDF
            if ii < num_1:
                fx_theo[ii] = (BN/100)/num_1
            elif ii >= num_1 and ii < (nyears - num_3):
                fx_theo[ii] = (NN/100)/num_2
            else:
                fx_theo[ii] = (AN/100)/num_3
            
        Fx_theo = np.cumsum(fx_theo)  #compute CDF
        #======find target rainfall from the theoretical curve
        target_percentile = np.arange(0.05,1.0,0.1) #array([ 0.05,  0.15,  0.25,  0.35,  0.45,  0.55,  0.65,  0.75,  0.85,  0.95])
        if Fx_theo[0] > target_percentile[0]: #if the first CDF is greater than the minimum target percentile (5%), then replace the minimum value
            target_rain0[:,i]=np.interp(target_percentile,Fx_theo,sorted_rain0,left=sorted_rain0[0])  #30 yrs of climatology to theoretical CDF SCF curve
        else:
            target_rain0[:,i]=np.interp(target_percentile,Fx_theo,sorted_rain0,left=0.0)  #30 yrs of climatology to theoretical CDF SCF curve

    #NOTE (EJ 5/24/2019): take target 10 values of temperature/srad from 500 resampled weather data 
    #                     because there is no reliable temperature forecast out there 
    #                     but for comparison, rainfall target values are also computed from the resampled data
    #for i in range(0,12):  #12 months
        rain_resampled_m [:, i] = np.sum(rain_resampled[:, m_doys_list[i]-1:m_doye_list[i]], axis = 1)
        tmax_resampled_m [:, i] = np.mean(tmax_resampled[:, m_doys_list[i]-1:m_doye_list[i]], axis = 1)
        tmin_resampled_m [:, i] = np.mean(tmin_resampled[:, m_doys_list[i]-1:m_doye_list[i]], axis = 1)
        srad_resampled_m [:, i] = np.mean(srad_resampled[:, m_doys_list[i]-1:m_doye_list[i]], axis = 1)
        
        #Rainfall - sort to make CDF curve
        sorted_rain = np.sort(rain_resampled_m [:, i])
        temp = np.zeros(len(sorted_rain))+1
        pdf = np.divide(temp,len(sorted_rain))  #1/30years 
        cdf = np.cumsum(pdf)  #compute CDF
        target_percentile = np.arange(0.05,1.0,0.1) #array([ 0.05,  0.15,  0.25,  0.35,  0.45,  0.55,  0.65,  0.75,  0.85,  0.95])
        #extract 10 target rainfall values from the CDF using linear interpolation 
        target_rain[:,i] = np.interp(target_percentile,cdf,sorted_rain,left=0.0) 

        #Tmax - sort to make CDF curve
        sorted_tmax = np.sort(tmax_resampled_m [:, i])
        #extract 10 target rainfall values from the CDF using linear interpolation 
        target_tmax[:,i] = np.interp(target_percentile,cdf,sorted_tmax,left=0.0) 

        #Tmin - sort to make CDF curve
        sorted_tmin = np.sort(tmin_resampled_m [:, i])
        #extract 10 target rainfall values from the CDF using linear interpolation 
        target_tmin[:,i] = np.interp(target_percentile,cdf,sorted_tmin,left=0.0) 

        #Srad - sort to make CDF curve
        sorted_srad = np.sort(srad_resampled_m [:, i])
        #extract 10 target rainfall values from the CDF using linear interpolation 
        target_srad[:,i] = np.interp(target_percentile,cdf,sorted_srad,left=0.0) 
        
        #=====shuffle 10 representative target values for each month
        arr = np.arange(n_rep)   #[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        np.random.shuffle(arr)
        for j in range(n_rep):
            target_rain_shuffl0[j,i] = target_rain0[arr[j],i] # from the theoretical SCF curve
        np.random.shuffle(arr)
        for j in range(n_rep):
            target_rain_shuffl[j,i] = target_rain[arr[j],i]
        np.random.shuffle(arr)
        for j in range(n_rep):
            target_tmax_shuffl[j,i] = target_tmax[arr[j],i]
        np.random.shuffle(arr)
        for j in range(n_rep):
            target_tmin_shuffl[j,i] = target_tmin[arr[j],i]
        np.random.shuffle(arr)
        for j in range(n_rep):
            target_srad_shuffl[j,i] = target_srad[arr[j],i]
            
        #replace 0 or <2mm monthly rainfall with minimum 2mm
        target_rain_shuffl[target_rain_shuffl < 2] = 2.0
        target_rain_shuffl0[target_rain_shuffl0 < 2] = 2.0

        # fig = plt.figure()
        # fig.suptitle('Selecting monthly target Rainfall', fontsize=14, fontweight='bold')
        # ax = fig.add_subplot(111)
        # ax.set_xlabel('Monthly Rainfall[mm/month]',fontsize=14)
        # ax.set_ylabel('CDF',fontsize=14)
        # ax.plot(sorted_rain,cdf, '--*',label='resampled')
        # ax.plot(sorted_rain0,Fx_theo, '--x',label='theoretical')
        # ax.plot(target_rain[:,i],target_percentile, 'o',label='target-res')
        # ax.plot(target_rain0[:,i],target_percentile, 'o',label='target-theo')
        # legend = ax.legend(loc='lower right', shadow=True, fontsize='large')
        # fig2 = plt.figure()
        # fig2.suptitle('Selecting monthly target Tmin', fontsize=14, fontweight='bold')
        # ax2 = fig2.add_subplot(111)
        # ax2.set_xlabel('Monthly avg Tmin',fontsize=14)
        # ax2.set_ylabel('CDF',fontsize=14)
        # ax2.plot(sorted_tmin,cdf, '-*',label='resampled')
        # ax2.plot(target_tmin[:,i],target_percentile, 'o',label='target')
        # legend = ax2.legend(loc='lower right', shadow=True, fontsize='large')    
        # fig3 = plt.figure()
        # fig3.suptitle('Selecting monthly target Tmax', fontsize=14, fontweight='bold')
        # ax3 = fig3.add_subplot(111)
        # ax3.set_xlabel('Monthly avg Tmax',fontsize=14)
        # ax3.set_ylabel('CDF',fontsize=14)
        # ax3.plot(sorted_tmax,cdf, '-*',label='resampled')
        # ax3.plot(target_tmax[:,i],target_percentile, 'o',label='target')
        # legend = ax3.legend(loc='lower right', shadow=True, fontsize='large')  
        # fig4 = plt.figure()
        # fig4.suptitle('Selecting monthly target Tmax', fontsize=14, fontweight='bold')
        # ax4 = fig4.add_subplot(111)
        # ax4.set_xlabel('Monthly avg SRad',fontsize=14)
        # ax4.set_ylabel('CDF',fontsize=14)
        # ax4.plot(sorted_srad,cdf, '-*',label='resampled')
        # ax4.plot(target_srad[:,i],target_percentile, 'o',label='target')
        # legend = ax4.legend(loc='lower right', shadow=True, fontsize='large')            
        # plt.show()

    # EJ(5/24/2019): Remove calling the old function, Estimate_Monthly_target 
    # n_rep = 10
    # target_rain_shuffl, target_tmax_shuffl, target_tmin_shuffl, target_srad_shuffl = Estimate_Monthly_target (WTD_df,n_rep)
    #write output dataframes and matrics to text file
    f.write("* Monthly target Tmax to correct WGEN output *" + "\n")
    f.write("   Jan     Feb    Mar    Apr    May    Jun    Jul    Aug    Sep    Oct    Nov    Dec" + "\n")
    for k in range(n_rep):
        for i in range(12):
            f.write("%7.2f" % target_tmax_shuffl[k][i])
        f.write("\n")


    f.write("* Monthly target Tmin to correct WGEN output *" + "\n")
    f.write("   Jan     Feb    Mar    Apr    May    Jun    Jul    Aug    Sep    Oct    Nov    Dec" + "\n")
    for k in range(n_rep):
        for i in range(12):
            f.write("%7.2f" % target_tmin_shuffl[k][i])
        f.write("\n")


    f.write("* Monthly target Srad to correct WGEN output *" + "\n")
    f.write("   Jan     Feb    Mar    Apr    May    Jun    Jul    Aug    Sep    Oct    Nov    Dec" + "\n")
    for k in range(n_rep):
        for i in range(12):
            f.write("%7.2f" % target_srad_shuffl[k][i])
        f.write("\n")

    f.write("* Monthly target rainfall amount to correct WGEN output *" + "\n")
    f.write("   Jan     Feb    Mar    Apr    May    Jun    Jul    Aug    Sep    Oct    Nov    Dec" + "\n")
    for k in range(n_rep):
        for i in range(12):
            #f.write("%7.0f" % target_rain_shuffl[k][i])
            f.write("%7.0f" % target_rain_shuffl0[k][i])
        f.write("\n")
    f.close()

    # print (time.clock() - start_time, "sec - after all computation & writing output")
    #====================================================================
    #====================================================================
    #====================================================================
    #====================================================================

    return rain_WTD_m , tmin_WTD_m, tmax_WTD_m, srad_WTD_m, tmax_resampled_m, tmin_resampled_m, srad_resampled_m     


