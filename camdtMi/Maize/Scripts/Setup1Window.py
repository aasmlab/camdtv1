from __init__ import *

class Setup1Window( BaseWidget):

    """ This class manages setup1 inputs and is used in the scenario script"""

    def __init__(self):
        #Setup1.__init__(self)
        #BaseWidget.__init__(self,'DSSAT Setup1')
        super(Setup1Window,self).__init__('DSSAT Setup1')
        self.parent = None

        # self._weatherStationValueList = [('APAR8830(Aparri,R-II)', 'APAR8830'), ('MALA8830(Malaybalay, R-X)', 'MALA8830'),
        #                                 ('PALA8830(Puerto Princesa, R-IV B)', 'PALA8830'), ('SANJ8830(San Jose, R-IV B)', 'SANJ8830'),
        #                                 ('TUGE8830(Tuguegarao,R-II)', 'TUGE8830'), ("CLAR9721(Clark Intl,R-III)", 'CLAR9721')
        #                                 ]

        self._weatherStationValueList = [   # 1988 - 2017
                                        
                                        ('Allegan', 'ALLE1701'), ('Branch', 'BRAN1701'),
                                        ('Cass', 'CASS1701'), ('Huron', 'HURO1701'),
                                        ('Ingham', 'INGH1701'), ("Montcalm", 'MONT1701'),
                                        ('Saginaw', 'SAGI1701'), ("Washtenaw", 'WASH1701'),
                                        # ('MALL1701', 'MALL1701'), ('MALL1801', 'MALL1801'),
                                        #     # 2017 & 2018
                                        # ('MASO1701', 'MASO1701'), 
                                        # ('MBRA1701', 'MBRA1701'),('MBRA1801', 'MBRA1801'), 
                                        # ('MCAS1701', 'MCAS1701'),('MCAS1801', 'MCAS1801'),
                                        # ("MHUR1701", 'MHUR1701'), ('MHUR1801', 'MHUR1801'), 
                                        # ("MING1701", 'MING1701'), ("MING1801", 'MING1801'),
                                        # ('MMAS1701', 'MMAS1701'), ('MMAS1801', 'MMAS1801'),
                                        # ('MMON1701', 'MMON1701'),('MMON1801', 'MMON1801'), 
                                        # ('MSAG1701', 'MSAG1701'),('MSAG1801', 'MSAG1801'),
                                        # ("MWAS1701", 'MWAS1701'), ('MWAS1801', 'MWAS1801'),
                                        ]

        self._cultivarTypeList = [('PH0001 NK8840', 'PH0001 NK8840'), ('PH0002 Dekalb9132', 'PH0002 Dekalb9132'),
                                        ('PH0003 DK6919s', 'PH0003 DK6919s'), ('PH0004 P30T80', 'PH0004 P30T80'),
                                        ('PH0005 IPB Var13', 'PH0005 IPB Var13')
                                        ]

        # self._soilTypeList = [('PH00000001(SICL)-QUINGUA DEEP', 'PH00000001'), ('PHS0000001(SICL)-QUINGUA  SHALLOW)', 'PHS0000001'),
        #                         ('PH00000002(C)-CANDABA DEEP', 'PH00000002'), ('PHS0000002(C)-CANDABA SHALLOW', 'PHS0000002'),
        #                         ('PH00000003(SICL)-MAAPAG  DEEP', 'PH00000003'), ('PHS0000003(SICL)-MAAPAG SHALLOW', 'PHS0000003'), 
        #                         ('PH00000004(C)-BABUYAN DEEP', 'PH00000004'), ('PHS0000004(C)-BABUYAN SHALLOW', 'PHS0000004'), 
        #                         ('PH00000005(C)-TORAN  DEEP', 'PH00000005'), ('PHS0000005(C)-TORAN SHALLOW', 'PHS0000005'), 
        #                         ('PH00000006(SICL)-SAN MANUEL  DEEP', 'PH00000006'), ('PHS0000006(SICL)-SAN MANUEL SHALLOW', 'PHS0000006'), 
        #                         ('PH00000008(C)-FARAON DEEP', 'PH00000008'), ('PH00000009(C)-LA CASTELLANA DEEP', 'PHS0000009'), 
        #                          ('PHS0000009(C)-LA CASTELLANA  SHALLOW', 'PHS0000009'),
        #                         ('PH00000007(SICL)-SAN MANUEL  DEEP', 'PH00000007'), ('PHS0000007(SICL)-SAN MANUEL SHALLOW', 'PHS0000007')
        #                      ]

        self._cultivarTypeList = [('PC0001', 'PC0001')
                                 ]

        self._soilTypeList = [('Kalamazoo Loam', 'ALLE190001 '), ('Montcalm Sandy Loam)', 'IBPT910006')
                             ]
        

        self._h20List = [('0.3(30% of AWC)', '0.3'), ('0.5(50% of AWC)', '0.5'),
                        ('0.7(70% of AWC)', '0.7'), ('1.0(100% of AWC)', '1') ]

        self._no3List = [('High(63 N kg/ha)', 'H'), ('Low(25 N kg/ha)', 'L')]

        # Declaration of our variables
        self._weatherStationValue = ""
        self._startYearValue, self._endYearValue = 0, 0
        self._startPlantingValue, self._endPlantingValue = 0, 0
        self._cultivarTypeValue = ''
        self._pltDensityValue = 0
        self._soilTypeValue = ''
        self._h20Value = 0
        self._no3Value = ''
        self._year1stPlt = 0
        self._nnList1 = [] # NN values fo year 1
        self._nnList2 = [] # NN values fo year 2
        self._nnListF = 24*[0]  # initialize the nn values to 0.  NN values that will be written in the csv
        self._bnListF = 24*[33]
        self._anListF = 24*[33]

        self._btnSCF1 = ControlButton('Reset SCF')
        self._btnSCF2 = ControlButton('Reset SCF')
        self._btnSCF1.value = self.__btnSCF1Action
        self._btnSCF2.value = self.__btnSCF2Action

        self._btnBN1  = ControlButton('Reset BN')
        self._btnBN2  = ControlButton('Reset BN')
        self._btnBN1.value = self.__btnBN1Action
        self._btnBN2.value = self.__btnBN2Action

        self._btnAN1  = ControlButton('Reset AN')
        self._btnAN2  = ControlButton('Reset AN')
        self._btnAN1.value = self.__btnAN1Action
        self._btnAN2.value = self.__btnAN2Action

        self._btnPlt1 = ControlButton('Reset Planting')
        self._btnPlt2 = ControlButton('Reset Planting')
        self._btnPlt1.value = self.__btnPlt1Action
        self._btnPlt2.value = self.__btnPlt2Action

        self._btnCmpNN = ControlButton('Compute NN')
        self._btnCmpNN.value = self.__btnCmpNNAction

        self._label1     = ControlLabel('\tWeather Station')    # Entry 1
        self._pltDate    = ControlText("Year of the first planting date:", default='2017')     # Entry 2 1970 <= x <= 2050
        self._yearL1     = ControlCheckBoxList("SCF")   # Entry 3
        self._yearL1.value = [('J1', False), ('F1', False),  ('M1', False),  ('A1', False), ('M1', False), \
             ('J1', False), ('J1', False), ('A1', False), ('S1', False), ('O1', False), ('N1', False), ('D1', False) ]
        
        self._yearL2     = ControlCheckBoxList("Planting")   # Entry 4
        self._yearL2.value = [('J1', False), ('F1', False),  ('M1', False),  ('A1', False), ('M1', False), \
             ('J1', False), ('J1', False), ('A1', False), ('S1', False), ('O1', False), ('N1', False), ('D1', False) ]

        self._yearL3     = ControlCheckBoxList("SCF")   # Entry 5
        self._yearL3.value = [('J2', False), ('F2', False),  ('M2', False),  ('A2', False), ('M2', False), \
             ('J2', False), ('J2', False), ('A2', False), ('S2', False), ('O2', False), ('N2', False), ('D2', False) ]
        
        self._yearL4     = ControlCheckBoxList("Planting")   # Entry 6
        self._yearL4.value = [('J2', False), ('F2', False),  ('M2', False),  ('A2', False), ('M2', False), \
             ('J2', False), ('J2', False), ('A2', False), ('S2', False), ('O2', False), ('N2', False), ('D2', False) ]
        
        
        self._year1   = ControlList("YEAR 1") # Entry 4 & 5 (BN & AN)
        #self._year1.height = 5000
        self._year1.horizontal_headers   = ['Month', "BN", "AN", "NN"] 
        self._year1.value   = self.iniValuesN()
        self._year1Backup = ControlList("YEAR 1 Backup") 

        self._year2   = ControlList("YEAR 2") # Entry 4 & 5 (BN & AN)
        self._year2.horizontal_headers   = ['Month', "BN", "AN", "NN"]
        self._year2.value   = self.iniValuesN()
        self._year2Backup = ControlList("YEAR 2 Backup")

        self._nnBackup1 = ControlList("NN 1")
        self._nnBackup2 = ControlList("NN 2")


        

        #Definition of the forms fields
        
        self._label2     = ControlLabel('\tSimulation Period')
        self._label3     = ControlLabel('\tPlanting Window (to test)')
        self._label4     = ControlLabel('\tPlanting details (Maize)')
        self._label5     = ControlLabel('\tSoil Condition')
        
        # Control Combos
        self._controlWaStat         = ControlCombo('Weather Station')
        self._controlpltDetails     = ControlCombo('Cultivar type')
        self._controlSoilTypes      = ControlCombo('Soil Type')
        self._controlWetSoil        = ControlCombo('Initial soil H20')
        self._controlNO3Soil        = ControlCombo('Initial soil NO3')

        # All Control Numbers
        self._simulStartYear = ControlNumber('Starting Year', default=1989, min=1989, max=2017)
        self._simulEndYear   = ControlNumber('Ending Year', default=2017, min=1988, max=2017)
        
        # All Control Text fields
        self._wnTestStart       = ControlText('Earliest Planting Date(DOY):', default='1')
        self._wnTestEnd         = ControlText('Latest Planting Date(DOY):', default='90')
        self._textPltDensity    = ControlText('Planting Density [plants/m2]:', default='7.6')

        #Define the button action
        self._buttonField  		= ControlButton('Update')
        self._buttonField.value = self.buttonUpdateAction


        # All resluts are stored here
        self.allValues = []


        self.formset = [

                    # First block
                    [   '\n\t1 - Weather Station & Planting Year', ('\t', '_controlWaStat','_pltDate', ' '), '  '],
                    
                    '=',

                    # Second block: Seasonal Climate Forecast
                    [
                        '\n\t2 - Seasonal Climate Forecast', 
                            [
                                
                                # Year 1
                                ('\t', '_year1', '||', '_yearL1', '||', '_yearL2', '||', 
                                    [ ('_btnBN1'), ('_btnAN1'), ('_btnSCF1'), ('_btnPlt1')], 
                                    
                                    ' ')

                                , '=',

                                # Year 2
                                ('\t', '_year2', '||', '_yearL3', '||', '_yearL4', '||', 
                                    [ ('_btnBN2'), ('_btnAN2'), ('_btnSCF2'), ('_btnPlt2')], 
                                    
                                    ' ')

                                , '=',

                                # BUtton
                                ('\t\t\t', '_btnCmpNN', '\t\t\t')
                                #  '_btnCmpNN'

                            ]
                            
                    ],

                    '=',

                    # 3rd block: Planting Details (Maize)
                    [
                        '\n\t3 - Planting Details (Maize)', ('\t','_controlpltDetails', '  ', '_textPltDensity', ' '), ' '
                    ],

                    '=',

                    # 4th block: Soil Condition
                    [
                        '\n\t4 - Soil Condition', [('\t', '_controlSoilTypes', ' '), ('\t','_controlWetSoil', ' '), ('\t','_controlNO3Soil', ' ')], ' '
                    ],

                    ['_buttonField']
            
            ]

        # Execution of different structure types
        self.__controlBoxWeatherStat()
        self.__controlBoxCulType()
        self.__controlBoxSoilType()
        self.__controlBoxh20Type()
        self.__controlBoxNO3Type()

    def iniValuesN(self):
        lis = []
        ini = [0, 33, 33, 34]
        for i in range(12):
            tmp_txt3 = ControlText('', default='34', readonly=True)
            ini[3] = tmp_txt3
            tmp_txt0 = ControlText('', default=str(i+1), readonly=True)
            ini[0] = tmp_txt0 
            lis.append(tuple(ini))
        return lis

    
    def __backupValues(self, elmtList, year,  bnFlg=True, anFlg=True ):
    #    """ the arg is a list of 12 sublist. Each sublist contains 4 elements
    #    if bn or an is True, it means do not backup, just replace current value with 33 """

        lis = []
        bnLIs = []
        anLIs = []

        for i in range(len(elmtList)):
            e = elmtList[i]
            ini = [0, 0, 0, 0]

            if bnFlg is False:
                bn = 33
            else:
                bn = int(e[1])
            bnLIs.append(bn)

            if anFlg is False:
                an = 33    
            else:
                an = int(e[2])
            anLIs.append(an)

            if (bnFlg is True) and (anFlg is True):
                nn = self.__computeNN(bn, an, year)
            else:
                nn = 34

            tmp_txt3 = ControlText('', default=repr(nn), readonly=True)
            tmp_txt0 = ControlText('', default=str(i+1), readonly=True)
            ini[0] = tmp_txt0 
            ini[1] = bn
            ini[2] = an
            ini[3] = tmp_txt3

            lis.append(tuple(ini))
            
        return lis, bnLIs, anLIs

    def __btnSCF1Action(self):
        
        self._yearL1.clear()
        self._yearL1.value = [('J1', False), ('F1', False),  ('M1', False),  ('A1', False), ('M1', False), \
             ('J1', False), ('J1', False), ('A1', False), ('S1', False), ('O1', False), ('N1', False), ('D1', False) ]

    def __btnSCF2Action(self):
        
        self._yearL3.clear()
        self._yearL3.value = [('J1', False), ('F1', False),  ('M1', False),  ('A1', False), ('M1', False), \
             ('J1', False), ('J1', False), ('A1', False), ('S1', False), ('O1', False), ('N1', False), ('D1', False) ]

    def __btnBN1Action(self):
        bnLis, anLis = [], []
        listA = self._year1.value
        self._year1Backup.value, bnLis, anLis = self.__backupValues(listA, 1, False, True )
        self._year1.clear()
        self._year1.value = self._year1Backup.value
        self._nnList1 = []

    def __btnBN2Action(self):
        bnLis, anLis = [], []
        listA = self._year2.value
        self._year2Backup.value, bnLis, anLis  = self.__backupValues(listA, 2, False, True )
        self._year2.clear()
        self._year2.value = self._year2Backup.value
        self._nnList2 = []

    def __btnAN1Action(self):
        
        bnLis, anLis = [], []
        listA = self._year1.value
        self._year1Backup.value, bnLis, anLis  = self.__backupValues(listA, 1, True, False )
        self._year1.clear()
        self._year1.value = self._year1Backup.value
        self._nnList1 = []
        

    def __btnAN2Action(self):
        
        bnLis, anLis = [], []
        
        listA = self._year2.value
        self._year2Backup.value, bnLis, anLis  = self.__backupValues(listA, 2, True, False )
        self._year2.clear()
        self._year2.value = self._year2Backup.valueZZZ

    def __btnPlt1Action(self):
        
        self._yearL2.clear()
        self._yearL2.value = [('J1', False), ('F1', False),  ('M1', False),  ('A1', False), ('M1', False), \
             ('J1', False), ('J1', False), ('A1', False), ('S1', False), ('O1', False), ('N1', False), ('D1', False) ]

    def __btnPlt2Action(self):
        
        self._yearL4.clear()
        self._yearL4.value = [('J1', False), ('F1', False),  ('M1', False),  ('A1', False), ('M1', False), \
             ('J1', False), ('J1', False), ('A1', False), ('S1', False), ('O1', False), ('N1', False), ('D1', False) ]

    def __btnCmpNNAction(self):
        
        bnLis1, anLis1 = [], []
        bnLis2, anLis2 = [], []
        listNN1 = self._year1.value
        listNN2 = self._year2.value

        self._nnBackup1.value, bnLis1, anLis1 = self.__backupValues(listNN1, 1, True, True )
        self._nnBackup2.value, bnLis2, anLis2 = self.__backupValues(listNN2, 2, True, True )

        self._year1.clear()
        self._year2.clear()

        self._year1.value = self._nnBackup1.value       
        self._year2.value = self._nnBackup2.value  

        for i in range(12):
            self._nnListF[i] =  self._nnList1[i]   
        for i in range(12, 24):
            self._nnListF[i] =  self._nnList2[i-12]   

        self._bnListF = bnLis1 + bnLis2
        self._anListF = anLis1 + anLis2

        #print(self._nnList1, self._nnList2)
        self._nnList1 = []
        self._nnList2 = []

    def __computeNN(self, bn, an, year):
        nn = 100 - bn - an
        if nn < 0:
            print("Error in computing NN: NN is negative!! Please check AN and BN")
            print("The value 0 will be assigned to NN")
            nn = 0
        else:
            if year == 1:
                self._nnList1.append(nn)
            elif year == 2:
                self._nnList2.append(nn)
            
        return nn

    def buttonUpdateAction(self):
        """Store/Update all inputs"""
        self.allValues = []
        self._weatherStationValue = self.setWeatherStaValue() #""
        self._startYearValue, self._endYearValue = self.setSimulationPeriod()
        self._startPlantingValue, self._endPlantingValue = self.setPlantingWindow()
        self._cultivarTypeValue = self.setCultivarTypeValue()
        self._pltDensityValue = self.setPlantingDensity()
        self._soilTypeValue = self.setSoilTypeValue()
        self._h20Value = self.seth20TypeValue()
        self._no3Value = self.setNO3TypeValue()

        
        self.allValues.append(self._weatherStationValue) #0 - Weather
        self.allValues.append(int(self._pltDate.value))      #1 - Start Y
        self.allValues.append(self._endYearValue)      #2 - end Y
        self.allValues.append(self._startPlantingValue)     #3 - Start Period
        self.allValues.append(self._endPlantingValue)     #4 - end Period
        self.allValues.append(self._cultivarTypeValue)     #5 - Cultivar type
        self.allValues.append(self._pltDensityValue)     #6 - density
        self.allValues.append(self._soilTypeValue)     #7 - soil type
        self.allValues.append(self._h20Value)     #8 - wet value
        self.allValues.append(self._no3Value)     #9 - NO3 value
        self.allValues.append(self.getNNList())     #10 - NN values
        self.allValues.append(self.getSCF1Values())     #11 - SCF values of year 1
        self.allValues.append(self.getSCF2Values())     #12 - SCF values of year 2
        self.allValues.append(self.getPlt1Values())     #13 - Planting values of year 1
        self.allValues.append(self.getPlt2Values())     #14 - Planting values of year 2
        self.allValues.append(self._bnListF)            #15 - BN values
        self.allValues.append(self._anListF)            #16 - AN values
        self.__summary()
        
    
    def getNNList(self):
        return self._nnListF

    def getSCF1Values(self):

        lis = 12*[0]

        for i in range(len(self._yearL1.items)):
            tup = self._yearL1.items[i]
            if tup[1] == True:
                lis[i] = 1

        return lis

    def getSCF2Values(self):
        lis = 12*[0]

        for i in range(len(self._yearL3.items)):
            tup = self._yearL3.items[i]
            if tup[1] == True:
                lis[i] = 1

        return lis

    def getPlt1Values(self):
        lis = 12*[0]
        # print(self._yearL2.items)
        for i in range(len(self._yearL2.items)):
            tup = self._yearL2.items[i]
            # print("tup is", tup)
            if tup[1] == True:
                lis[i] = 1

        return lis
        
    def getPlt2Values(self):
        lis = 12*[0]
        # print(self._yearL4.items)
        for i in range(len(self._yearL4.items)):
            tup = self._yearL4.items[i]
            if tup[1] == True:
                lis[i] = 1

        return lis

    def __summary(self):
        #print("EOD: {}; LPD: {}; Density: {}".format(self._startPlantingValue, self._endPlantingValue, _pltDensityValue))
        print("__SUMMARY___")
        for e in self.allValues:
            print(e)

    def clearRes(self):
        self.allValues = []

    def Setup1Results(self):
        """returns a list of the 10 input values"""

        return self.allValues

    def __controlBoxWeatherStat(self):
        stationList = self._weatherStationValueList
        for i in range(len(stationList)):
            stationName = stationList[i][0]
            stationCode = stationList[i][1]
            self._controlWaStat.add_item(stationName, stationCode)

        self._controlWaStat.text = stationList[0][0]
        #self._controlWaStat.changed_event = self.__getWeatherStaValue

    def __controlBoxCulType(self):
        cultivarList = self._cultivarTypeList
        for i in range(len(cultivarList)):
            culName = cultivarList[i][0]
            culCode = cultivarList[i][1]
            self._controlpltDetails.add_item(culName, culCode)

        self._controlpltDetails.text = cultivarList[0][0]
        #self._controlpltDetails.changed_event = self.__getCultivarTypeValue

    def __controlBoxSoilType(self):
        soilList = self._soilTypeList
        for i in range(len(soilList)):
            soilName = soilList[i][0]
            soilCode = soilList[i][1]
            self._controlSoilTypes.add_item(soilName, soilCode)

        self._controlSoilTypes.text = soilList[0][0]
        #self._controlSoilTypes.changed_event = self.__getSoilTypeValue


    def __controlBoxh20Type(self):
        wetSoilList = self._h20List
        for i in range(len(wetSoilList)):
            soilName = wetSoilList[i][0]
            soilCode = wetSoilList[i][1]
            self._controlWetSoil.add_item(soilName, soilCode)

        self._controlWetSoil.text = wetSoilList[0][0]
        #self._controlWetSoil.changed_event = self.__geth20TypeValue

    def __controlBoxNO3Type(self):
        no3List = self._no3List
        for i in range(len(no3List)):
            soilName = no3List[i][0]
            soilCode = no3List[i][1]
            self._controlNO3Soil.add_item(soilName, soilCode)

        self._controlNO3Soil.text = no3List[0][0]
        #self._controlNO3Soil.changed_event = self.__getNO3TypeValue

    def setWeatherStaValue(self):
        """ This is a setter. It sets and store the weather station value"""
        return self._controlWaStat.value

    def setCultivarTypeValue(self):
        """ This is a setter. It sets and store the cultivar type"""
        return self._controlpltDetails.value

    def setSoilTypeValue(self):
        """ This is a setter. It sets and store the soil type"""
        return self._controlSoilTypes.value

    def seth20TypeValue(self):
        """ This is a setter. It sets and store the h20 type"""
        return self._controlWetSoil.value

    def setNO3TypeValue(self):
        """ This is a setter. It sets and store the h20 type"""
        return self._controlNO3Soil.value

    def __getNO3TypeValue(self):
        """ This is a getter. It gets and store the h20 type"""
        print(self._controlNO3Soil.value)

    def setSimulationPeriod(self):
        """ This is a setter. It sets and store the simulation year"""
        return self._simulStartYear.value, self._simulEndYear.value

    def setPlantingWindow(self):
        """ This is a setter. It sets and store the planting period"""        
        return self._wnTestStart.value, self._wnTestEnd.value

    def setPlantingDensity(self):
        """ This is a setter. It sets and store the planting density"""
        return self._textPltDensity.value
