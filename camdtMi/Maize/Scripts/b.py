def writeSNX_main(self, SNX_fname ,crop, plt_date, obs_flag):
        self.Wdir_path = self.__getWorkingDir()
        temp_snx =  self.Wdir_path.replace("/","\\") + "\\PHMZTEMP.SNX" 
        
        fr = open(temp_snx,"r") #opens temp SNX file to read
        fw = open(SNX_fname,"w") #opens SNX file to write

        #===set up parameters
        MI= '0'    #'1'  #str(self._setup2.getApplyIrr())  #should be one character 
        MF= str(self._setup2.getApplyFert())    #'1'

        #print("Results1: ", self.setup1Results)
        INGENO = self.setup1Results[5] #cultivar type
        WSTA = self.setup1Results[0][0:4]  #'SANJ'
        ID_SOIL = self.setup1Results[7]
        start_year = self.setup1Results[1] #'2002'
        NYERS='1'  #repr(int(end_year)-int(start_year)+1) #'12'
        temp=int(plt_date%1000)-30  #initial condition: 30 day before planting  #EJ(3/13/2019)

        if temp <= 0 :   #EJ(3/13/2019)
            if calendar.isleap(int(start_year)-1):  
                temp = 366 + temp
            else:
                temp = 365 + temp
            ICDAT = repr((plt_date//1000 -1)*1000+temp)
        else:
            ICDAT = repr((plt_date//1000)*1000+temp)

        i_NO3 = self.setup1Results[9] #'H' #or 'L'
        SNH4 = 1.5  #**EJ(5/27/2015) followed by Walter's ETMZDSS6.SNX
        #PDATE=start_year[2:]+repr(plt_date).zfill(3)
        PDATE=repr(plt_date)[2:]      #YYDOY format

        # if plt_date > 244: #244 (Sep 1) is an approximate number to prevent growing period goes beyond the available late year data
        #     NYERS = str(int(NYERS)-1)   #adjust total simulation years  

        FInputs1 = self._setup2.getFertInpList1()
        FInputs2 = self._setup2.getFertInpList2()
        FInputs3 = self._setup2.getFertInpList3()

        FDATE1  =   FInputs1[0] #'0'
        FAMN1   =   FInputs1[1] #'50'  #1st fertilizer application amount
        FMCD1   =   FInputs1[2] #'50'  #1st fertilizer Material   
        if FInputs1[3] != "None":      #1st fertilizer application
            FACD1   = FInputs1[3]
        else:
             FACD1  = '-99' 

        FDATE2  =   FInputs2[0]  #'60'
        FAMN2   =   FInputs2[1] #'90'  #2nd fertilizer application amount
        FMCD2   =   FInputs2[2] #'50'  #2nd fertilizer Material  
        if FInputs2[3] != "None":      #2nd fertilizer application
            FACD2   = FInputs2[3]
        else:
             FACD2  = '-99'     

        FDATE3  =   FInputs3[0]  #'60'
        FAMN3   =   FInputs3[1] #'90'  #3rd fertilizer application amount
        FMCD3   =   FInputs3[2] #'50'  #3rd fertilizer Material    
        if FInputs3[3] != "None":      #3rd fertilizer application
            FACD3   = FInputs3[3]
        else:
             FACD3  = '-99' 

        # temp=int(plt_date)-3 #EJ(6/9/2015) #simuation starting date
       
        SDATE=ICDAT    #EJ(3/13/2019)

        if self._setup2.getApplyIrr() == 1:
            IRRIG='A'  #automatic, or 'N' (no irrigation)
        else:
            IRRIG='N'  #automatic, or 'N' (no irrigation)

        if self._setup2.getApplyFert == 1:
            FERTI='D' # 'D'= Days after planting, 'R'=on report date, or 'N' (no fertilizer)
        else:
            FERTI='N'

        IC_w_ratio = float(self.setup1Results[8]) #e.g., 0.7 
        IMETH = self._setup2.getApplyIrrMeth() #'IR001' #irrigation method
        #===end of setting up paramters

        
         #read lines 1-9 from temp file
        for line in range(0,14):
            temp_str=fr.readline()
            fw.write(temp_str)

        #write *TREATMENTS 
        CU='1'
        SA='0'
        IC='1'
        MP='1'
        MR='0'
        MC='0'
        MT='0'
        ME='0'
        MH='0'
        SM='1'
        temp_str=fr.readline()
        for i in range(0,100):
            FL = str(i+1)
            fw.write('{0:3s}{1:31s}{2:3s}{3:3s}{4:3s}{5:3s}{6:3s}{7:3s}{8:3s}{9:3s}{10:3s}{11:3s}{12:3s}{13:3s}'.format(FL.rjust(3),'1 0 0 ERiMA DCC1                 1',
                        FL.rjust(3),SA.rjust(3), IC.rjust(3), MP.rjust(3), MI.rjust(3), MF.rjust(3), MR.rjust(3), MC.rjust(3), 
                        MT.rjust(3), ME.rjust(3), MH.rjust(3), SM.rjust(3)))  
            fw.write(" \n")  
        if obs_flag == 1:
            FL = str(101)  #if observed weather is available, run #101 treatment with observed weather
            fw.write('{0:3s}{1:31s}{2:3s}{3:3s}{4:3s}{5:3s}{6:3s}{7:3s}{8:3s}{9:3s}{10:3s}{11:3s}{12:3s}{13:3s}'.format(FL.rjust(3),'1 0 0 ERiMA DCC1                 1',
                        FL.rjust(3),SA.rjust(3), IC.rjust(3), MP.rjust(3), MI.rjust(3), MF.rjust(3), MR.rjust(3), MC.rjust(3), 
                        MT.rjust(3), ME.rjust(3), MH.rjust(3), SM.rjust(3)))       
            fw.write(" \n")

        #read lines from temp file
        for line in range(0,3):
            temp_str=fr.readline()
            fw.write(temp_str)

        #write *CULTIVARS
        temp_str=fr.readline()
        new_str=temp_str[0:6] + INGENO + temp_str[26:]
        fw.write(new_str)
        fw.write(" \n")

        #read lines from temp file
        for line in range(0,3):
            temp_str=fr.readline()
            fw.write(temp_str)

        #================write *FIELDS   
        #Get soil info from *.SOL
        #soil_depth, wp, fc, nlayer = get_soil_IC(ID_SOIL)  
        soil_depth, wp, fc, nlayer, SLTX = self.get_soil_IC(ID_SOIL)  # <=========== ***********************
        temp_str=fr.readline()
        SLDP = repr(soil_depth[-1])
        for i in range(0,100):
            FL = str(i+1)
            ID_FIELD = WSTA + str(i+1).zfill(4)  #WSTA = 'SANJ'
            WSTA_ID = str(i+1).zfill(4)
            fw.write('{0:3s}{1:8s}{2:5s}{3:3s}{4:6s}{5:4s}  {6:10s}{7:4s}'.format(FL.rjust(3), ID_FIELD, WSTA_ID.rjust(5), '       -99   -99   -99   -99   -99   -99 ',
                                                SLTX.ljust(6), SLDP.rjust(4), ID_SOIL,' -99'))  
            fw.write(" \n")  
        if obs_flag == 1:
            FL = str(101)  #if observed weather is available, run #101 treatment with observed weather
            ID_FIELD = WSTA + str(101).zfill(4)  #WSTA = 'SANJ'
            WSTA_ID = WSTA
            fw.write('{0:3s}{1:8s}{2:5s}{3:3s}{4:6s}{5:4s}  {6:10s}{7:4s}'.format(FL.rjust(3), ID_FIELD, WSTA_ID.rjust(5), '       -99   -99   -99   -99   -99   -99 ',
                                                SLTX.ljust(6), SLDP.rjust(4), ID_SOIL,' -99'))  
            fw.write(" \n")  

        temp_str=fr.readline()  #@L ...........XCRD ...........YCRD .....ELEV
        fw.write(temp_str)
        temp_str=fr.readline()  # 1             -99             -99       -99   ==> skip
        #================write *FIELDS - second section
        for i in range(0,100):
            FL = str(i+1)
            fw.write('{0:3s}{1:89s}'.format(FL.rjust(3), '            -99             -99       -99               -99   -99   -99   -99   -99   -99'))
            fw.write(" \n")  
        if obs_flag == 1:
            FL = str(101)  #if observed weather is available, run #101 treatment with observed weather
            fw.write('{0:3s}{1:89s}'.format(FL.rjust(3), '            -99             -99       -99               -99   -99   -99   -99   -99   -99'))
            fw.write(" \n")  
        
        #read lines from temp file
        for line in range(0,3):
            temp_str=fr.readline()
            fw.write(temp_str)
        #write *INITIAL CONDITIONS   
        temp_str=fr.readline()
        new_str=temp_str[0:9] + ICDAT[2:] + temp_str[14:]
        fw.write(new_str)
        temp_str=fr.readline() #@C  ICBL  SH2O  SNH4  SNO3 
        fw.write(temp_str)

        #Get soil info from *.SOL
        #soil_depth, wp, fc, nlayer = get_soil_IC(ID_SOIL)  
        temp_str=fr.readline()
        for nline in range(0,nlayer):
            if nline == 0:  #first layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                # SH2O=0.7*(float(fc[nline])- float(wp[nline]))+ float(wp[nline])#EJ(6/25/2015): initial AWC=70% of maximum AWC
                if i_NO3 == 'H':
                    SNO3='14'  # arbitrary... following to Uruguay DSS
                elif i_NO3 == 'L':
                    SNO3='5'  # arbitrary... following to Uruguay DSS
                else:
                    # self.ini_NO3_err.activate() 
                    print("Error of NO3 value")                  
            elif nline == 1:  #second layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                if i_NO3 == 'H':
                    SNO3='14'  # arbitrary... following to Uruguay DSS
                elif i_NO3 == 'L':
                    SNO3='5'  # arbitrary... following to Uruguay DSS
                else:
                    # self.ini_NO3_err.activate() 
                    print("Error of NO3 value") 
            elif nline == 2:  #third layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
            elif nline == 3:  #forth layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
            elif nline == 4:  #fifth layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
            elif nline == 5:  #sixth layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
            else:
                temp_SH2O=fc[nline] #float
                SNO3='0'  #**EJ(5/27/2015)
            
            SH2O=repr(temp_SH2O)[0:5]  #convert float to string
            new_str=temp_str[0:5] + repr(soil_depth[nline]).rjust(3) + ' ' + SH2O.rjust(5) + temp_str[14:22]+ SNO3.rjust(4)+ "\n"
            fw.write(new_str)
        fw.write("  \n")

        for nline in range(0,10):
            temp_str=fr.readline()
            #print temp_str
            if temp_str[0:9] == '*PLANTING':
                break

        fw.write(temp_str) #*PLANTING DETAILS  
        temp_str=fr.readline() #@P PDATE EDATE
        fw.write(temp_str)


        #write *PLANTING DETAILS
        temp_str=fr.readline()
        PPOP    = self.setup1Results[6] #planting density
        PPOE    = self.setup1Results[6] #planting density       
        #new_str=temp_str[0:3] + PDATE + temp_str[8:]
        new_str=temp_str[0:3] + PDATE + temp_str[8:14] + PPOP.rjust(6) + PPOE.rjust(6) + temp_str[26:]
        fw.write(new_str)


        #read lines from temp file
        for line in range(0,3):
            temp_str=fr.readline()
            fw.write(temp_str)

        #write *FERTILIZERS (INORGANIC)
        if MF == '1':
            temp_str=fr.readline()
            new_str=temp_str[0:5] + str(FDATE1).rjust(3) + ' '+ str(FMCD1).rjust(5)+' '+str(FACD1).rjust(5)+temp_str[20:30]+ str(FAMN1).rjust(2) + temp_str[32:]
            fw.write(new_str)
            temp_str=fr.readline()
            if self._setup2.getApplyNumFert() == '2':
                new_str=temp_str[0:5] + str(FDATE2).rjust(3) + ' '+ str(FMCD2).rjust(5)+' '+str(FACD2).rjust(5)+temp_str[20:30]+ str(FAMN2).rjust(2) + temp_str[32:]
                fw.write(new_str)
            if self._setup2.getApplyNumFert() == '3':
                new_str=temp_str[0:5] + str(FDATE2).rjust(3) + ' '+ str(FMCD2).rjust(5)+' '+str(FACD2).rjust(5)+temp_str[20:30]+ str(FAMN2).rjust(2) + temp_str[32:]
                fw.write(new_str)
                new_str=temp_str[0:5] + str(FDATE3).rjust(3) + ' '+ str(FMCD3).rjust(5)+' '+str(FACD3).rjust(5)+temp_str[20:30]+ str(FAMN3).rjust(2) + temp_str[32:]
                fw.write(new_str)
            #check errors in User's input
            # if self.nfertilizer.getvalue() == '':
            #     self.fert_err.activate()
            # if self.label006.cget("text") == '': #'N/A':
            #     self.fert_err.activate()
            # if self.label007.cget("text") == '': #'N/A':
            #     self.fert_err.activate()
            if self._setup2.getApplyNumFert() == '0':
                print("Error: NUmber of fertlization app is 0.")

        else:  #
            temp_str=fr.readline()
            fw.write(temp_str)
            temp_str=fr.readline()
            fw.write(temp_str)

        #read lines from temp file
        for line in range(0,3):
            temp_str=fr.readline()
            fw.write(temp_str)
        
        #write *SIMULATION CONTROLS
        temp_str=fr.readline()
        new_str=temp_str[0:33]+ SDATE[2:] + temp_str[38:]
        fw.write(new_str)
        temp_str=fr.readline() #@N OPTIONS
        fw.write(temp_str)
        temp_str=fr.readline()  # 1 OP     
        fw.write(temp_str)
        temp_str=fr.readline()  #@N METHODS 
        fw.write(temp_str)
        temp_str=fr.readline()  # 1 ME      
        fw.write(temp_str)
        temp_str=fr.readline()  #@N MANAGEMENT 
        fw.write(temp_str)
        temp_str=fr.readline()  # 1 MA   
        new_str=temp_str[0:25] + IRRIG + temp_str[26:31]+ FERTI + temp_str[32:]
        fw.write(new_str)
        temp_str=fr.readline()  #@N OUTPUTS
        fw.write(temp_str)
        temp_str=fr.readline()  # 1 OU   
        fw.write(temp_str)
            
        #read lines from temp file
        for line in range(0,5):
            temp_str=fr.readline()
            fw.write(temp_str)
        #irrigation method
        temp_str=fr.readline()  #  1 IR 
        new_str=temp_str[0:39] + IMETH + temp_str[44:]
        fw.write(new_str)

        #read lines from temp file
        for line in range(0,7):
            temp_str=fr.readline()
            fw.write(temp_str)

        irr_meth = self._setup2.getApplyIrrMeth()

        #read lines from temp file
        for line in range(0,7):
            temp_str=fr.readline()
            fw.write(temp_str) 

        fr.close()
        fw.close()
    #====End of WRITE *.SNX