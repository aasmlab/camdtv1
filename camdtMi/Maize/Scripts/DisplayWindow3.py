from __init__ import *


class DisplayWindow3( BaseWidget):

    """ Main window for showing the diferent """

    def __init__(self):
        BaseWidget.__init__(self,'Display Window')
        self.parent = None

        self._imgPath = [
                            ['', '', '', '', '', ''],  # 0 Yield Seasonal figs $Scenarios 1
                            ['', '', '', '', '', ''],  # 1 Yield Hist figs $Scenarios 2
                            ['', '', '', '', '', ''],  # 2 Scenarios 3
                            ['', '','', ''],           # 3 WSGD & NSTD figs index 0&1 for Season, index2&3 for His
                            ['', '', '', ''],          # 4 weather figs index3 is for Plot_CUm_Rain (Seasonal)
                            ['', '', '', ''],           # 5 weather figs index3 is for Plot_CUm_Rain (Hist)
                            ['', '', '', '']               # 6 comparison in Hist (scenario 1, 2, 3)
                        ]

        self._display = ControlImage('Images')

        self._buttonOne  		= ControlButton('Historical I. Display Yield Estimation (Boxplot)')
        self._buttonOne.value = self.__buttonOneAction

        self._buttonOneFor  		= ControlButton('Forecast I. Display Yield Estimation (Boxplot)')
        self._buttonOneFor.value = self.__buttonOneForAction


        self._imageOneS3 = ControlImage('Boxplot: Yield Estimation')     
        self._imageOneS3For = ControlImage('Boxplot: Yield Estimation (Seasonal)')   

        ########

        self._buttonTwo  		= ControlButton('Historical II. Display Yield Estimation (Exceedance Curve)')
        self._buttonTwo.value = self.__buttonTwoAction

        self._buttonTwoFor  		= ControlButton('Forecast II. Display Yield Estimation (Exceedance Curve)')
        self._buttonTwoFor.value = self.__buttonTwoForAction
    

        self._imageTwoS3 = ControlImage('Curve: Yield Estimation')      
        self._imageTwoS3For = ControlImage('Curve: Yield Estimation (Seasonal)')  

        ########

        self._buttonThree 		= ControlButton('Historical III. Display Maturity date (Boxplot)')
        self._buttonThree.value = self.__buttonThreeAction

        self._buttonThreeFor  		= ControlButton('Forecast III. Display Maturity date (Boxplot)')
        self._buttonThreeFor.value = self.__buttonThreeForAction


        self._imageThreeS3 = ControlImage('Boxplot: Maturity date')
        self._imageThreeS3For = ControlImage('Boxplot: Maturity date (Seasonal)')  

        #################

        self._buttonFour  		= ControlButton('Historical IV. Display Maturity date (Exceedance Curve)') 
        self._buttonFour.value = self.__buttonFourAction

        self._buttonFourFor  		= ControlButton('Forecast IV. Display Maturity date (Exceedance Curve)')
        self._buttonFourFor.value = self.__buttonFourForAction

        self._imageFourS3 = ControlImage('Curve: Maturity date')
        self._imageFourS3For = ControlImage('Curve: Maturity date (Seasonal)')  

        #################

        self._buttonFive 		= ControlButton('Historical V. Display Anthesis date (Boxplot)')
        self._buttonFive.value = self.__buttonFiveAction

        self._buttonFiveFor  		= ControlButton('Forecast V. Display Anthesis date (Boxplot)')
        self._buttonFiveFor.value = self.__buttonFiveForAction
          
        self._imageFiveS3 = ControlImage('Boxplot: Anthesis date')     
        self._imageFiveS3For = ControlImage('Boxplot: Anthesis date (Seasonal)')  
  
        ##################

        self._buttonSix  		= ControlButton('Historical VI. Display Anthesis date (Exceedance Curve)') 
        self._buttonSix.value = self.__buttonSixAction

        self._buttonSixFor  		= ControlButton('Forecast VI. Display Anthesis date (Exceedance Curve)')
        self._buttonSixFor.value = self.__buttonSixForAction

        self._imageSixS3 = ControlImage('Curve: Anthesis date')
        self._imageSixS3For = ControlImage('Curve: Anthesis date (Seasonal)')  

        ################
        self._btnCumRain        = ControlButton('Historical VII. Display Cumulative Precipitaion')
        self._btnCumRainFor     = ControlButton('Forecast  VII. Display Cumulative Precipitaion')
        self._btnCumRain.value  = self.__F1
        self._btnCumRainFor.value  = self.__F1For


        self._imageF1S3 = ControlImage('Historical')
        self._imageF1S3For = ControlImage('Seasonal') 
        ##################

        self._btnNStress        = ControlButton('Historical VIII. Display NSTD')
        self._btnNStressFor     = ControlButton('Forecast  VIII. Display NSTD')
        self._btnNStress.value  = self.__F2
        self._btnNStressFor.value  = self.__F2For


        self._imageF2S3 = ControlImage('Historical')
        self._imageF2S3For = ControlImage('Seasonal') 
        ##################

        self._btnSRAD        = ControlButton('Historical IX. Display SRAD')
        self._btnSRADFor     = ControlButton('Forecast  IX. Display SRAD')
        self._btnSRAD.value  = self.__F3
        self._btnSRADFor.value  = self.__F3For

        self._imageF3S3 = ControlImage('Historical')
        self._imageF3S3For = ControlImage('Seasonal') 
        ##################

        self._btnTMax        = ControlButton('Historical X. Display TMax')
        self._btnTMaxFor     = ControlButton('Forecast  X. Display TMax')
        self._btnTMax.value  = self.__F4
        self._btnTMaxFor.value  = self.__F4For



        self._imageF4S3 = ControlImage('Historical')
        self._imageF4S3For = ControlImage('Seasonal') 
        ##################


        self._btnTMin        = ControlButton('Historical XI. Display TMin')
        self._btnTMinFor     = ControlButton('Forecast  XI. Display TMin')
        self._btnTMin.value  = self.__F5
        self._btnTMinFor.value  = self.__F5For



        self._imageF5S3 = ControlImage('Historical')
        self._imageF5S3For = ControlImage('Seasonal') 
        ##################


        self._btnWStress        = ControlButton('Historical XII. Display Water Stress')
        self._btnWStressFor     = ControlButton('Forecast  XII. Display Water Stress')
        self._btnWStress.value  = self.__F6
        self._btnWStressFor.value  = self.__F6For

        self._imageF6S3 = ControlImage('Historical')
        self._imageF6S3For = ControlImage('Seasonal') 
        ##################


        self._imageMid = ControlImage('Comparison')

        self._buttonMid3  		= ControlButton('Compare Yield Hist vs. Seas')
        self._buttonMid3.value = self.__buttonMid3Action


        self.formset = [
           [ # The buttons

                ('\t', '*** Seasonal ***', ' ' ),
                ('\t', '_buttonOneFor', ' ' ), 
                ( '\t', '_buttonTwoFor', ' ' ), 
                ('\t', '_buttonThreeFor', ' ' ), 
                ('\t',  '_buttonFourFor', ' '),  
                ('\t', '_buttonFiveFor', ' ' ), 
                ( '\t', '_buttonSixFor', ' '),
                ( '\t', '_btnCumRainFor', ' '),
                ( '\t', '_btnNStressFor', ' '),
                ( '\t', '_btnSRADFor', ' '),
                ( '\t', '_btnTMaxFor', ' '),
                ( '\t', '_btnTMinFor', ' '),
                ( '\t', '_btnWStressFor', ' '),

                ( '\t', '_buttonMid3', ' '),

                ('\t', '*** Historical ***', ' ' ),
                

                ('\t', '_buttonOne', ' ' ), 
                ( '\t', '_buttonTwo', ' ' ), 
                ('\t', '_buttonThree', ' ' ), 
                ('\t',  '_buttonFour', ' '),  
                ('\t', '_buttonFive', ' ' ), 
                ( '\t', '_buttonSix', ' '),
                ( '\t', '_btnCumRain', ' '),
                ( '\t', '_btnNStress', ' '),
                ( '\t', '_btnSRAD', ' '),
                ( '\t', '_btnTMax', ' '),
                ( '\t', '_btnTMin', ' '),
                ( '\t', '_btnWStress', ' '),

                    '\n',
           
            ]
            ,

            '||', # Sections split vertically

            #[ # The Images section

                 ( "_imageOneS3For",  "_imageOneS3", '         '), 
                 ( "_imageTwoS3For",    "_imageTwoS3", '         '),
                 ( "_imageThreeS3For",    "_imageThreeS3", '         '),
                 ( "_imageFourS3For",   "_imageFourS3", '         '),
                 ( "_imageFiveS3For",   "_imageFiveS3", '         '),
                 ( "_imageSixS3For",   "_imageSixS3", '         '),
                #  '\n',
                    ( '_imageMid', '   '),
                #  '\n',
                 
                 ( "_imageF1S3For",  "_imageF1S3", '         '),
                 ( "_imageF2S3For",   "_imageF2S3", '         '),
                 ( "_imageF3S3For",   "_imageF3S3", '         '),
                 ( "_imageF4S3For",   "_imageF4S3", '         '),
                 ( "_imageF5S3For",  "_imageF5S3", '         '),
                 ( "_imageF6S3For",  "_imageF6S3", '         '),
                 '\n',
            #]
        
                        ]


    def updateImgPath(self, index1, index2, value):
        self._imgPath[index1][index2] = value

    def displayImage(self, value1, image1):
            """ takes in the value (pathname) of an image and displays it
            image must be control.image """
            #self._display = image
            image1.value = cv2.imread(value1, 1)

    def FillIn(self, value, image):

        if value == '':
            image.hide()
        else:
            image.show()
            self.displayImage(value, image)

    def __buttonMid3Action(self):

        value1 = self._imgPath[6][3]
        self.FillIn(value1, self._imageMid)

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()

        self._imageSixS3.hide()
        self._imageSixS3For.hide()


        self._imageF1S3.hide()
        self._imageF1S3For.hide()
 

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    

    def __buttonOneAction(self):
        value1 = self._imgPath[1][0]
        self.FillIn(value1, self._imageOneS3)
        self._imageMid.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()

        self._imageSixS3.hide()
        self._imageSixS3For.hide()


        self._imageF1S3.hide()
        self._imageF1S3For.hide()
 

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()
        
    def __buttonOneForAction(self): 
        value2 = self._imgPath[0][0]

        self.FillIn(value2, self._imageOneS3For)
        self._imageMid.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()

        self._imageSixS3.hide()
        self._imageSixS3For.hide()


        self._imageF1S3.hide()
        self._imageF1S3For.hide()
 

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    def __buttonTwoAction(self):
        
        value1 = self._imgPath[1][3] 
        self.FillIn(value1, self._imageTwoS3)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()

        self._imageSixS3.hide()
        self._imageSixS3For.hide()


        self._imageF1S3.hide()
        self._imageF1S3For.hide()
 

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    def __buttonTwoForAction(self):
        
        value1 = self._imgPath[0][3] 
        self.FillIn(value1, self._imageTwoS3For)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()

        self._imageSixS3.hide()
        self._imageSixS3For.hide()


        self._imageF1S3.hide()
        self._imageF1S3For.hide()
 

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    
    
    def __buttonThreeAction(self):
        
        value1 = self._imgPath[1][2] 
        self.FillIn(value1, self._imageThreeS3)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()

        self._imageSixS3.hide()
        self._imageSixS3For.hide()


        self._imageF1S3.hide()
        self._imageF1S3For.hide()
 

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()
        
    def __buttonThreeForAction(self):
        
        value1 = self._imgPath[0][2] 
        self.FillIn(value1, self._imageThreeS3For)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()

        self._imageSixS3.hide()
        self._imageSixS3For.hide()


        self._imageF1S3.hide()
        self._imageF1S3For.hide()
 

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()


    def __buttonFourAction(self):
        value1 = self._imgPath[1][5]
        self.FillIn(value1, self._imageFourS3)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()

        self._imageSixS3.hide()
        self._imageSixS3For.hide()


        self._imageF1S3.hide()
        self._imageF1S3For.hide()
 

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()
        
        

    def __buttonFourForAction(self):
        value1 = self._imgPath[0][5]
        self.FillIn(value1, self._imageFourS3For)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()

        self._imageSixS3.hide()
        self._imageSixS3For.hide()


        self._imageF1S3.hide()
        self._imageF1S3For.hide()
 

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    def __buttonFiveAction(self):
        value1 = self._imgPath[1][1]
        self.FillIn(value1, self._imageFiveS3)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageSixS3.hide()
        self._imageSixS3For.hide()


        self._imageF1S3.hide()
        self._imageF1S3For.hide()
 

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    def __buttonFiveForAction(self):
        value1 = self._imgPath[0][1]
        self.FillIn(value1, self._imageFiveS3For)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageSixS3.hide()
        self._imageSixS3For.hide()


        self._imageF1S3.hide()
        self._imageF1S3For.hide()
 

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()


    def __buttonSixAction(self):
        value1 = self._imgPath[1][4]
        self.FillIn(value1, self._imageSixS3)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()


        self._imageF1S3.hide()
        self._imageF1S3For.hide()
 

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    
    def __buttonSixForAction(self):
        value1 = self._imgPath[0][4]
        self.FillIn(value1, self._imageSixS3For)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()


        self._imageF1S3.hide()
        self._imageF1S3For.hide()
 

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    def __F1(self):
        value1 = self._imgPath[5][3]
        self.FillIn(value1, self._imageF1S3)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()


        self._imageSixS3.hide()
        self._imageSixS3For.hide()
 

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    def __F1For(self):
        value1 = self._imgPath[4][3]
        self.FillIn(value1, self._imageF1S3For)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()


        self._imageSixS3.hide()
        self._imageSixS3For.hide()
 

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    def __F2(self):
        value1 = self._imgPath[3][3]
        self.FillIn(value1, self._imageF2S3)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()


        self._imageSixS3.hide()
        self._imageSixS3For.hide()
 

        self._imageF1S3.hide()
        self._imageF1S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    def __F2For(self):
        value1 = self._imgPath[3][1]
        self.FillIn(value1, self._imageF2S3For)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()


        self._imageSixS3.hide()
        self._imageSixS3For.hide()
 

        self._imageF1S3.hide()
        self._imageF1S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    def __F3(self):
        value1 = self._imgPath[5][2]
        self.FillIn(value1, self._imageF3S3)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()


        self._imageSixS3.hide()
        self._imageSixS3For.hide()
 

        self._imageF1S3.hide()
        self._imageF1S3For.hide()

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    def __F3For(self):
        value1 = self._imgPath[4][2]
        self.FillIn(value1, self._imageF3S3For)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()


        self._imageSixS3.hide()
        self._imageSixS3For.hide()
 

        self._imageF1S3.hide()
        self._imageF1S3For.hide()

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    def __F4(self):
        value1 = self._imgPath[5][0]
        self.FillIn(value1, self._imageF4S3)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()


        self._imageSixS3.hide()
        self._imageSixS3For.hide()
 

        self._imageF1S3.hide()
        self._imageF1S3For.hide()

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    def __F4For(self):
        value1 = self._imgPath[4][0]
        self.FillIn(value1, self._imageF4S3For)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()


        self._imageSixS3.hide()
        self._imageSixS3For.hide()
 

        self._imageF1S3.hide()
        self._imageF1S3For.hide()

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF5S3.hide()
        self._imageF5S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    def __F5(self):
        value1 = self._imgPath[5][1]
        self.FillIn(value1, self._imageF5S3)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()


        self._imageSixS3.hide()
        self._imageSixS3For.hide()
 

        self._imageF1S3.hide()
        self._imageF1S3For.hide()

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    def __F5For(self):
        value1 = self._imgPath[4][1]
        self.FillIn(value1, self._imageF5S3For)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()


        self._imageSixS3.hide()
        self._imageSixS3For.hide()
 

        self._imageF1S3.hide()
        self._imageF1S3For.hide()

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()


        self._imageF6S3.hide()
        self._imageF6S3For.hide()

    def __F6(self):
        value1 = self._imgPath[3][2]
        self.FillIn(value1, self._imageF6S3)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()


        self._imageSixS3.hide()
        self._imageSixS3For.hide()
 

        self._imageF1S3.hide()
        self._imageF1S3For.hide()

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()


        self._imageF5S3.hide()
        self._imageF5S3For.hide()

    def __F6For(self):
        value1 = self._imgPath[3][0]
        self.FillIn(value1, self._imageF6S3For)
        self._imageMid.hide()

        self._imageOneS3.hide()
        self._imageOneS3For.hide()

        self._imageTwoS3.hide()
        self._imageTwoS3For.hide()

        self._imageThreeS3.hide()
        self._imageThreeS3For.hide()

        self._imageFourS3.hide()
        self._imageFourS3For.hide()

        self._imageFiveS3.hide()
        self._imageFiveS3For.hide()


        self._imageSixS3.hide()
        self._imageSixS3For.hide()
 

        self._imageF1S3.hide()
        self._imageF1S3For.hide()

        self._imageF2S3.hide()
        self._imageF2S3For.hide()

        self._imageF3S3.hide()
        self._imageF3S3For.hide()

        self._imageF4S3.hide()
        self._imageF4S3For.hide()


        self._imageF5S3.hide()
        self._imageF5S3For.hide()