# INSTALLATION GUIDE (WINDOWS): CAMDT-2019


Please get [DSSAT47](https://iri.columbia.edu/~eunjin/DSSAT47.zip) from here. And put it under C:\ drive

1.  Install Anaconda. Download it [here!](https://repo.anaconda.com/archive/Anaconda3-2019.03-Windows-x86_64.exe)
2.  Install Git for Winodws. Get it from [here](https://git-scm.com/downloads)
3.  Go to [CAMDT webpage](https://gitlab.com/Kpodov/camdt-2019) and download it as a zip file
4.  Copy the zip file on you desktop. Unzip the directory unto your desktop
5.  Go in the unzipped directory:
    - right-click on "install_dependencies" to edit
    - Replace "USERNAME" with your own Windows username
    - Save and exit
    - Then double-click on "install_dependencies". (This will install pip and pyforms)
6.  Now, go into the directory of choice (Maize or Rice)
7.  If in the Maize directory, go into the Scripts directory. Then, double-click on "run_maize"
8.  OR If in the Rice directory, go into the Scripts directory. Then, double-click on "run_rice"


Please note: If at any point, you can't see some buttons or scroll down the window, just resize the window to fullscreen

Check the User guide folder for more information

