from __init__ import *

class Setup2Window( BaseWidget):

    """ This class manages setup2 inputs and is used in the scenario script"""

    def __init__(self):
        #Setup1.__init__(self)
        BaseWidget.__init__(self,'DSSAT Setup2')
        self.parent = None

        # All resluts are stored here
        self.allValues = []

        self._irrList = [("IR001(Furrow)","IR001"),  ("IR003(Flood)", "IR003"), ("IR004(Sprinkler)", "IR004") ]
        
        self._materialList = [("FE001(Ammonium nitrate)", "FE001"), ("FE004(Anhydrous ammonia)", "FE004"), 
         ("FE005(Urea)","FE005"), ("None", "None")]
        
        self._applyList = [("AP001(Broadcast, not incorporated)", "AP001"), ("AP002(Broadcast, incorporated)", "AP002"), 
        ("AP003(Banded on surface)", "AP003") , ("AP004(Banded beneath surface)", "AP004"), 
        ("AP011(Broadcast on flooded/saturated soil, none in soil)", "AP011"), ("None","None")]

        # Declaration of variables
        self._needFert = 0 # Fertlization or not?
        self._numFertApp = 0 # Number of fertilizer application
        self._numFertAppBtn = 0 # Button corrsponding to Number
        self._needIrr = 0 # Irrrigation or not?
        self._irrMethodValue = '' # Irrrigation or not?
        # if needs for fertilizer application store the four inputs here: Days after planting, Amount,  Fertilizer material, Application Method
        self._fertInpList1 = [0, 0, "None", "None"]
        self._fertInpList2 = [0, 0, "None", "None"]
        self._fertInpList3 = [0, 0, "None", "None"]

        self._IrrMth = 3 # 0 = Automatic when required(soil saturation), 1 = Automatic(periodic flooding), 2 = On Reported dates

        # IRR (Automatic - saturation of soil)
        self._Irr1Values = [30, 50, 1] # Management soil depth(cm), Threshold(% of max available), 'Efficiency fraction
        self._Irr1Lis   = ControlList('Saturation of soil')
        self._Irr1Lis.horizontal_headers = ['Descriptions', 'Values']
        self._Irr1Lis.value   = [('Management soil depth(cm)', self._Irr1Values[0]),
                                ('Threshold(% of max available)', self._Irr1Values[1]),
                                ('Efficiency fraction', self._Irr1Values[2])
                                 ]

        # IRR Automatic - repeated flooding
        self._Irr2Values = [-3, -1, 100, 50, 10, 70] # DBP, DAP, Bund Height, Flooding depth, Interval, End of appl
        self._Irr2Lis   = ControlList('Repeated Flooding')
        self._Irr2Lis.horizontal_headers = ['Descriptions', 'Values']
        self._Irr2Lis.value   = [('Puddling Date', self._Irr2Values[0]),
                                ('1st Irr date', self._Irr2Values[1]),
                                ('Bund height (mm)', self._Irr2Values[2]),
                                ('Flooding depth (mm)', self._Irr2Values[3]),
                                ('Interval ("n" days)', self._Irr2Values[4]),
                                ('End of Application', self._Irr2Values[5])
                                 ]

        # IRR On Reported Dates
        self._Irr3ck1    = ControlCheckBox("")
        self._Irr3ck2    = ControlCheckBox("")
        self._Irr3Txt1  = ControlText('Puddling date', default='-3')
        self._Irr3Txt2  = ControlText('Threshold(% of max available):', default='2')
        self._Irr3Txt3  = ControlText('Number of Irrigation?', default='1')
        self._Irr3Values = [-3, 2, [], [], 0] # DAP, Percolation rate, list of irr1, list of irr2, Number of Irr 
        self._Irr3Lis   = ControlList('On reported dates')
        self._Irr3Lis.horizontal_headers = ['No. of Irrigation', '1st', '2nd']
        self._Irr3Lis.value = [('Date (YYDOY)', -1, 20),
                                ('Bund Height', 100, 150),
                                ('Flood depth', 50, 50),
                                ('Constant depth?', 'yes', 'yes' )
                                ]
        

        #Definition of the forms fields
        self._label1     = ControlLabel('\t1 - Do you want to apply fertilizer?')
        self._label3     = ControlLabel('\t2 - Do you want to apply irrigation?')

        self._label1st     = ControlLabel('\tFirst Application')
        self._label2nd     = ControlLabel('\tSecond Application')
        self._label3rd     = ControlLabel('\tThird Application')

        self._slider1stDays = ControlSlider('Days after planting:\t', Default=0, minimum=0, maximum=180)
        self._slider1stAmount = ControlSlider('Amount (N, kg/ha):\t\t', Default=0, minimum=0, maximum=150)
        self._slider1stMat = ControlCombo('Fertilizer material:\t\t')
        self._slider1stApp = ControlCombo('Application method:\t\t')

        self._slider2ndDays = ControlSlider('', Default=0, minimum=0, maximum=180)
        self._slider2ndAmount = ControlSlider('', Default=0, minimum=0, maximum=150)
        self._slider2ndMat = ControlCombo('')
        self._slider2ndApp = ControlCombo('')

        self._slider3rdDays = ControlSlider('', Default=0, minimum=0, maximum=180)
        self._slider3rdAmount = ControlSlider('', Default=0, minimum=0, maximum=150)
        self._slider3rdMat = ControlCombo('')
        self._slider3rdApp = ControlCombo('')


        # Control Combos
        self._irrMethod         = ControlCombo('\t\tIrrigation Method')
        self._controlFertApp 	= ControlLabel('Number of fertilizer application? ')
        self._controlIrrMeth    = ControlCombo('\tIrrigation')
        self._controlFertMeth   = ControlCombo('\tFertilization application')

        #Define the button actions

            # Do you want to apply the fertilizer?
        self._buttonYes  		= ControlButton('Yes')
        self._buttonYes.value   = self.__buttonYesAction
        self._buttonNo  		= ControlButton('No')
        self._buttonNo.value    = self.__buttonNoAction


        self._buttonYesIrr  		= ControlButton('Yes (Automatic when required)')
        self._buttonYesIrr.value    = self.__buttonYesIrrAction

        self._buttonNoIrr  		= ControlButton('No')
        self._buttonNoIrr.value = self.__buttonNoIrrAction

        self._buttonOneApp  		= ControlButton('1')
        self._buttonOneApp.value = self.__buttonOneAppAction

        self._buttonTwoApp  		= ControlButton('2')
        self._buttonTwoApp.value = self.__buttonTwoAppAction

        self._buttonThreeApp  		= ControlButton('3')
        self._buttonThreeApp.value = self.__buttonThreeAppAction

        self._buttonUpdate  		= ControlButton('Update') 
        self._buttonUpdate.value = self.buttonUpdateAction


        self._btnIrr1   = ControlButton("Automatic (soil saturation)")
        self._btnIrr1.value   = self.__btnIrr1Action

        self._btnIrr2   = ControlButton("Automatic (periodic flooding)")
        self._btnIrr2.value   = self.__btnIrr2Action

        self._btnIrr3   = ControlButton("On reported dates")
        self._btnIrr3.value   = self.__btnIrr3Action

        self._btnIrr4   = ControlButton("No Irrigation")
        self._btnIrr4.value   = self.__btnIrr4Action
        

        # layout and structure 
        self.formset = [
            #('_controlFertMeth',  ('',' ')),
            ('_label1',  ('_buttonYes','_buttonNo'), ' '),
            
            ('=', ('\t', '_controlFertApp', '_buttonOneApp', '_buttonTwoApp', '_buttonThreeApp'), ' ' ),
            ( 
                ('\t\t', '\t\t', '\t\t', '_label1st',  ' ' ), (' ', '_label2nd',  ' ' ), (' ', '_label3rd',  ' ' )

            ),
            [
                 
                ('\t', '_slider1stDays', ' ' ), 
                ('\t', '_slider1stAmount', ' ' ), 
                ('\t', '_slider1stMat', ' ' ), 
                ('\t', '_slider1stApp', ' '), 

                '||',
                
                ( '_slider2ndDays', ' ' ), 
                ( '_slider2ndAmount', ' ' ), 
                ('_slider2ndMat', ' ' ), 
                ( '_slider2ndApp', ' '), 

                '||',

                ( '_slider3rdDays', '\t\t' ), 
                ( '_slider3rdAmount', '\t\t' ), 
                ('_slider3rdMat', '\t\t' ), 
                ( '_slider3rdApp', '\t\t'), 
                '||'


                ],
            '=',

            ('\n', ' ' ), 

            [    
                # ('_label3',  ('_buttonYesIrr','_buttonNoIrr'), ' '),
                # ('_label3',  ('_btnIrr1','_btnIrr2', '_btnIrr3','_btnIrr4'), ' '),
                ('_label3',  ('_btnIrr2', '_btnIrr3','_btnIrr4'), ' '),
                '=',

                # Saturation & Flooding

                # (' ', '_Irr1Lis', '||', '_Irr2Lis', '  ')
                (' ', '_Irr2Lis'),

                # ,

                '||',

                [
                    (' ', '_Irr3Txt3',  '_Irr3Txt1', '||', '_Irr3Txt2', '  '), ('_Irr3Lis', '  ')
                ],
                ' '
            ],

            ' ',
            (' ', '_buttonUpdate', ' '), ' '   
            ]
        
        self.__controlBoxIrr1()
        self.__controlBoxMat(self._materialList, self._slider1stMat)
        self.__controlBoxMat(self._materialList, self._slider2ndMat)
        self.__controlBoxMat(self._materialList, self._slider3rdMat)
        self.__controlBoxApp(self._applyList, self._slider1stApp)
        self.__controlBoxApp(self._applyList, self._slider2ndApp)
        self.__controlBoxApp(self._applyList, self._slider3rdApp)
        self.__hideInfo()
        self._irrMethod.hide()

    def __controlBoxIrr1(self):
         
        for i in range(len(self._irrList)):
            name = self._irrList[i][0]
            code = self._irrList[i][1]
            self._irrMethod.add_item(name, code)

        self._irrMethod.text = self._irrList[0][0]

    def __controlBoxMat(self, matList, controlBox):
         
        for i in range(len(matList)):
            name = matList[i][0]
            code = matList[i][1]
            controlBox.add_item(name, code)

        controlBox.text = matList[0][0]

    def __controlBoxApp(self, appList, controlBox):
         
        for i in range(len(appList)):
            name = appList[i][0]
            code = appList[i][1]
            controlBox.add_item(name, code)

        controlBox.text = appList[0][0]

    def __print_value(self):
        print(self._controlFertMeth.value)

    def __buttonOneAppAction(self):
        self._numFertAppBtn = 1
        self._label1st.show()
        self._slider1stDays.show()
        self._slider1stAmount.show()
        self._slider1stMat.show()
        self._slider1stApp.show()

        self._label2nd.hide()
        self._label3rd.hide()
        self._slider2ndDays.hide()
        self._slider2ndAmount.hide()
        self._slider2ndMat.hide()
        self._slider2ndApp.hide()
        self._slider3rdDays.hide()
        self._slider3rdAmount.hide()
        self._slider3rdMat.hide()
        self._slider3rdApp.hide()

    def __buttonTwoAppAction(self):
        self._numFertAppBtn = 2
        self._controlFertApp.show()
        self._buttonOneApp.show()
        self._buttonTwoApp.show()
        self._buttonThreeApp.show()
        self._label1st.show()
        self._label2nd.show()
        self._slider1stDays.show()
        self._slider1stAmount.show()
        self._slider1stMat.show()
        self._slider1stApp.show()
        self._label2nd.show()        
        self._slider2ndDays.show()
        self._slider2ndAmount.show()
        self._slider2ndMat.show()
        self._slider2ndApp.show()

        self._label3rd.hide()
        self._slider3rdDays.hide()
        self._slider3rdAmount.hide()
        self._slider3rdMat.hide()
        self._slider3rdApp.hide()

    def __buttonThreeAppAction(self):
        self._numFertAppBtn = 3
        self._controlFertApp.show()
        self._buttonOneApp.show()
        self._buttonTwoApp.show()
        self._buttonThreeApp.show()
        self._label1st.show()
        self._label2nd.show()
        self._label3rd.show()
        self._slider1stDays.show()
        self._slider1stAmount.show()
        self._slider1stMat.show()
        self._slider1stApp.show()
        self._slider2ndDays.show()
        self._slider2ndAmount.show()
        self._slider2ndMat.show()
        self._slider2ndApp.show()
        self._slider3rdDays.show()
        self._slider3rdAmount.show()
        self._slider3rdMat.show()
        self._slider3rdApp.show()

    def __buttonYesAction(self):
        self._needFert = 1
        self._controlFertApp.show()
        self._buttonOneApp.show()
        self._buttonTwoApp.show()
        self._buttonThreeApp.show()
  
    def __buttonNoAction(self):
        self._needFert = 0
        self.__hideInfo()

    def __hideInfo(self):
        self._controlFertApp.hide()
        self._buttonOneApp.hide()
        self._buttonTwoApp.hide()
        self._buttonThreeApp.hide()
        self._label1st.hide()
        self._label2nd.hide()
        self._label3rd.hide()
        self._slider1stDays.hide()
        self._slider1stAmount.hide()
        self._slider1stMat.hide()
        self._slider1stApp.hide()
        self._slider2ndDays.hide()
        self._slider2ndAmount.hide()
        self._slider2ndMat.hide()
        self._slider2ndApp.hide()
        self._slider3rdDays.hide()
        self._slider3rdAmount.hide()
        self._slider3rdMat.hide()
        self._slider3rdApp.hide()
        self._Irr3Txt1.hide()
        self._Irr3Txt2.hide()
        self._Irr3Txt3.hide()
        self._Irr1Lis.hide()
        self._Irr2Lis.hide()
        self._Irr3Lis.hide()

    def __buttonYesIrrAction(self):
        self._needIrr = 1
        self._irrMethod.show()
    
    def __buttonNoIrrAction(self):
        self._needIrr = 0
        self._irrMethod.hide()

    def getApplyFert(self):
        return self._needFert

    def getApplyNumFert(self):
        if self._needFert == 1:
            return self._numFertAppBtn
        else:
            return 0

    def getApplyIrr(self):
        return self._needIrr

    def getApplyIrrMeth(self):
        if self._needIrr == 1:
            return self._irrMethod.value
        else:
            return self._irrList[0][1]

    def __checkFertInputsValid(self):
        """Check if all inputs intered are valid before moving on"""
        pass

    def getFertInpList1(self):
        """The 1st fertilizer application - return a list of 4 elements in this order: 
            1. Days after planting, 2.Amount, 3.Fertilizer material, 4.Application Method"""

        return [self._slider1stDays.value, self._slider1stAmount.value,
                self._slider1stMat.value, self._slider1stApp.value]

    def getFertInpList2(self):
        """The 2nd fertilizer application - return a list of 4 elements in this order: 
            1. Days after planting, 2.Amount, 3.Fertilizer material, 4.Application Method"""

        return [self._slider2ndDays.value, self._slider2ndAmount.value,
                self._slider2ndMat.value, self._slider2ndApp.value]

    def getFertInpList3(self):
        """The 3rd fertilizer application - return a list of 4 elements in this order: 
            1. Days after planting, 2.Amount, 3.Fertilizer material, 4.Application Method"""

        return [self._slider3rdDays.value, self._slider3rdAmount.value,
                self._slider3rdMat.value, self._slider3rdApp.value]


    def getIrr1Values(self):
        
        for i in range(len(self._Irr1Lis.value)):
            tmp = self._Irr1Lis.value[i][1]
            self._Irr1Values[i] = int(tmp)
        return self._Irr1Values

    def getIrr2Values(self):
        
        for i in range(len(self._Irr2Lis.value)):
            tmp = self._Irr2Lis.value[i][1]
            self._Irr2Values[i] = int(tmp)
        return self._Irr2Values

    def getIrr3Values(self):
        res1 = []
        res2 = []
        for i in range(len(self._Irr3Lis.value)):
            tmp1 = self._Irr3Lis.value[i][1]
            try:
                res1.append(int(tmp1))
            except ValueError:
                res1.append(tmp1)

            tmp2 = self._Irr3Lis.value[i][2]
            try:
                res2.append(int(tmp2))
            except ValueError:
                res2.append(tmp2)
                       

        self._Irr3Values[0] = int(self._Irr3Txt1.value)    
        self._Irr3Values[1] = int(self._Irr3Txt2.value)    
        self._Irr3Values[2] = res1    
        self._Irr3Values[3] = res2    
        self._Irr3Values[4] = int(self._Irr3Txt3.value)    
        
        return self._Irr3Values

    def buttonUpdateAction(self):
        self._numFertApp = self.getApplyNumFert()
        self.allValues.append(self._numFertApp)     # 0: num of fertilizer

        self._fertInpList1 = self.getFertInpList1()
        self._fertInpList2 = self.getFertInpList2()
        self._fertInpList3 = self.getFertInpList3()
        self.allValues.append(self._fertInpList1)   # 1: Fert Appl 1 values
        self.allValues.append(self._fertInpList2)   # 2: Fert Appl 2 values
        self.allValues.append(self._fertInpList3)   # 3: Fert Appl 3 values

        self._irrMethodValue = self.getApplyIrrMeth()
        # self.allValues.append(self._irrMethodValue)
        self.allValues.append(self.getIrr1Values())   # 4: irr - Saturation of soil values
        self.allValues.append(self.getIrr2Values())   # 5: irr - Repeated Flooding
        self.allValues.append(self.getIrr3Values())   # 6: irr - Reported dates
        self.allValues.append(self._IrrMth)           # 7: irr - Method (0,1,2 or 3)
        
        self.__summary()

    def __summary(self):
        print("__SUMMARY___")
        i = 0
        for e in self.allValues:
            print(i, e)
            i+=1
        i = 0   


    def Setup2Results(self):
        """returns a list of the 5 input values"""

        return self.allValues

    def __btnIrr1Action(self):
        # self._IrrMth = 0
        # self._Irr1Lis.show()
        # self._Irr3Txt1.hide()
        # self._Irr3Txt2.hide()
        # self._Irr3Txt3.hide()
        # self._Irr2Lis.hide()
        # self._Irr3Lis.hide()
        pass

    def __btnIrr2Action(self):
        self._IrrMth = 1
        self._Irr2Lis.show()
        self._Irr3Txt1.hide()
        self._Irr3Txt2.hide()
        self._Irr3Txt3.hide()
        self._Irr1Lis.hide()
        self._Irr3Lis.hide()

    def __btnIrr3Action(self):
        self._IrrMth = 2
        self._Irr3Lis.show()
        self._Irr3Txt1.show()
        self._Irr3Txt2.show()
        self._Irr3Txt3.show()
        self._Irr1Lis.hide()
        self._Irr2Lis.hide()

    def __btnIrr4Action(self):
        self._IrrMth = 3
        self._Irr3Txt1.hide()
        self._Irr3Txt2.hide()
        self._Irr3Txt3.hide()
        self._Irr1Lis.hide()
        self._Irr2Lis.hide()
        self._Irr3Lis.hide()
