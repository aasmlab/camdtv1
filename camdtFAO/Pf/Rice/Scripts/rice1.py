title = 'CAMDT for Dynamic Cropping Calendar-Rice'

# from Tkinter import *
# import Tkinter #Tkinter is the PYthon interface to Tk, the GUI toolkit for Tcl/Tk

# try:
#     # Python2
#     import Tkinter as tk
# except ImportError:
#     # Python3
#     import tkinter as tk
from tkinter import *
import tkinter #Tkinter is the PYthon interface to Tk, the GUI toolkit for Tcl/Tk
import tkinter.filedialog
from tkinter.filedialog import askdirectory
from tkinter import Label
import Pmw #Pmw(Python megawidgets) are composite widgets written entirely in Python using Tkinter widgets as base classes
       #Pmw provide a convinent ways to add functionality to an appilcation without the need to writ ea lot of code (e.g., Combobox) 
import datetime    #to convert date to doy or vice versa 
import math
import subprocess  #to run executable
import shutil   #to remove a foler which is not empty
import os   #operating system
import numpy as np
import matplotlib.pyplot as plt  #to create plots
import fnmatch   # Unix filename pattern matching => to remove PILI0*.WTD
import os.path
from scipy.stats import rankdata #to make a rank to create yield exceedance curve
import pandas as pd
import csv
import calendar
from Create_WTH import create_WTH_main
import time

# function for setting the colors of the box plots pairs
def setBoxColors(bp):
    plt.setp(bp['boxes'][0], color='blue')
    plt.setp(bp['caps'][0], color='blue')
    plt.setp(bp['caps'][1], color='blue')
    plt.setp(bp['whiskers'][0], color='blue')
    plt.setp(bp['whiskers'][1], color='blue')
    plt.setp(bp['fliers'][0], color='blue')
    plt.setp(bp['fliers'][1], color='blue')
    plt.setp(bp['medians'][0], color='blue')

    plt.setp(bp['boxes'][1], color='red')
    plt.setp(bp['caps'][2], color='red')
    plt.setp(bp['caps'][3], color='red')
    plt.setp(bp['whiskers'][2], color='red')
    plt.setp(bp['whiskers'][3], color='red')
    plt.setp(bp['fliers'][1], color='red')
    plt.setp(bp['fliers'][1], color='red')
    plt.setp(bp['medians'][1], color='red')

def get_soil_IC(ID_SOIL):
    SOL_file=Wdir_path.replace("/","\\") + "\\PH.SOL" 
    #initialize
    depth_layer=[]
    ll_layer=[]
    ul_layer=[]
    n_layer=0
    soil_flag=0
    count=0
    fname = open(SOL_file,"r") #opens *.SOL
    for line in fname:
        if ID_SOIL in line:
            #soil_depth=line[33:36]
            soil_depth=line[34:38]  #PH.SOL
            sltx=line[25:29]  #PH.SOL
            soil_flag=1
        if soil_flag == 1:
            count=count+1
            if count >= 7:
                depth_layer.append(int(line[0:6]))
                ll_layer.append(float(line[13:18]))
                ul_layer.append(float(line[19:24]))
                n_layer=n_layer+1
                if int(line[3:6]) == int(soil_depth):
                    yield depth_layer  #list 
                    yield ll_layer
                    yield ul_layer
                    yield n_layer
                    yield sltx
                    fname.close()
                    break
def find_station_info(station_name):
    if station_name == 'SANJ':
        LAT = 12.35    
        LONG = 121.033
        ELEV = 3
        TAV = 28.3
        AMP = 2.1
    elif station_name == 'APAR':
        LAT = 18.359    
        LONG = 121.630
        ELEV = 3
        TAV = 27.3
        AMP = 5.0
    elif station_name == 'MALA':
        LAT = 8.151    
        LONG = 125.134
        ELEV = 627
        TAV = 24.2
        AMP = 1.6
    elif station_name == 'PALA':
        LAT = 9.740    
        LONG = 118.759
        ELEV = 16
        TAV = 27.9 
        AMP = 1.8
    elif station_name == 'TUGE':
        LAT = 17.637    
        LONG = 121.752
        ELEV = 62
        TAV = 26.9
        AMP = 5.8
    elif station_name == 'CLAR':
        LAT = 15.183   
        LONG = 120.533
        ELEV = 151
        TAV = 27.1
        AMP = 3.6
    else:
        LAT = -99    
        LONG = -99
        ELEV = -99
        TAV = -99
        AMP = -99
    return LAT, LONG, ELEV, TAV, AMP 

#=============================================
# check last observed data from *.WTD
def find_obs_date(WTD_fname):
    data1 = np.loadtxt(WTD_fname,skiprows=1)
    #convert numpy array to dataframe
    WTD_df = pd.DataFrame({'YEAR':data1[:,0].astype(int)//1000,    #python 3.6: / --> //
        'DOY':data1[:,0].astype(int)%1000,
        'SRAD':data1[:,1],
        'TMAX':data1[:,2],
        'TMIN':data1[:,3],
        'RAIN':data1[:,4]})
    WTD_last_date = int(repr(WTD_df.YEAR.values[-1]) + repr(WTD_df.DOY.values[-1]).zfill(3))
    del WTD_df
    return WTD_last_date
#=============================================
# check last observed data from *.WTD
def find_obs_years(WTD_fname, IC_date, hv_date): #note: IC_date and hv_date is integer in [YYYYDOY] 
    data1 = np.loadtxt(WTD_fname,skiprows=1)
    WTD_df = pd.DataFrame({'YEAR':data1[:,0].astype(int)//1000,    #python 3.6: / --> //
        'DOY':data1[:,0].astype(int)%1000,
        'SRAD':data1[:,1],
        'TMAX':data1[:,2],
        'TMIN':data1[:,3],
        'RAIN':data1[:,4]})
    year1 = WTD_df.YEAR[(WTD_df["DOY"] == IC_date%1000)].values[0]
    year2 = WTD_df.YEAR[(WTD_df["DOY"] == hv_date%1000)].values[-1]
    if IC_date%1000 < hv_date%1000:  #when IC_date and hv_date are in a same year
        sim_years = year2 - year1 + 1
    else:
        sim_years = year2 - year1  #when a growing period go beyond next year
    del WTD_df
    return sim_years, year1
#=============================================
#Determine pdate_1[YYYYDOY] and pdate_2[YYYYDOY]
def find_plt_date(start_year, lst, lst2, lst_frst, lst_frst2):
    #=============================================
    m_doys_list = [1,32,60,91,121,152,182,213,244,274,305,335]  #starting date of each month for regular years
    m_doye_list = [31,59,90,120,151,181,212,243,273,304,334,365] #ending date of each month for regular years
    m_doys_list2 = [1,32,61,92,122,153,183,214,245,275,306,336]  #starting date of each month for leap years
    m_doye_list2 = [31,60,91,121,152,182,213,244,274,305,335,366] #ending date of each month for leap years
    numday_list = [31,28,31,30,31,30,31,31,30,31,30,31]
    #======================================================================
    #==> (1)find the first planting month
    #check year 1 first
    p_index=[i for i in range(len(lst)) if lst[i]== 1]  #range (python 3):   #xrange (python 2):
    s_index=[i for i in range(len(lst_frst)) if lst_frst[i]== 1] 
    p_index2=[i for i in range(len(lst2)) if lst2[i]== 1]
    s_index2=[i for i in range(len(lst_frst2)) if lst_frst2[i]== 1] 
    # plt1_month = p_index[0] + 1
    if len(p_index) > 0:
        pdate_1 = int(start_year)*1000 + m_doys_list[p_index[0]]
        if calendar.isleap(int(start_year)):  pdate_1 = int(start_year)*1000 + m_doys_list2[p_index[0]]
        if len(s_index) > 0:
            frst_date1 = int(start_year)*1000 + m_doys_list[s_index[0]]
            if calendar.isleap(int(start_year)):  frst_date1 = int(start_year)*1000 + m_doys_list2[s_index[0]]
        else: #check year 2
            s_index2=[i for i in range(len(lst_frst2)) if lst_frst2[i]== 1]  #range (python 3):
            frst_date1 = (int(start_year)+1)*1000 + m_doys_list[s_index2[0]]
            if calendar.isleap(int(start_year)+1):  frst_date1 = (int(start_year)+1)*1000 + m_doys_list2[s_index2[0]]
    else: # len(p_index) == 0:  #If cannot find planting month in the year 1
        #plt1_month = p_index2[0] + 1
        pdate_1 = int(start_year)*1000 + m_doys_list[p_index2[0]]
        if calendar.isleap(int(start_year)):  pdate_1 = int(start_year)*1000 + m_doys_list2[p_index2[0]]
        if len(s_index) > 0:  #check year 1 first  => case 2
            frst_date1 = (int(start_year)-1)*1000 + m_doys_list[s_index[0]]
            if calendar.isleap(int(start_year)-1):  frst_date1 = (int(start_year)-1)*1000 + m_doys_list2[s_index[0]]
        else: #check year 2
            frst_date1 = int(start_year)*1000 + m_doys_list[s_index2[0]]
            if calendar.isleap(int(start_year)):  frst_date1 = int(start_year)*1000 + m_doys_list2[s_index2[0]]

    #==> (2)find the last planting month 
    p2_index=[i for i in range(len(lst2)) if lst2[i]== 1]   #check year 2 first
    # plt2_month = p2_index[-1]  #last element
    if len(p2_index) == 0:  #If cannot find planting month in the year 2, search year 1
        p2_index2=[i for i in range(len(lst)) if lst[i]== 1]
        #plt2_month = p2_index2[-1]
        pdate_2 = int(start_year)*1000 + m_doye_list[p2_index2[-1]]
        if calendar.isleap(int(start_year)):  pdate_2 = int(start_year)*1000 + m_doye_list2[p2_index2[-1]]
        end_date = 365  #int(start_year)*1000 + 365
        if calendar.isleap(int(start_year)):  end_date = 366 #int(start_year)*1000 + 366
        if (pdate_2%1000 + 180) > end_date:   #180 is arbitrary number to fully cover weather data until late maturity
            hv_date = (pdate_2//1000 + 1)*1000 + (pdate_2%1000 + 180) - end_date  
        else:
            hv_date = pdate_2 + 180
    else: #If can find planting month in the year 2
        if len(p_index) == 0:  #If cannot find planting month in the year 1, the first planting date is in year 2
            #case 2
            pdate_2 = int(start_year)*1000 + m_doye_list[p2_index[-1]]
            if calendar.isleap(int(start_year)):  pdate_2 = int(start_year)*1000 + m_doye_list2[p2_index[-1]]
            hv_date = pdate_2 + 180
        else: #case 3 => plating window covers both year 1 and year 2
            pdate_2 = (int(start_year) + 1)*1000 + m_doye_list[p2_index[-1]]
            if calendar.isleap(int(start_year) + 1):  pdate_2 = (int(start_year) + 1)*1000 + m_doye_list2[p2_index[-1]]
            hv_date = pdate_2 + 180
    #make a list of planting window months
    pwindow_m= []
    if len(p_index) > 0:
        for i in range(len(p_index)):
            pwindow_m.append(p_index[i] + 1)
        if len(p_index2) > 0:
            for i in range(len(p_index2)):
                pwindow_m.append(p_index2[i] + 1)
    else:
        if len(p_index2) > 0:
            for i in range(len(p_index2)):
                pwindow_m.append(p_index2[i] + 1)
        else:
            print ("error! - no planting window is found!")
            os.system('pause')
    #make a list of SCF window months
    fwindow_m= []
    if len(s_index) > 0:
        for i in range(len(s_index)):
            fwindow_m.append(s_index[i] + 1)
        if len(s_index2) > 0:
            for i in range(len(s_index2)):
                fwindow_m.append(s_index2[i] + 1)
    else:
        if len(s_index2) > 0:
            for i in range(len(s_index2)):
                fwindow_m.append(s_index2[i]+ 1)
        else:
            print ("error! - no SCF window is found!")
            os.system('pause')
    return pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m
#End of determining pdate_1[YYYYDOY] and pdate_2[YYYYDOY]
#=============================================
# class Checkbar(Frame):
#     def __init__(self, parent=None, picks=[], side=LEFT, anchor=W):
#         Frame.__init__(self, parent)
#         self.vars = []
#         for pick in picks:
#             var = IntVar()
#             chk = Checkbutton(self, text=pick, variable=var)
#             chk.pack(side=side, anchor=anchor, expand=YES)
#             self.vars.append(var)
#     def state(self):
#         return map((lambda var: var.get()), self.vars)

class Checkbar(Frame):
    def __init__(self, parent=None, picks=[], side=LEFT, anchor=W):
        Frame.__init__(self, parent)
        self.vars = []
        for pick in picks:
            var = IntVar()
            if pick == 'J':
                chk = Checkbutton(self, text=pick, variable=var, state=DISABLED)
            else:
                chk = Checkbutton(self, text=pick, variable=var)
            chk.pack(side=side, anchor=anchor, expand=YES)
            self.vars.append(var)
    def state(self):
        return map((lambda var: var.get()), self.vars)
   #map() function returns a list of the results
   #  after applying the given function to each item of a given iterable (list, tuple etc.)
   #syntax: map(fun, iter)
   #Syntax: lambda arguments : expression
   
    def clear(self):
        ##print(self.vars)
        for i in range(12):
            self.vars[i].set(0)
        ##print(self.vars[0].get())
        #return map((lambda var: var.set(0)), self.vars)
    
class CAMDT:     
    sf = frame = None  #initially, no frame
    def __init__(self, parent):  #initializing an instance
        global Fbutton1, IRbutton
        global Cbutton
        global var
        global floodVar1,floodVar2,floodVar3 #constant flooding depth or not :EJ(8/2/2016)
        global Rbutton2,Rbutton3,cul_Rbutton,Fbutton1,IRbutton
        # Create and pack a NoteBook.
        notebook = Pmw.NoteBook(parent)
        notebook.pack(fill = 'both', expand = 1, padx = 10, pady = 10)
#=========================First page for "DSSAT baseline setup - I "
        page1 = notebook.add('DSSAT setup 1')
        notebook.tab('DSSAT setup 1').focus_set()
       # 1) ADD SCROLLED FRAME
        sf_p1 = Pmw.ScrolledFrame(page1,usehullsize=1, hull_width=700, hull_height=220)
        sf_p1.pack(padx=5, pady=3, fill='both', expand=YES)
        sf_p1.pack_propagate(0) # don't shrink

        # set up "weather station"
        group11 = Pmw.Group(sf_p1.interior(), tag_text = 'Weather Station & Planting Year',tag_font = Pmw.logicalfont('Helvetica', 0.5))
        group11.pack(fill = 'x', side=TOP, padx = 10, pady = 2)
         #assign frame 1
        fm11=Frame(group11.interior())      
        Wstation_list = ("APAR8830(Aparri,R-II)","TUGE8830(Tuguegarao,R-II)","PALA8830(Puerto Princesa, R-IV B)","SANJ8830(San Jose, R-IV B)",
                         "MALA8830(Malaybalay, R-X)","CLAR9721(Clark Intl,R-III)",)
        self.Wstation = Pmw.ComboBox(fm11, label_text='Weather Station:', labelpos='wn',
                        listbox_width=20, dropdown=1,
                     #   selectioncommand = self.AvailableYears,
                        scrolledlist_items=Wstation_list,
                        entryfield_entry_state=DISABLED)
        self.Wstation.pack(fill = 'x', side=LEFT,padx = 10, pady = 5)
        self.Wstation.selectitem(Wstation_list[3]) 
        fm11.pack(fill='x', expand=1,padx=10,side=LEFT)

        fm12=Frame(group11.interior())
        self.startyear = Pmw.EntryField(fm12, labelpos = 'w',
            label_text = 'Year of the first planting date:',value = '2018',
            validate = {'validator': 'numeric', 'min' : 1970, 'max' : 2050, 'minstrict' : 0})
        self.startyear.pack(fill = 'x', side=LEFT, padx = 10, pady = 5)
        fm12.pack(fill='x', expand=1,padx=10,side=LEFT)

        # fm13=Frame(group11.interior())
        # self.frstyear = Pmw.EntryField(fm13, labelpos = 'w',
        #     label_text = 'Year of the first monthly SCF:',value = '2018',
        #     validate = {'validator': 'numeric', 'min' : 1970, 'max' : 2050, 'minstrict' : 0})
        # self.frstyear.pack(fill = 'x', side=LEFT, padx = 10, pady = 5)
        # fm13.pack(fill='x', expand=1,padx=10,side=LEFT)


        # set up Monthly Seasonal climate forecats in tercile probability format
        group12 = Pmw.Group(sf_p1.interior(), tag_text = 'Seasonal Climate Forecast',tag_font = Pmw.logicalfont('Helvetica', 0.5))
        group12.pack(fill = 'x', side=TOP, padx = 10, pady = 2)

        fm_2=Frame(group12.interior()) 
        Label(fm_2, text='Planting:').pack(side=LEFT) #, padx = 1, pady = 5) 
        Label(fm_2, text='(YR1)').pack(side=LEFT, padx = 3, pady = 5) 
        self.plt_button1 = Checkbar(fm_2, ['J','F1', 'M1', 'A1','M1','J1','J1','A1','S1','O1','N1','D1'])
        self.plt_button2 = Checkbar(fm_2, ['J2','F2', 'M2', 'A2','M2','J2','J2','A2','S2','O2','N2','D2'])
        self.plt_button1.pack(side=LEFT,  fill=X)
        Label(fm_2, text='(YR2)').pack(side=LEFT, padx = 2, pady = 5) 
        self.plt_button2.pack(side=LEFT,  fill=X)
        self.reset_button2=tkinter.Button(fm_2, text ="RESET",command=self.clear_all_button2).pack(side=LEFT)
        fm_2.pack(fill='x', expand=1,padx=10,side=TOP) 
    #   lng.config(relief=GROOVE, bd=2)
    
        fm_1=Frame(group12.interior()) 
        Label(fm_1, text='        SCF:').pack(side=LEFT, padx = 2, pady = 5) 
        Label(fm_1, text='(YR1)').pack(side=LEFT)
        self.scf_button1 = Checkbar(fm_1, ['J1','F1', 'M1', 'A1','M1','J1','J1','A1','S1','O1','N1','D1'])
        self.scf_button2 = Checkbar(fm_1, ['J2','F2', 'M2', 'A2','M2','J2','J2','A2','S2','O2','N2','D2'])
        self.scf_button1.pack(side=LEFT,  fill=X)
        Label(fm_1, text='(YR2)').pack(side=LEFT, padx = 2, pady = 5) 
        self.scf_button2.pack(side=LEFT)
        self.reset_button1=tkinter.Button(fm_1, text ="RESET",command=self.clear_all_button1).pack(side=LEFT)
        fm_1.pack(fill='x', expand=1,padx=10,side=TOP)  

 #-2nd frame - BN
        fm14=Frame(group12.interior())
        Label(fm14, text='        BN:').pack(side=LEFT)
        Label(fm14, text='(YR1)').pack(side=LEFT)
        self.BN_11 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_11.pack( side=LEFT, padx = 1, pady = 5)  #Jan
        self.BN_12 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_12.pack( side=LEFT, padx = 1, pady = 5) #Feb
        self.BN_13 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_13.pack( side=LEFT, padx = 1, pady = 5) #Mar
        self.BN_14 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_14.pack( side=LEFT, padx = 1, pady = 5) #Apr
        self.BN_15 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_15.pack( side=LEFT, padx = 1, pady = 5) #May
        self.BN_16 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_16.pack( side=LEFT, padx = 1, pady = 5) #June
        self.BN_17 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_17.pack( side=LEFT) #, padx = 5, pady = 5) #July
        self.BN_18 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_18.pack( side=LEFT) #, padx = 5, pady = 5) #Aug
        self.BN_19 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_19.pack( side=LEFT) #, padx = 5, pady = 5) #Sep
        self.BN_110 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_110.pack( side=LEFT) #, padx = 5, pady = 5) #Oct
        self.BN_111 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_111.pack( side=LEFT) #, padx = 5, pady = 5) #Nov
        self.BN_112 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_112.pack( side=LEFT) #, padx = 5, pady = 5) #Dec
        Label(fm14, text='   (YR2)').pack(side=LEFT) 
        self.BN_21 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_21.pack( side=LEFT, padx = 1, pady = 5)    #Jan
        self.BN_22 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_22.pack( side=LEFT, padx = 1, pady = 5)    #Feb
        self.BN_23 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_23.pack( side=LEFT, padx = 1, pady = 5)    #Mar
        self.BN_24 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_24.pack( side=LEFT, padx = 1, pady = 5)   #Apr
        self.BN_25 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_25.pack( side=LEFT, padx = 1, pady = 5)     #May
        self.BN_26 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_26.pack( side=LEFT, padx = 1, pady = 5)    #June
        self.BN_27 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_27.pack( side=LEFT) #, padx = 5, pady = 5)    #July
        self.BN_28 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_28.pack( side=LEFT) #, padx = 5, pady = 5)    #Aug
        self.BN_29 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_29.pack( side=LEFT) #, padx = 5, pady = 5)    #Sep
        self.BN_210 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_210.pack( side=LEFT,padx = 2, pady = 5)  #Oct
        self.BN_211 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_211.pack( side=LEFT)  #Nov
        self.BN_212 = Pmw.EntryField(fm14, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.BN_212.pack( side=LEFT)   #Dec
        self.reset_BN_buttion=tkinter.Button(fm14, text ="RESET",command=self.reset_BN).pack(side=LEFT,padx = 4, pady = 5)
        fm14.pack(fill='x', expand=1,padx=10,side=TOP)
 #-3rd frame - AN
        fm15=Frame(group12.interior())
        Label(fm15, text='        AN:').pack(side=LEFT)
        Label(fm15, text='(YR1)').pack(side=LEFT)
        self.AN_11 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_11.pack( side=LEFT, padx = 1, pady = 5)  #Jan
        self.AN_12 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_12.pack( side=LEFT, padx = 1, pady = 5)  #Feb
        self.AN_13 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_13.pack( side=LEFT, padx = 1, pady = 5)  #Mar
        self.AN_14 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_14.pack( side=LEFT, padx = 1, pady = 5)  #Apr
        self.AN_15 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_15.pack( side=LEFT, padx = 1, pady = 5)  #May
        self.AN_16 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_16.pack( side=LEFT, padx = 1, pady = 5)   #June
        self.AN_17 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_17.pack( side=LEFT) #, padx = 5, pady = 5)  #July
        self.AN_18 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_18.pack( side=LEFT) #, padx = 5, pady = 5)  #Aug
        self.AN_19 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_19.pack( side=LEFT) #, padx = 5, pady = 5)  #Sep
        self.AN_110 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_110.pack( side=LEFT) #, padx = 5, pady = 5) #Oct
        self.AN_111 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_111.pack( side=LEFT) #, padx = 5, pady = 5)  #Nov
        self.AN_112 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_112.pack( side=LEFT) #, padx = 5, pady = 5)  #Dec
        Label(fm15, text='   (YR2)').pack(side=LEFT)  
        self.AN_21 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_21.pack( side=LEFT, padx = 1, pady = 5)     #Jan
        self.AN_22 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_22.pack( side=LEFT, padx = 1, pady = 5)   #Feb
        self.AN_23 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_23.pack( side=LEFT, padx = 1, pady = 5)    #Mar
        self.AN_24 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_24.pack( side=LEFT, padx = 1, pady = 5)    #Apr
        self.AN_25 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_25.pack( side=LEFT, padx = 1, pady = 5)    #May
        self.AN_26 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_26.pack( side=LEFT, padx = 1, pady = 5)    #June
        self.AN_27 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_27.pack( side=LEFT) #, padx = 5, pady = 5)   #July
        self.AN_28 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_28.pack( side=LEFT) #, padx = 5, pady = 5)   #Aug
        self.AN_29 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_29.pack( side=LEFT) #, padx = 5, pady = 5)   #Sep
        self.AN_210 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_210.pack( side=LEFT,padx = 2, pady = 5)   #Oct
        self.AN_211 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_211.pack( side=LEFT)   #Nov
        self.AN_212 = Pmw.EntryField(fm15, labelpos = 'w',entry_width=5, value = '33', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.AN_212.pack( side=LEFT)    #Dec
        self.reset_AN_buttion=tkinter.Button(fm15, text ="RESET",command=self.reset_AN).pack(side=LEFT,padx = 4, pady = 5)
        fm15.pack(fill='x', expand=1,padx=10,side=TOP)
         #-4th  frame - NN
        fm16=Frame(group12.interior())
        Label(fm16, text='        NN:').pack(side=LEFT)
        Label(fm16, text='(YR1)').pack(side=LEFT)
        # self.NN_label1 = Label(fm16, text='     NN:') #,padx=5, pady=5)
        # self.NN_label1.grid(row=0,column=0, sticky=W) #rowspan=1,columnspan=1) 
        self.NN_label2 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label2.pack(side=LEFT, padx = 3, pady = 5)  
        self.NN_label3 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label3.pack(side=LEFT, padx = 3, pady = 5)  
        self.NN_label4 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label4.pack(side=LEFT, padx = 3, pady = 5)  
        self.NN_label5 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label5.pack(side=LEFT, padx = 3, pady = 5)  
        self.NN_label6 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label6.pack(side=LEFT, padx = 3, pady = 5)  
        self.NN_label7 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label7.pack(side=LEFT, padx = 3, pady = 5)  
        self.NN_label8 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label8.pack(side=LEFT, padx = 2, pady = 5)  
        self.NN_label9 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label9.pack(side=LEFT, padx = 2, pady = 5)  
        self.NN_label10 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label10.pack(side=LEFT, padx = 2, pady = 5)  
        self.NN_label11 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label11.pack(side=LEFT, padx = 2, pady = 5)  
        self.NN_label12 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label12.pack(side=LEFT, padx = 2, pady = 5)  
        self.NN_label13 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label13.pack(side=LEFT, padx = 2, pady = 5)  
        Label(fm16, text='(YR2)').pack(side=LEFT, padx = 5, pady = 5)  
        self.NN_label14 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label14.pack(side=LEFT, padx = 3, pady = 5)  
        self.NN_label15 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label15.pack(side=LEFT, padx = 3, pady = 5)  
        self.NN_label16 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label16.pack(side=LEFT, padx = 3, pady = 5)  
        self.NN_label17 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label17.pack(side=LEFT, padx = 3, pady = 5)  
        self.NN_label18 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label18.pack(side=LEFT, padx = 3, pady = 5)  
        self.NN_label19 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label19.pack(side=LEFT, padx = 3, pady = 5)  
        self.NN_label20 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label20.pack(side=LEFT, padx = 2, pady = 5)  
        self.NN_label21 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label21.pack(side=LEFT, padx = 2, pady = 5)  
        self.NN_label22 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label22.pack(side=LEFT, padx = 2, pady = 5)  
        self.NN_label23 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label23.pack(side=LEFT, padx = 3, pady = 5)  
        self.NN_label24 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label24.pack(side=LEFT, padx = 3, pady = 5)  
        self.NN_label25 = Label(fm16, text='34',relief='sunken',padx=10, pady=5)
        self.NN_label25.pack(side=LEFT, padx = 3, pady = 5) 
        fm16.pack(fill='x', expand=1,padx=10,side=TOP)

       

        fm_3=Frame(group12.interior()) 
        self.param_button1=tkinter.Button(fm_3,
                text = 'Click to compute NN',
                command=self.allstates,bg='gray70').pack(side=TOP,anchor=N, padx = 5, pady = 2)
        fm_3.pack(fill='x', expand=1,padx=10,side=TOP)

        fm_4=Frame(sf_p1.interior()) 
        # #Assign a group for planting method
        # group32 = Pmw.Group(fm_4, tag_text = 'Planting method',tag_font = Pmw.logicalfont('Helvetica', 0.5))
        # group32.pack(fill='both', expand=1,side=TOP, padx = 10, pady = 2)  
        # # Radio button to select "Planting method"
        # Rbutton2 = tkinter.IntVar()
        # plt_option=[('Dry seed', 0), ('Transplanting', 1)]
        # for text, value in plt_option:
        #     Radiobutton(group32.interior(), text=text, value=value, variable=Rbutton2).pack(side=LEFT,expand=YES)
        # Rbutton2.set(1)  #By default- Transplanting

        # select maize cultivar and set planting details (for now only planting density)
        group13 = Pmw.Group(fm_4, tag_text = 'Planting Details (Rice)',tag_font = Pmw.logicalfont('Helvetica', 0.5))
        group13.pack(fill='both', expand=1, side=LEFT, padx = 10, pady = 5)    
                #Assign a group for planting method
        group32 = Pmw.Group(group13.interior(), tag_text = 'Planting method',tag_font = Pmw.logicalfont('Helvetica', 0.5))
        group32.pack(fill='both', expand=1,side=TOP, padx = 10, pady = 2)  
        # Radio button to select "Planting method"
        Rbutton2 = tkinter.IntVar()
        plt_option=[('direct seeding', 0), ('Transplanting', 1)]
        for text, value in plt_option:
            Radiobutton(group32.interior(), text=text, value=value, variable=Rbutton2).pack(side=LEFT,expand=YES)
        Rbutton2.set(1)  #By default- Transplanting

        cul_list = ("PHPS01 PSB Rc82","PHME01 Mestiso 20","PHPS02 NSIC Rc160","PHPS03 PSB Rc18-CMU",
                    "PHPS04 PSB Rc18-UPLB", "PHPS05 PSB Rc18-CLSU", "PHPS06 NSIC-Rc222-CMU","PHPS07 NSIC-Rc222-UPLB",
                    "PHPS08 NSIC-Rc222-CLSU","PHME02 Mestiso 19-CMU","PHME03 Mestiso 19-UPLB","PHME04 Mestiso 19-CLSU",
                    "PHPS09 PSB Rc82-UPLB ","PHPS10 NSIC Rc238-CMU","PHPS11 NSIC Rc302-UPLB","PHPS12 NSIC Rc216-CLSU",
                    "PHPS13 NSIC Rc298-CLSU")	
        self.cul_type = Pmw.ComboBox(group13.interior(), label_text='Cultivar type:', labelpos='wn',
                        listbox_width=15, dropdown=1,
                        scrolledlist_items=cul_list,
                        entryfield_entry_state=DISABLED)
        self.cul_type.pack(fill = 'x',side=TOP, padx = 10, pady = 2)
        self.cul_type.selectitem(cul_list[0])
        #-planting distribution
        pdist_list = ("Hills","Rows","Broadcast")
        self.plt_dist = Pmw.ComboBox(group13.interior(), label_text='Planting distribution:', labelpos='wn',
                        listbox_width=15, dropdown=1,
                        scrolledlist_items=pdist_list,
                        entryfield_entry_state=DISABLED)
        self.plt_dist.pack(fill = 'x', padx = 10, pady = 2)
        self.plt_dist.selectitem(pdist_list[1])
        #-planting population at seedling (plant/m2)
        self.ppop_seed=Pmw.EntryField(group13.interior(), labelpos='w', label_text='Plt population at seedling(plt/m2):',
            value = '75',validate = {'validator': 'real', 'min' : 1, 'max' : 100, 'minstrict' : 0})
        self.ppop_seed.pack(fill='x', side=TOP,expand=1,padx=10, pady = 0.5)
        self.ppop_emer=Pmw.EntryField(group13.interior(), labelpos='w', label_text='Plt population at emergence(plt/m2):',
            value = '25',validate = {'validator': 'real', 'min' : 1, 'max' : 80, 'minstrict' : 0})
        self.ppop_emer.pack(fill='x', side=TOP,expand=1,padx=10, pady = 0.5)
        self.row_space=Pmw.EntryField(group13.interior(), labelpos='w', label_text='Planting row spacing(cm):',
            value = '20',validate = {'validator': 'real', 'min' : 1, 'max' : 80, 'minstrict' : 0})
        self.row_space.pack(fill='x', side=TOP,expand=1,padx=10, pady = 0.5)
        self.row_dir=Pmw.EntryField(group13.interior(), labelpos='w', label_text='Row direction(deg from North):',
            value = '0',validate = {'validator': 'real', 'min' : 0, 'max' : 360, 'minstrict' : 0})
        self.row_dir.pack(fill='x', side=TOP,expand=1,padx=10, pady = 0.5)
        self.plt_depth=Pmw.EntryField(group13.interior(), labelpos='w', label_text='Planting depth(cm):',
            value = '2',validate = {'validator': 'real', 'min' : 1, 'max' : 50, 'minstrict' : 0})
        self.plt_depth.pack(fill='x', side=TOP,expand=1,padx=10, pady = 0.5)
        #aline labels
        entries = (self.cul_type, self.plt_dist, self.ppop_seed, self.ppop_emer, self.row_space, self.row_dir, self.plt_depth)
        Pmw.alignlabels(entries)    

        # select soil type and set initial soil condition
        group14 = Pmw.Group(fm_4, tag_text = 'Soil Condition',tag_font = Pmw.logicalfont('Helvetica', 0.5))
        group14.pack(fill='both', expand=1, side=LEFT, padx = 10, pady = 5)  
        #soil types
        soil_list = ("PH00000001(SICL)-QUINGUA DEEP","PHS0000001(SICL)-QUINGUA  SHALLOW","PH00000002(C)-CANDABA DEEP","PHS0000002(C)-CANDABA SHALLOW",
                    "PH00000003(SICL)-MAAPAG  DEEP","PHS0000003(SICL)-MAAPAG SHALLOW","PH00000004(C)-BABUYAN DEEP","PHS0000004(C)-BABUYAN SHALLOW",
                    "PH00000005(C)-TORAN  DEEP","PHS0000005(C)-TORAN SHALLOW","PH00000006(SICL)-SAN MANUEL  DEEP","PHS0000006(SICL)-SAN MANUEL  SHALLOW",
                    "PH00000007(SICL)-SAN MANUEL  DEEP","PHS0000007(SICL)-SAN MANUEL  SHALLOW", "PH00000008(C)-FARAON  DEEP", 
                    "PH00000009(C)-LA CASTELLANA DEEP  ","PHS0000009(C)-LA CASTELLANA  SHALLOW")
        self.soil_type = Pmw.ComboBox(group14.interior(), label_text='Soil type:', labelpos='wn',
                        listbox_width=35, dropdown=1,
                        scrolledlist_items=soil_list,
                        entryfield_entry_state=DISABLED)
        self.soil_type.pack(fill = 'x',side=TOP, padx = 2, pady = 2)
        self.soil_type.selectitem(soil_list[0]) 

        # set up "initial H2O condition"
        wetness_list = ("0.3(30% of AWC)","0.5(50% of AWC)","0.7(70% of AWC)","1.0(100% of AWC)")
        self.wet_soil = Pmw.ComboBox(group14.interior(), label_text='Initial soil H20:', labelpos='wn',
                        listbox_width=15, dropdown=1,
                        scrolledlist_items=wetness_list,
                        entryfield_entry_state=DISABLED)
        self.wet_soil.pack(fill = 'x',side=TOP, padx = 2, pady = 2)
        self.wet_soil.selectitem(wetness_list[2]) 

        # set up "initial NO3 condition"
        NO3_list = ("High(63 N kg/ha)","Low(25 N kg/ha)")
        self.NO3_soil = Pmw.ComboBox(group14.interior(), label_text='Initial soil NO3:', labelpos='wn',
                        listbox_width=15, dropdown=1,
                        scrolledlist_items=NO3_list,
                        entryfield_entry_state=DISABLED)
        self.NO3_soil.pack(fill = 'x',side=TOP, padx = 2, pady = 2)
        self.NO3_soil.selectitem(NO3_list[0]) 
        #aline labels
        entries = (self.soil_type, self.wet_soil, self.NO3_soil)
        Pmw.alignlabels(entries)    
        fm_4.pack(fill='x', expand=1,padx=10,side=TOP)
#=========================2nd page for "DSSAT baseline setup - II "
        page2 = notebook.add('DSSAT setup 2')
        notebook.tab('DSSAT setup 2').focus_set()
       # 1) ADD SCROLLED FRAME
        sf_p2 = Pmw.ScrolledFrame(page2) #,usehullsize=1, hull_width=700, hull_height=220)
        sf_p2.pack(padx=5, pady=3, fill='both', expand=YES)

        #assign frame 1
        fm21=Frame(sf_p2.interior())
        group21 = Pmw.Group(fm21, tag_text = 'Fertilization application',tag_font = Pmw.logicalfont('Helvetica', 0.5))
        group21.pack(fill='both', expand=1, side=TOP, padx = 10, pady = 5)    
      
        Fbutton1 = tkinter.IntVar()
        Frt_option=[('Fertilization', 1), ('No Fertilization', 0)]
        for text, value in Frt_option:
            Radiobutton(group21.interior(), text=text, command = self.empty_fert_label, value=value, variable=Fbutton1).pack(side=LEFT,expand=YES)
        Fbutton1.set(0)  #By default- no Fertilization
        # Create button to launch the dialog 
        frt_button=tkinter.Button(fm21,
                text = 'Click to add more details for fertilizer',
                command = self.getFertInput,bg='gray70').pack(side=TOP,anchor=N)

        # Create the "fertilizer" contents of the page.
        group22 = Pmw.Group(fm21, tag_text = 'Fertilizer application',tag_font = Pmw.logicalfont('Helvetica', 0.5))
        group22.pack(fill = 'x', side=TOP,expand=1, padx = 2, pady = 5)
        self.nfertilizer = Pmw.EntryField(group22.interior(), labelpos = 'w',
            label_text = 'Number of fertilizer applications? ',
            validate = {'validator': 'numeric', 'min' : 1, 'max' : 3, 'minstrict' : 0})
        self.nfertilizer.pack( side=TOP, anchor=W,padx = 5, pady = 5)  
        frame_frt =Frame(group22.interior())
        label000 = Label(frame_frt, text='No. application', padx=5, pady=5)
        label001 = Label(frame_frt, text='Days after planting',padx=5, pady=5)
        label002 = Label(frame_frt, text='Amount (N, kg/ha)', padx=5, pady=5)
        label003 = Label(frame_frt, text='Fertilizer material',padx=5, pady=5)
        label004 = Label(frame_frt, text='Application method',padx=5, pady=5)
        self.label005 = Label(frame_frt, text='1st:',padx=5, pady=5)
        self.label006 = Label(frame_frt, text='N/A',relief='sunken',width=13)  
        self.label007 = Label(frame_frt, text='N/A',relief='sunken',width=15) 
        self.label008 = Label(frame_frt, text='N/A',relief='sunken',width=13) 
        self.label009 = Label(frame_frt, text='N/A',relief='sunken',width=25) 
        self.label010 = Label(frame_frt, text='2nd:',padx=5, pady=5) 
        self.label011 = Label(frame_frt, text='N/A',relief='sunken',width=13) 
        self.label012 = Label(frame_frt, text='N/A',relief='sunken',width=15) 
        self.label013 = Label(frame_frt, text='N/A',relief='sunken',width=13) 
        self.label014 = Label(frame_frt, text='N/A',relief='sunken',width=25)
        self.label015 = Label(frame_frt, text='3rd:',padx=5, pady=5) 
        self.label016 = Label(frame_frt, text='N/A',relief='sunken',width=13) 
        self.label017 = Label(frame_frt, text='N/A',relief='sunken',width=15) 
        self.label018 = Label(frame_frt, text='N/A',relief='sunken',width=13) 
        self.label019 = Label(frame_frt, text='N/A',relief='sunken',width=25)        
        #aline  empty label
        entries = (label000, label001, label002, label003, label004)
        i=0
        for entry in entries:
            entry.grid(row=0,column=i) #, sticky=W) 
            i=i+1
        entries = (self.label005, self.label006, self.label007, self.label008, self.label009)
        i=0
        for entry in entries:
            entry.grid(row=1,column=i) #, sticky=W) 
            i=i+1
        entries = (self.label010, self.label011, self.label012, self.label013, self.label014)
        i=0
        for entry in entries:
            entry.grid(row=2,column=i) #, sticky=W) 
            i=i+1
        entries = (self.label015, self.label016, self.label017, self.label018, self.label019)
        i=0
        for entry in entries:
            entry.grid(row=3,column=i) #, sticky=W) 
            i=i+1
        frame_frt.pack(fill = 'x', expand = 1,side=TOP)
        fm21.pack(fill = 'both', expand = 1,side=TOP)
#=========IRRIGATION==================================================
        #assign frame 2
        fm22=Frame(sf_p2.interior())
        # Radio button to select "irrigation method"
        group23 = Pmw.Group(fm22, tag_text = 'Irrigation',tag_font = Pmw.logicalfont('Helvetica', 0.5))
        group23.pack(fill='x', expand=1,side=TOP, padx = 10, pady = 5)          

        IRbutton = tkinter.IntVar()
        #IR_option=[('Automatic when required(soil saturation)', 0),('Automatic(periodic flooding)', 1), ('On Reported dates', 2),('No Irrigation', 3)]
        IR_option=[('Automatic(periodic flooding)', 1), ('On Reported dates', 2),('No Irrigation', 3)]
        
        for text, value in IR_option:
            Radiobutton(group23.interior(), text=text, value=value, variable=IRbutton).pack(side=LEFT,expand=YES)
        IRbutton.set(1)  #By default- no irrigation
        
        #Create button to launch the dialog 
        Irr_button=tkinter.Button(fm22,
                text = 'Click to add more details for irrigation',
                command = self.getIrrInput,bg='gray70').pack(side=TOP,anchor=N)
        
        # set up Irrigation details
#         # Create the "Automatic irrigation" contents of the page.
#         group44 = Pmw.Group(fm22, tag_text = 'Irrigation (Automatic - saturation of soil)',tag_font = Pmw.logicalfont('Helvetica', 0.5))
#         group44.pack(fill = 'x', side=TOP,expand=1, padx = 2, pady = 5)
#         self.label401 = Label(group44.interior(), text='Management soil depth(cm):',anchor=W, padx=10)
#         self.label401.pack(side=LEFT, padx = 3, pady = 5) 
#         self.label402 = Label(group44.interior(), text='N/A',relief='sunken', padx=5)
#         self.label402.pack(side=LEFT, padx = 3, pady = 5) 
#         self.label403 = Label(group44.interior(), text='Threshold(% of max available):',anchor=W,padx=10)
#         self.label403.pack(side=LEFT, padx = 3, pady = 5) 
#         self.label404 = Label(group44.interior(), text='N/A',relief='sunken',padx=5)
#         self.label404.pack(side=LEFT, padx = 3, pady = 5) 
# ##        self.label405 = Label(group44.interior(), text='End point(% of max available):',anchor=W, padx=10)
# ##        self.label405.grid(row=1,column=0, sticky=W)
# ##        self.label406 = Label(group44.interior(), text='N/A',relief='sunken',padx=5)
# ##        self.label406.grid(row=1,column=1, sticky=W)
#         self.label407 = Label(group44.interior(), text='Efficiency fraction:',anchor=W,padx=10)
#         self.label407.pack(side=LEFT, padx = 3, pady = 5)
#         self.label408 = Label(group44.interior(), text='N/A',relief='sunken',padx=5)
#         self.label408.pack(side=LEFT, padx = 3, pady = 5)

        #automatic (periodic) irrigation for every n days  **** <<=====================================
        group43 = Pmw.Group(fm22, tag_text = 'Irrigation (Automatic - repeated flooding)',tag_font = Pmw.logicalfont('Helvetica', 0.5))
        group43.pack(fill = 'x', side=TOP,expand=1, padx = 2, pady = 5)
        self.label417 = Label(group43.interior(), text='Puddling date(DBP):',padx=5)
        self.label417.pack(side=LEFT, padx = 3, pady = 5) 
        self.label418= Label(group43.interior(), text='N/A',relief='sunken',width=13)  
        self.label418.pack(side=LEFT, padx = 3, pady = 5) 
        self.label419 = Label(group43.interior(), text='First Irrg dage (DAP):',padx=5)
        self.label419.pack(side=LEFT, padx = 3, pady = 5) 
        self.label420= Label(group43.interior(), text='N/A',relief='sunken',width=13)  
        self.label420.pack(side=LEFT, padx = 3, pady = 5) 
        self.label415 = Label(group43.interior(), text='Bund height (mm):',anchor=W, padx=10)
        self.label415.pack(side=LEFT, padx = 3, pady = 5) 
        self.label416 = Label(group43.interior(), text='N/A',relief='sunken', padx=5)
        self.label416.pack(side=LEFT, padx = 3, pady = 5) 
        self.label409 = Label(group43.interior(), text='Flooding depth (mm):',anchor=W, padx=10)
        self.label409.pack(side=LEFT, padx = 3, pady = 5) 
        self.label410 = Label(group43.interior(), text='N/A',relief='sunken', padx=5)
        self.label410.pack(side=LEFT, padx = 3, pady = 5) 
        self.label411 = Label(group43.interior(), text='Interval (every "n" days):',anchor=W,padx=10)
        self.label411.pack(side=LEFT, padx = 3, pady = 5) 
        self.label412 = Label(group43.interior(), text='N/A',relief='sunken',padx=5)
        self.label412.pack(side=LEFT, padx = 3, pady = 5) 
        self.label413 = Label(group43.interior(), text='End of applicaiton (DAP):',anchor=W,padx=10)
        self.label413.pack(side=LEFT, padx = 3, pady = 5) 
        self.label414 = Label(group43.interior(), text='N/A',relief='sunken',padx=5)
        self.label414.pack(side=LEFT, padx = 3, pady = 5) 
         # Create the "manual irrigation" contents of the page.
        group45 = Pmw.Group(fm22, tag_text = 'Irrigation (On Reported dates)',tag_font = Pmw.logicalfont('Helvetica', 0.5))
        group45.pack(fill = 'x', side=TOP,expand=1, padx = 2, pady = 5)
        self.nirrigation = Pmw.EntryField(group45.interior(), labelpos = 'w',
            label_text = 'Number of irrigations? ',
            validate = {'validator': 'numeric', 'min' : 1, 'max' : 3, 'minstrict' : 0})
        self.nirrigation.pack( side=TOP, padx = 5, anchor=W, pady = 5)
        frame_in1 =Frame(group45.interior())
        self.label120 = Label(frame_in1, text='Puddling date(DAP):',padx=5)
        self.label120.pack(side=LEFT, padx = 3, pady = 5) 
        self.label121= Label(frame_in1, text='N/A',relief='sunken',width=13)  
        self.label121.pack(side=LEFT, padx = 3, pady = 5) 
##        self.label122 = Label(frame_in1, text='Puddling:',padx=5)
##        self.label122.grid(row=1,column=0, sticky=W)
##        self.label123= Label(frame_in1, text='N/A',relief='sunken',width=13) 
##        self.label123.grid(row=1,column=1, sticky=W)
        self.label124 = Label(frame_in1, text='Percolation rate(mm/day):',padx=5)
        self.label124.pack(side=LEFT, padx = 3, pady = 5) 
        self.label125= Label(frame_in1, text='N/A',relief='sunken',width=13)
        self.label125.pack(side=LEFT, padx = 3, pady = 5) 
        frame_in1.pack(fill = 'x', expand = 1,side=TOP)

        frame_in2 =Frame(group45.interior())
        label100 = Label(frame_in2, text='No. irrigation', width=10, padx=5,pady=5)
        label101 = Label(frame_in2, text='Date(YYDOY)', width=13,padx=5,pady=5)
        label102 = Label(frame_in2, text='Bund height',width=15, padx=5,pady=5)
        label103 = Label(frame_in2, text='Flood depth', width=20, padx=5,pady=5)
        label1031 = Label(frame_in2, text='Constant depth?', width=20, padx=5,pady=5)
        self.label104 = Label(frame_in2, text='1st:',width=10,padx=5)
        self.label105 = Label(frame_in2, text='N/A',relief='sunken',width=13)  
        self.label106 = Label(frame_in2, text='N/A',relief='sunken',width=15) 
        self.label107 = Label(frame_in2, text='N/A',relief='sunken',width=20) 
        self.label1071 = Label(frame_in2, text='N/A',relief='sunken',width=20) 
        self.label108 = Label(frame_in2, text='2nd:',width=10,padx=5) 
        self.label109 = Label(frame_in2, text='N/A',relief='sunken',width=13) 
        self.label110 = Label(frame_in2, text='N/A',relief='sunken',width=15) 
        self.label111 = Label(frame_in2, text='N/A',relief='sunken',width=20)    
        self.label1111 = Label(frame_in2, text='N/A',relief='sunken',width=20) 
        self.label112 = Label(frame_in2, text='3rd:',width=10,padx=5) 
        self.label113 = Label(frame_in2, text='N/A',relief='sunken',width=13) 
        self.label114 = Label(frame_in2, text='N/A',relief='sunken',width=15) 
        self.label115 = Label(frame_in2, text='N/A',relief='sunken',width=20) 
        self.label1151 = Label(frame_in2, text='N/A',relief='sunken',width=20) 
        #aline  empty label
        entries = (label100, label101, label102, label103,label1031)
        i=0
        for entry in entries:
            entry.grid(row=0,column=i) #, sticky=W) 
            i=i+1
        entries = (self.label104, self.label105, self.label106, self.label107,self.label1071)
        i=0
        for entry in entries:
            entry.grid(row=1,column=i) #, sticky=W) 
            i=i+1
        entries = (self.label108, self.label109, self.label110, self.label111,self.label1111)
        i=0
        for entry in entries:
            entry.grid(row=2,column=i) #, sticky=W) 
            i=i+1
        entries = (self.label112, self.label113, self.label114, self.label115,self.label1151)
        i=0
        for entry in entries:
            entry.grid(row=3,column=i) #, sticky=W) 
            i=i+1
        frame_in2.pack(fill = 'x', expand = 1,side=TOP)      
     
        fm22.pack(fill = 'x', expand = 1,side=TOP)

#=========================3rd  page for "Scenario setup"
        page3 = notebook.add('*Scenarios setup')
        notebook.tab('*Scenarios setup').focus_set()

       # 1) ADD SCROLLED FRAME
        sf_p3 = Pmw.ScrolledFrame(page3) #,usehullsize=1, hull_width=700, hull_height=220)
        sf_p3.pack(padx=5, pady=3, fill='both', expand=YES)

        group31 = Pmw.Group(sf_p3.interior(), tag_text = 'Working directory')
        group31.pack(fill = 'x', expand = 1, anchor=N, padx = 10, pady = 5)     
        #Working directory
        fm31=Frame(group31.interior())
        self.WDir_label0=Label(fm31, text='Working directory:', padx=5, pady=5)
        self.WDir_label0.pack(fill = 'x', side=LEFT, anchor=W, padx = 10, pady = 5)
        self.WDir_label = Label(fm31, text='N/A',relief='sunken', padx=5, pady=5,width=50)
        self.WDir_label.pack(fill = 'x', side=LEFT, anchor=W, padx = 10, pady = 5)
       # self.WDir_label.config(text='C:/PH_DCC_Python/DCC_MZ')  ###<<<===========hard-coded
        fm31.pack(side=TOP,anchor=W)

        fm32=Frame(group31.interior())
       # Create button to get file path
        self.file_button=tkinter.Button(fm32,
                text = 'Click to select working directory',
                command = self.getWdir,bg='gray70').pack(side=TOP,anchor=N)
        fm32.pack(side=TOP,anchor=W)
        WDir_label2=Label(fm32, text='*NOTE: Make sure all input files are in the chosen directory').pack(side=TOP,anchor=W)
        WDir_label3=Label(fm32, text='          Output files will be created under the chosen directory with new scenario names').pack(side=TOP, anchor=W)

        # 2nd group for What-If Scenarios"
        group33 = Pmw.Group(sf_p3.interior(), tag_text = 'What-If scenarios')
        group33.pack(fill = 'x', expand = 1,anchor=N, padx = 10, pady = 5)   
        # self.nscenario = Pmw.EntryField(group33.interior(), labelpos = 'w',
        #     label_text = 'How many scenarios? ',
        #     validate = {'validator': 'numeric', 'min' : 1, 'max' : 5, 'minstrict' : 0})
        # self.nscenario.pack( side=TOP, padx = 5, pady = 5)      
        fm33=Frame(group33.interior())
        label31 = Label(fm33, text='Scenario Name').pack(side=TOP)
        label310 = Label(fm33, text='(4char) ').pack(side=TOP)
        self.name1 = Pmw.EntryField(fm33, labelpos = 'w',entry_width=10,
            label_text = '1:',
            validate = {'validator': 'alphanumeric', 'min' : 1, 'max' : 4, 'minstrict' : 0})
        self.name1.pack( side=TOP, padx = 5, pady = 5) 
        self.name2 = Pmw.EntryField(fm33, labelpos = 'w',entry_width=10,
            label_text = '2:',
            validate = {'validator': 'alphanumeric', 'min' : 1, 'max' : 4, 'minstrict' : 0})
        self.name2.pack( side=TOP, padx = 5, pady = 5) 
        self.name3 = Pmw.EntryField(fm33, labelpos = 'w',entry_width=10,
            label_text = '3:',
            validate = {'validator': 'alphanumeric', 'min' : 1, 'max' : 4, 'minstrict' : 0})
        self.name3.pack( side=TOP, padx = 5, pady = 5)  
        fm33.pack(side=LEFT)

        #-7th frame - steps for visualizations
        fm39=Frame(group33.interior())
        label37 = Label(fm39, text='Steps').pack(side=TOP)
        label370 = Label(fm39, text='(Days)').pack(side=TOP)
        self.step1 =Pmw.EntryField(fm39, labelpos = 'w',entry_width=5, value = '10',
            validate = {'validator': 'numeric', 'min' : 4, 'max' : 50, 'minstrict' : 0})  #min step days are 4!!!
        self.step1.pack( side=TOP, padx = 5, pady = 5) 
        self.step2 =Pmw.EntryField(fm39, labelpos = 'w',entry_width=5,
            validate = {'validator': 'numeric', 'min' : 4, 'max' : 50, 'minstrict' : 0}) 
        self.step2.pack( side=TOP, padx = 5, pady = 5) 
        self.step3 =Pmw.EntryField(fm39, labelpos = 'w',entry_width=5,
            validate = {'validator': 'numeric', 'min' : 4, 'max' : 50, 'minstrict' : 0}) 
        self.step3.pack( side=TOP, padx = 5, pady = 5) 
        fm39.pack(side=LEFT)      

         #-2nd frame
        fm34=Frame(group33.interior())
        label32 = Label(fm34, text='     ').pack(side=TOP)
        label320 = Label(fm34, text='     ').pack(side=TOP)
       # Create button to get file path
        self.param_button1=tkinter.Button(fm34,
                text = 'Run DSSAT with seasonal climate forecasts',
                command = self.writeSNX1_DSSAT,bg='green').pack(side=TOP,anchor=N, padx = 5, pady = 2)
        self.param_button2=tkinter.Button(fm34,
                text = 'Run DSSAT with seasonal climate forecasts',
                command = self.writeSNX2_DSSAT,bg='green').pack(side=TOP,anchor=N, padx = 5, pady = 2)
        self.param_button3=tkinter.Button(fm34,
                text = 'Run DSSAT with seasonal climate forecasts',
                command = self.writeSNX3_DSSAT,bg='green').pack(side=TOP,anchor=N, padx = 5, pady = 2)
        fm34.pack(side=LEFT)
         #-3rd frame
        fm35=Frame(group33.interior())
        label33 = Label(fm35, text='  ').pack(side=TOP)
        label330 = Label(fm35, text='Crop').pack(side=TOP) 
        self.label34 = Label(fm35, text='N/A',relief='sunken')
        self.label34.pack( side=TOP,padx = 5, pady = 5) 
        self.label35 = Label(fm35, text='N/A',relief='sunken')
        self.label35.pack( side=TOP,padx = 5, pady = 5) 
        self.label36 = Label(fm35, text='N/A',relief='sunken')
        self.label36.pack( side=TOP,padx = 5, pady = 5) 

        fm35.pack(side=LEFT)
         #-4th frame - earliest planting date
        fm36=Frame(group33.interior())
        label34 = Label(fm36, text='First Planting').pack(side=TOP)
        label340 = Label(fm36, text='(YYYYDOY)').pack(side=TOP) 
        self.pdate11 = Label(fm36,text='N/A',relief='sunken')
        self.pdate11.pack( side=TOP,padx = 5, pady = 5) 
        self.pdate12 = Label(fm36,text='N/A',relief='sunken')
        self.pdate12.pack( side=TOP,padx = 5, pady = 5)
        self.pdate13 = Label(fm36,text='N/A',relief='sunken')
        self.pdate13.pack( side=TOP,padx = 5, pady = 5)
        fm36.pack(side=LEFT)
         #-5th frame - last planting date
        fm37=Frame(group33.interior())
        label35 = Label(fm37, text='Last planting').pack(side=TOP)
        label350 = Label(fm37, text='(YYYYDOY)').pack(side=TOP)
        self.pdate21 = Label(fm37,text='N/A',relief='sunken')
        self.pdate21.pack( side=TOP,padx = 5, pady = 5) 
        self.pdate22 = Label(fm37,text='N/A',relief='sunken')
        self.pdate22.pack( side=TOP,padx = 5, pady = 5)
        self.pdate23 = Label(fm37,text='N/A',relief='sunken')
        self.pdate23.pack( side=TOP,padx = 5, pady = 5) 
        fm37.pack(side=LEFT)
 
        fm38=Frame(group33.interior())
        label36 = Label(fm38, text='     ').pack(side=TOP)
        label360 = Label(fm38, text='     ').pack(side=TOP)
       # Create button to get file path
        self.param_button1=tkinter.Button(fm38,
                text = 'Run DSSAT with historical weather',
                command = self.writeSNX1_DSSAT_hist,bg='yellow').pack(side=TOP,anchor=N, padx = 5, pady = 2)
        self.param_button2=tkinter.Button(fm38,
                text = 'Run DSSAT with historical weather',
                command = self.writeSNX2_DSSAT_hist,bg='yellow').pack(side=TOP,anchor=N, padx = 5, pady = 2)
        self.param_button3=tkinter.Button(fm38,
                text = 'Run DSSAT with historical weather',
                command = self.writeSNX3_DSSAT_hist,bg='yellow').pack(side=TOP,anchor=N, padx = 5, pady = 2)
        fm38.pack(side=LEFT)

##        #-7th frame - steps for visualizations
##        fm39=Frame(group33.interior())
##        label37 = Label(fm39, text='Steps').pack(side=TOP)
##        label370 = Label(fm39, text='(Days)').pack(side=TOP)
##        self.step1 =Pmw.EntryField(fm39, labelpos = 'w',entry_width=5,
##            validate = {'validator': 'alphanumeric', 'min' : 1, 'max' : 10, 'minstrict' : 0})
##        self.step1.pack( side=TOP, padx = 5, pady = 5) 
##        self.step2 =Pmw.EntryField(fm39, labelpos = 'w',entry_width=5,
##            validate = {'validator': 'alphanumeric', 'min' : 1, 'max' : 10, 'minstrict' : 0})
##        self.step2.pack( side=TOP, padx = 5, pady = 5) 
##        self.step3 =Pmw.EntryField(fm39, labelpos = 'w',entry_width=5,
##            validate = {'validator': 'alphanumeric', 'min' : 1, 'max' : 10, 'minstrict' : 0})
##        self.step3.pack( side=TOP, padx = 5, pady = 5) 
##        fm39.pack(side=LEFT)       

        #-8th frame for comments
        fm40=Frame(group33.interior())
        label37 = Label(fm40, text=' ').pack(side=TOP)
        label370 = Label(fm40, text='Notes').pack(side=TOP)
        self.comment1 = Pmw.EntryField(fm40, labelpos = 'w')
        self.comment1.pack( side=TOP, padx = 5, pady = 5) 
        self.comment2 = Pmw.EntryField(fm40, labelpos = 'w')
        self.comment2.pack( side=TOP, padx = 5, pady = 5) 
        self.comment3 = Pmw.EntryField(fm40, labelpos = 'w')
        self.comment3.pack( side=TOP, padx = 5, pady = 5)   
        fm40.pack(side=LEFT)
        
        # group34 = Pmw.Group(sf_p3.interior(), tag_text = 'Run DSSAT & Display Outputs')
        # group34.pack(fill = 'x', expand = 1,anchor=N, padx = 10, pady = 5)

        # # #2. Run CAMDT
        # # button32=tkinter.Button(group34.interior(), text = 'Run DSSAT',
        # #         command = self.RunDSSAT,bg='green').pack(fill = 'x', expand = 1,side=TOP,anchor=N, padx = 10, pady = 5)
        # fm38=Frame(group34.interior())
        # # Create button to execute graphs (1)yield boxplot
        # button33=tkinter.Button(fm38,text = 'I. Display Yield Estimation (Boxplot)',
        #         command = self.Yield_boxplot,bg='peachpuff1').pack(fill = 'x', expand = 1,side=LEFT,anchor=N, padx = 10, pady = 5)
        # # Create button to execute graphs (2) Anthesis date
        # button34=tkinter.Button(fm38, text = 'II. Display Anthesis date (Boxplot)',  
        #         command = self.ADAT_boxplot,bg='peachpuff1').pack(fill = 'x', expand = 1,side=LEFT,anchor=N, padx = 10, pady = 5)
        # # Create button to execute graphs (2) Maturity date
        # button35=tkinter.Button(fm38, text = 'III. Display Maturity date (Boxplot)',  
        #         command = self.MDAT_boxplot,bg='peachpuff1').pack(fill = 'x', expand = 1,side=LEFT,anchor=N, padx = 10, pady = 5)
        # fm38.pack(fill = 'x', expand = 1,side=TOP)
        # fm39=Frame(group34.interior())
        # # Create button to execute graphs (2) yield exceedance curve
        # button36=tkinter.Button(fm39, text = 'IV. Display Yield Estimation (Exceedance Curve)',
        #         command = self.Yield_exceedance,bg='peachpuff1').pack(fill = 'x', expand = 1,side=LEFT,anchor=N, padx = 10, pady = 5)
        # button37=tkinter.Button(fm39, text = 'V. Display Anthesis date (Exceedance Curve)',
        #         command = self.ADAT_exceedance,bg='peachpuff1').pack(fill = 'x', expand = 1,side=LEFT,anchor=N, padx = 10, pady = 5)
        # button38=tkinter.Button(fm39, text = 'VI. Display Maturity date (Exceedance Curve)',
        #         command = self.MDAT_exceedance,bg='peachpuff1').pack(fill = 'x', expand = 1,side=LEFT,anchor=N, padx = 10, pady = 5)
        # fm39.pack(fill = 'x', expand = 1,side=TOP)
#=========================4th  page for "Credit"
        page6 = notebook.add('*CREDIT')
        notebook.tab('*CREDIT').focus_set()

       # 1) ADD SCROLLED FRAME
        sf_p6 = Pmw.ScrolledFrame(page6) #,usehullsize=1, hull_width=700, hull_height=220)
        sf_p6.pack(padx=5, pady=3, fill='both', expand=YES)

        group61 = Pmw.Group(sf_p6.interior(), tag_text = 'Authors:')
        group61.pack(fill = 'x',anchor=N, padx = 10, pady = 5)

        self.authrs1=Label(group61.interior(), text='Eunjin Han, Ph.D. at IRI', padx=5, pady=5)
        self.authrs1.pack(side=TOP, anchor=W, expand=YES)
        self.authrs2=Label(group61.interior(), text='Amor Ines, Ph.D. at Michigan State Univ. and IRI', padx=5, pady=5)
        self.authrs2.pack(side=TOP, anchor=W, expand=YES)
        self.authrs3=Label(group61.interior(), text='Walter Baethgen, Ph.D. at IRI', padx=5, pady=5)
        self.authrs3.pack(side=TOP, anchor=W, expand=YES)

        notebook.setnaturalsize()
#======End of creatign pages================================
        #error message when irrigation info is missing
        self.irrigation_err = Pmw.MessageDialog(parent, title = 'Error in Irrigation Input',
                   defaultbutton = 0,
                   message_text = 'No Irrigaiotn input! Please click the Button for more detiled input on DSSAT setup2 page.')
        self.irrigation_err.withdraw()
        #error message when fertilizer info is missing
        self.fertilizer_err = Pmw.MessageDialog(parent, title = 'Error in Fertilizer Input',
                   defaultbutton = 0,
                   message_text = 'No Fertilizer input! Please click the Button for more detiled input on DSSAT setup2 page.')
        self.fertilizer_err.withdraw()        
        #error message when there is no number of fertilizer applications
        self.nfert_err_dialog = Pmw.MessageDialog(parent, title = 'Error in fertilizer input',
                   defaultbutton = 0,
                   message_text = 'Number of fertilizer applications should be provided in "DSSAT setup 2" tab!')
        self.nfert_err_dialog .iconname('Error in fertilizer input')
        self.nfert_err_dialog .withdraw()

        #Dialog to get more information about fertilizer application
        self.fert_dialog = Pmw.Dialog(parent, title='Fertilization Application')
        self.frt_group1 = Pmw.Group(self.fert_dialog.interior(), tag_text = '1st application')
        self.frt_group1.pack(fill = 'both',side=TOP, expand = 1, padx = 10, pady = 5)  
        self.day1 = Pmw.EntryField(self.frt_group1.interior(), labelpos = 'w',
            label_text = 'Days after planting:',value = '1',
            validate = {'validator': 'numeric', 'min' : 0, 'max' : 200, 'minstrict' : 0})
        self.amount1 = Pmw.EntryField(self.frt_group1.interior(), labelpos = 'w',
            label_text = 'Amount (N, kg/ha):',value = '25',
            validate = {'validator': 'real', 'min' : 0, 'max' : 200, 'minstrict' : 0})
        #-fertilizer material
        material_list = ("FE001(Ammonium nitrate)","FE004(Anhydrous ammonia)","FE005(Urea)","None")
        self.fert_mat1 = Pmw.ComboBox(self.frt_group1.interior(),label_text='fertilizer material:',labelpos='wn',
                        listbox_width=15, dropdown=1,
                        scrolledlist_items=material_list,
                        entryfield_entry_state=DISABLED)
        first =material_list[2]
        self.fert_mat1.selectitem(first)
        #-fertilizer application method
        apply_list = ("AP001(Broadcast, not incorporated)","AP002(Broadcast, incorporated)","AP003(Banded on surface)",
                      "AP004(Banded beneath surface)","AP011(Broadcast on flooded/saturated soil, none in soil)","None")
        self.fert_app1 = Pmw.ComboBox(self.frt_group1.interior(),label_text='application method:',labelpos='wn',
                        listbox_width=15, dropdown=1,
                        scrolledlist_items=apply_list,
                        entryfield_entry_state=DISABLED)
        first =apply_list[0]
        self.fert_app1.selectitem(first)
        #aline labels
        entries = (self.day1, self.amount1, self.fert_mat1, self.fert_app1)
        for entry in entries:
            entry.pack(fill='x', side=TOP, expand=1, padx=10, pady=2)
        Pmw.alignlabels(entries)
        #2nd fertilizer application
        self.frt_group2 = Pmw.Group(self.fert_dialog.interior(), tag_text = '2nd application')
        self.frt_group2.pack(fill = 'both', side=TOP, expand = 1, padx = 10, pady = 5)  
        self.day2 = Pmw.EntryField(self.frt_group2.interior(), labelpos = 'w',
            label_text = 'Days after planting:',value = '40',
            validate = {'validator': 'numeric', 'min' : 1, 'max' : 200, 'minstrict' : 0})
        self.amount2 = Pmw.EntryField(self.frt_group2.interior(), labelpos = 'w',
            label_text = 'Amount (N, kg/ha):',value = '25',
            validate = {'validator': 'real', 'min' : 1, 'max' : 200, 'minstrict' : 0})
        #-fertilizer material
        self.fert_mat2 = Pmw.ComboBox(self.frt_group2.interior(),label_text='fertilizer material:',labelpos='wn',
                        listbox_width=15, dropdown=1,
                        scrolledlist_items=material_list,
                        entryfield_entry_state=DISABLED)
        first =material_list[2]
        self.fert_mat2.selectitem(first)
        #-fertilizer application method
        self.fert_app2 = Pmw.ComboBox(self.frt_group2.interior(),label_text='application method:',labelpos='wn',
                        listbox_width=15, dropdown=1,
                        scrolledlist_items=apply_list,
                        entryfield_entry_state=DISABLED)
        first =apply_list[0]
        self.fert_app2.selectitem(first)
        #aline labels
        entries = (self.day2, self.amount2, self.fert_mat2, self.fert_app2)
        for entry in entries:
            entry.pack(fill='x', side=TOP, expand=1, padx=10, pady=2)
        Pmw.alignlabels(entries)
        #3rd fertilizer application
        self.frt_group3 = Pmw.Group(self.fert_dialog.interior(), tag_text = '3rd application')
        self.frt_group3.pack(fill = 'both', side=TOP, expand = 1, padx = 10, pady = 5)  
        self.day3 = Pmw.EntryField(self.frt_group3.interior(), labelpos = 'w',
            label_text = 'Days after planting:',
            validate = {'validator': 'numeric', 'min' : 1, 'max' : 200, 'minstrict' : 0})
        self.amount3 = Pmw.EntryField(self.frt_group3.interior(), labelpos = 'w',
            label_text = 'Amount (N, kg/ha):',
            validate = {'validator': 'real', 'min' : 1, 'max' : 200, 'minstrict' : 0})
        #-fertilizer material
        self.fert_mat3 = Pmw.ComboBox(self.frt_group3.interior(),label_text='fertilizer material:',labelpos='wn',
                        listbox_width=15, dropdown=1,
                        scrolledlist_items=material_list,
                        entryfield_entry_state=DISABLED)
        first =material_list[3]  #'none'
        self.fert_mat3.selectitem(first)
        #-fertilizer application method
        self.fert_app3 = Pmw.ComboBox(self.frt_group3.interior(),label_text='application method:',labelpos='wn',
                        listbox_width=15, dropdown=1,
                        scrolledlist_items=apply_list,
                        entryfield_entry_state=DISABLED)
        first =apply_list[5]  #'none'
        self.fert_app3.selectitem(first)
        #aline labels
        entries = (self.day3, self.amount3, self.fert_mat3, self.fert_app3)
        for entry in entries:
            entry.pack(fill='x', side=TOP, expand=1, padx=10, pady=2)
        Pmw.alignlabels(entries)
        self.fert_dialog.withdraw() 
        #========end of fert_dialog
        #============to add more specification for automatic irrigation(auto when required)
        self.AutoIrrDialog = Pmw.Dialog(parent, title='Automatic irrigation when required (only soil saturated)')
        self.irr_depth = Pmw.EntryField(self.AutoIrrDialog.interior(), labelpos = 'w',
            label_text = 'Management soil depth(cm):',validate = {'validator': 'real', 'min' : 1, 'max' : 200, 'minstrict' : 0},
            value = '30')
        #-threshold
        self.irr_thresh = Pmw.EntryField(self.AutoIrrDialog.interior(), labelpos = 'w',
            label_text = 'Threshold(% of maximum available water triggering irrigation ):', validate = {'validator': 'real', 'min' : 1, 'max' : 100, 'minstrict' : 0},
            value = '50')
         #- Efficiency fraction 
        self.eff_fraction = Pmw.EntryField(self.AutoIrrDialog.interior(), labelpos = 'w',
            label_text = 'Efficiency fraction:', validate = {'validator': 'real', 'min' : 0, 'max' : 1, 'minstrict' : 0},
            value = '1')
        #aline labels
        entries = (self.irr_depth, self.irr_thresh, self.eff_fraction)
        for entry in entries:
            entry.pack(fill='x', expand=1, padx=10, pady=5)
        Pmw.alignlabels(entries)
        self.AutoIrrDialog.withdraw()
 #============to add more specification for automatic irrigation(periiodic irrigation to keep water flooded )
        self.AutoIrrDialog_flood = Pmw.Dialog(parent, title='Automatic irrigation (periodic floodidng)')
        self.irr_depth2 = Pmw.EntryField(self.AutoIrrDialog_flood.interior(), labelpos = 'w',
            label_text = 'Flooding depth(mm):',validate = {'validator': 'numeric', 'min' : 0, 'max' : 300, 'minstrict' : 0},
            value = '50')
        # irrigation interval
        self.irrg_interval = Pmw.EntryField(self.AutoIrrDialog_flood.interior(), labelpos = 'w',
            label_text = 'Interval (every "n" days):', validate = {'validator': 'numeric', 'min' : 1, 'max' : 100, 'minstrict' : 0},
            value = '10')
         #- end of application
        self.irrg_end_date = Pmw.EntryField(self.AutoIrrDialog_flood.interior(), labelpos = 'w',
            label_text = 'End of applicaiton (Days after transplanting):', validate = {'validator': 'numeric', 'min' : 0, 'minstrict' : 0},
            value = '70')
         #- bund height
        self.irrg_bheight = Pmw.EntryField(self.AutoIrrDialog_flood.interior(), labelpos = 'w',
            label_text = 'Bund height (mm):', validate = {'validator': 'numeric', 'min' : 0, 'minstrict' : 0},
            value = '100')
         #- puddling date
        self.irrg_plddate = Pmw.EntryField(self.AutoIrrDialog_flood.interior(), labelpos = 'w',
            label_text = 'Puddling date (Days After Transplanting):', validate = {'validator': 'real'},
            value = '-3')
         #- First irrigation date (DAP)
        self.irrg_irrdate1 = Pmw.EntryField(self.AutoIrrDialog_flood.interior(), labelpos = 'w',
            label_text = 'First irrigation date (Days After Transplanting):', validate = {'validator': 'real'},
            value = '-1')
        #aline labels
        entries = (self.irrg_plddate, self.irrg_irrdate1, self.irrg_bheight, self.irr_depth2, self.irrg_interval, self.irrg_end_date)
        for entry in entries:
            entry.pack(fill='x', expand=1, padx=10, pady=5)
        Pmw.alignlabels(entries)
        self.AutoIrrDialog_flood.withdraw()

 #============to add more specification for Reported irrigation
        self.ReptIrrDialog = Pmw.Dialog(parent, title='Irrigation on Report Dates')
        self.irr_group0 = Pmw.Group(self.ReptIrrDialog.interior(), tag_text = 'Puddling')
        self.irr_group0.pack(fill = 'both',side=TOP, expand = 1, padx = 10, pady = 5)  
        self.pud_date = Pmw.EntryField(self.irr_group0.interior(), labelpos = 'w',
            label_text = 'Puddling date(Days After planting):',
            validate = {'validator': 'real'},value = '-3')
##        self.puddling = Pmw.EntryField(self.irr_group0.interior(), labelpos = 'w',
##            label_text = 'Puddling:',
##            validate = {'validator': 'numeric', 'min' : 0, 'max' : 1, 'minstrict' : 0},value = '0')
        self.pec_rate = Pmw.EntryField(self.irr_group0.interior(), labelpos = 'w',
            label_text = 'Percolation rate(mm/day):',
            validate = {'validator': 'real', 'min' : 0, 'max' : 100, 'minstrict' : 0},value = '2')
        #aline labels
        entries = (self.pud_date,self.pec_rate)
        for entry in entries:
            entry.pack(fill='x', side=TOP, expand=1, padx=10, pady=2)
        Pmw.alignlabels(entries)
        #1st irrigation
        self.irr_group1 = Pmw.Group(self.ReptIrrDialog.interior(), tag_text = '1st irrigation')
        self.irr_group1.pack(fill = 'both',side=TOP, expand = 1, padx = 10, pady = 5)  
        self.irr_day1 = Pmw.EntryField(self.irr_group1.interior(), labelpos = 'w',
            label_text = 'Irrigation date(days after planting):',value = '-1',   #none value is used as a flag
            validate = {'validator': 'real'})
        self.height1 = Pmw.EntryField(self.irr_group1.interior(), labelpos = 'w',
            label_text = 'Bund height(mm):',
            validate = {'validator': 'real', 'min' : 1, 'max' : 1000, 'minstrict' : 0},value = '100')
        #-flood depth
        self.flood1 = Pmw.EntryField(self.irr_group1.interior(), labelpos = 'w',
            label_text = 'Flood depth (mm):',
            validate = {'validator': 'real', 'min' : 0, 'max' : 1000, 'minstrict' : 0},value = '50')
        self.label_ir0= Label(self.irr_group1.interior(), 
            text='Constant flood depth? ',anchor=W) 
        self.label_ir1= Label(self.irr_group1.interior(), 
            text='  -Yes: Maintain constant specified flood depth until next irrigation record.',anchor=W) 
        self.label_ir2= Label(self.irr_group1.interior(), 
            text='  -No: Regular irrigation (bunded or upland)',anchor=W)
        #aline labels
        entries = (self.irr_day1, self.height1, self.flood1,self.label_ir0,self.label_ir1,self.label_ir2)
        for entry in entries:
            entry.pack(fill='x', side=TOP, expand=1, padx=10, pady=2)
        Pmw.alignlabels(entries)
        
        floodVar1 = tkinter.IntVar()
        constant_fld_option=[('Yes', 0), ('No', 1)]
        for text, value in constant_fld_option:
            Radiobutton(self.irr_group1.interior(), text=text, value=value, variable=floodVar1).pack(side=LEFT,expand=YES)
        floodVar1.set(0)  #By default- constant flooding depth
        
        #2nd irrigation
        self.irr_group2 = Pmw.Group(self.ReptIrrDialog.interior(), tag_text = '2nd irrigation')
        self.irr_group2.pack(fill = 'both', side=TOP, expand = 1, padx = 10, pady = 5)  
        self.irr_day2 = Pmw.EntryField(self.irr_group2.interior(), labelpos = 'w',
            label_text = 'Irrigation date(days after planting):',value = 20,   #none value is used as a flag
            validate = {'validator': 'numeric'})
        self.height2 = Pmw.EntryField(self.irr_group2.interior(), labelpos = 'w',
            label_text = 'Bund height(mm):',
            validate = {'validator': 'real', 'min' : 1, 'max' : 1000, 'minstrict' : 0},value = '150')
        #-flood depth
        self.flood2 = Pmw.EntryField(self.irr_group2.interior(), labelpos = 'w',
            label_text = 'Flood depth (mm):',
            validate = {'validator': 'real', 'min' : 0, 'max' : 1000, 'minstrict' : 0},value = '50')   
        self.label_ir4= Label(self.irr_group2.interior(), 
            text='Constant flood depth? ',anchor=W) 
        entries = (self.irr_day2, self.height2, self.flood2,self.label_ir4)
        for entry in entries:
            entry.pack(fill='x', side=TOP, expand=1, padx=10, pady=2)
        Pmw.alignlabels(entries)

        floodVar2 = tkinter.IntVar()
        for text, value in constant_fld_option:
            Radiobutton(self.irr_group2.interior(), text=text, value=value, variable=floodVar2).pack(side=LEFT,expand=YES)
        floodVar2.set(0)  #By default- constant flooding depth

        #3rd irrigation
        self.irr_group3 = Pmw.Group(self.ReptIrrDialog.interior(), tag_text = '3rd irrigation')
        self.irr_group3.pack(fill = 'both', side=TOP, expand = 1, padx = 10, pady = 5)  
        self.irr_day3 = Pmw.EntryField(self.irr_group3.interior(), labelpos = 'w',
            label_text = 'Irrigation date(days after planting):',value = 'None',   #none value is used as a flag
            validate = {'validator': 'numeric'})
        self.height3 = Pmw.EntryField(self.irr_group3.interior(), labelpos = 'w',
            label_text = 'Bund height(mm):',
            validate = {'validator': 'real', 'min' : 1, 'max' : 1000, 'minstrict' : 0})
        #-flood depth
        self.flood3 = Pmw.EntryField(self.irr_group3.interior(), labelpos = 'w',
            label_text = 'Flood depth (mm):',
            validate = {'validator': 'real', 'min' : 1, 'max' : 1000, 'minstrict' : 0})   
        self.label_ir5= Label(self.irr_group3.interior(), 
            text='Constant flood depth? ',anchor=W) 
        #aline labels
        entries = (self.irr_day3, self.height3, self.flood3,self.label_ir5)
        for entry in entries:
            entry.pack(fill='x', side=TOP, expand=1, padx=10, pady=2)
        Pmw.alignlabels(entries)
        floodVar3 = tkinter.IntVar()
        for text, value in constant_fld_option:
            Radiobutton(self.irr_group3.interior(), text=text, value=value, variable=floodVar3).pack(side=LEFT,expand=YES)
        floodVar3.set(0)  #By default- constant flooding depth

        self.ReptIrrDialog.withdraw()
                #============to add more specification for automatic irrigation(auto when required)
        #======================================================================================================
        #error message when there is no scenario name
        self.fert_err = Pmw.MessageDialog(parent, title = 'Error message in fertilizer application',
                   defaultbutton = 0,
                   message_text = 'Error in Fertilizer inputs')
        self.fert_err.withdraw()

        #error message when there is no scenario name
        self.writeSNX_err = Pmw.MessageDialog(parent, title = 'Error message in writing param.txt',
                   defaultbutton = 0,
                   message_text = 'No scenario name available!')
        self.writeSNX_err.withdraw()

        #error message when there is no soil type selected
        self.soiltype_err = Pmw.MessageDialog(parent, title = 'Error message in setting initial NO3',
                   defaultbutton = 0,
                   message_text = 'No soil type is selected!')
        self.soiltype_err.withdraw()

        #error message when there is no initial NO3 is chosen
        self.ini_NO3_err = Pmw.MessageDialog(parent, title = 'Error message in setting initial NO3',
                   defaultbutton = 0,
                   message_text = 'No initial NO3 (H or L) is chosen!')
        self.ini_NO3_err.withdraw()

        #error message when there is no scenario name
        self.Eanalysis_err = Pmw.MessageDialog(parent, title = 'Error in Economical Analysis',
                   defaultbutton = 0,
                   message_text = 'No cost/price info added!')
        self.Eanalysis_err.withdraw()
        
#===sub functions===========================================     
    def allstates(self):
        list_BN = [self.BN_11.getvalue(), self.BN_12.getvalue(), self.BN_13.getvalue(), self.BN_14.getvalue(),
                  self.BN_15.getvalue(), self.BN_16.getvalue(), self.BN_17.getvalue(), self.BN_18.getvalue(),
                  self.BN_19.getvalue(), self.BN_110.getvalue(), self.BN_111.getvalue(), self.BN_112.getvalue(),
                  self.BN_21.getvalue(), self.BN_22.getvalue(), self.BN_23.getvalue(), self.BN_24.getvalue(),
                  self.BN_25.getvalue(), self.BN_26.getvalue(), self.BN_27.getvalue(), self.BN_28.getvalue(),
                  self.BN_29.getvalue(), self.BN_210.getvalue(), self.BN_211.getvalue(), self.BN_212.getvalue()]
        list_AN = [self.AN_11.getvalue(), self.AN_12.getvalue(), self.AN_13.getvalue(), self.AN_14.getvalue(),
                  self.AN_15.getvalue(), self.AN_16.getvalue(), self.AN_17.getvalue(), self.AN_18.getvalue(),
                  self.AN_19.getvalue(), self.AN_110.getvalue(), self.AN_111.getvalue(), self.AN_112.getvalue(),
                  self.AN_21.getvalue(), self.AN_22.getvalue(), self.AN_23.getvalue(), self.AN_24.getvalue(),
                  self.AN_25.getvalue(), self.AN_26.getvalue(), self.AN_27.getvalue(), self.AN_28.getvalue(),
                  self.AN_29.getvalue(), self.AN_210.getvalue(), self.AN_211.getvalue(), self.AN_212.getvalue()]
        list_NN = []
        for i in range(len(list_BN)):
            temp = 100- int(list_BN[i]) - int(list_AN[i])
            list_NN.append(temp)
            if temp < 0:
                print("Error in computing NN: NN is negative!! Please check AN and BN")
                os.system('pause')
        #Fill NN labes
        self.NN_label2.configure(text=repr(100- int(list_BN[0]) - int(list_AN[0])))
        self.NN_label3.configure(text=repr(100- int(list_BN[1]) - int(list_AN[1])))
        self.NN_label4.configure(text=repr(100- int(list_BN[2]) - int(list_AN[2])))
        self.NN_label5.configure(text=repr(100- int(list_BN[3]) - int(list_AN[3])))
        self.NN_label6.configure(text=repr(100- int(list_BN[4]) - int(list_AN[4])))
        self.NN_label7.configure(text=repr(100- int(list_BN[5]) - int(list_AN[5])))
        self.NN_label8.configure(text=repr(100- int(list_BN[6]) - int(list_AN[6])))
        self.NN_label9.configure(text=repr(100- int(list_BN[7]) - int(list_AN[7])))
        self.NN_label10.configure(text=repr(100- int(list_BN[8]) - int(list_AN[8])))
        self.NN_label11.configure(text=repr(100- int(list_BN[9]) - int(list_AN[9])))
        self.NN_label12.configure(text=repr(100- int(list_BN[10]) - int(list_AN[10])))
        self.NN_label13.configure(text=repr(100- int(list_BN[11]) - int(list_AN[11])))
        self.NN_label14.configure(text=repr(100- int(list_BN[12]) - int(list_AN[12])))
        self.NN_label15.configure(text=repr(100- int(list_BN[13]) - int(list_AN[13])))
        self.NN_label16.configure(text=repr(100- int(list_BN[14]) - int(list_AN[14]))) 
        self.NN_label17.configure(text=repr(100- int(list_BN[15]) - int(list_AN[15])))  
        self.NN_label18.configure(text=repr(100- int(list_BN[16]) - int(list_AN[16])))
        self.NN_label19.configure(text=repr(100- int(list_BN[17]) - int(list_AN[17]))) 
        self.NN_label20.configure(text=repr(100- int(list_BN[18]) - int(list_AN[18])))
        self.NN_label21.configure(text=repr(100- int(list_BN[19]) - int(list_AN[19]))) 
        self.NN_label22.configure(text=repr(100- int(list_BN[20]) - int(list_AN[20]))) 
        self.NN_label23.configure(text=repr(100- int(list_BN[21]) - int(list_AN[21])))
        self.NN_label24.configure(text=repr(100- int(list_BN[22]) - int(list_AN[22])))
        self.NN_label25.configure(text=repr(100- int(list_BN[23]) - int(list_AN[23])))

    def clear_all_button1(self): 
        self.scf_button1.clear()
        self.scf_button2.clear()
    def clear_all_button2(self): 
        self.plt_button1.clear()
        self.plt_button2.clear()
    def reset_BN(self): 
        self.BN_11.setentry(33)
        self.BN_12.setentry(33)
        self.BN_13.setentry(33)
        self.BN_14.setentry(33)
        self.BN_15.setentry(33)
        self.BN_16.setentry(33)
        self.BN_17.setentry(33)
        self.BN_18.setentry(33)
        self.BN_19.setentry(33)
        self.BN_110.setentry(33)
        self.BN_111.setentry(33)
        self.BN_112.setentry(33)
        self.BN_21.setentry(33) 
        self.BN_22.setentry(33)
        self.BN_23.setentry(33)
        self.BN_24.setentry(33)
        self.BN_25.setentry(33)
        self.BN_26.setentry(33)
        self.BN_27.setentry(33)
        self.BN_28.setentry(33)
        self.BN_29.setentry(33)
        self.BN_210.setentry(33)
        self.BN_211.setentry(33)
        self.BN_212.setentry(33)
    def reset_AN(self): 
        self.AN_11.setentry(33)
        self.AN_12.setentry(33)
        self.AN_13.setentry(33)
        self.AN_14.setentry(33)
        self.AN_15.setentry(33)
        self.AN_16.setentry(33)
        self.AN_17.setentry(33)
        self.AN_18.setentry(33)
        self.AN_19.setentry(33)
        self.AN_110.setentry(33)
        self.AN_111.setentry(33)
        self.AN_112.setentry(33)
        self.AN_21.setentry(33) 
        self.AN_22.setentry(33)
        self.AN_23.setentry(33)
        self.AN_24.setentry(33)
        self.AN_25.setentry(33)
        self.AN_26.setentry(33)
        self.AN_27.setentry(33)
        self.AN_28.setentry(33)
        self.AN_29.setentry(33)
        self.AN_210.setentry(33)
        self.AN_211.setentry(33)
        self.AN_212.setentry(33)

  
    #make labels empty whenever new crop type is chosen
    def emptylabel(self):
        self.label_01.configure(text='N/A')
        self.label_02.configure(text='N/A')
        self.label_03.configure(text='N/A')
        self.label_04.configure(text='N/A')
        self.label_05.configure(text='N/A')
        
    #make labels empty whenever tring to add fertilizer input
    def empty_fert_label(self):
        self.label006.configure(text='N/A')
        self.label007.configure(text='N/A')
        self.label008.configure(text='N/A')
        self.label009.configure(text='N/A')
        self.label011.configure(text='N/A')
        self.label012.configure(text='N/A')
        self.label013.configure(text='N/A')
        self.label014.configure(text='N/A')
        self.nfertilizer.setentry("")
        
    #Get irrigationinformation
    def getIrrInput(self):
        # self.label402.configure(text='N/A')
        # self.label404.configure(text='N/A')
        # self.label408.configure(text='N/A')
        self.label009.configure(text='N/A')
        self.label410.configure(text='N/A')
        self.label412.configure(text='N/A')
        self.label414.configure(text='N/A')
        self.label416.configure(text='N/A')
        self.label418.configure(text='N/A')
        self.label420.configure(text='N/A')
        self.label121.configure(text='N/A')
        self.label125.configure(text='N/A')
        self.label105.configure(text='N/A')
        self.label106.configure(text='N/A')
        self.label107.configure(text='N/A')
        self.label1071.configure(text='N/A')
        self.label109.configure(text='N/A') 
        self.label110.configure(text='N/A')
        self.label111.configure(text='N/A')    
        self.label1111.configure(text='N/A') 
        self.label113.configure(text='N/A')
        self.label114.configure(text='N/A')
        self.label115.configure(text='N/A')
        self.label1151.configure(text='N/A')
        if IRbutton.get() == 0:  #Automatic when required - soil moistuer is added
            self.AutoIrrDialog.activate()
            # self.label402.configure(text=self.irr_depth.getvalue(),background='honeydew1')
            # self.label404.configure(text=self.irr_thresh.getvalue(),background='honeydew1')
            # self.label408.configure(text=self.eff_fraction.getvalue(),background='honeydew1')    
        elif IRbutton.get() == 1:  #Automatic when required - Flooding water is added
            self.AutoIrrDialog_flood.activate()
            self.label410.configure(text=self.irr_depth2.getvalue(),background='honeydew1')
            self.label412.configure(text=self.irrg_interval.getvalue(),background='honeydew1')
            self.label414.configure(text=self.irrg_end_date.getvalue(),background='honeydew1') 
            self.label416.configure(text=self.irrg_bheight.getvalue(),background='honeydew1')
            self.label418.configure(text=self.irrg_plddate.getvalue(),background='honeydew1')
            self.label420.configure(text=self.irrg_irrdate1.getvalue(),background='honeydew1')
        elif IRbutton.get() == 2:  #on reported dates
            self.ReptIrrDialog.activate()
            #print ('constant flooding depth?  ', floodVar1, floodVar2, floodVar3)
            if self.irr_day1.getvalue() != "None":
                self.label105.configure(text=self.irr_day1.getvalue(),background='honeydew1')
                self.label106.configure(text=self.height1.getvalue(),background='honeydew1')
                self.label107.configure(text=self.flood1.getvalue(),background='honeydew1') 
                self.label121.configure(text=self.pud_date.getvalue(),background='honeydew1')       
               # self.label123.configure(text=self.puddling.getvalue(),background='honeydew1') 
                self.label125.configure(text=self.pec_rate.getvalue(),background='honeydew1')   
                if floodVar1.get() == 0:
                    self.label1071.configure(text='Yes',background='honeydew1')  
                else:
                    self.label1071.configure(text='No',background='honeydew1')  
            if self.irr_day2.getvalue() != "None":
                self.label109.configure(text=self.irr_day2.getvalue(),background='honeydew1')
                self.label110.configure(text=self.height2.getvalue(),background='honeydew1')
                self.label111.configure(text=self.flood2.getvalue(),background='honeydew1')
                if floodVar2.get() == 0:
                    self.label1111.configure(text='Yes',background='honeydew1')  
                else:
                    self.label1111.configure(text='No',background='honeydew1')  
            if self.irr_day3.getvalue() != "None":
                self.label113.configure(text=self.irr_day3.getvalue(),background='honeydew1')
                self.label114.configure(text=self.height3.getvalue(),background='honeydew1')
                self.label115.configure(text=self.flood3.getvalue(),background='honeydew1')
                if floodVar3.get() == 0:
                    self.label1151.configure(text='Yes',background='honeydew1')  
                else:
                    self.label1151.configure(text='No',background='honeydew1')
                        
    #Get fertilizer application information
    def getFertInput(self):
        #print result
        if Fbutton1.get() == 1:
            self.fert_dialog.activate()
            if self.fert_mat1.getvalue()[0] != "None" and self.day1.getvalue() != '' and self.amount1.getvalue() != '':
                self.label006.configure(text=self.day1.getvalue(),background='honeydew1')
                self.label007.configure(text=self.amount1.getvalue(),background='honeydew1')
                self.label008.configure(text=self.fert_mat1.getvalue()[0],background='honeydew1')
                self.label009.configure(text=self.fert_app1.getvalue()[0],background='honeydew1')
               # print '3rd application is', self.fert_app1.getvalue()[0] #getvalue => tuple or list?
            else:
                self.fert_err.activate()
            if self.fert_mat2.getvalue()[0] != "None" and self.fert_app2.getvalue()[0] != "None" and self.day2.getvalue() != '' and self.amount2.getvalue() != '':
                self.label011.configure(text=self.day2.getvalue(),background='honeydew1')
                self.label012.configure(text=self.amount2.getvalue(),background='honeydew1')
                self.label013.configure(text=self.fert_mat2.getvalue()[0],background='honeydew1')
                self.label014.configure(text=self.fert_app2.getvalue()[0],background='honeydew1')
            else:
                self.label011.configure(text='N/A',background='honeydew1')
                self.label012.configure(text='N/A',background='honeydew1')
                self.label013.configure(text='N/A',background='honeydew1')
                self.label014.configure(text='N/A',background='honeydew1')
            if self.fert_mat3.getvalue()[0] != "None" and self.fert_app3.getvalue()[0] != "None" and self.day3.getvalue() != '' and self.amount3.getvalue() != '':
                self.label016.configure(text=self.day3.getvalue(),background='honeydew1')
                self.label017.configure(text=self.amount3.getvalue(),background='honeydew1')
                self.label018.configure(text=self.fert_mat3.getvalue()[0],background='honeydew1')
                self.label019.configure(text=self.fert_app3.getvalue()[0],background='honeydew1')
            else:
                self.label016.configure(text='N/A',background='honeydew1')
                self.label017.configure(text='N/A',background='honeydew1')
                self.label018.configure(text='N/A',background='honeydew1')
                self.label019.configure(text='N/A',background='honeydew1')
    #Select working directory to run CAMDT fortran executable
    def getWdir(self):
        global Wdir_path
        Wdir_path=askdirectory(initialdir="C:\\", title="Select working dir")
        self.WDir_label.configure(text=Wdir_path)
        self.WDir_label.configure(background='lavenderblush1')

    def writeSNX1_DSSAT(self):   #write 'n' numbers of SNX files for 'n' days of possible planting dates
        #check errorneous cases for fertilizer
        if Fbutton1.get() == 1 :  #fertilizer button is selected
            if self.label006.cget("text") == 'N/A':
                self.fertilizer_err.activate()
        #check errorneous cases for  irrigation 
        if IRbutton.get() == 0 :  #Automatic when required is selected
            print("This auto-irrigation option was deleted")
            # if self.label402.cget("text") == 'N/A':
            #     self.irrigation_err.activate()
        if IRbutton.get() == 2:  #On Reported dates is selected
            if self.label105.cget("text") == 'N/A':
                self.irrigation_err.activate()
        if IRbutton.get() == 1:  #automatic (periodic) irrigation for every n days
            if self.label418.cget("text") == 'N/A':
                self.irrigation_err.activate()

        if self.name1.getvalue() == "":
            self.writeSNX_err.activate()
        else:
            sname=self.name1.getvalue()
            WSTA=self.Wstation.getvalue()[0][0:4] #'SANJ'
            WTD_fname=Wdir_path.replace("/","\\") + "\\"+ WSTA + ".WTD"
            WTD_last_date = find_obs_date (WTD_fname)

            start_year=self.startyear.getvalue() #'2002'
            obs_flag = 0  #by default, assumes no obs weather data is available
            #=============================================
            #Determine pdate_1[YYYYDOY] and pdate_2[YYYYDOY]
            lst = list(self.plt_button1.state())  #Year 1 for planting
            lst2 = list(self.plt_button2.state())   #Year 2 for planting
            lst_frst = list(self.scf_button1.state()) #Year 1 for SCF
            lst_frst2 = list(self.scf_button2.state())
            pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m = find_plt_date (start_year, lst, lst2, lst_frst, lst_frst2)

            if WTD_last_date >= hv_date: #180 is arbitrary number to fully cover weather data until late maturity
                obs_flag = 1
            self.pdate11.configure(text=repr(pdate_1),background='honeydew1')
            self.pdate21.configure(text=repr(pdate_2),background='honeydew1')
            step = int(self.step1.getvalue())#5 days distance for visualizing boxplots
            #cr_type='RI'
            self.label34.configure(text='RI',background='honeydew1')
            SNX_list = []
            os.chdir(Wdir_path)  #change directory
            param_fname=Wdir_path.replace("/","\\") + "\\param_RI_SNX.txt"
            if pdate_2%1000 > pdate_1%1000:
                for iday in range(pdate_1,pdate_2+1,step):   
                    self.write_param_main(param_fname,self.Wstation.getvalue()[0][0:8], sname, iday, obs_flag)  # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_FRST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\R"+repr(iday%1000).zfill(3)+sname+".SNX"
                    SNX_list.append(SNX_fname)
            else: #if planting window covers two consecutive years
                #go through Year 1
                end_day = int(start_year)*1000 + 365
                if calendar.isleap(int(start_year)):  end_day = int(start_year)*1000 + 366
                for iday in range(pdate_1,end_day + 1,step):   
                    self.write_param_main(param_fname,self.Wstation.getvalue()[0][0:8], sname, iday, obs_flag)  # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_FRST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\R"+repr(iday%1000).zfill(3)+sname+".SNX"   
                    SNX_list.append(SNX_fname) 
                    temp_date = iday
                #next, go throught Year 2
                start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 365  #e.g, 364 + 5 - 365 = DOY 4
                if calendar.isleap(int(start_year)):  start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 366
                for iday in range(start_day,pdate_2 + 1,step):   #e.g, 2018004 to 2018030
                    self.write_param_main(param_fname,self.Wstation.getvalue()[0][0:8], sname, iday, obs_flag)  # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_FRST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\R"+repr(iday%1000).zfill(3)+sname+".SNX"   
                    SNX_list.append(SNX_fname)                              
            #=============================================
            # Write scenario summary into a text file 
            # => this can be also used for input for Create_WTH subroutine
            #=============================================
            LAT, LONG, ELEV, TAV, AMP = find_station_info(WSTA)  #Call a function to find more info about station to write header in *.WTH
            summary_fname=Wdir_path.replace("/","\\") + "\\" + sname+"_summary.txt"
            f = open(summary_fname,"w") 
            working_dir=Wdir_path.replace("/","\\")
            f.write("Directory:  " + working_dir + "\n")
            f.write("Station_name: " + WSTA + " \n")
            f.write("LAT: " + repr(LAT) + " \n")
            f.write("LONG: " + repr(LONG) + " \n")
            f.write("ELEV: " + repr(ELEV) + " \n")
            f.write("TAV: " + repr(TAV) + " \n")
            f.write("AMP: " + repr(AMP) + " \n")
            f.write("First_Plt_Date: " + repr(pdate_1) + " \n")
            f.write("Last_Plt_Date: " + repr(pdate_2) + " \n")
            f.write("Harvest_Date: " + repr(hv_date) + " \n")
            f.write("frst_start_Date: " + repr(frst_date1) + " \n")
            f.write("Planting_window: " + repr(pwindow_m) + " \n")
            f.write("SCF_window: " + repr(fwindow_m) + " \n")
            # f.write("Cultivar: " + self.cul_type.getvalue()[0][0:18] + " \n")  #INGENO=self.cul_type.getvalue()[0][0:18] #cultivar type
            # f.write("Planting_density: " + self.plt_density.getvalue()[0:3] + " \n")  # PPOP=self.plt_density.getvalue()[0:3] #planting density
            # f.write("Soil_type: " +self.soil_type.getvalue()[0][0:10]  + " \n")  #ID_SOIL=self.soil_type.getvalue()[0][0:10]
            # f.write("Irrigation_A(1)_N(0): " + repr(IRbutton.get()) + " \n")    #automatic or no-irrigation
            # if IRbutton.get() == 1:
            #     f.write("IRR_method: " + self.irr_method.getvalue()[0][0:5] + " \n") 
            # f.write("Fertilizer_Y(1)_N(0): " + repr(Fbutton1.get()) + " \n")  
            # if Fbutton1.get() == 1:
            #     f.write("FDATE1: " + self.label006.cget("text") + " \n")  
            #     f.write("FDATE2: " + self.label011.cget("text") + " \n")
            #     f.write("FDATE3: " + self.label016.cget("text") + " \n")
            #     f.write("F_amount1: " + self.label007.cget("text")  + " \n")
            #     f.write("F_amount2: " + self.label012.cget("text") + " \n")
            #     f.write("F_amount3: " + self.label017.cget("text") + " \n")
            #     f.write("F_method1: " + self.label008.cget("text")[0:5] + " \n")
            #     f.write("F_method2: " + self.label013.cget("text")[0:5] + " \n")
            #     f.write("F_method3: " + self.label018.cget("text")[0:5] + " \n")
    
            f.close()
            #=============================================
            # Write SCF information in the Page 1 into *.csv file to feed to WGEN
            #=============================================
            list_BN = [self.BN_11.getvalue(), self.BN_12.getvalue(), self.BN_13.getvalue(), self.BN_14.getvalue(),
                    self.BN_15.getvalue(), self.BN_16.getvalue(), self.BN_17.getvalue(), self.BN_18.getvalue(),
                    self.BN_19.getvalue(), self.BN_110.getvalue(), self.BN_111.getvalue(), self.BN_112.getvalue(),
                    self.BN_21.getvalue(), self.BN_22.getvalue(), self.BN_23.getvalue(), self.BN_24.getvalue(),
                    self.BN_25.getvalue(), self.BN_26.getvalue(), self.BN_27.getvalue(), self.BN_28.getvalue(),
                    self.BN_29.getvalue(), self.BN_210.getvalue(), self.BN_211.getvalue(), self.BN_212.getvalue()]
            list_NN = [self.NN_label2.cget("text"), self.NN_label3.cget("text"), self.NN_label4.cget("text"), self.NN_label5.cget("text"),
                    self.NN_label6.cget("text"), self.NN_label7.cget("text"), self.NN_label8.cget("text"), self.NN_label9.cget("text"),
                    self.NN_label10.cget("text"), self.NN_label11.cget("text"), self.NN_label12.cget("text"), self.NN_label13.cget("text"),
                    self.NN_label14.cget("text"), self.NN_label15.cget("text"), self.NN_label16.cget("text"), self.NN_label17.cget("text"),
                    self.NN_label18.cget("text"), self.NN_label19.cget("text"), self.NN_label20.cget("text"), self.NN_label21.cget("text"),
                    self.NN_label22.cget("text"), self.NN_label23.cget("text"), self.NN_label24.cget("text"), self.NN_label25.cget("text")]
            list_AN = [self.AN_11.getvalue(), self.AN_12.getvalue(), self.AN_13.getvalue(), self.AN_14.getvalue(),
                    self.AN_15.getvalue(), self.AN_16.getvalue(), self.AN_17.getvalue(), self.AN_18.getvalue(),
                    self.AN_19.getvalue(), self.AN_110.getvalue(), self.AN_111.getvalue(), self.AN_112.getvalue(),
                    self.AN_21.getvalue(), self.AN_22.getvalue(), self.AN_23.getvalue(), self.AN_24.getvalue(),
                    self.AN_25.getvalue(), self.AN_26.getvalue(), self.AN_27.getvalue(), self.AN_28.getvalue(),
                    self.AN_29.getvalue(), self.AN_210.getvalue(), self.AN_211.getvalue(), self.AN_212.getvalue()]

            temp_csv=Wdir_path.replace("/","\\") + "\\SCF_input_monthly1.csv" 
            with open(temp_csv, 'w', newline='') as csvfile:
                scfwriter = csv.writer(csvfile, delimiter=',',
                                        quotechar='|', quoting=csv.QUOTE_MINIMAL)
                scfwriter.writerow(['Month','1','2', '3', '4','5','6','7','8','9','10','11','12'])
                scfwriter.writerow(['BN'] + list_BN[:12])
                scfwriter.writerow(['NN'] + list_NN[:12])
                scfwriter.writerow(['AN'] + list_AN[:12])
            temp_csv=Wdir_path.replace("/","\\") + "\\SCF_input_monthly2.csv" 
            with open(temp_csv, 'w', newline='') as csvfile:
                scfwriter = csv.writer(csvfile, delimiter=',',
                                        quotechar='|', quoting=csv.QUOTE_MINIMAL)
                scfwriter.writerow(['Month','1','2', '3', '4','5','6','7','8','9','10','11','12'])
                scfwriter.writerow(['BN'] + list_BN[12:])
                scfwriter.writerow(['NN'] + list_NN[12:])
                scfwriter.writerow(['AN'] + list_AN[12:])

            #print(list(self.plt_button1.state()), list(self.plt_button2.state()))
            temp_snx=Wdir_path.replace("/","\\") + "\\button_write.csv" 
            with open(temp_snx, 'w', newline='') as csvfile:
                scfwriter = csv.writer(csvfile, delimiter=',',
                                        quotechar='|', quoting=csv.QUOTE_MINIMAL)
                scfwriter.writerow(['Month','J1','F1', 'M1', 'A1','M1','J1','J1','A1','S1','O1','N1','D1', 'J2','F2', 'M2', 'A2','M2','J2','J2','A2','S2','O2','N2','D2'])
                scfwriter.writerow(['SCF'] + list(self.scf_button1.state()) + list(self.scf_button2.state()))
                scfwriter.writerow(['plt'] + list(self.plt_button1.state()) + list(self.plt_button2.state()))
##            scfwriter.writerow(self.scf_button1.state())
            #=============================================
            # Generate 100 weather realizations
            #=============================================
            # Get a list of all files in directory
            for rootDir, subdirs, filenames in os.walk(Wdir_path):
                # Find the files that matches the given patterm
                for filename in fnmatch.filter(filenames, '0*.WTH'):
                    try:
                        os.remove(os.path.join(rootDir, filename))
                    except OSError:
                        print("Error while deleting file")
            create_WTH_main(Wdir_path,WTD_fname, WSTA, LAT, LONG, ELEV, TAV, AMP, pdate_1, pdate_2, step, hv_date, frst_date1)
            #=============================================
            # Create DSSBatch.v47 file and run DSSAT executable 
            #=============================================
            entries = ("PlantGro.OUT","Evaluate.OUT","SCF_input_monthly1.csv", "SCF_input_monthly2.csv", "FloodW.out", "FloodN.out",
                     "ET.OUT","OVERVIEW.OUT","PlantN.OUT","SoilNi.OUT","Weather.OUT", 
                    "SolNBalSum.OUT","SoilNoBal.OUT","SoilTemp.OUT","SoilWat.OUT","SoilWatBal.OUT","Summary.OUT") 
            self.writeV47_main(SNX_list, obs_flag)   ##write *.V47 batch file
            #==RUN DSSAT with ARGUMENT 
            start_time = time.perf_counter() # => not compatible with Python 2.7
            os.chdir(Wdir_path)  #change directory
            args = "DSCSM047.EXE N DSSBatch.v47 "
            subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
            end_time0 = time.perf_counter() # => not compatible with Python 2.7
            print('It took {0:5.1f} sec to finish 100 weather realizations x n planting dates'.format(end_time0-start_time))
            start_time1 = time.perf_counter()  # => not compatible with Python 2.7

            #create a new folder to save outputs of the target scenario
            new_folder=self.name1.getvalue()+"_output"
            if os.path.exists(new_folder):
                shutil.rmtree(new_folder)   #remove existing folder
            os.makedirs(new_folder)
            #copy outputs to the new folder
            dest_dir=Wdir_path + "\\"+new_folder
           # print 'dest_dir is', dest_dir
            for entry in entries:
                source_file=Wdir_path + "\\" + entry
                if os.path.isfile(source_file):
                    shutil.move(source_file, dest_dir)
                else:
                    print( '**Error!!- No DSSAT output files => Check if DSSAT simuation was successful')

            #move *.SNX file to the new folder with output files
            for i in range(len(SNX_list)):
                shutil.move(SNX_list[i], dest_dir)
            shutil.move(summary_fname, dest_dir)
          #  temp = "WGEN_out_" + WSTA + repr(pdate_1//1000)+".txt"
            shutil.move("WGEN_input_param_month_" + WSTA + ".txt", dest_dir)
            shutil.move("WGEN_out_" + WSTA + repr(pdate_1//1000)+".txt", dest_dir)
            #=============================================
            # Make a boxplot and exceedance curve of yield and other output
            #=============================================
            self.Yield_boxplot(sname, SNX_list, start_year, obs_flag)
            #=============================================
            # Make plots of water/nitrogen stresss
            #=============================================
            self.WSGD_plot(sname, SNX_list, start_year, obs_flag)
            #=============================================
            # Make plots of weather time series (Tmin, Tmax, Srad)
            #=============================================
            self.weather_plot(sname, SNX_list, start_year, obs_flag)
            #=============================================
            # Make plots of cumulative rainfall time series
            #=============================================
            self.plot_cum_rain(sname, SNX_list, start_year, obs_flag)
            print('It took {0:5.1f} sec to finish 100 simulations & to post-process outputs'.format(end_time0-start_time))
            print ("** Yeah!! DSSAT Simulation has been finished successfully!")
            print ("** Please check all output and figures in your working directory.")

    def writeSNX2_DSSAT(self):
        #check errorneous cases for fertilizer
        if Fbutton1.get() == 1 :  #fertilizer button is selected
            if self.label006.cget("text") == 'N/A':
                self.fertilizer_err.activate()
        #check errorneous cases for  irrigation 
        if IRbutton.get() == 0 :  #Automatic when required is selected
            print("This auto-irrigation option was deleted")
            # if self.label402.cget("text") == 'N/A':
            #     self.irrigation_err.activate()
        if IRbutton.get() == 2:  #On Reported dates is selected
            if self.label105.cget("text") == 'N/A':
                self.irrigation_err.activate()
        if IRbutton.get() == 1:  #automatic (periodic) irrigation for every n days
            if self.label418.cget("text") == 'N/A':
                self.irrigation_err.activate()

        if self.name2.getvalue() == "":
            self.writeSNX_err.activate()
        else:
            sname=self.name2.getvalue()
            WSTA=self.Wstation.getvalue()[0][0:4] #'SANJ'
            WTD_fname=Wdir_path.replace("/","\\") + "\\"+ WSTA + ".WTD"
            WTD_last_date = find_obs_date (WTD_fname)

            start_year=self.startyear.getvalue() #'2002'
            obs_flag = 0  #by default, assumes no obs weather data is available
            #=============================================
            #Determine pdate_1[YYYYDOY] and pdate_2[YYYYDOY]
            lst = list(self.plt_button1.state())  #Year 1 for planting
            lst2 = list(self.plt_button2.state())   #Year 2 for planting
            lst_frst = list(self.scf_button1.state()) #Year 1 for SCF
            lst_frst2 = list(self.scf_button2.state())
            pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m = find_plt_date (start_year, lst, lst2, lst_frst, lst_frst2)

            if WTD_last_date >= hv_date: #180 is arbitrary number to fully cover weather data until late maturity
                obs_flag = 1
            self.pdate12.configure(text=repr(pdate_1),background='honeydew1')
            self.pdate22.configure(text=repr(pdate_2),background='honeydew1')
            step = int(self.step2.getvalue())#5 days distance for visualizing boxplots
            #cr_type='RI'
            self.label35.configure(text='RI',background='honeydew1')
            SNX_list = []
            os.chdir(Wdir_path)  #change directory
            param_fname=Wdir_path.replace("/","\\") + "\\param_RI_SNX.txt"
            if pdate_2%1000 > pdate_1%1000:
                for iday in range(pdate_1,pdate_2+1,step):   
                    self.write_param_main(param_fname,self.Wstation.getvalue()[0][0:8], sname, iday, obs_flag)  # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_FRST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\R"+repr(iday%1000).zfill(3)+sname+".SNX"
                    SNX_list.append(SNX_fname)
            else: #if planting window covers two consecutive years
                #go through Year 1
                end_day = int(start_year)*1000 + 365
                if calendar.isleap(int(start_year)):  end_day = int(start_year)*1000 + 366
                for iday in range(pdate_1,end_day + 1,step):   
                    self.write_param_main(param_fname,self.Wstation.getvalue()[0][0:8], sname, iday, obs_flag)  # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_FRST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\R"+repr(iday%1000).zfill(3)+sname+".SNX"   
                    SNX_list.append(SNX_fname) 
                    temp_date = iday
                #next, go throught Year 2
                start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 365  #e.g, 364 + 5 - 365 = DOY 4
                if calendar.isleap(int(start_year)):  start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 366
                for iday in range(start_day,pdate_2 + 1,step):   #e.g, 2018004 to 2018030
                    self.write_param_main(param_fname,self.Wstation.getvalue()[0][0:8], sname, iday, obs_flag)  # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_FRST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\R"+repr(iday%1000).zfill(3)+sname+".SNX"   
                    SNX_list.append(SNX_fname)                                                       
            #=============================================
            # Write scenario summary into a text file 
            # => this can be also used for input for Create_WTH subroutine
            #=============================================
            LAT, LONG, ELEV, TAV, AMP = find_station_info(WSTA)  #Call a function to find more info about station to write header in *.WTH
            summary_fname=Wdir_path.replace("/","\\") + "\\" + sname+"_summary.txt"
            f = open(summary_fname,"w") 
            working_dir=Wdir_path.replace("/","\\")
            f.write("Directory:  " + working_dir + "\n")
            f.write("Station_name: " + WSTA + " \n")
            f.write("LAT: " + repr(LAT) + " \n")
            f.write("LONG: " + repr(LONG) + " \n")
            f.write("ELEV: " + repr(ELEV) + " \n")
            f.write("TAV: " + repr(TAV) + " \n")
            f.write("AMP: " + repr(AMP) + " \n")
            f.write("First_Plt_Date: " + repr(pdate_1) + " \n")
            f.write("Last_Plt_Date: " + repr(pdate_2) + " \n")
            f.write("Harvest_Date: " + repr(hv_date) + " \n")
            f.write("frst_start_Date: " + repr(frst_date1) + " \n")
            f.write("Planting_window: " + repr(pwindow_m) + " \n")
            f.write("SCF_window: " + repr(fwindow_m) + " \n")
            # f.write("Cultivar: " + self.cul_type.getvalue()[0][0:18] + " \n")  #INGENO=self.cul_type.getvalue()[0][0:18] #cultivar type
            # f.write("Planting_density: " + self.plt_density.getvalue()[0:3] + " \n")  # PPOP=self.plt_density.getvalue()[0:3] #planting density
            # f.write("Soil_type: " +self.soil_type.getvalue()[0][0:10]  + " \n")  #ID_SOIL=self.soil_type.getvalue()[0][0:10]
            # f.write("Irrigation_A(1)_N(0): " + repr(IRbutton.get()) + " \n")    #automatic or no-irrigation
            # if IRbutton.get() == 1:
            #     f.write("IRR_method: " + self.irr_method.getvalue()[0][0:5] + " \n") 
            # f.write("Fertilizer_Y(1)_N(0): " + repr(Fbutton1.get()) + " \n")  
            # if Fbutton1.get() == 1:
            #     f.write("FDATE1: " + self.label006.cget("text") + " \n")  
            #     f.write("FDATE2: " + self.label011.cget("text") + " \n")
            #     f.write("FDATE3: " + self.label016.cget("text") + " \n")
            #     f.write("F_amount1: " + self.label007.cget("text")  + " \n")
            #     f.write("F_amount2: " + self.label012.cget("text") + " \n")
            #     f.write("F_amount3: " + self.label017.cget("text") + " \n")
            #     f.write("F_method1: " + self.label008.cget("text")[0:5] + " \n")
            #     f.write("F_method2: " + self.label013.cget("text")[0:5] + " \n")
            #     f.write("F_method3: " + self.label018.cget("text")[0:5] + " \n")
    
            f.close()
            #=============================================
            # Write SCF information in the Page 1 into *.csv file to feed to WGEN
            #=============================================
            list_BN = [self.BN_11.getvalue(), self.BN_12.getvalue(), self.BN_13.getvalue(), self.BN_14.getvalue(),
                    self.BN_15.getvalue(), self.BN_16.getvalue(), self.BN_17.getvalue(), self.BN_18.getvalue(),
                    self.BN_19.getvalue(), self.BN_110.getvalue(), self.BN_111.getvalue(), self.BN_112.getvalue(),
                    self.BN_21.getvalue(), self.BN_22.getvalue(), self.BN_23.getvalue(), self.BN_24.getvalue(),
                    self.BN_25.getvalue(), self.BN_26.getvalue(), self.BN_27.getvalue(), self.BN_28.getvalue(),
                    self.BN_29.getvalue(), self.BN_210.getvalue(), self.BN_211.getvalue(), self.BN_212.getvalue()]
            list_NN = [self.NN_label2.cget("text"), self.NN_label3.cget("text"), self.NN_label4.cget("text"), self.NN_label5.cget("text"),
                    self.NN_label6.cget("text"), self.NN_label7.cget("text"), self.NN_label8.cget("text"), self.NN_label9.cget("text"),
                    self.NN_label10.cget("text"), self.NN_label11.cget("text"), self.NN_label12.cget("text"), self.NN_label13.cget("text"),
                    self.NN_label14.cget("text"), self.NN_label15.cget("text"), self.NN_label16.cget("text"), self.NN_label17.cget("text"),
                    self.NN_label18.cget("text"), self.NN_label19.cget("text"), self.NN_label20.cget("text"), self.NN_label21.cget("text"),
                    self.NN_label22.cget("text"), self.NN_label23.cget("text"), self.NN_label24.cget("text"), self.NN_label25.cget("text")]
            list_AN = [self.AN_11.getvalue(), self.AN_12.getvalue(), self.AN_13.getvalue(), self.AN_14.getvalue(),
                    self.AN_15.getvalue(), self.AN_16.getvalue(), self.AN_17.getvalue(), self.AN_18.getvalue(),
                    self.AN_19.getvalue(), self.AN_110.getvalue(), self.AN_111.getvalue(), self.AN_112.getvalue(),
                    self.AN_21.getvalue(), self.AN_22.getvalue(), self.AN_23.getvalue(), self.AN_24.getvalue(),
                    self.AN_25.getvalue(), self.AN_26.getvalue(), self.AN_27.getvalue(), self.AN_28.getvalue(),
                    self.AN_29.getvalue(), self.AN_210.getvalue(), self.AN_211.getvalue(), self.AN_212.getvalue()]

            temp_csv=Wdir_path.replace("/","\\") + "\\SCF_input_monthly1.csv" 
            with open(temp_csv, 'w', newline='') as csvfile:
                scfwriter = csv.writer(csvfile, delimiter=',',
                                        quotechar='|', quoting=csv.QUOTE_MINIMAL)
                scfwriter.writerow(['Month','1','2', '3', '4','5','6','7','8','9','10','11','12'])
                scfwriter.writerow(['BN'] + list_BN[:12])
                scfwriter.writerow(['NN'] + list_NN[:12])
                scfwriter.writerow(['AN'] + list_AN[:12])
            temp_csv=Wdir_path.replace("/","\\") + "\\SCF_input_monthly2.csv" 
            with open(temp_csv, 'w', newline='') as csvfile:
                scfwriter = csv.writer(csvfile, delimiter=',',
                                        quotechar='|', quoting=csv.QUOTE_MINIMAL)
                scfwriter.writerow(['Month','1','2', '3', '4','5','6','7','8','9','10','11','12'])
                scfwriter.writerow(['BN'] + list_BN[12:])
                scfwriter.writerow(['NN'] + list_NN[12:])
                scfwriter.writerow(['AN'] + list_AN[12:])

            #print(list(self.plt_button1.state()), list(self.plt_button2.state()))
            temp_snx=Wdir_path.replace("/","\\") + "\\button_write.csv" 
            with open(temp_snx, 'w', newline='') as csvfile:
                scfwriter = csv.writer(csvfile, delimiter=',',
                                        quotechar='|', quoting=csv.QUOTE_MINIMAL)
                scfwriter.writerow(['Month','J1','F1', 'M1', 'A1','M1','J1','J1','A1','S1','O1','N1','D1', 'J2','F2', 'M2', 'A2','M2','J2','J2','A2','S2','O2','N2','D2'])
                scfwriter.writerow(['SCF'] + list(self.scf_button1.state()) + list(self.scf_button2.state()))
                scfwriter.writerow(['plt'] + list(self.plt_button1.state()) + list(self.plt_button2.state()))
##            scfwriter.writerow(self.scf_button1.state())
            #=============================================
            # Generate 100 weather realizations
            #=============================================
            # Get a list of all files in directory
            for rootDir, subdirs, filenames in os.walk(Wdir_path):
                # Find the files that matches the given patterm
                for filename in fnmatch.filter(filenames, '0*.WTH'):
                    try:
                        os.remove(os.path.join(rootDir, filename))
                    except OSError:
                        print("Error while deleting file")
            create_WTH_main(Wdir_path,WTD_fname, WSTA, LAT, LONG, ELEV, TAV, AMP, pdate_1, pdate_2, step, hv_date, frst_date1)
            #=============================================
            # Create DSSBatch.v47 file and run DSSAT executable 
            #=============================================
            entries = ("PlantGro.OUT","Evaluate.OUT","SCF_input_monthly1.csv", "SCF_input_monthly2.csv", "FloodW.out", "FloodN.out",
                     "ET.OUT","OVERVIEW.OUT","PlantN.OUT","SoilNi.OUT","Weather.OUT", 
                    "SolNBalSum.OUT","SoilNoBal.OUT","SoilTemp.OUT","SoilWat.OUT","SoilWatBal.OUT","Summary.OUT") 
            self.writeV47_main(SNX_list, obs_flag)   ##write *.V47 batch file
            #==RUN DSSAT with ARGUMENT 
            start_time = time.perf_counter() # => not compatible with Python 2.7
            os.chdir(Wdir_path)  #change directory
            args = "DSCSM047.EXE N DSSBatch.v47 "
            subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
            end_time0 = time.perf_counter() # => not compatible with Python 2.7
            print('It took {0:5.1f} sec to finish 100 weather realizations x n planting dates'.format(end_time0-start_time))
            start_time1 = time.perf_counter()  # => not compatible with Python 2.7

            #create a new folder to save outputs of the target scenario
            new_folder=self.name2.getvalue()+"_output"
            if os.path.exists(new_folder):
                shutil.rmtree(new_folder)   #remove existing folder
            os.makedirs(new_folder)
            #copy outputs to the new folder
            dest_dir=Wdir_path + "\\"+new_folder
           # print 'dest_dir is', dest_dir
            for entry in entries:
                source_file=Wdir_path + "\\" + entry
                if os.path.isfile(source_file):
                    shutil.move(source_file, dest_dir)
                else:
                    print( '**Error!!- No DSSAT output files => Check if DSSAT simuation was successful')

            #move *.SNX file to the new folder with output files
            for i in range(len(SNX_list)):
                shutil.move(SNX_list[i], dest_dir)
            shutil.move(summary_fname, dest_dir)
          #  temp = "WGEN_out_" + WSTA + repr(pdate_1//1000)+".txt"
            shutil.move("WGEN_input_param_month_" + WSTA + ".txt", dest_dir)
            shutil.move("WGEN_out_" + WSTA + repr(pdate_1//1000)+".txt", dest_dir)
            #=============================================
            # Make a boxplot and exceedance curve of yield and other output
            #=============================================
            self.Yield_boxplot(sname, SNX_list, start_year, obs_flag)
            #=============================================
            # Make plots of water/nitrogen stresss
            #=============================================
            self.WSGD_plot(sname, SNX_list, start_year, obs_flag)
            #=============================================
            # Make plots of weather time series (Tmin, Tmax, Srad)
            #=============================================
            self.weather_plot(sname, SNX_list, start_year, obs_flag)
            #=============================================
            # Make plots of cumulative rainfall time series
            #=============================================
            self.plot_cum_rain(sname, SNX_list, start_year, obs_flag)
            print('It took {0:5.1f} sec to finish 100 simulations & to post-process outputs'.format(end_time0-start_time))
            print ("** Yeah!! DSSAT Simulation has been finished successfully!")
            print ("** Please check all output and figures in your working directory.")

    def writeSNX3_DSSAT(self):
        #check errorneous cases for fertilizer
        if Fbutton1.get() == 1 :  #fertilizer button is selected
            if self.label006.cget("text") == 'N/A':
                self.fertilizer_err.activate()
        #check errorneous cases for  irrigation 
        if IRbutton.get() == 0 :  #Automatic when required is selected
            print("This auto-irrigation option was deleted")
            # if self.label402.cget("text") == 'N/A':
            #     self.irrigation_err.activate()
        if IRbutton.get() == 2:  #On Reported dates is selected
            if self.label105.cget("text") == 'N/A':
                self.irrigation_err.activate()
        if IRbutton.get() == 1:  #automatic (periodic) irrigation for every n days
            if self.label418.cget("text") == 'N/A':
                self.irrigation_err.activate()

        if self.name3.getvalue() == "":
            self.writeSNX_err.activate()
        else:
            sname=self.name3.getvalue()
            WSTA=self.Wstation.getvalue()[0][0:4] #'SANJ'
            WTD_fname=Wdir_path.replace("/","\\") + "\\"+ WSTA + ".WTD"
            WTD_last_date = find_obs_date (WTD_fname)

            # pdate_1 = int(self.pdate1.getvalue())  #EJ(3/12/2019)
            # pdate_2 = int(self.pdate2.getvalue())
            start_year=self.startyear.getvalue() #'2002'
            obs_flag = 0  #by default, assumes no obs weather data is available
            #=============================================
            #Determine pdate_1[YYYYDOY] and pdate_2[YYYYDOY]
            lst = list(self.plt_button1.state())  #Year 1 for planting
            lst2 = list(self.plt_button2.state())   #Year 2 for planting
            lst_frst = list(self.scf_button1.state()) #Year 1 for SCF
            lst_frst2 = list(self.scf_button2.state())
            pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m = find_plt_date (start_year, lst, lst2, lst_frst, lst_frst2)

            if WTD_last_date >= hv_date: #180 is arbitrary number to fully cover weather data until late maturity
                obs_flag = 1
            self.pdate13.configure(text=repr(pdate_1),background='honeydew1')
            self.pdate23.configure(text=repr(pdate_2),background='honeydew1')
            step = int(self.step3.getvalue())#5 days distance for visualizing boxplots
            #cr_type='RI'
            self.label36.configure(text='RI',background='honeydew1')
            SNX_list = []
            os.chdir(Wdir_path)  #change directory
            param_fname=Wdir_path.replace("/","\\") + "\\param_RI_SNX.txt"
            if pdate_2%1000 > pdate_1%1000:
                for iday in range(pdate_1,pdate_2+1,step):   
                    self.write_param_main(param_fname,self.Wstation.getvalue()[0][0:8], sname, iday, obs_flag)  # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_FRST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\R"+repr(iday%1000).zfill(3)+sname+".SNX"
                    SNX_list.append(SNX_fname)
            else: #if planting window covers two consecutive years
                #go through Year 1
                end_day = int(start_year)*1000 + 365
                if calendar.isleap(int(start_year)):  end_day = int(start_year)*1000 + 366
                for iday in range(pdate_1,end_day + 1,step):   
                    self.write_param_main(param_fname,self.Wstation.getvalue()[0][0:8], sname, iday, obs_flag)  # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_FRST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\R"+repr(iday%1000).zfill(3)+sname+".SNX"   
                    SNX_list.append(SNX_fname) 
                    temp_date = iday
                #next, go throught Year 2
                start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 365  #e.g, 364 + 5 - 365 = DOY 4
                if calendar.isleap(int(start_year)):  start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 366
                for iday in range(start_day,pdate_2 + 1,step):   #e.g, 2018004 to 2018030
                    self.write_param_main(param_fname,self.Wstation.getvalue()[0][0:8], sname, iday, obs_flag)  # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_FRST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\R"+repr(iday%1000).zfill(3)+sname+".SNX"   
                    SNX_list.append(SNX_fname)                                  
            #=============================================
            # Write scenario summary into a text file 
            # => this can be also used for input for Create_WTH subroutine
            #=============================================
            LAT, LONG, ELEV, TAV, AMP = find_station_info(WSTA)  #Call a function to find more info about station to write header in *.WTH
            summary_fname=Wdir_path.replace("/","\\") + "\\" + sname+"_summary.txt"
            f = open(summary_fname,"w") 
            working_dir=Wdir_path.replace("/","\\")
            f.write("Directory:  " + working_dir + "\n")
            f.write("Station_name: " + WSTA + " \n")
            f.write("LAT: " + repr(LAT) + " \n")
            f.write("LONG: " + repr(LONG) + " \n")
            f.write("ELEV: " + repr(ELEV) + " \n")
            f.write("TAV: " + repr(TAV) + " \n")
            f.write("AMP: " + repr(AMP) + " \n")
            f.write("First_Plt_Date: " + repr(pdate_1) + " \n")
            f.write("Last_Plt_Date: " + repr(pdate_2) + " \n")
            f.write("Harvest_Date: " + repr(hv_date) + " \n")
            f.write("frst_start_Date: " + repr(frst_date1) + " \n")
            f.write("Planting_window: " + repr(pwindow_m) + " \n")
            f.write("SCF_window: " + repr(fwindow_m) + " \n")
            # f.write("Cultivar: " + self.cul_type.getvalue()[0][0:18] + " \n")  #INGENO=self.cul_type.getvalue()[0][0:18] #cultivar type
            # f.write("Planting_density: " + self.plt_density.getvalue()[0:3] + " \n")  # PPOP=self.plt_density.getvalue()[0:3] #planting density
            # f.write("Soil_type: " +self.soil_type.getvalue()[0][0:10]  + " \n")  #ID_SOIL=self.soil_type.getvalue()[0][0:10]
            # f.write("Irrigation_A(1)_N(0): " + repr(IRbutton.get()) + " \n")    #automatic or no-irrigation
            # if IRbutton.get() == 1:
            #     f.write("IRR_method: " + self.irr_method.getvalue()[0][0:5] + " \n") 
            # f.write("Fertilizer_Y(1)_N(0): " + repr(Fbutton1.get()) + " \n")  
            # if Fbutton1.get() == 1:
            #     f.write("FDATE1: " + self.label006.cget("text") + " \n")  
            #     f.write("FDATE2: " + self.label011.cget("text") + " \n")
            #     f.write("FDATE3: " + self.label016.cget("text") + " \n")
            #     f.write("F_amount1: " + self.label007.cget("text")  + " \n")
            #     f.write("F_amount2: " + self.label012.cget("text") + " \n")
            #     f.write("F_amount3: " + self.label017.cget("text") + " \n")
            #     f.write("F_method1: " + self.label008.cget("text")[0:5] + " \n")
            #     f.write("F_method2: " + self.label013.cget("text")[0:5] + " \n")
            #     f.write("F_method3: " + self.label018.cget("text")[0:5] + " \n")
    
            f.close()
            #=============================================
            # Write SCF information in the Page 1 into *.csv file to feed to WGEN
            #=============================================
            list_BN = [self.BN_11.getvalue(), self.BN_12.getvalue(), self.BN_13.getvalue(), self.BN_14.getvalue(),
                    self.BN_15.getvalue(), self.BN_16.getvalue(), self.BN_17.getvalue(), self.BN_18.getvalue(),
                    self.BN_19.getvalue(), self.BN_110.getvalue(), self.BN_111.getvalue(), self.BN_112.getvalue(),
                    self.BN_21.getvalue(), self.BN_22.getvalue(), self.BN_23.getvalue(), self.BN_24.getvalue(),
                    self.BN_25.getvalue(), self.BN_26.getvalue(), self.BN_27.getvalue(), self.BN_28.getvalue(),
                    self.BN_29.getvalue(), self.BN_210.getvalue(), self.BN_211.getvalue(), self.BN_212.getvalue()]
            list_NN = [self.NN_label2.cget("text"), self.NN_label3.cget("text"), self.NN_label4.cget("text"), self.NN_label5.cget("text"),
                    self.NN_label6.cget("text"), self.NN_label7.cget("text"), self.NN_label8.cget("text"), self.NN_label9.cget("text"),
                    self.NN_label10.cget("text"), self.NN_label11.cget("text"), self.NN_label12.cget("text"), self.NN_label13.cget("text"),
                    self.NN_label14.cget("text"), self.NN_label15.cget("text"), self.NN_label16.cget("text"), self.NN_label17.cget("text"),
                    self.NN_label18.cget("text"), self.NN_label19.cget("text"), self.NN_label20.cget("text"), self.NN_label21.cget("text"),
                    self.NN_label22.cget("text"), self.NN_label23.cget("text"), self.NN_label24.cget("text"), self.NN_label25.cget("text")]
            list_AN = [self.AN_11.getvalue(), self.AN_12.getvalue(), self.AN_13.getvalue(), self.AN_14.getvalue(),
                    self.AN_15.getvalue(), self.AN_16.getvalue(), self.AN_17.getvalue(), self.AN_18.getvalue(),
                    self.AN_19.getvalue(), self.AN_110.getvalue(), self.AN_111.getvalue(), self.AN_112.getvalue(),
                    self.AN_21.getvalue(), self.AN_22.getvalue(), self.AN_23.getvalue(), self.AN_24.getvalue(),
                    self.AN_25.getvalue(), self.AN_26.getvalue(), self.AN_27.getvalue(), self.AN_28.getvalue(),
                    self.AN_29.getvalue(), self.AN_210.getvalue(), self.AN_211.getvalue(), self.AN_212.getvalue()]

            temp_csv=Wdir_path.replace("/","\\") + "\\SCF_input_monthly1.csv" 
            with open(temp_csv, 'w', newline='') as csvfile:
                scfwriter = csv.writer(csvfile, delimiter=',',
                                        quotechar='|', quoting=csv.QUOTE_MINIMAL)
                scfwriter.writerow(['Month','1','2', '3', '4','5','6','7','8','9','10','11','12'])
                scfwriter.writerow(['BN'] + list_BN[:12])
                scfwriter.writerow(['NN'] + list_NN[:12])
                scfwriter.writerow(['AN'] + list_AN[:12])
            temp_csv=Wdir_path.replace("/","\\") + "\\SCF_input_monthly2.csv" 
            with open(temp_csv, 'w', newline='') as csvfile:
                scfwriter = csv.writer(csvfile, delimiter=',',
                                        quotechar='|', quoting=csv.QUOTE_MINIMAL)
                scfwriter.writerow(['Month','1','2', '3', '4','5','6','7','8','9','10','11','12'])
                scfwriter.writerow(['BN'] + list_BN[12:])
                scfwriter.writerow(['NN'] + list_NN[12:])
                scfwriter.writerow(['AN'] + list_AN[12:])

            #print(list(self.plt_button1.state()), list(self.plt_button2.state()))
            temp_snx=Wdir_path.replace("/","\\") + "\\button_write.csv" 
            with open(temp_snx, 'w', newline='') as csvfile:
                scfwriter = csv.writer(csvfile, delimiter=',',
                                        quotechar='|', quoting=csv.QUOTE_MINIMAL)
                scfwriter.writerow(['Month','J1','F1', 'M1', 'A1','M1','J1','J1','A1','S1','O1','N1','D1', 'J2','F2', 'M2', 'A2','M2','J2','J2','A2','S2','O2','N2','D2'])
                scfwriter.writerow(['SCF'] + list(self.scf_button1.state()) + list(self.scf_button2.state()))
                scfwriter.writerow(['plt'] + list(self.plt_button1.state()) + list(self.plt_button2.state()))
##            scfwriter.writerow(self.scf_button1.state())
            #=============================================
            # Generate 100 weather realizations
            #=============================================
            # Get a list of all files in directory
            for rootDir, subdirs, filenames in os.walk(Wdir_path):
                # Find the files that matches the given patterm
                for filename in fnmatch.filter(filenames, '0*.WTH'):
                    try:
                        os.remove(os.path.join(rootDir, filename))
                    except OSError:
                        print("Error while deleting file")
            create_WTH_main(Wdir_path,WTD_fname, WSTA, LAT, LONG, ELEV, TAV, AMP, pdate_1, pdate_2, step, hv_date, frst_date1) 
            #=============================================
            # Create DSSBatch.v47 file and run DSSAT executable 
            #=============================================
            entries = ("PlantGro.OUT","Evaluate.OUT","SCF_input_monthly1.csv", "SCF_input_monthly2.csv", "FloodW.out", "FloodN.out",
                     "ET.OUT","OVERVIEW.OUT","PlantN.OUT","SoilNi.OUT","Weather.OUT",
                    "SolNBalSum.OUT","SoilNoBal.OUT","SoilTemp.OUT","SoilWat.OUT","SoilWatBal.OUT","Summary.OUT") 
            self.writeV47_main(SNX_list, obs_flag)   ##write *.V47 batch file
            #==RUN DSSAT with ARGUMENT 
            start_time = time.perf_counter() # => not compatible with Python 2.7
            os.chdir(Wdir_path)  #change directory
            args = "DSCSM047.EXE N DSSBatch.v47 "
            subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
            end_time0 = time.perf_counter() # => not compatible with Python 2.7
            print('It took {0:5.1f} sec to finish 100 weather realizations x n planting dates'.format(end_time0-start_time))
            start_time1 = time.perf_counter()  # => not compatible with Python 2.7

            #create a new folder to save outputs of the target scenario
            new_folder=self.name3.getvalue()+"_output"
            if os.path.exists(new_folder):
                shutil.rmtree(new_folder)   #remove existing folder
            os.makedirs(new_folder)
            #copy outputs to the new folder
            dest_dir=Wdir_path + "\\"+new_folder
           # print 'dest_dir is', dest_dir
            for entry in entries:
                source_file=Wdir_path + "\\" + entry
                if os.path.isfile(source_file):
                    shutil.move(source_file, dest_dir)
                else:
                    print( '**Error!!- No DSSAT output files => Check if DSSAT simuation was successful')

            #move *.SNX file to the new folder with output files
            for i in range(len(SNX_list)):
                shutil.move(SNX_list[i], dest_dir)
            shutil.move(summary_fname, dest_dir)
          #  temp = "WGEN_out_" + WSTA + repr(pdate_1//1000)+".txt"
            shutil.move("WGEN_input_param_month_" + WSTA + ".txt", dest_dir)
            shutil.move("WGEN_out_" + WSTA + repr(pdate_1//1000)+".txt", dest_dir)
            #=============================================
            # Make a boxplot and exceedance curve of yield and other output
            #=============================================
            self.Yield_boxplot(sname, SNX_list, start_year, obs_flag)
            #=============================================
            # Make plots of water/nitrogen stresss
            #=============================================
            self.WSGD_plot(sname, SNX_list, start_year, obs_flag)
            #=============================================
            # Make plots of weather time series (Tmin, Tmax, Srad)
            #=============================================
            self.weather_plot(sname, SNX_list, start_year, obs_flag)
            #=============================================
            # Make plots of cumulative rainfall time series
            #=============================================
            self.plot_cum_rain(sname, SNX_list, start_year, obs_flag)
            print('It took {0:5.1f} sec to finish 100 simulations & to post-process outputs'.format(end_time0-start_time))
            print ("** Yeah!! DSSAT Simulation has been finished successfully!")
            print ("** Please check all output and figures in your working directory.")

    def writeV47_main(self, snx_list, obs_flag):
        temp_dv4=Wdir_path.replace("/","\\") + "\\DSSBatch_TEMP_RI.v47"
        dv4_fname=Wdir_path.replace("/","\\") + "\\DSSBatch.v47" 
        fr = open(temp_dv4,"r") #opens temp DV4 file to read
        fw = open(dv4_fname,"w") 
        #read template and write lines
        for line in range(0,10):
            temp_str=fr.readline()
            fw.write(temp_str)
            
        temp_str=fr.readline()
        #write SNX file with each different planting date
        for i in range(len(snx_list)):   #EJ(3/12/2019) for finding optimal planting dates
            #new_str=Wdir_path.replace("/","\\")+ "\\" + snx_list[i]
            #new_str2='{:<95}'.format(new_str)+ temp_str[95:]
            for j in range(100):
                #new_str2='{:<95}'.format(snx_list[i])+ temp_str[95:]
                new_str2='{0:<95}{1:4s}'.format(snx_list[i], repr(j+1).rjust(4))+ temp_str[99:]
                fw.write(new_str2)
                fw.write(" \n")
                
            if obs_flag ==1:
                new_str2='{0:<95}{1:4s}'.format(snx_list[i], repr(101).rjust(4))+ temp_str[99:]
                fw.write(new_str2) 
                fw.write(" \n")               
        fr.close()
        fw.close()

    def write_param_main(self,param_fname, station_name7, sname, plt_date, obs_flag): #write only one param.txt to write one SNX file
        f = open(param_fname,"w") #opens file 
        working_dir=Wdir_path.replace("/","\\")
        f.write("!Working directory where DSSAT4.5 is installed and weather or soil files can be found \n")
        f.write("Directory:  " + working_dir + "\n")
        f.write("!  \n")
        f.write("!==(I) Simulation set up \n")
        f.write("!(1) Choose weather station (8char) \n")
        f.write("Station_name: " + station_name7 + " \n")
        f.write("Scenario_name: "+ sname + " \n")
        f.write("!(2)Observed weather data available: Yes(1), No(0) \n")
        f.write("obs_flag: " + repr(obs_flag) + " \n")
        f.write("! \n")
        f.write("!!(3)Planting Year and Date \n")
        f.write("Planting_Year: " + repr(plt_date//1000) + " \n")
        f.write("Planting_doy: " + repr(plt_date%1000) + " \n")
        f.write("! \n")
        f.write("!==========(III) DSSAT set up  \n")
        f.write("Crop_type: Rice   \n")
        f.write("!Planting method: dry seed (0) or transplanting (1)   \n")
        f.write("Plt_method: "+str(Rbutton2.get())+" \n")
        f.write("!Planting detail \n")
        f.write("!Planting distribution: Hills (H), Rows (R), Brocast(B) \n")
        PLDS=self.plt_dist.getvalue()[0][0:1] #"Hills", "Rows", "Rows"
        f.write("PLDS: "+PLDS+ " \n")
        f.write("!Planting population at seedling, plant/m2 \n")
        f.write("PPOP: "+self.ppop_seed.getvalue()+ " \n")
        f.write("!Planting population at emergence, plant/m2 \n")
        f.write("PPOE: "+self.ppop_emer.getvalue()+ " \n")
        f.write("!Planting Row spacing, cm \n")
        f.write("PLRS: "+self.row_space.getvalue()+ " \n")
        f.write("!Row Direction , degrees from North \n")
        f.write("PLRD: "+self.row_dir.getvalue()+ " \n")
        f.write("!Planting depth , cm \n")
        f.write("PLDP: "+self.plt_depth.getvalue()+ " \n")
        f.write("!  \n")
        f.write("!(2) soil type: Initial soil H2O: 0.3, 0.5, 0.7 or 1), Initial soil NO3(H, M, L) \n")
       # soil = self.soil_type.getvalue()[0][-11:-1]  #"SCL(WI_ANPH007), "LoamySand(WI_ANPH008)", "Clay(WI_VRPH021)"),"Clay(WI_VRPH043)","SCL2(WI_CMPH009)"):
        f.write("Soil_type: "+ self.soil_type.getvalue()[0][0:10] + " \n")
        f.write("Initial_H2O: "+self.wet_soil.getvalue()[0][0:3]+"\n")
        f.write("Initial_NO3: " + self.NO3_soil.getvalue()[0][0:4]+"\n")
        f.write("!  \n")
        f.write("(3)Cultivar \n")
        f.write("VAR_num: " + self.cul_type.getvalue()[0][0:6]+ "\n")
        f.write("VAR_name: " + self.cul_type.getvalue()[0][7:]+ "\n")   
        f.write("!  \n")
        f.write("!(4)Fertilizer: Yes(1), or No(0) -No automatic option \n")
        f.write("Fertilization: "+ str(Fbutton1.get())+ " \n")
        f.write("!(4-1) if Yes, 'days after sawing', 'amount','material','applications' \n")
       # print 'fertilization opt: ', Fbutton1.get()
        if(Fbutton1.get() == 1):  #fertilizer applied
          #  print 'Number_applications: ', self.nfertilizer.getvalue()
            if self.nfertilizer.getvalue() == '':  #no of fertilization is required
                  self.nfert_err_dialog.activate()
                # self.val_dialog1.activate()
            f.write("Number_applications: "+self.nfertilizer.getvalue()+ "\n")  
            f.write("Fertilizer_1(days): "+self.label006.cget("text")+ "\n")    
            f.write("Fertilizer_1(amount): "+self.label007.cget("text")+ "\n")         
            f.write("Fertilizer_1(material): "+self.label008.cget("text")+ "\n")  
            f.write("Fertilizer_1(application): "+self.label009.cget("text")+ "\n")  
            f.write("Fertilizer_2(days): "+self.label011.cget("text")+ "\n")  
            f.write("Fertilizer_2(amount): "+self.label012.cget("text")+ "\n")        
            f.write("Fertilizer_2(material): "+self.label013.cget("text")+ "\n")     
            f.write("Fertilizer_2(application): "+self.label014.cget("text")+ "\n")   
            f.write("Fertilizer_3(days): "+self.label016.cget("text")+ "\n")     
            f.write("Fertilizer_3(amount): "+self.label017.cget("text")+ "\n")  
            f.write("Fertilizer_3(material): "+self.label018.cget("text")+ "\n")    
            f.write("Fertilizer_3(application): "+self.label019.cget("text")+ "\n")  
        else:  #no fertilizer
            f.write("Number_applications: \n")         
            f.write("Fertilizer_1(days): \n")    
            f.write("Fertilizer_1(amount): \n")    
            f.write("Fertilizer_1(material): \n")         
            f.write("Fertilizer_1(application): \n")    
            f.write("Fertilizer_2(days): \n")    
            f.write("Fertilizer_2(amount): \n")         
            f.write("Fertilizer_2(material): \n")    
            f.write("Fertilizer_2(application): \n")  
            f.write("Fertilizer_3(days): \n")    
            f.write("Fertilizer_3(amount): \n") 
            f.write("Fertilizer_3(material): \n")    
            f.write("Fertilizer_3(application): \n") 
        f.write("!  \n")
        f.write("!(5) Irrigation: Automatic soil(0),Automatic flooding(1), on reported dates (2), no irrigation(3)  \n")
        f.write("Irrigation_method: "+ str(IRbutton.get())+ " \n")
        f.write("!(5-1)if Automatic(0) \n")
        if(IRbutton.get() == 0):  #Automatic when required
            print("This auto-irrigation option was deleted")
            # f.write("Management_depth(cm):  "+self.label402.cget("text")+ "\n")  
            # f.write("Threshold: "+self.label404.cget("text")+ "\n")    
            # f.write("EndPoint: 100 \n")  #"+self.label406.cget("text")+ "\n")         
            # f.write("Efficiency_Fraction: "+self.label408.cget("text")+ "\n")  
            # f.write("End_of_Application: IB001 \n") #"+self.label410.cget("text")+ "\n")  
            # f.write("Method: IR001 \n") #"+self.label412.cget("text")+ "\n")  
            # f.write("Amount: \n")  
            # f.write("!(5-2)if Manual  \n")  
            # f.write("Number_irrigation: \n")   
            # f.write("!(5-2-1) if transplanted \n")   
            # f.write("Puddling_date: \n")   
            # f.write("Puddling: \n")   
            # f.write("!whether transplanted or dry seeds \n")   
            # f.write("Percolation_rate: \n")   
            # f.write("First_irrigation_date: \n")
            # f.write("Bund_height_1: \n")
            # f.write("Flood_depth_1: \n")  
            # f.write("Constant_depth1?: Not added  \n")   
            # f.write("Second_irrigation_date: \n")   
            # f.write("Bund_height_2: \n")
            # f.write("Flood_depth_2: \n")  
            # f.write("Constant_depth2?: Not added   \n")   
            # f.write("Third_irrigation_date: \n")   
            # f.write("Bund_height_3: \n")
            # f.write("Flood_depth_3: \n") 
            # f.write("Constant_depth3?: Not added   \n")  
            # f.write("!(5-1)Automotic-repeated flooding   \n")  
            # f.write("Puddling_date:   \n")  
            # f.write("1st_irrig_date(DAP):   \n")  
            # f.write("Bund_height:   \n")  
            # f.write("Flooding_depth:   \n")  
            # f.write("Interval:   \n")  
            # f.write("EndApplication(DAP):   \n")  
        elif(IRbutton.get() == 2):  # irrigation on report dates
            if self.nirrigation.getvalue() == '':  #no of fertilization is required
                  self.nirr_err_dialog.activate()
            f.write("Management_depth(cm): \n") 
            f.write("Threshold: \n")   
            f.write("EndPoint: \n")        
            f.write("Efficiency_Fraction: \n") 
            f.write("End_of_Application: \n") 
            f.write("Method: \n") 
            f.write("Amount: \n")  
            f.write("!(5-2)if Manual \n")  
            f.write("Number_irrigation:  "+self.nirrigation.getvalue()+ "\n")     
            f.write("!(5-2-1) if transplanted  \n")   
            f.write("Puddling_date: "+self.label121.cget("text")+ "\n")     
            f.write("Puddling:  0 \n") #"+self.label123.cget("text")+ "\n")    
            f.write("!whether transplanted or dry seeds \n")   
            f.write("Percolation_rate: "+self.label125.cget("text")+ "\n")    
            f.write("First_irrigation_date: "+self.label105.cget("text")+ "\n")  
            f.write("Bund_height_1: "+self.label106.cget("text")+ "\n")    
            f.write("Flood_depth_1: "+self.label107.cget("text")+ "\n")
            f.write("Constant_depth1?: "+self.label1071.cget("text")+ "\n")   
            f.write("Second_irrigation_date: "+self.label109.cget("text")+ "\n")     
            f.write("Bund_height_2: "+self.label110.cget("text")+ "\n")  
            f.write("Flood_depth_2: "+self.label111.cget("text")+ "\n")
            f.write("Constant_depth2?: "+self.label1111.cget("text")+ "\n") 
            f.write("Third_irrigation_date: "+self.label113.cget("text")+ "\n")     
            f.write("Bund_height_3: "+self.label114.cget("text")+ "\n")  
            f.write("Flood_depth_3: "+self.label115.cget("text")+ "\n")
            f.write("Constant_depth3?: "+self.label1151.cget("text")+ "\n") 
            f.write("!(5-1)Automotic-repeated flooding   \n")  
            f.write("Puddling_date:   \n")  
            f.write("1st_irrig_date(DAP):   \n")  
            f.write("Bund_height:   \n")  
            f.write("Flooding_depth:   \n")  
            f.write("Interval:   \n")  
            f.write("EndApplication(DAP):   \n") 
        elif(IRbutton.get() == 3):  # no irrigation
            f.write("Management_depth(cm):   \n") 
            f.write("Threshold:   \n")   
            f.write("EndPoint:   \n")        
            f.write("Efficiency_Fraction:   \n") 
            f.write("End_of_Application:  \n") 
            f.write("Method:   \n") 
            f.write("Amount: \n")  
            f.write("!(5-2)if Manual  \n")   
            f.write("Number_irrigation:  \n")   
            f.write("!(5-2-1) if transplanted  \n")   
            f.write("Puddling_date:  \n")   
            f.write("Puddling:   \n")   
            f.write("!whether transplanted or dry seeds \n")   
            f.write("Percolation_rate:  \n")   
            f.write("First_irrigation_date: \n")   
            f.write("Bund_height_1:   \n")  
            f.write("Flood_depth_1:   \n")
            f.write("Constant_depth1?: \n")
            f.write("Second_irrigation_date:  \n")   
            f.write("Bund_height_2: \n")   
            f.write("Flood_depth_2:  \n")
            f.write("Constant_depth2?: \n")
            f.write("Third_irrigation_date:  \n")   
            f.write("Bund_height_3: \n")   
            f.write("Flood_depth_3: \n")
            f.write("Constant_depth3?: \n")
            f.write("!(5-1)Automotic-repeated flooding   \n")  
            f.write("Puddling_date:   \n")  
            f.write("1st_irrig_date(DAP):   \n")  
            f.write("Bund_height:   \n")  
            f.write("Flooding_depth:   \n")  
            f.write("Interval:   \n")  
            f.write("EndApplication(DAP):   \n") 
        else:  #repeated irrigation
            f.write("Management_depth(cm):   \n") 
            f.write("Threshold:   \n")   
            f.write("EndPoint:   \n")        
            f.write("Efficiency_Fraction:   \n") 
            f.write("End_of_Application:  \n") 
            f.write("Method:   \n") 
            f.write("Amount: \n")  
            f.write("!(5-2)if Manual  \n")   
            f.write("Number_irrigation:  \n")   
            f.write("!(5-2-1) if transplanted  \n")   
            f.write("Puddling_date:  \n")   
            f.write("Puddling:   \n")   
            f.write("!whether transplanted or dry seeds \n")   
            f.write("Percolation_rate:  \n")   
            f.write("First_irrigation_date: \n")   
            f.write("Bund_height_1:   \n")  
            f.write("Flood_depth_1:   \n")
            f.write("Constant_depth1?: \n")
            f.write("Second_irrigation_date:  \n")   
            f.write("Bund_height_2: \n")   
            f.write("Flood_depth_2:  \n")
            f.write("Constant_depth2?: \n")
            f.write("Third_irrigation_date:  \n")   
            f.write("Bund_height_3: \n")   
            f.write("Flood_depth_3: \n")
            f.write("Constant_depth3?: \n")
            f.write("!(5-1)Automotic-repeated flooding   \n")  
            f.write("Puddling_date: " +self.label418.cget("text")+ "\n")  
            f.write("1st_irrig_date(DAP): " +self.label420.cget("text")+ "\n")
            f.write("Bund_height: " +self.label416.cget("text")+ "\n")
            f.write("Flooding_depth: " +self.label410.cget("text")+ "\n")
            f.write("Interval: " +self.label412.cget("text")+ "\n")
            f.write("EndApplication(DAP): " +self.label414.cget("text")+ "\n")
        f.close()
    #====End of WRITE param.txt

    ##############################################################
    def Yield_boxplot(self,sname, SNX_list, start_year, obs_flag):
        fname1 = Wdir_path + "\\"+ sname+"_output\\SUMMARY.OUT"
        pdate_list = []
        year_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(int(SNX_list[i][-11:-8]))
            if i == 0:
                year_list.append(int(start_year))
            else:
                if pdate_list[i] > pdate_list[i-1]:
                    year_list.append(int(start_year))
                else:
                    year_list.append(int(start_year) + 1)

        df_OUT=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
        PDAT = df_OUT.ix[:,14].values  #read 14th column only => Planting date 
        HWAM = df_OUT.ix[:,21].values  #read 21th column only
        ADAT = df_OUT.ix[:,16].values  #read 21th column only
        MDAT = df_OUT.ix[:,17].values  #read 21th column only

    #    year_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize
        DOY_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize
        ADAT_array = np.zeros(len(ADAT)) #np.empty((ndays,1))*np.nan #initialize
        MDAT_array = np.zeros(len(MDAT)) #np.empty((ndays,1))*np.nan #initialize
        for i in range(len(HWAM)):
           # year_array[i] = int(repr(PDAT[i])[:4])
            DOY_array[i] = int(repr(PDAT[i])[4:])
            #MDAT_array[i] = int(repr(MDAT[i])[4:])
            if ADAT[i] == 0:  #in case emergence does not happen
                ADAT_array[i] = np.nan
                MDAT_array[i] = np.nan
            elif ADAT[i] == -99:  #in case emergence does not happen
                ADAT_array[i] = np.nan
                MDAT_array[i] = np.nan
            elif MDAT[i] == -99: 
                MDAT_array[i] = np.nan
            else:
                ADAT_array[i] = int(repr(ADAT[i])[4:])
                MDAT_array[i] = int(repr(MDAT[i])[4:])
                
        #make three arrays into a dataframe
        df = pd.DataFrame({'DOY': DOY_array, 'ADAT':ADAT_array, 'MDAT':MDAT_array, 'HWAM':HWAM}, columns=['DOY','ADAT','MDAT','HWAM'])

        # #make an empty array [nyears * n_plt_dates]
        n_year = 100
        # For Boxplot
        yield_n = np.empty([n_year,len(pdate_list)])*np.nan
        ADAT_n = np.empty([n_year,len(pdate_list)])*np.nan
        MDAT_n = np.empty([n_year,len(pdate_list)])*np.nan
        # For exceedance curve
        sorted_yield_n = np.empty([n_year,len(pdate_list)])*np.nan
        Fx_scf = np.empty([n_year,len(pdate_list)])*np.nan       
        sorted_ADAT_n = np.empty([n_year,len(pdate_list)])*np.nan   
        Fx_scf_ADAT = np.empty([n_year,len(pdate_list)])*np.nan
        sorted_MDAT_n = np.empty([n_year,len(pdate_list)])*np.nan
        # Fx_scf_MDAT = np.empty([n_year,len(pdate_list)])*np.nan
        if obs_flag == 0:  #if there is no observed weather available 
            for count in range(len(pdate_list)):
                Pdate = pdate_list[count]
                yield_n[:,count] = df.HWAM[(df["DOY"] == Pdate)].values  
                ADAT_n[:,count] = df.ADAT[(df["DOY"] == Pdate)].values 
                MDAT_n[:,count] = df.MDAT[(df["DOY"] == Pdate)].values  
                sorted_yield_n[:,count] = np.sort(df.HWAM[(df["DOY"] == Pdate)].values)
                fx_scf = [1.0/len(sorted_yield_n)] * len(sorted_yield_n) #pdf
                #Fx_scf = np.cumsum(fx_scf)
                Fx_scf[:,count] = 1.0-np.cumsum(fx_scf)  #for exceedance curve
                sorted_ADAT_n[:,count] = np.sort(df.ADAT[(df["DOY"] == Pdate)].values)
                sorted_MDAT_n[:,count] = np.sort(df.MDAT[(df["DOY"] == Pdate)].values)
                temp = sorted_ADAT_n[:,count]
                temp2 = temp[~np.isnan(temp)]
                fx_scf_ADAT = [1.0/len(temp2)] * len(temp2) #pdf
                Fx_scf_ADAT[:len(fx_scf_ADAT),count] = 1.0-np.cumsum(fx_scf_ADAT)  #for exceedance curve         
        else:   
            obs_yield = []
            obs_ADAT = []
            obs_MDAT = []
            for count in range(len(pdate_list)):
                Pdate = pdate_list[count]
                yield_n[:,count] = df.HWAM[(df["DOY"] == Pdate)].values[:-1]  
                ADAT_n[:,count] = df.ADAT[(df["DOY"] == Pdate)].values[:-1] 
                MDAT_n[:,count] = df.MDAT[(df["DOY"] == Pdate)].values[:-1] 
                sorted_yield_n[:,count] = np.sort(df.HWAM[(df["DOY"] == Pdate)].values[:-1])
                fx_scf = [1.0/len(sorted_yield_n)] * len(sorted_yield_n) #pdf
                #Fx_scf = np.cumsum(fx_scf)
                Fx_scf[:,count] = 1.0-np.cumsum(fx_scf)  #for exceedance curve
                sorted_ADAT_n[:,count] = np.sort(df.ADAT[(df["DOY"] == Pdate)].values[:-1])
                sorted_MDAT_n[:,count] = np.sort(df.MDAT[(df["DOY"] == Pdate)].values[:-1])
                temp = sorted_ADAT_n[:,count]
                temp2 = temp[~np.isnan(temp)]
                fx_scf_ADAT = [1.0/len(temp2)] * len(temp2) #pdf
                Fx_scf_ADAT[:len(fx_scf_ADAT),count] = 1.0-np.cumsum(fx_scf_ADAT)  #for exceedance curve    
                #collect simulated results from observed weather
                obs_yield.append(df.HWAM[(df["DOY"] == Pdate)].values[-1])      
                obs_ADAT.append(df.ADAT[(df["DOY"] == Pdate)].values[-1])  
                obs_MDAT.append(df.MDAT[(df["DOY"] == Pdate)].values[-1])  
       # Fx_scf_ADAT = Fx_scf
        #To remove 'nan' from data array
        #https://stackoverflow.com/questions/44305873/how-to-deal-with-nan-value-when-plot-boxplot-using-python
        mask = ~np.isnan(yield_n)
        filtered_yield = [d[m] for d, m in zip(yield_n.T, mask.T)]
        mask2 = ~np.isnan(ADAT_n)
        filtered_data2 = [d[m] for d, m in zip(ADAT_n.T, mask2.T)]
        mask3 = ~np.isnan(MDAT_n)
        filtered_data3 = [d[m] for d, m in zip(MDAT_n.T, mask3.T)]
        
        if obs_flag == 1: 
            #replace ADAT = 0 with nan
            obs_ADAT = [np.nan if x==1 else x for x in obs_ADAT]
            obs_MDAT = [np.nan if x==1 else x for x in obs_MDAT]

        #X data for plot
        myXList=[i+1 for i in range(len(pdate_list))]
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            temp_str = repr(year_list[count]) + " " + repr(pdate_list[count]) 
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)
        # 1) Plotting Yield 
        fig = plt.figure()
        fig.suptitle('Estimated Yields with differnt planting dates', fontsize=14, fontweight='bold')
        ax = fig.add_subplot(111)
        ax.set_xlabel('Planting Date[MM/DD]',fontsize=14)
        ax.set_ylabel('Yield [kg/ha]',fontsize=14)
        if obs_flag == 1:
            # Plot a line between yields with observed weather
            plt.plot(myXList, obs_yield, 'go-')
        ax.boxplot(filtered_yield,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
        fig_name = Wdir_path + "\\"+sname+"_output\\"+sname+"_Yield_boxplot.png"
        plt.savefig(fig_name)      

        # 2) Plotting ADAT
        fig2 = plt.figure()
        fig2.suptitle('Estimated Anthesis Date with differnt planting dates', fontsize=14, fontweight='bold')
        ax2 = fig2.add_subplot(111)
        ax2.set_xlabel('Planting Date[DOY]',fontsize=14)
        ax2.set_ylabel('Anthesis Date [DOY]',fontsize=14)
        if obs_flag == 1:
            # Plot a line between yields with observed weather
            plt.plot(myXList, obs_ADAT, 'go-')
        ax2.boxplot(filtered_data2,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
        fig_name2 = Wdir_path + "\\"+ sname+"_output\\"+sname+"_ADAT_boxplot.png"
        plt.savefig(fig_name2)       

        # 3) Plotting MDAT
        fig3 = plt.figure()
        fig3.suptitle('Estimated Maturity Date with differnt planting dates', fontsize=14, fontweight='bold')
        ax3 = fig3.add_subplot(111)
        ax3.set_xlabel('Planting Date[DOY]',fontsize=14)
        ax3.set_ylabel('Maturity Date [DOY]',fontsize=14)
        if obs_flag == 1:
            # Plot a line between yields with observed weather
            plt.plot(myXList, obs_MDAT, 'go-')
        ax3.boxplot(filtered_data3,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
        fig_name = Wdir_path + "\\"+ sname+"_output\\"+sname+"_MDAT_boxplot.png"
        plt.savefig(fig_name)    

        #4) Plotting yield exceedance curve
        fig4 = plt.figure()
        fig4.suptitle('Yield Exceedance Curve', fontsize=14, fontweight='bold')
        ax4 = fig4.add_subplot(111)
        ax4.set_xlabel('Yield [kg/ha]',fontsize=14)
        ax4.set_ylabel('Probability of Exceedance [-]',fontsize=14)
        plt.ylim(0, 1) 
        for x in range(len(pdate_list)):
            ax4.plot(sorted_yield_n[:,x],Fx_scf[:,x],'o-', label=scename[x])
            #vertical line for the yield with observed weather
            if obs_flag == 1:
                x_data=[obs_yield[x],obs_yield[x]] #only two points to draw a line
                y_data=[0,1]
                temp='w/ obs wth (' + scename[x] + ')'
                ax4.plot(x_data,y_data,'-.', label=temp)
                plt.ylim(0, 1)
        box = ax4.get_position()  # Shrink current axis by 15%
        ax4.set_position([box.x0, box.y0, box.width * 0.70, box.height])
        plt.grid(True)
        ax4.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
        fig_name = Wdir_path + "\\" + sname + "_output\\" + sname +"_Yield_exceedance.png"
        fig4.set_size_inches(10, 8)
        plt.savefig(fig_name)   

        #5) Plotting ADAT exceedance curve
        fig5 = plt.figure()
        fig5.suptitle('Anthesis Date Exceedance Curve', fontsize=14, fontweight='bold')
        ax5 = fig5.add_subplot(111)
        ax5.set_xlabel('Anthesis Date [DOY]',fontsize=14)
        ax5.set_ylabel('Probability of Exceedance [-]',fontsize=14)
        plt.ylim(0, 1) 
        for x in range(len(pdate_list)):
            ax5.plot(sorted_ADAT_n[:,x],Fx_scf_ADAT[:,x],'o-', label=scename[x])
            #vertical line for the yield with observed weather
            if obs_flag == 1:
                x_data=[obs_ADAT[x],obs_ADAT[x]] #only two points to draw a line
                y_data=[0,1]
                temp='w/ obs wth (' + scename[x] + ')'
                ax5.plot(x_data,y_data,'-.', label=temp)
                plt.ylim(0, 1)
        box = ax5.get_position()
        ax5.set_position([box.x0, box.y0, box.width * 0.70, box.height])
        plt.grid(True)
        ax5.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
        fig_name = Wdir_path + "\\"+ sname +"_output\\" + sname +"_ADAT_exceedance.png"
        fig5.set_size_inches(10, 8)
        plt.savefig(fig_name)    

        #6) Plotting MDAT exceedance curve
        fig6 = plt.figure()
        fig6.suptitle('Maturity Date Exceedance Curve', fontsize=14, fontweight='bold')
        ax6 = fig6.add_subplot(111)
        ax6.set_xlabel('Maturity Date [DOY]',fontsize=14)
        ax6.set_ylabel('Probability of Exceedance [-]',fontsize=14)
        plt.ylim(0, 1) 
        for x in range(len(pdate_list)):
            ax6.plot(sorted_MDAT_n[:,x],Fx_scf_ADAT[:,x],'o-', label=scename[x])
            #vertical line for the yield with observed weather
            if obs_flag == 1:
                x_data=[obs_MDAT[x],obs_MDAT[x]] #only two points to draw a line
                y_data=[0,1]
                temp='w/ obs wth (' + scename[x] + ')'
                ax6.plot(x_data,y_data,'-.', label=temp)
                plt.ylim(0, 1)
        box = ax6.get_position()
        ax6.set_position([box.x0, box.y0, box.width * 0.70, box.height])
        plt.grid(True)
        ax6.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
        fig_name = Wdir_path + "\\"+ sname +"_output\\" + sname +"_MDAT_exceedance.png"
        fig6.set_size_inches(10, 8)
        plt.savefig(fig_name)    

    ##############################################################
    def WSGD_plot(self,sname, SNX_list, start_year, obs_flag):
        fname1 = Wdir_path + "\\"+ sname+"_output\\PlantGro.OUT"
        pdate_list = []
        # year_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(SNX_list[i][-11:-8])
            # if i == 0:
            #     year_list.append(int(start_year))
            # else:
            #     if pdate_list[i] > pdate_list[i-1]:
            #         year_list.append(int(start_year))
            #     else:
            #         year_list.append(int(start_year) + 1)      
        if obs_flag == 1:
            nrealiz = 101
        else:
            nrealiz = 100
        n_days = 210 #approximate growing days
        WSGD_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
        NSTD_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN 
        read_flag = 0  #off
        n_count = 0  #count of n weahter realizations
        d_count = 0  #day counter during a growing season
        p_index_old = 0
        p_count = 0  #count for different planting date scenario
        with open(fname1) as fp:
            for line in fp:
                if line[18:21] == '  0':   #line[18:21] => DAP
                    p_index = pdate_list.index(line[6:9])   #line[6:9] => DOY
                    read_flag = 1  #on
                    if p_index > p_index_old:  #move to a new planting date scenario
                        p_count = p_count + 1
                        n_count = 0
                #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
                if read_flag ==1 and line == '\n':  #after reading last growing day
                    read_flag = 0 #off
                    p_index_old = p_index
                    d_count = 0
                    n_count = n_count + 1
                if read_flag == 1:
                    WSGD_3D[d_count, n_count, p_count] = float(line[105:111]) #[127:133])
                    NSTD_3D[d_count, n_count, p_count] = float(line[118:123]) #[134:140])
                    d_count = d_count + 1

        # WSGD_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
        # NSTD_avg = np.empty((n_days,len(pdate_list),))*np.NAN  
        WSGD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
        WSGD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        WSGD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        NSTD_50 = np.empty((n_days,len(pdate_list),))*np.NAN  
        NSTD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        NSTD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        if obs_flag == 1:
            WSGD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
            NSTD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
        
        #plot
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            if count > 0:
                if int(pdate_list[count]) < int(pdate_list[count-1]):
                    start_year = repr(int(start_year) + 1)
            temp_str = start_year + " " + pdate_list[count]
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)

        for i in range(len(pdate_list)):
            # WSGD_avg[:, i] = np.nanmean(WSGD_3D[:, 0:100, i], axis = 1)
            # NSTD_avg[:, i] = np.nanmean(WSGD_3D[:, 0:100, i], axis = 1)
            WSGD_50[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 50, axis = 1)
            WSGD_25[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 25, axis = 1)
            WSGD_75[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 75, axis = 1)
            NSTD_50[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 50, axis = 1)
            NSTD_25[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 25, axis = 1)
            NSTD_75[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 75, axis = 1)
            if obs_flag == 1:
                WSGD_obs[:, i] = WSGD_3D[:, -1, i]
                NSTD_obs[:, i] = NSTD_3D[:, -1, i]
        #================================== 
        #1) Plot WSGD Water stress
        nrow = math.ceil(len(pdate_list)/2.0)
        ncol = 2
        fig, axs = plt.subplots(nrow, ncol)
        fig.suptitle('WSGD (Water Stress Index)')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    xdata = range(len(WSGD_50[:,0]))
                    axs[j, i].plot(xdata, WSGD_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(WSGD_50[:,p_count]))
                    yerr_low = WSGD_50[:, p_count] -WSGD_25[:, p_count]
                    yerr_up = WSGD_75[:, p_count] - WSGD_50[:, p_count]
                    axs[j, i].errorbar(xdata, WSGD_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, WSGD_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(WSGD_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='Water Stress Index [-]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(13, 8)
        fig_name = Wdir_path + "\\"+ sname +"_output\\" + sname +"_WStress.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        #==================================
        #2) Plot NSTD Nitrogen stress
        nrow = math.ceil(len(pdate_list)/2.0)
        ncol = 2
        fig, axs = plt.subplots(nrow, ncol)
        fig.suptitle('NSTD (Nitrogen Stress Index)')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    xdata = range(len(NSTD_50[:,0]))
                    axs[j, i].plot(xdata, NSTD_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(NSTD_50[:,p_count]))
                    yerr_low = NSTD_50[:, p_count] -NSTD_25[:, p_count]
                    yerr_up = NSTD_75[:, p_count] - NSTD_50[:, p_count]
                    axs[j, i].errorbar(xdata, NSTD_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, NSTD_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(NSTD_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='Nitrogen Stress Index [-]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = Wdir_path + "\\"+ sname +"_output\\" + sname +"_NStress.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
    ##############################################################  
    def weather_plot(self,sname, SNX_list, start_year, obs_flag):
        # TMND   MIN TEMP °C     Minimum daily temperature (°C)                           .
        # TMXD   MAX TEMP °C     Maximum daily temperature (°C) 
        # PRED   PRECIP mm/d     Precipitation depth (mm/d) ==>> ??????                        .
        # SRAD   SRAD MJ/m2.d    Solar radiation (MJ/(m2.d))                              .
        # TAVD   AVG TEMP °C     Average daily temperature (°C)
        fname1 = Wdir_path + "\\"+ sname+"_output\\Weather.OUT"
        pdate_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(SNX_list[i][-11:-8])

        if obs_flag == 1:
            nrealiz = 101
        else:
            nrealiz = 100
        n_days = 210 #approximate growing days
        TMXD_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
        TMND_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN 
        SRAD_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN 
        read_flag = 0  #off
        n_count = 0  #count of n weahter realizations
        d_count = 0  #day counter during a growing season
        p_count = 0  #count for different planting date scenario
        with open(fname1) as fp:
            for line in fp:
                if line[6:9] == pdate_list[p_count]:   #line[12:15] => DAS
                    read_flag = 1  #on
                if read_flag ==1 and line == '\n':  #after reading last growing day
                    read_flag = 0 #off
                    d_count = 0
                    n_count = n_count + 1
                    if n_count == nrealiz:  #move to next planting date scenario
                        p_count = p_count + 1
                        n_count = 0
                if read_flag == 1:
                    TMXD_3D[d_count, n_count, p_count] = float(line[60:64])
                    TMND_3D[d_count, n_count, p_count] = float(line[67:71])
                    SRAD_3D[d_count, n_count, p_count] = float(line[39:43])
                    d_count = d_count + 1

        # TMXD_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
        # TMND_avg = np.empty((n_days,len(pdate_list),))*np.NAN  
        TMXD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMXD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMXD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMND_50 = np.empty((n_days,len(pdate_list),))*np.NAN  
        TMND_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMND_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        SRAD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
        SRAD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        SRAD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        if obs_flag == 1:
            TMXD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
            TMND_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
            SRAD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
    
        #plot
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            if count > 0:
                if int(pdate_list[count]) < int(pdate_list[count-1]):
                    start_year = repr(int(start_year) + 1)
            temp_str = start_year + " " + pdate_list[count]
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)

        for i in range(len(pdate_list)):
            TMXD_50[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 50, axis = 1)
            TMXD_25[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 25, axis = 1)
            TMXD_75[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 75, axis = 1)
            TMND_50[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 50, axis = 1)
            TMND_25[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 25, axis = 1)
            TMND_75[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 75, axis = 1)
            SRAD_50[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 50, axis = 1)
            SRAD_25[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 25, axis = 1)
            SRAD_75[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 75, axis = 1)
            if obs_flag == 1:
                TMXD_obs[:, i] = TMXD_3D[:, -1, i]
                TMND_obs[:, i] = TMND_3D[:, -1, i]
                SRAD_obs[:, i] = SRAD_3D[:, -1, i]
        #================================== 
        #1) Plot TMXD
        nrow = math.ceil(len(pdate_list)/2.0)
        ncol = 2
        fig, axs = plt.subplots(nrow, ncol)
        fig.suptitle('TMXD (Maximum daily temperature)')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    xdata = range(len(TMXD_50[:,0]))
                    axs[j, i].plot(xdata, TMXD_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(TMXD_50[:,p_count]))
                    yerr_low = TMXD_50[:, p_count] -TMXD_25[:, p_count]
                    yerr_up = TMXD_75[:, p_count] - TMXD_50[:, p_count]
                    axs[j, i].errorbar(xdata, TMXD_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, TMXD_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(TMXD_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='Temperature[C]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = Wdir_path + "\\"+ sname +"_output\\" + sname +"_Tmax.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        #==================================
        #2) Plot TMND
        fig, axs = plt.subplots(nrow, ncol)
        fig.suptitle('TMND (Minimum daily temperature)')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    axs[j, i].plot(xdata, TMND_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(TMND_50[:,p_count]))
                    yerr_low = TMND_50[:, p_count] -TMND_25[:, p_count]
                    yerr_up = TMND_75[:, p_count] - TMND_50[:, p_count]
                    axs[j, i].errorbar(xdata, TMND_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, TMND_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(TMND_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='Temperature[C]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = Wdir_path + "\\"+ sname +"_output\\" + sname +"_Tmin.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        #==================================
        #3) Plot SRAD   SRAD MJ/m2.d    Solar radiation (MJ/(m2.d)) 
        fig, axs = plt.subplots(nrow, ncol)
        fig.suptitle('SARD (Daily solar radiation (MJ/(m2.d))')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    axs[j, i].plot(xdata, SRAD_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(SRAD_50[:,p_count]))
                    yerr_low = SRAD_50[:, p_count] -SRAD_25[:, p_count]
                    yerr_up = SRAD_75[:, p_count] - SRAD_50[:, p_count]
                    axs[j, i].errorbar(xdata, SRAD_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, SRAD_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(SRAD_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='SRAD[MJ/(m2.d)]')
    # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = Wdir_path + "\\"+ sname +"_output\\" + sname +"_SRAD.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
    ##############################################################
    def plot_cum_rain(self,sname, SNX_list, start_year, obs_flag):
        fname1 = Wdir_path + "\\"+ sname+"_output\\SoilWat.OUT"
        pdate_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(SNX_list[i][-11:-8])

        if obs_flag == 1:
            nrealiz = 101
        else:
            nrealiz = 100
        n_days = 210 + 30 #approximate growing days + 30 days from IC to plt date
        PREC_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]

        read_flag = 0  #off
        n_count = 0  #count of n weahter realizations
        d_count = 0  #day counter during a growing season
        p_count = 0  #count for different planting date scenario
        with open(fname1) as fp:
            for line in fp:
                if line[12:15] == '  0':   #line[12:15] => DAS
                    read_flag = 1  #on
                if read_flag ==1 and line == '\n':  #after reading last growing day
                    read_flag = 0 #off
                    d_count = 0
                    n_count = n_count + 1
                    if n_count == nrealiz:  #move to next planting date scenario
                        p_count = p_count + 1
                        n_count = 0
                if read_flag == 1:
                    PREC_3D[d_count, n_count, p_count] = float(line[44:48])
                    d_count = d_count + 1

        # PREC_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
        PREC_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
        PREC_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        PREC_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        if obs_flag == 1:
            PREC_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
    
        #plot
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            if count > 0:
                if int(pdate_list[count]) < int(pdate_list[count-1]):
                    start_year = repr(int(start_year) + 1)
            temp_str = start_year + " " + pdate_list[count]
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)

        for i in range(len(pdate_list)):
            PREC_50[:, i] = np.nanpercentile(PREC_3D[:, 0:100, i], 50, axis = 1)
            PREC_25[:, i] = np.nanpercentile(PREC_3D[:, 0:100, i], 25, axis = 1)
            PREC_75[:, i] = np.nanpercentile(PREC_3D[:, 0:100, i], 75, axis = 1)
            if obs_flag == 1:
                PREC_obs[:, i] = PREC_3D[:, -1, i]
        #================================== 
        #1) Plot PREC
        nrow = math.ceil(len(pdate_list)/2.0)
        ncol = 2
        fig, axs = plt.subplots(nrow, ncol)
        fig.suptitle('Cumulative Precipitation')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    xdata = range(len(PREC_50[:,0]))
                    axs[j, i].plot(xdata, PREC_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(PREC_50[:,p_count]))
                    yerr_low = PREC_50[:, p_count] -PREC_25[:, p_count]
                    yerr_up = PREC_75[:, p_count] - PREC_50[:, p_count]
                    axs[j, i].errorbar(xdata, PREC_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, PREC_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(PREC_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After SIMULATION [DAS]', ylabel='Cum. Rainfall[mm]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = Wdir_path + "\\"+ sname +"_output\\" + sname +"_CumRain.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)

######################################################################
# For historical runs
######################################################################
    def writeSNX1_DSSAT_hist(self): 
        #check errorneous cases for fertilizer
        if Fbutton1.get() == 1 :  #fertilizer button is selected
            if self.label006.cget("text") == 'N/A':
                self.fertilizer_err.activate()
        #check errorneous cases for  irrigation 
        if IRbutton.get() == 0 :  #Automatic when required is selected
            print("This auto-irrigation option was deleted")
            # if self.label402.cget("text") == 'N/A':
            #     self.irrigation_err.activate()
        if IRbutton.get() == 2:  #On Reported dates is selected
            if self.label105.cget("text") == 'N/A':
                self.irrigation_err.activate()
        if IRbutton.get() == 1:  #automatic (periodic) irrigation for every n days
            if self.label418.cget("text") == 'N/A':
                self.irrigation_err.activate()

        if self.name1.getvalue() == "":
            self.writeSNX_err.activate()
        else:
            sname=self.name1.getvalue()
            WSTA=self.Wstation.getvalue()[0][0:4] #'SANJ'
            WTD_fname=Wdir_path.replace("/","\\") + "\\"+ WSTA + ".WTD"
            WTD_last_date = find_obs_date (WTD_fname)

            start_year=self.startyear.getvalue() #'2002'
            obs_flag = 0  #by default, assumes no obs weather data is available
            #=============================================
            #Determine pdate_1[YYYYDOY] and pdate_2[YYYYDOY]
            lst = list(self.plt_button1.state())  #Year 1 for planting
            lst2 = list(self.plt_button2.state())   #Year 2 for planting
            lst_frst = list(self.scf_button1.state()) #Year 1 for SCF
            lst_frst2 = list(self.scf_button2.state())
            pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m = find_plt_date (start_year, lst, lst2, lst_frst, lst_frst2)

            #find simulation start date
            temp=int(pdate_1%1000)-30  #initial condition: 30 day before planting  #EJ(3/13/2019)
            if temp <= 0 :   #EJ(3/13/2019)
                if calendar.isleap(int(start_year)-1):  
                    temp = 366 + temp
                else:
                    temp = 365 + temp
                IC_date = (pdate_1//1000 -1)*1000+temp
            else:
                IC_date = (pdate_1//1000)*1000+temp
            #Determines how many crop growing seasons are available from the historical observed.
            sim_years, obs_year1 = find_obs_years(WTD_fname, IC_date, hv_date) # <=======********

            if WTD_last_date >= hv_date: #180 is arbitrary number to fully cover weather data until late maturity
                obs_flag = 1
            step = int(self.step1.getvalue())#5 days distance for visualizing boxplots
            SNX_list = []
            os.chdir(Wdir_path)  #change directory
            param_fname=Wdir_path.replace("/","\\") + "\\param_RI_SNX_hist.txt"
            if pdate_2%1000 > pdate_1%1000:
                for iday in range(pdate_1,pdate_2+1,step):   
                    self.write_param_main_hist(param_fname,self.Wstation.getvalue()[0][0:8], sname,sim_years, obs_year1, iday)  # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_HIST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                    SNX_list.append(SNX_fname)
            else: #if planting window covers two consecutive years
                #go through Year 1
                end_day = int(start_year)*1000 + 365
                if calendar.isleap(int(start_year)):  end_day = int(start_year)*1000 + 366
                for iday in range(pdate_1,end_day + 1,step):   
                    self.write_param_main_hist(param_fname,self.Wstation.getvalue()[0][0:8], sname,sim_years, obs_year1,iday)   # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_HIST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                    SNX_list.append(SNX_fname) 
                    temp_date = iday
                #next, go throught Year 2
                start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 365  #e.g, 364 + 5 - 365 = DOY 4
                if calendar.isleap(int(start_year)):  start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 366
                for iday in range(start_day,pdate_2 + 1,step):   #e.g, 2018004 to 2018030
                    self.write_param_main_hist(param_fname,self.Wstation.getvalue()[0][0:8], sname,sim_years, obs_year1,iday)  # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_HIST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                    SNX_list.append(SNX_fname)   
            #=============================================
            # Create DSSBatch.v47 file and run DSSAT executable 
            #=============================================
            entries = ("PlantGro.OUT","Evaluate.OUT", "ET.OUT","OVERVIEW.OUT","PlantN.OUT","SoilNi.OUT","Weather.OUT", "FloodW.out", "FloodN.out", 
                    "SolNBalSum.OUT","SoilNoBal.OUT","SoilTemp.OUT","SoilWat.OUT","SoilWatBal.OUT","Summary.OUT") 
            self.writeV47_main_hist(SNX_list, obs_flag)   ##write *.V47 batch file
            #==RUN DSSAT with ARGUMENT 
            start_time = time.perf_counter() # => not compatible with Python 2.7
            os.chdir(Wdir_path)  #change directory
            args = "DSCSM047.EXE N DSSBatch.v47 "
            subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
            end_time0 = time.perf_counter() # => not compatible with Python 2.7
            print('It took {0:5.1f} sec to finish n years of obs weather x n planting dates'.format(end_time0-start_time))
            start_time1 = time.perf_counter()  # => not compatible with Python 2.7

            #create a new folder to save outputs of the target scenario
            new_folder=self.name1.getvalue()+"_output_hist"
            if os.path.exists(new_folder):
                shutil.rmtree(new_folder)   #remove existing folder
            os.makedirs(new_folder)
            #copy outputs to the new folder
            dest_dir=Wdir_path + "\\"+new_folder
            # print 'dest_dir is', dest_dir
            for entry in entries:
                source_file=Wdir_path + "\\" + entry
                if os.path.isfile(source_file):
                    shutil.move(source_file, dest_dir)
                else:
                    print( '**Error!!- No DSSAT output files => Check if DSSAT simuation was successful')

            #move *.SNX file to the new folder with output files
            for i in range(len(SNX_list)):
                shutil.move(SNX_list[i], dest_dir)
            #=============================================
            # Make a boxplot and exceedance curve of yield and other output
            #=============================================
            self.Yield_boxplot_hist(sname, SNX_list, start_year, sim_years, obs_flag)
            #=============================================
            # Make plots of two yield boxplots(forecast-based vs. historical)
            #=============================================
            self.Yield_boxplot_compare(sname, SNX_list, start_year, sim_years,obs_flag)
            #=============================================
            # Write tercile yield forecast
            #=============================================
            self.Yield_tercile_forecast(sname, SNX_list, start_year, sim_years,obs_flag)
            #=============================================
            # Make plots of water/nitrogen stresss
            #=============================================
            self.WSGD_plot_hist(sname, SNX_list, start_year, sim_years,obs_flag)
            #=============================================
            # Make plots of weather time series (Tmin, Tmax, Srad)
            #=============================================
            self.weather_plot_hist(sname, SNX_list, start_year, sim_years,obs_flag)
            #=============================================
            # Make plots of cumulative rainfall time series
            #=============================================
            self.plot_cum_rain_hist(sname, SNX_list, start_year, sim_years,obs_flag)
            print('========================================================================')
            print('It took {0:5.1f} sec to finish 100 simulations & to post-process outputs'.format(end_time0-start_time))
            print ("** Yeah!! DSSAT Simulation has been finished successfully!")
            print ("** Please check all output and figures in your working directory.")
            print('========================================================================')
######################################################################
    def writeSNX2_DSSAT_hist(self): 
        #check errorneous cases for fertilizer
        if Fbutton1.get() == 1 :  #fertilizer button is selected
            if self.label006.cget("text") == 'N/A':
                self.fertilizer_err.activate()
        #check errorneous cases for  irrigation 
        if IRbutton.get() == 0 :  #Automatic when required is selected
            print("This auto-irrigation option was deleted")
            # if self.label402.cget("text") == 'N/A':
            #     self.irrigation_err.activate()
        if IRbutton.get() == 2:  #On Reported dates is selected
            if self.label105.cget("text") == 'N/A':
                self.irrigation_err.activate()
        if IRbutton.get() == 1:  #automatic (periodic) irrigation for every n days
            if self.label418.cget("text") == 'N/A':
                self.irrigation_err.activate()

        if self.name2.getvalue() == "":
            self.writeSNX_err.activate()
        else:
            sname=self.name2.getvalue()
            WSTA=self.Wstation.getvalue()[0][0:4] #'SANJ'
            WTD_fname=Wdir_path.replace("/","\\") + "\\"+ WSTA + ".WTD"
            WTD_last_date = find_obs_date (WTD_fname)

            start_year=self.startyear.getvalue() #'2002'
            obs_flag = 0  #by default, assumes no obs weather data is available
            #=============================================
            #Determine pdate_1[YYYYDOY] and pdate_2[YYYYDOY]
            lst = list(self.plt_button1.state())  #Year 1 for planting
            lst2 = list(self.plt_button2.state())   #Year 2 for planting
            lst_frst = list(self.scf_button1.state()) #Year 1 for SCF
            lst_frst2 = list(self.scf_button2.state())
            pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m = find_plt_date (start_year, lst, lst2, lst_frst, lst_frst2)

            #find simulation start date
            temp=int(pdate_1%1000)-30  #initial condition: 30 day before planting  #EJ(3/13/2019)
            if temp <= 0 :   #EJ(3/13/2019)
                if calendar.isleap(int(start_year)-1):  
                    temp = 366 + temp
                else:
                    temp = 365 + temp
                IC_date = (pdate_1//1000 -1)*1000+temp
            else:
                IC_date = (pdate_1//1000)*1000+temp
            #Determines how many crop growing seasons are available from the historical observed.
            sim_years, obs_year1 = find_obs_years(WTD_fname, IC_date, hv_date) # <=======********

            if WTD_last_date >= hv_date: #180 is arbitrary number to fully cover weather data until late maturity
                obs_flag = 1
            step = int(self.step2.getvalue())#5 days distance for visualizing boxplots
            SNX_list = []
            os.chdir(Wdir_path)  #change directory
            param_fname=Wdir_path.replace("/","\\") + "\\param_RI_SNX_hist.txt"
            if pdate_2%1000 > pdate_1%1000:
                for iday in range(pdate_1,pdate_2+1,step):   
                    self.write_param_main_hist(param_fname,self.Wstation.getvalue()[0][0:8], sname,sim_years, obs_year1,iday)   # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_HIST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                    SNX_list.append(SNX_fname)
            else: #if planting window covers two consecutive years
                #go through Year 1
                end_day = int(start_year)*1000 + 365
                if calendar.isleap(int(start_year)):  end_day = int(start_year)*1000 + 366
                for iday in range(pdate_1,end_day + 1,step):   
                    self.write_param_main_hist(param_fname,self.Wstation.getvalue()[0][0:8], sname,sim_years,obs_year1, iday)  # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_HIST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                    SNX_list.append(SNX_fname) 
                    temp_date = iday
                #next, go throught Year 2
                start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 365  #e.g, 364 + 5 - 365 = DOY 4
                if calendar.isleap(int(start_year)):  start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 366
                for iday in range(start_day,pdate_2 + 1,step):   #e.g, 2018004 to 2018030
                    self.write_param_main_hist(param_fname,self.Wstation.getvalue()[0][0:8], sname,sim_years,obs_year1, iday)   # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_HIST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                    SNX_list.append(SNX_fname)     
            #=============================================
            # Create DSSBatch.v47 file and run DSSAT executable 
            #=============================================
            entries = ("PlantGro.OUT","Evaluate.OUT", "ET.OUT","OVERVIEW.OUT","PlantN.OUT","SoilNi.OUT","Weather.OUT", "FloodW.out", "FloodN.out",
                    "SolNBalSum.OUT","SoilNoBal.OUT","SoilTemp.OUT","SoilWat.OUT","SoilWatBal.OUT","Summary.OUT") 
            self.writeV47_main_hist(SNX_list, obs_flag)   ##write *.V47 batch file
            #==RUN DSSAT with ARGUMENT 
            start_time = time.perf_counter() # => not compatible with Python 2.7
            os.chdir(Wdir_path)  #change directory
            args = "DSCSM047.EXE N DSSBatch.v47 "
            subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
            end_time0 = time.perf_counter() # => not compatible with Python 2.7
            print('It took {0:5.1f} sec to finish n years of obs weather x n planting dates'.format(end_time0-start_time))
            start_time1 = time.perf_counter()  # => not compatible with Python 2.7

            #create a new folder to save outputs of the target scenario
            new_folder=self.name2.getvalue()+"_output_hist"
            if os.path.exists(new_folder):
                shutil.rmtree(new_folder)   #remove existing folder
            os.makedirs(new_folder)
            #copy outputs to the new folder
            dest_dir=Wdir_path + "\\"+new_folder
            # print 'dest_dir is', dest_dir
            for entry in entries:
                source_file=Wdir_path + "\\" + entry
                if os.path.isfile(source_file):
                    shutil.move(source_file, dest_dir)
                else:
                    print( '**Error!!- No DSSAT output files => Check if DSSAT simuation was successful')

            #move *.SNX file to the new folder with output files
            for i in range(len(SNX_list)):
                shutil.move(SNX_list[i], dest_dir)
            #=============================================
            # Make a boxplot and exceedance curve of yield and other output
            #=============================================
            self.Yield_boxplot_hist(sname, SNX_list, start_year, sim_years, obs_flag)
            #=============================================
            # Make plots of two yield boxplots(forecast-based vs. historical)
            #=============================================
            self.Yield_boxplot_compare(sname, SNX_list, start_year, sim_years,obs_flag)
            #=============================================
            # Write tercile yield forecast
            #=============================================
            self.Yield_tercile_forecast(sname, SNX_list, start_year, sim_years,obs_flag)
            #=============================================
            # Make plots of water/nitrogen stresss
            #=============================================
            self.WSGD_plot_hist(sname, SNX_list, start_year, sim_years,obs_flag)
            #=============================================
            # Make plots of weather time series (Tmin, Tmax, Srad)
            #=============================================
            self.weather_plot_hist(sname, SNX_list, start_year, sim_years,obs_flag)
            #=============================================
            # Make plots of cumulative rainfall time series
            #=============================================
            self.plot_cum_rain_hist(sname, SNX_list, start_year, sim_years,obs_flag)
            print('========================================================================')
            print('It took {0:5.1f} sec to finish 100 simulations & to post-process outputs'.format(end_time0-start_time))
            print ("** Yeah!! DSSAT Simulation has been finished successfully!")
            print ("** Please check all output and figures in your working directory.")
            print('========================================================================')
######################################################################
    def writeSNX3_DSSAT_hist(self): 
        #check errorneous cases for fertilizer
        if Fbutton1.get() == 1 :  #fertilizer button is selected
            if self.label006.cget("text") == 'N/A':
                self.fertilizer_err.activate()
        #check errorneous cases for  irrigation 
        if IRbutton.get() == 0 :  #Automatic when required is selected
            print("This auto-irrigation option was deleted")
            # if self.label402.cget("text") == 'N/A':
            #     self.irrigation_err.activate()
        if IRbutton.get() == 2:  #On Reported dates is selected
            if self.label105.cget("text") == 'N/A':
                self.irrigation_err.activate()
        if IRbutton.get() == 1:  #automatic (periodic) irrigation for every n days
            if self.label418.cget("text") == 'N/A':
                self.irrigation_err.activate()

        if self.name3.getvalue() == "":
            self.writeSNX_err.activate()
        else:
            sname=self.name3.getvalue()
            WSTA=self.Wstation.getvalue()[0][0:4] #'SANJ'
            WTD_fname=Wdir_path.replace("/","\\") + "\\"+ WSTA + ".WTD"
            WTD_last_date = find_obs_date (WTD_fname)

            start_year=self.startyear.getvalue() #'2002'
            obs_flag = 0  #by default, assumes no obs weather data is available
            #=============================================
            #Determine pdate_1[YYYYDOY] and pdate_2[YYYYDOY]
            lst = list(self.plt_button1.state())  #Year 1 for planting
            lst2 = list(self.plt_button2.state())   #Year 2 for planting
            lst_frst = list(self.scf_button1.state()) #Year 1 for SCF
            lst_frst2 = list(self.scf_button2.state())
            pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m = find_plt_date (start_year, lst, lst2, lst_frst, lst_frst2)

            #find simulation start date
            temp=int(pdate_1%1000)-30  #initial condition: 30 day before planting  #EJ(3/13/2019)
            if temp <= 0 :   #EJ(3/13/2019)
                if calendar.isleap(int(start_year)-1):  
                    temp = 366 + temp
                else:
                    temp = 365 + temp
                IC_date = (pdate_1//1000 -1)*1000+temp
            else:
                IC_date = (pdate_1//1000)*1000+temp
            #Determines how many crop growing seasons are available from the historical observed.
            sim_years, obs_year1 = find_obs_years(WTD_fname, IC_date, hv_date) # <=======********

            if WTD_last_date >= hv_date: #180 is arbitrary number to fully cover weather data until late maturity
                obs_flag = 1
            step = int(self.step3.getvalue())#5 days distance for visualizing boxplots
            SNX_list = []
            os.chdir(Wdir_path)  #change directory
            param_fname=Wdir_path.replace("/","\\") + "\\param_RI_SNX_hist.txt"
            if pdate_2%1000 > pdate_1%1000:
                for iday in range(pdate_1,pdate_2+1,step):   
                    self.write_param_main_hist(param_fname,self.Wstation.getvalue()[0][0:8], sname,sim_years, obs_year1,iday)   # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_HIST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                    SNX_list.append(SNX_fname)
            else: #if planting window covers two consecutive years
                #go through Year 1
                end_day = int(start_year)*1000 + 365
                if calendar.isleap(int(start_year)):  end_day = int(start_year)*1000 + 366
                for iday in range(pdate_1,end_day + 1,step):   
                    self.write_param_main_hist(param_fname,self.Wstation.getvalue()[0][0:8], sname,sim_years, obs_year1,iday)   # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_HIST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                    SNX_list.append(SNX_fname) 
                    temp_date = iday
                #next, go throught Year 2
                start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 365  #e.g, 364 + 5 - 365 = DOY 4
                if calendar.isleap(int(start_year)):  start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 366
                for iday in range(start_day,pdate_2 + 1,step):   #e.g, 2018004 to 2018030
                    self.write_param_main_hist(param_fname,self.Wstation.getvalue()[0][0:8], sname,sim_years, obs_year1, iday)  # Make a Param.txt to feed to Fortran executable                           
                    args = "RI_SNX_HIST.exe " + param_fname
                    subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
                    SNX_fname=Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                    SNX_list.append(SNX_fname)   
            #=============================================
            # Create DSSBatch.v47 file and run DSSAT executable 
            #=============================================
            entries = ("PlantGro.OUT","Evaluate.OUT", "ET.OUT","OVERVIEW.OUT","PlantN.OUT","SoilNi.OUT","Weather.OUT", "FloodW.out", "FloodN.out",
                    "SolNBalSum.OUT","SoilNoBal.OUT","SoilTemp.OUT","SoilWat.OUT","SoilWatBal.OUT","Summary.OUT") 
            self.writeV47_main_hist(SNX_list, obs_flag)   ##write *.V47 batch file
            #==RUN DSSAT with ARGUMENT 
            start_time = time.perf_counter() # => not compatible with Python 2.7
            os.chdir(Wdir_path)  #change directory
            args = "DSCSM047.EXE N DSSBatch.v47 "
            subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
            end_time0 = time.perf_counter() # => not compatible with Python 2.7
            print('It took {0:5.1f} sec to finish n years of obs weather x n planting dates'.format(end_time0-start_time))
            start_time1 = time.perf_counter()  # => not compatible with Python 2.7

            #create a new folder to save outputs of the target scenario
            new_folder=self.name3.getvalue()+"_output_hist"
            if os.path.exists(new_folder):
                shutil.rmtree(new_folder)   #remove existing folder
            os.makedirs(new_folder)
            #copy outputs to the new folder
            dest_dir=Wdir_path + "\\"+new_folder
            # print 'dest_dir is', dest_dir
            for entry in entries:
                source_file=Wdir_path + "\\" + entry
                if os.path.isfile(source_file):
                    shutil.move(source_file, dest_dir)
                else:
                    print( '**Error!!- No DSSAT output files => Check if DSSAT simuation was successful')

            #move *.SNX file to the new folder with output files
            for i in range(len(SNX_list)):
                shutil.move(SNX_list[i], dest_dir)
            #=============================================
            # Make a boxplot and exceedance curve of yield and other output
            #=============================================
            self.Yield_boxplot_hist(sname, SNX_list, start_year, sim_years, obs_flag)
            #=============================================
            # Make plots of two yield boxplots(forecast-based vs. historical)
            #=============================================
            self.Yield_boxplot_compare(sname, SNX_list, start_year, sim_years,obs_flag)
            #=============================================
            # Write tercile yield forecast
            #=============================================
            self.Yield_tercile_forecast(sname, SNX_list, start_year, sim_years,obs_flag)
            #=============================================
            # Make plots of water/nitrogen stresss
            #=============================================
            self.WSGD_plot_hist(sname, SNX_list, start_year, sim_years,obs_flag)
            #=============================================
            # Make plots of weather time series (Tmin, Tmax, Srad)
            #=============================================
            seelf.weather_plot_hist(sname, SNX_list, start_year, sim_years,obs_flag)
            #=============================================
            # Make plots of cumulative rainfall time series
            #=============================================
            self.plot_cum_rain_hist(sname, SNX_list, start_year, sim_years,obs_flag)
            print('========================================================================')
            print('It took {0:5.1f} sec to finish 100 simulations & to post-process outputs'.format(end_time0-start_time))
            print ("** Yeah!! DSSAT Simulation has been finished successfully!")
            print ("** Please check all output and figures in your working directory.")
            print('========================================================================')
######################################################################
    def writeV47_main_hist(self, snx_list, obs_flag):
        temp_dv4=Wdir_path.replace("/","\\") + "\\DSSBatch_TEMP_RI.v47"
        dv4_fname=Wdir_path.replace("/","\\") + "\\DSSBatch.v47" 
        fr = open(temp_dv4,"r") #opens temp DV4 file to read
        fw = open(dv4_fname,"w") 
        #read template and write lines
        for line in range(0,10):
            temp_str=fr.readline()
            fw.write(temp_str)
            
        temp_str=fr.readline()
        #write SNX file with each different planting date
        for i in range(len(snx_list)):   #EJ(3/12/2019) for finding optimal planting dates
            new_str2='{:<95}'.format(snx_list[i])+ temp_str[95:]
           # new_str2='{0:<95}{1:4s}'.format(snx_list[i], repr(j+1).rjust(4))+ temp_str[99:]
            fw.write(new_str2)             
            fw.write(" \n")  
        fr.close()
        fw.close()
 
    def write_param_main_hist(self,param_fname,station_name7, sname, sim_years,obs_year1, plt_date):   #Note: plt_date is in YYYYDOY format             
        f = open(param_fname,"w") #opens file 
        working_dir=Wdir_path.replace("/","\\")
        f.write("!Working directory where DSSAT4.5 is installed and weather or soil files can be found \n")
        f.write("Directory:  " + working_dir + "\n")
        f.write("!  \n")
        f.write("!==(I) Simulation set up \n")
        f.write("!(1) Choose weather station (8char) \n")
        f.write("Station_name: " + station_name7 + " \n")
        f.write("Scenario_name: "+ sname + " \n")
        f.write("!(2)Observed weather data available: Yes(1), No(0) \n")
        f.write("num_years: " + repr(sim_years) + " \n")
        f.write("! \n")
        f.write("!!(3)First Obs Year and Planting DOY \n")
        f.write("1st_obs_year: " + repr(obs_year1) + " \n")
        f.write("Planting_doy: " + repr(plt_date%1000) + " \n")
        f.write("! \n")
        f.write("!==========(III) DSSAT set up  \n")
        f.write("Crop_type: Rice   \n")
        f.write("!Planting method: dry seed (0) or transplanting (1)   \n")
        f.write("Plt_method: "+str(Rbutton2.get())+" \n")
        f.write("!Planting detail \n")
        f.write("!Planting distribution: Hills (H), Rows (R), Brocast(B) \n")
        PLDS=self.plt_dist.getvalue()[0][0:1] #"Hills", "Rows", "Rows"
        f.write("PLDS: "+PLDS+ " \n")
        f.write("!Planting population at seedling, plant/m2 \n")
        f.write("PPOP: "+self.ppop_seed.getvalue()+ " \n")
        f.write("!Planting population at emergence, plant/m2 \n")
        f.write("PPOE: "+self.ppop_emer.getvalue()+ " \n")
        f.write("!Planting Row spacing, cm \n")
        f.write("PLRS: "+self.row_space.getvalue()+ " \n")
        f.write("!Row Direction , degrees from North \n")
        f.write("PLRD: "+self.row_dir.getvalue()+ " \n")
        f.write("!Planting depth , cm \n")
        f.write("PLDP: "+self.plt_depth.getvalue()+ " \n")
        f.write("!  \n")
        f.write("!(2) soil type: Initial soil H2O: 0.3, 0.5, 0.7 or 1), Initial soil NO3(H, M, L) \n")
       # soil = self.soil_type.getvalue()[0][-11:-1]  #"SCL(WI_ANPH007), "LoamySand(WI_ANPH008)", "Clay(WI_VRPH021)"),"Clay(WI_VRPH043)","SCL2(WI_CMPH009)"):
        f.write("Soil_type: "+ self.soil_type.getvalue()[0][0:10] + " \n")
        f.write("Initial_H2O: "+self.wet_soil.getvalue()[0][0:3]+"\n")
        f.write("Initial_NO3: " + self.NO3_soil.getvalue()[0][0:4]+"\n")
        f.write("!  \n")
        f.write("(3)Cultivar \n")
        f.write("VAR_num: " + self.cul_type.getvalue()[0][0:6]+ "\n")
        f.write("VAR_name: " + self.cul_type.getvalue()[0][7:]+ "\n")   
        f.write("!  \n")
        f.write("!(4)Fertilizer: Yes(1), or No(0) -No automatic option \n")
        f.write("Fertilization: "+ str(Fbutton1.get())+ " \n")
        f.write("!(4-1) if Yes, 'days after sawing', 'amount','material','applications' \n")
       # print 'fertilization opt: ', Fbutton1.get()
        if(Fbutton1.get() == 1):  #fertilizer applied
          #  print 'Number_applications: ', self.nfertilizer.getvalue()
            if self.nfertilizer.getvalue() == '':  #no of fertilization is required
                  self.nfert_err_dialog.activate()
                # self.val_dialog1.activate()
            f.write("Number_applications: "+self.nfertilizer.getvalue()+ "\n")  
            f.write("Fertilizer_1(days): "+self.label006.cget("text")+ "\n")    
            f.write("Fertilizer_1(amount): "+self.label007.cget("text")+ "\n")         
            f.write("Fertilizer_1(material): "+self.label008.cget("text")+ "\n")  
            f.write("Fertilizer_1(application): "+self.label009.cget("text")+ "\n")  
            f.write("Fertilizer_2(days): "+self.label011.cget("text")+ "\n")  
            f.write("Fertilizer_2(amount): "+self.label012.cget("text")+ "\n")        
            f.write("Fertilizer_2(material): "+self.label013.cget("text")+ "\n")     
            f.write("Fertilizer_2(application): "+self.label014.cget("text")+ "\n")   
            f.write("Fertilizer_3(days): "+self.label016.cget("text")+ "\n")     
            f.write("Fertilizer_3(amount): "+self.label017.cget("text")+ "\n")  
            f.write("Fertilizer_3(material): "+self.label018.cget("text")+ "\n")    
            f.write("Fertilizer_3(application): "+self.label019.cget("text")+ "\n")  
        else:  #no fertilizer
            f.write("Number_applications: \n")         
            f.write("Fertilizer_1(days): \n")    
            f.write("Fertilizer_1(amount): \n")    
            f.write("Fertilizer_1(material): \n")         
            f.write("Fertilizer_1(application): \n")    
            f.write("Fertilizer_2(days): \n")    
            f.write("Fertilizer_2(amount): \n")         
            f.write("Fertilizer_2(material): \n")    
            f.write("Fertilizer_2(application): \n")  
            f.write("Fertilizer_3(days): \n")    
            f.write("Fertilizer_3(amount): \n") 
            f.write("Fertilizer_3(material): \n")    
            f.write("Fertilizer_3(application): \n") 
        f.write("!  \n")
        f.write("!(5) Irrigation: Automatic soil(0),Automatic flooding(1), on reported dates (2), no irrigation(3)  \n")
        f.write("Irrigation_method: "+ str(IRbutton.get())+ " \n")
        f.write("!(5-1)if Automatic(0) \n")
        if(IRbutton.get() == 0):  #Automatic when required
            print("This auto-irrigation option was deleted")
            # f.write("Management_depth(cm):  "+self.label402.cget("text")+ "\n")  
            # f.write("Threshold: "+self.label404.cget("text")+ "\n")    
            # f.write("EndPoint: 100 \n")  #"+self.label406.cget("text")+ "\n")         
            # f.write("Efficiency_Fraction: "+self.label408.cget("text")+ "\n")  
            # f.write("End_of_Application: IB001 \n") #"+self.label410.cget("text")+ "\n")  
            # f.write("Method: IR001 \n") #"+self.label412.cget("text")+ "\n")  
            # f.write("Amount: \n")  
            # f.write("!(5-2)if Manual  \n")  
            # f.write("Number_irrigation: \n")   
            # f.write("!(5-2-1) if transplanted \n")   
            # f.write("Puddling_date: \n")   
            # f.write("Puddling: \n")   
            # f.write("!whether transplanted or dry seeds \n")   
            # f.write("Percolation_rate: \n")   
            # f.write("First_irrigation_date: \n")
            # f.write("Bund_height_1: \n")
            # f.write("Flood_depth_1: \n")  
            # f.write("Constant_depth1?: Not added  \n")   
            # f.write("Second_irrigation_date: \n")   
            # f.write("Bund_height_2: \n")
            # f.write("Flood_depth_2: \n")  
            # f.write("Constant_depth2?: Not added   \n")   
            # f.write("Third_irrigation_date: \n")   
            # f.write("Bund_height_3: \n")
            # f.write("Flood_depth_3: \n") 
            # f.write("Constant_depth3?: Not added   \n")  
            # f.write("!(5-1)Automotic-repeated flooding   \n")  
            # f.write("Puddling_date:   \n")  
            # f.write("1st_irrig_date(DAP):   \n")  
            # f.write("Bund_height:   \n")  
            # f.write("Flooding_depth:   \n")  
            # f.write("Interval:   \n")  
            # f.write("EndApplication(DAP):   \n")  
        elif(IRbutton.get() == 2):  # irrigation on report dates
            if self.nirrigation.getvalue() == '':  #no of fertilization is required
                  self.nirr_err_dialog.activate()
            f.write("Management_depth(cm): \n") 
            f.write("Threshold: \n")   
            f.write("EndPoint: \n")        
            f.write("Efficiency_Fraction: \n") 
            f.write("End_of_Application: \n") 
            f.write("Method: \n") 
            f.write("Amount: \n")  
            f.write("!(5-2)if Manual \n")  
            f.write("Number_irrigation:  "+self.nirrigation.getvalue()+ "\n")     
            f.write("!(5-2-1) if transplanted  \n")   
            f.write("Puddling_date: "+self.label121.cget("text")+ "\n")     
            f.write("Puddling:  0 \n") #"+self.label123.cget("text")+ "\n")    
            f.write("!whether transplanted or dry seeds \n")   
            f.write("Percolation_rate: "+self.label125.cget("text")+ "\n")    
            f.write("First_irrigation_date: "+self.label105.cget("text")+ "\n")  
            f.write("Bund_height_1: "+self.label106.cget("text")+ "\n")    
            f.write("Flood_depth_1: "+self.label107.cget("text")+ "\n")
            f.write("Constant_depth1?: "+self.label1071.cget("text")+ "\n")   
            f.write("Second_irrigation_date: "+self.label109.cget("text")+ "\n")     
            f.write("Bund_height_2: "+self.label110.cget("text")+ "\n")  
            f.write("Flood_depth_2: "+self.label111.cget("text")+ "\n")
            f.write("Constant_depth2?: "+self.label1111.cget("text")+ "\n") 
            f.write("Third_irrigation_date: "+self.label113.cget("text")+ "\n")     
            f.write("Bund_height_3: "+self.label114.cget("text")+ "\n")  
            f.write("Flood_depth_3: "+self.label115.cget("text")+ "\n")
            f.write("Constant_depth3?: "+self.label1151.cget("text")+ "\n") 
            f.write("!(5-1)Automotic-repeated flooding   \n")  
            f.write("Puddling_date:   \n")  
            f.write("1st_irrig_date(DAP):   \n")  
            f.write("Bund_height:   \n")  
            f.write("Flooding_depth:   \n")  
            f.write("Interval:   \n")  
            f.write("EndApplication(DAP):   \n") 
        elif(IRbutton.get() == 3):  # no irrigation
            f.write("Management_depth(cm):   \n") 
            f.write("Threshold:   \n")   
            f.write("EndPoint:   \n")        
            f.write("Efficiency_Fraction:   \n") 
            f.write("End_of_Application:  \n") 
            f.write("Method:   \n") 
            f.write("Amount: \n")  
            f.write("!(5-2)if Manual  \n")   
            f.write("Number_irrigation:  \n")   
            f.write("!(5-2-1) if transplanted  \n")   
            f.write("Puddling_date:  \n")   
            f.write("Puddling:   \n")   
            f.write("!whether transplanted or dry seeds \n")   
            f.write("Percolation_rate:  \n")   
            f.write("First_irrigation_date: \n")   
            f.write("Bund_height_1:   \n")  
            f.write("Flood_depth_1:   \n")
            f.write("Constant_depth1?: \n")
            f.write("Second_irrigation_date:  \n")   
            f.write("Bund_height_2: \n")   
            f.write("Flood_depth_2:  \n")
            f.write("Constant_depth2?: \n")
            f.write("Third_irrigation_date:  \n")   
            f.write("Bund_height_3: \n")   
            f.write("Flood_depth_3: \n")
            f.write("Constant_depth3?: \n")
            f.write("!(5-1)Automotic-repeated flooding   \n")  
            f.write("Puddling_date:   \n")  
            f.write("1st_irrig_date(DAP):   \n")  
            f.write("Bund_height:   \n")  
            f.write("Flooding_depth:   \n")  
            f.write("Interval:   \n")  
            f.write("EndApplication(DAP):   \n") 
        else:  #repeated irrigation
            f.write("Management_depth(cm):   \n") 
            f.write("Threshold:   \n")   
            f.write("EndPoint:   \n")        
            f.write("Efficiency_Fraction:   \n") 
            f.write("End_of_Application:  \n") 
            f.write("Method:   \n") 
            f.write("Amount: \n")  
            f.write("!(5-2)if Manual  \n")   
            f.write("Number_irrigation:  \n")   
            f.write("!(5-2-1) if transplanted  \n")   
            f.write("Puddling_date:  \n")   
            f.write("Puddling:   \n")   
            f.write("!whether transplanted or dry seeds \n")   
            f.write("Percolation_rate:  \n")   
            f.write("First_irrigation_date: \n")   
            f.write("Bund_height_1:   \n")  
            f.write("Flood_depth_1:   \n")
            f.write("Constant_depth1?: \n")
            f.write("Second_irrigation_date:  \n")   
            f.write("Bund_height_2: \n")   
            f.write("Flood_depth_2:  \n")
            f.write("Constant_depth2?: \n")
            f.write("Third_irrigation_date:  \n")   
            f.write("Bund_height_3: \n")   
            f.write("Flood_depth_3: \n")
            f.write("Constant_depth3?: \n")
            f.write("!(5-1)Automotic-repeated flooding   \n")  
            f.write("Puddling_date: " +self.label418.cget("text")+ "\n")  
            f.write("1st_irrig_date(DAP): " +self.label420.cget("text")+ "\n")
            f.write("Bund_height: " +self.label416.cget("text")+ "\n")
            f.write("Flooding_depth: " +self.label410.cget("text")+ "\n")
            f.write("Interval: " +self.label412.cget("text")+ "\n")
            f.write("EndApplication(DAP): " +self.label414.cget("text")+ "\n")
        f.close()
    #====End of WRITE param.txt

    ##############################################################
######################################################################
    ##############################################################
    def Yield_boxplot_hist(self,sname, SNX_list, start_year, sim_years, obs_flag):
        fname1 = Wdir_path + "\\"+ sname+"_output_hist\\SUMMARY.OUT"
        pdate_list = []
        year_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(int(SNX_list[i][-11:-8]))
            if i == 0:
                year_list.append(int(start_year))
            else:
                if pdate_list[i] > pdate_list[i-1]:
                    year_list.append(int(start_year))
                else:
                    year_list.append(int(start_year) + 1)

        df_OUT=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
        PDAT = df_OUT.ix[:,14].values  #read 14th column only => Planting date 
        HWAM = df_OUT.ix[:,21].values  #read 21th column only
        ADAT = df_OUT.ix[:,16].values  #read 21th column only
        MDAT = df_OUT.ix[:,17].values  #read 21th column only

        year_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize
        DOY_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize
        ADAT_array = np.zeros(len(ADAT)) #np.empty((ndays,1))*np.nan #initialize
        MDAT_array = np.zeros(len(MDAT)) #np.empty((ndays,1))*np.nan #initialize
        for i in range(len(HWAM)):
            year_array[i] = int(repr(PDAT[i])[:4])
            DOY_array[i] = int(repr(PDAT[i])[4:])
            #MDAT_array[i] = int(repr(MDAT[i])[4:])
            if ADAT[i] == 0:  #in case emergence does not happen
                ADAT_array[i] = np.nan
                MDAT_array[i] = np.nan
            elif ADAT[i] == -99:  #in case emergence does not happen
                ADAT_array[i] = np.nan
                MDAT_array[i] = np.nan
            elif MDAT[i] == -99: 
                MDAT_array[i] = np.nan
            else:
                ADAT_array[i] = int(repr(ADAT[i])[4:])
                MDAT_array[i] = int(repr(MDAT[i])[4:])
                
        #make three arrays into a dataframe
        df = pd.DataFrame({'Year': year_array,'DOY': DOY_array, 'ADAT':ADAT_array, 'MDAT':MDAT_array, 'HWAM':HWAM}, columns=['Year','DOY','ADAT','MDAT','HWAM'])

        # #make an empty array [nyears * n_plt_dates]
        n_year = sim_years
        # For Boxplot
        yield_n = np.empty([n_year,len(pdate_list)])*np.nan
        ADAT_n = np.empty([n_year,len(pdate_list)])*np.nan
        MDAT_n = np.empty([n_year,len(pdate_list)])*np.nan
        # For exceedance curve
        sorted_yield_n = np.empty([n_year,len(pdate_list)])*np.nan
        Fx_scf = np.empty([n_year,len(pdate_list)])*np.nan       
        sorted_ADAT_n = np.empty([n_year,len(pdate_list)])*np.nan   
        Fx_scf_ADAT = np.empty([n_year,len(pdate_list)])*np.nan
        sorted_MDAT_n = np.empty([n_year,len(pdate_list)])*np.nan
        # Fx_scf_MDAT = np.empty([n_year,len(pdate_list)])*np.nan
        for count in range(len(pdate_list)):
            Pdate = pdate_list[count]
            yield_n[:,count] = df.HWAM[(df["DOY"] == Pdate)].values  
            ADAT_n[:,count] = df.ADAT[(df["DOY"] == Pdate)].values 
            MDAT_n[:,count] = df.MDAT[(df["DOY"] == Pdate)].values  
            sorted_yield_n[:,count] = np.sort(df.HWAM[(df["DOY"] == Pdate)].values)
            fx_scf = [1.0/len(sorted_yield_n)] * len(sorted_yield_n) #pdf
            #Fx_scf = np.cumsum(fx_scf)
            Fx_scf[:,count] = 1.0-np.cumsum(fx_scf)  #for exceedance curve
            sorted_ADAT_n[:,count] = np.sort(df.ADAT[(df["DOY"] == Pdate)].values)
            sorted_MDAT_n[:,count] = np.sort(df.MDAT[(df["DOY"] == Pdate)].values)
            temp = sorted_ADAT_n[:,count]
            temp2 = temp[~np.isnan(temp)]
            fx_scf_ADAT = [1.0/len(temp2)] * len(temp2) #pdf
            Fx_scf_ADAT[:len(fx_scf_ADAT),count] = 1.0-np.cumsum(fx_scf_ADAT)  #for exceedance curve      

        if obs_flag == 1:
            obs_yield = []
            obs_ADAT = []
            obs_MDAT = []
            for count in range(len(pdate_list)):
                Pdate = pdate_list[count]
                if count > 0 and pdate_list[count] < pdate_list[count-1]:  #when two consecutive years
                    start_year = repr(int(start_year)+1)
                #collect simulated results from observed weather
                obs_yield.append(df.HWAM[(df["DOY"] == Pdate) & (df["Year"] == int(start_year))].values[0])      
                obs_ADAT.append(df.ADAT[(df["DOY"] == Pdate) & (df["Year"] == int(start_year))].values[0])
                obs_MDAT.append(df.MDAT[(df["DOY"] == Pdate) & (df["Year"] == int(start_year))].values[0])
       # Fx_scf_ADAT = Fx_scf
        #To remove 'nan' from data array
        #https://stackoverflow.com/questions/44305873/how-to-deal-with-nan-value-when-plot-boxplot-using-python
        mask = ~np.isnan(yield_n)
        filtered_yield = [d[m] for d, m in zip(yield_n.T, mask.T)]
        mask2 = ~np.isnan(ADAT_n)
        filtered_data2 = [d[m] for d, m in zip(ADAT_n.T, mask2.T)]
        mask3 = ~np.isnan(MDAT_n)
        filtered_data3 = [d[m] for d, m in zip(MDAT_n.T, mask3.T)]
        
        if obs_flag == 1: 
            #replace ADAT = 0 with nan
            obs_ADAT = [np.nan if x==1 else x for x in obs_ADAT]
            obs_MDAT = [np.nan if x==1 else x for x in obs_MDAT]

        #X data for plot
        myXList=[i+1 for i in range(len(pdate_list))]
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            temp_str = repr(year_list[count]) + " " + repr(pdate_list[count]) 
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)
        # 1) Plotting Yield 
        fig = plt.figure()
        fig.suptitle('Estimated Yields using historical weather', fontsize=12, fontweight='bold')
        ax = fig.add_subplot(111)
        ax.set_xlabel('Planting Date[MM/DD]',fontsize=14)
        ax.set_ylabel('Yield [kg/ha]',fontsize=14)
        if obs_flag == 1:
            # Plot a line between yields with observed weather
            plt.plot(myXList, obs_yield, 'go-')
        ax.boxplot(filtered_yield,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
        fig_name = Wdir_path + "\\"+sname+"_output_hist\\"+sname+"_Yield_boxplot.png"
        plt.savefig(fig_name)      

        # 2) Plotting ADAT
        fig2 = plt.figure()
        fig2.suptitle('Estimated Anthesis Dates using historical weather', fontsize=12, fontweight='bold')
        ax2 = fig2.add_subplot(111)
        ax2.set_xlabel('Planting Date[DOY]',fontsize=14)
        ax2.set_ylabel('Anthesis Date [DOY]',fontsize=14)
        if obs_flag == 1:
            # Plot a line between yields with observed weather
            plt.plot(myXList, obs_ADAT, 'go-')
        ax2.boxplot(filtered_data2,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
        fig_name2 = Wdir_path + "\\"+ sname+"_output_hist\\"+sname+"_ADAT_boxplot.png"
        plt.savefig(fig_name2)       

        # 3) Plotting MDAT
        fig3 = plt.figure()
        fig3.suptitle('Estimated Maturity Dates using historical weather', fontsize=12, fontweight='bold')
        ax3 = fig3.add_subplot(111)
        ax3.set_xlabel('Planting Date[DOY]',fontsize=14)
        ax3.set_ylabel('Maturity Date [DOY]',fontsize=14)
        if obs_flag == 1:
            # Plot a line between yields with observed weather
            plt.plot(myXList, obs_MDAT, 'go-')
        ax3.boxplot(filtered_data3,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
        fig_name = Wdir_path + "\\"+ sname+"_output_hist\\"+sname+"_MDAT_boxplot.png"
        plt.savefig(fig_name)    

        #4) Plotting yield exceedance curve
        fig4 = plt.figure()
        fig4.suptitle('Yield Exceedance Curve using historical weather', fontsize=12, fontweight='bold')
        ax4 = fig4.add_subplot(111)
        ax4.set_xlabel('Yield [kg/ha]',fontsize=14)
        ax4.set_ylabel('Probability of Exceedance [-]',fontsize=14)
        plt.ylim(0, 1) 
        for x in range(len(pdate_list)):
            ax4.plot(sorted_yield_n[:,x],Fx_scf[:,x],'o-', label=scename[x])
            #vertical line for the yield with observed weather
            if obs_flag == 1:
                x_data=[obs_yield[x],obs_yield[x]] #only two points to draw a line
                y_data=[0,1]
                temp='w/ obs wth (' + scename[x] + ')'
                ax4.plot(x_data,y_data,'-.', label=temp)
                plt.ylim(0, 1)
        box = ax4.get_position()  # Shrink current axis by 15%
        ax4.set_position([box.x0, box.y0, box.width * 0.70, box.height])
        plt.grid(True)
        ax4.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
        fig_name = Wdir_path + "\\" + sname + "_output_hist\\" + sname +"_Yield_exceedance.png"
        plt.savefig(fig_name)   

        #5) Plotting ADAT exceedance curve
        fig5 = plt.figure()
        fig5.suptitle('Anthesis Date Exceedance Curve using historical weather', fontsize=12, fontweight='bold')
        ax5 = fig5.add_subplot(111)
        ax5.set_xlabel('Anthesis Date [DOY]',fontsize=14)
        ax5.set_ylabel('Probability of Exceedance [-]',fontsize=14)
        plt.ylim(0, 1) 
        for x in range(len(pdate_list)):
            ax5.plot(sorted_ADAT_n[:,x],Fx_scf_ADAT[:,x],'o-', label=scename[x])
            #vertical line for the yield with observed weather
            if obs_flag == 1:
                x_data=[obs_ADAT[x],obs_ADAT[x]] #only two points to draw a line
                y_data=[0,1]
                temp='w/ obs wth (' + scename[x] + ')'
                ax5.plot(x_data,y_data,'-.', label=temp)
                plt.ylim(0, 1)
        box = ax5.get_position()
        ax5.set_position([box.x0, box.y0, box.width * 0.70, box.height])
        plt.grid(True)
        ax5.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
        fig_name = Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_ADAT_exceedance.png"
        plt.savefig(fig_name)    

        #6) Plotting MDAT exceedance curve
        fig6 = plt.figure()
        fig6.suptitle('Maturity Date Exceedance Curve using historical weather', fontsize=12, fontweight='bold')
        ax6 = fig6.add_subplot(111)
        ax6.set_xlabel('Maturity Date [DOY]',fontsize=14)
        ax6.set_ylabel('Probability of Exceedance [-]',fontsize=14)
        plt.ylim(0, 1) 
        for x in range(len(pdate_list)):
            ax6.plot(sorted_MDAT_n[:,x],Fx_scf_ADAT[:,x],'o-', label=scename[x])
            #vertical line for the yield with observed weather
            if obs_flag == 1:
                x_data=[obs_MDAT[x],obs_MDAT[x]] #only two points to draw a line
                y_data=[0,1]
                temp='w/ obs wth (' + scename[x] + ')'
                ax6.plot(x_data,y_data,'-.', label=temp)
                plt.ylim(0, 1)
        box = ax6.get_position()
        ax6.set_position([box.x0, box.y0, box.width * 0.70, box.height])
        plt.grid(True)
        ax6.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
        fig_name = Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_MDAT_exceedance.png"
        plt.savefig(fig_name)    

    ##############################################################
    def WSGD_plot_hist(self,sname, SNX_list, start_year, sim_years,obs_flag):
        fname1 = Wdir_path + "\\"+ sname+"_output_hist\\PlantGro.OUT"
        pdate_list = []
        # year_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(SNX_list[i][-11:-8])
                    
        n_days = 210 #approximate growing days
        WSGD_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
        NSTD_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN 
        if obs_flag == 1:
            WSGD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
            NSTD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
        read_flag = 0  #off
        n_count = 0  #count of n weahter realizations
        d_count = 0  #day counter during a growing season
        p_index_old = 0
        p_count = 0  #count for different planting date scenario
        obs_read_flag = 0
        with open(fname1) as fp:
            for line in fp:
                if line[18:21] == '  0':   #line[18:21] => DAP
                    p_index = pdate_list.index(line[6:9])   #line[6:9] => DOY
                    read_flag = 1  #on
                    if line[1:5] == start_year:  #if this is the observed year
                        obs_read_flag = 1  #on
                    if p_index > p_index_old:  #move to a new planting date scenario
                        p_count = p_count + 1
                        n_count = 0
                #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
                if read_flag ==1 and line == '\n':  #after reading last growing day
                    read_flag = 0 #off
                    p_index_old = p_index
                    d_count = 0
                    n_count = n_count + 1
                if read_flag == 1:
                    WSGD_3D[d_count, n_count, p_count] = float(line[105:111]) #[127:133])
                    NSTD_3D[d_count, n_count, p_count] = float(line[118:123]) #[134:140])
                    if obs_flag == 1 and obs_read_flag == 1:
                        WSGD_obs[d_count, p_count] = float(line[105:111]) #[127:133])
                        NSTD_obs[d_count, p_count] = float(line[118:123]) #[134:140])
                    d_count = d_count + 1     

        # WSGD_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
        # NSTD_avg = np.empty((n_days,len(pdate_list),))*np.NAN  
        WSGD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
        WSGD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        WSGD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        NSTD_50 = np.empty((n_days,len(pdate_list),))*np.NAN  
        NSTD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        NSTD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 

        #plot
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            if count > 0:
                if int(pdate_list[count]) < int(pdate_list[count-1]):
                    start_year = repr(int(start_year) + 1)
            temp_str = start_year + " " + pdate_list[count]
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)

        for i in range(len(pdate_list)):
            # WSGD_avg[:, i] = np.nanmean(WSGD_3D[:, 0:100, i], axis = 1)
            # NSTD_avg[:, i] = np.nanmean(WSGD_3D[:, 0:100, i], axis = 1)
            WSGD_50[:, i] = np.nanpercentile(WSGD_3D[:, 0:sim_years, i], 50, axis = 1)
            WSGD_25[:, i] = np.nanpercentile(WSGD_3D[:, 0:sim_years, i], 25, axis = 1)
            WSGD_75[:, i] = np.nanpercentile(WSGD_3D[:, 0:sim_years, i], 75, axis = 1)
            NSTD_50[:, i] = np.nanpercentile(NSTD_3D[:, 0:sim_years, i], 50, axis = 1)
            NSTD_25[:, i] = np.nanpercentile(NSTD_3D[:, 0:sim_years, i], 25, axis = 1)
            NSTD_75[:, i] = np.nanpercentile(NSTD_3D[:, 0:sim_years, i], 75, axis = 1)
        #================================== 
        #1) Plot WSGD Water stress
        nrow = math.ceil(len(pdate_list)/2.0)
        ncol = 2
        fig, axs = plt.subplots(nrow, ncol)
        fig.suptitle('WSGD (Water Stress Index)')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    xdata = range(len(WSGD_50[:,0]))
                    axs[j, i].plot(xdata, WSGD_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(WSGD_50[:,p_count]))
                    yerr_low = WSGD_50[:, p_count] -WSGD_25[:, p_count]
                    yerr_up = WSGD_75[:, p_count] - WSGD_50[:, p_count]
                    axs[j, i].errorbar(xdata, WSGD_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, WSGD_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(WSGD_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='Water Stress Index [-]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(13, 8)
        fig_name = Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_WStress.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        #==================================
        #2) Plot NSTD Nitrogen stress
        nrow = math.ceil(len(pdate_list)/2.0)
        ncol = 2
        fig, axs = plt.subplots(nrow, ncol)
        fig.suptitle('NSTD (Nitrogen Stress Index)')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    xdata = range(len(NSTD_50[:,0]))
                    axs[j, i].plot(xdata, NSTD_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(NSTD_50[:,p_count]))
                    yerr_low = NSTD_50[:, p_count] -NSTD_25[:, p_count]
                    yerr_up = NSTD_75[:, p_count] - NSTD_50[:, p_count]
                    axs[j, i].errorbar(xdata, NSTD_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, NSTD_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(NSTD_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='Nitrogen Stress Index [-]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_NStress.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
    ##############################################################  
    def weather_plot_hist(self,sname, SNX_list, start_year, sim_years, obs_flag):
        fname1 = Wdir_path + "\\"+ sname+"_output_hist\\Weather.OUT"
        # TMND   MIN TEMP °C     Minimum daily temperature (°C)                           .
        # TMXD   MAX TEMP °C     Maximum daily temperature (°C) 
        # PRED   PRECIP mm/d     Precipitation depth (mm/d) ==>> ??????                        .
        # SRAD   SRAD MJ/m2.d    Solar radiation (MJ/(m2.d))                              .
        # TAVD   AVG TEMP °C     Average daily temperature (°C)
        pdate_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(SNX_list[i][-11:-8])

        n_days = 210 #approximate growing days
        TMXD_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
        TMND_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN 
        SRAD_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN 
        if obs_flag == 1:
            TMXD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
            TMND_obs = np.empty((n_days,len(pdate_list),))*np.NAN
            SRAD_obs = np.empty((n_days,len(pdate_list),))*np.NAN

        read_flag = 0  #off
        n_count = 0  #count of n weahter realizations
        d_count = 0  #day counter during a growing season
        p_count = 0  #count for different planting date scenario
        obs_read_flag = 0
        with open(fname1) as fp:
            for line in fp:
                if line[6:9] == pdate_list[p_count]:   #line[12:15] => DAS
                    read_flag = 1  #on
                    if line[1:5] == start_year:  #if this is the observed year
                        obs_read_flag = 1  #on
                if read_flag ==1 and line == '\n':  #after reading last growing day
                    read_flag = 0 #off
                    d_count = 0
                    n_count = n_count + 1
                    if n_count == sim_years:  #move to next planting date scenario
                        p_count = p_count + 1
                        n_count = 0
                if read_flag == 1:
                    TMXD_3D[d_count, n_count, p_count] = float(line[60:64])
                    TMND_3D[d_count, n_count, p_count] = float(line[67:71])
                    SRAD_3D[d_count, n_count, p_count] = float(line[39:43])
                    if obs_flag == 1 and obs_read_flag == 1:
                        TMXD_obs[d_count, p_count] = float(line[60:64])
                        TMND_obs[d_count, p_count] = float(line[67:71])
                        SRAD_obs[d_count, p_count] = float(line[39:43])
                    d_count = d_count + 1

        # TMXD_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
        # TMND_avg = np.empty((n_days,len(pdate_list),))*np.NAN  
        TMXD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMXD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMXD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMND_50 = np.empty((n_days,len(pdate_list),))*np.NAN  
        TMND_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMND_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        SRAD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
        SRAD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        SRAD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
    
        #plot
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            if count > 0:
                if int(pdate_list[count]) < int(pdate_list[count-1]):
                    start_year = repr(int(start_year) + 1)
            temp_str = start_year + " " + pdate_list[count]
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)

        for i in range(len(pdate_list)):
            TMXD_50[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 50, axis = 1)
            TMXD_25[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 25, axis = 1)
            TMXD_75[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 75, axis = 1)
            TMND_50[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 50, axis = 1)
            TMND_25[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 25, axis = 1)
            TMND_75[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 75, axis = 1)
            SRAD_50[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 50, axis = 1)
            SRAD_25[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 25, axis = 1)
            SRAD_75[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 75, axis = 1)
        #================================== 
        #1) Plot TMXD
        nrow = math.ceil(len(pdate_list)/2.0)
        ncol = 2
        fig, axs = plt.subplots(nrow, ncol)
        fig.suptitle('TMXD (Maximum daily temperature)')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    xdata = range(len(TMXD_50[:,0]))
                    axs[j, i].plot(xdata, TMXD_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(TMXD_50[:,p_count]))
                    yerr_low = TMXD_50[:, p_count] -TMXD_25[:, p_count]
                    yerr_up = TMXD_75[:, p_count] - TMXD_50[:, p_count]
                    axs[j, i].errorbar(xdata, TMXD_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, TMXD_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(TMXD_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='Temperature[C]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_Tmax.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        #==================================
        #2) Plot TMND
        fig, axs = plt.subplots(nrow, ncol)
        fig.suptitle('TMND (Minimum daily temperature)')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    axs[j, i].plot(xdata, TMND_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(TMND_50[:,p_count]))
                    yerr_low = TMND_50[:, p_count] -TMND_25[:, p_count]
                    yerr_up = TMND_75[:, p_count] - TMND_50[:, p_count]
                    axs[j, i].errorbar(xdata, TMND_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, TMND_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(TMND_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='Temperature[C]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_Tmin.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        #==================================
        #3) Plot SRAD   SRAD MJ/m2.d    Solar radiation (MJ/(m2.d)) 
        fig, axs = plt.subplots(nrow, ncol)
        fig.suptitle('SARD (Daily solar radiation (MJ/(m2.d))')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    axs[j, i].plot(xdata, SRAD_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(SRAD_50[:,p_count]))
                    yerr_low = SRAD_50[:, p_count] -SRAD_25[:, p_count]
                    yerr_up = SRAD_75[:, p_count] - SRAD_50[:, p_count]
                    axs[j, i].errorbar(xdata, SRAD_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, SRAD_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(SRAD_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='SRAD[MJ/(m2.d)]')
    # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_SRAD.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
    ##############################################################
    def plot_cum_rain_hist(self,sname, SNX_list, start_year, sim_years, obs_flag):
        fname1 = Wdir_path + "\\"+ sname+"_output_hist\\SoilWat.OUT"
        pdate_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(SNX_list[i][-11:-8])

        n_days = 210 + 30 #approximate growing days + 30 days from IC to plt date
        PREC_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
        if obs_flag == 1:
            PREC_obs= np.empty((n_days,len(pdate_list),))*np.NAN 

        read_flag = 0  #off
        n_count = 0  #count of n weahter realizations
        d_count = 0  #day counter during a growing season
        p_count = 0  #count for different planting date scenario
        obs_read_flag = 0
        with open(fname1) as fp:
            for line in fp:
                if line[12:15] == '  0':   #line[12:15] => DAS
                    read_flag = 1  #on
                    if line[1:5] == start_year:  #if this is the observed year
                        obs_read_flag = 1  #on
                if read_flag ==1 and line == '\n':  #after reading last growing day
                    read_flag = 0 #off
                    d_count = 0
                    n_count = n_count + 1
                    if n_count == sim_years:  #move to next planting date scenario
                        p_count = p_count + 1
                        n_count = 0
                if read_flag == 1:
                    PREC_3D[d_count, n_count, p_count] = float(line[44:48])
                    if obs_flag == 1 and obs_read_flag == 1:
                        PREC_obs[d_count, p_count] = float(line[44:48])
                    d_count = d_count + 1

        # PREC_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
        PREC_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
        PREC_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        PREC_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
    
        #plot
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            if count > 0:
                if int(pdate_list[count]) < int(pdate_list[count-1]):
                    start_year = repr(int(start_year) + 1)
            temp_str = start_year + " " + pdate_list[count]
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)

        for i in range(len(pdate_list)):
            PREC_50[:, i] = np.nanpercentile(PREC_3D[:, 0:sim_years, i], 50, axis = 1)
            PREC_25[:, i] = np.nanpercentile(PREC_3D[:, 0:sim_years, i], 25, axis = 1)
            PREC_75[:, i] = np.nanpercentile(PREC_3D[:, 0:sim_years, i], 75, axis = 1)

        #================================== 
        #1) Plot PREC
        nrow = math.ceil(len(pdate_list)/2.0)
        ncol = 2
        fig, axs = plt.subplots(nrow, ncol)
        fig.suptitle('Cumulative Precipitation')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    xdata = range(len(PREC_50[:,0]))
                    axs[j, i].plot(xdata, PREC_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(PREC_50[:,p_count]))
                    yerr_low = PREC_50[:, p_count] -PREC_25[:, p_count]
                    yerr_up = PREC_75[:, p_count] - PREC_50[:, p_count]
                    axs[j, i].errorbar(xdata, PREC_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, PREC_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(PREC_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After SIMULATION [DAS]', ylabel='Cum. Rainfall[mm]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_CumRain.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
##############################################################
    def Yield_boxplot_compare(self, sname, SNX_list, start_year, sim_years, obs_flag):
        fname1 = Wdir_path + "\\"+ sname+"_output\\SUMMARY.OUT"
        if obs_flag == 1:
            nrealiz = 101
        else:
            nrealiz = 100
        pdate_list = []
        year_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(int(SNX_list[i][-11:-8]))
            if i == 0:
                year_list.append(int(start_year))
            else:
                if pdate_list[i] > pdate_list[i-1]:
                    year_list.append(int(start_year))
                else:
                    year_list.append(int(start_year) + 1)
        #check if output  SUMMARY.OUt exists
        if not os.path.isfile(fname1):
            print( '**Error!!- SUMMARY.OUT file does not exist under ####_output folder')
            os.system('pause')

        df_OUT=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
        PDAT = df_OUT.ix[:,14].values  #read 14th column only => Planting date 
        HWAM = df_OUT.ix[:,21].values  #read 21th column only
        # ADAT = df_OUT.ix[:,16].values  #read 21th column only
        # MDAT = df_OUT.ix[:,17].values  #read 21th column only

    #    year_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize
        DOY_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize
        # ADAT_array = np.zeros(len(ADAT)) #np.empty((ndays,1))*np.nan #initialize
        # MDAT_array = np.zeros(len(MDAT)) #np.empty((ndays,1))*np.nan #initialize
        for i in range(len(HWAM)):
            # year_array[i] = int(repr(PDAT[i])[:4])
            DOY_array[i] = int(repr(PDAT[i])[4:])
            # #MDAT_array[i] = int(repr(MDAT[i])[4:])
            # if ADAT[i] == 0:  #in case emergence does not happen
            #     ADAT_array[i] = np.nan
            #     MDAT_array[i] = np.nan
            # elif ADAT[i] == -99:  #in case emergence does not happen
            #     ADAT_array[i] = np.nan
            #     MDAT_array[i] = np.nan
            # elif MDAT[i] == -99: 
            #     MDAT_array[i] = np.nan
            # else:
            #     ADAT_array[i] = int(repr(ADAT[i])[4:])
            #     MDAT_array[i] = int(repr(MDAT[i])[4:])
                
        #make three arrays into a dataframe
        df = pd.DataFrame({'DOY': DOY_array, 'HWAM':HWAM}, columns=['DOY','HWAM'])
        #df = pd.DataFrame({'DOY': DOY_array, 'ADAT':ADAT_array, 'MDAT':MDAT_array, 'HWAM':HWAM}, columns=['DOY','ADAT','MDAT','HWAM'])

        # #make an empty array [nyears * n_plt_dates]
        n_year = 100
        # For Boxplot
        yield_n = np.empty([n_year,len(pdate_list)])*np.nan
        # ADAT_n = np.empty([n_year,len(pdate_list)])*np.nan
        # MDAT_n = np.empty([n_year,len(pdate_list)])*np.nan

        if obs_flag == 0:  #if there is no observed weather available 
            for count in range(len(pdate_list)):
                Pdate = pdate_list[count]
                yield_n[:,count] = df.HWAM[(df["DOY"] == Pdate)].values  
                # ADAT_n[:,count] = df.ADAT[(df["DOY"] == Pdate)].values 
                # MDAT_n[:,count] = df.MDAT[(df["DOY"] == Pdate)].values           
        else:   
            obs_yield = []
            # obs_ADAT = []
            # obs_MDAT = []
            for count in range(len(pdate_list)):
                Pdate = pdate_list[count]
                yield_n[:,count] = df.HWAM[(df["DOY"] == Pdate)].values[:-1]  
                # ADAT_n[:,count] = df.ADAT[(df["DOY"] == Pdate)].values[:-1] 
                # MDAT_n[:,count] = df.MDAT[(df["DOY"] == Pdate)].values[:-1] 
                #  #collect simulated results from observed weather
                obs_yield.append(df.HWAM[(df["DOY"] == Pdate)].values[-1])      
                # obs_ADAT.append(df.ADAT[(df["DOY"] == Pdate)].values[-1])  
                # obs_MDAT.append(df.MDAT[(df["DOY"] == Pdate)].values[-1])  
        # Fx_scf_ADAT = Fx_scf
        #To remove 'nan' from data array
        #https://stackoverflow.com/questions/44305873/how-to-deal-with-nan-value-when-plot-boxplot-using-python
        mask = ~np.isnan(yield_n)
        filtered_yield = [d[m] for d, m in zip(yield_n.T, mask.T)]
        # mask2 = ~np.isnan(ADAT_n)
        # filtered_data2 = [d[m] for d, m in zip(ADAT_n.T, mask2.T)]
        # mask3 = ~np.isnan(MDAT_n)
        # filtered_data3 = [d[m] for d, m in zip(MDAT_n.T, mask3.T)]
        
        # if obs_flag == 1: 
        #     #replace ADAT = 0 with nan
        #     obs_ADAT = [np.nan if x==1 else x for x in obs_ADAT]
        #     obs_MDAT = [np.nan if x==1 else x for x in obs_MDAT]
    ##====================================================
    ##READ Yields from historical weather observations
        fname1 = Wdir_path + "\\"+ sname+"_output_hist\\SUMMARY.OUT"

        #check if output  SUMMARY.OUt exists
        if not os.path.isfile(fname1):
            print( '**Error!!- SUMMARY.OUT file does not exist under ####_output_hist folder')
            os.system('pause')

        df_OUT2=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
        PDAT2 = df_OUT2.ix[:,14].values  #read 14th column only => Planting date 
        HWAM2 = df_OUT2.ix[:,21].values  #read 21th column only
        # ADAT2 = df_OUT2.ix[:,16].values  #read 21th column only
        # MDAT2 = df_OUT2.ix[:,17].values  #read 21th column only

        year_array2 = np.zeros(len(HWAM2)) #np.empty((ndays,1))*np.nan #initialize
        DOY_array2 = np.zeros(len(HWAM2)) #np.empty((ndays,1))*np.nan #initialize
        # ADAT2_array2 = np.zeros(len(ADAT2)) #np.empty((ndays,1))*np.nan #initialize
        # MDAT2_array2 = np.zeros(len(MDAT2)) #np.empty((ndays,1))*np.nan #initialize
        for i in range(len(HWAM2)):
            year_array2[i] = int(repr(PDAT2[i])[:4])
            DOY_array2[i] = int(repr(PDAT2[i])[4:])
            # #MDAT2_array2[i] = int(repr(MDAT2[i])[4:])
            # if ADAT2[i] == 0:  #in case emergence does not happen
            #     ADAT2_array2[i] = np.nan
            #     MDAT2_array2[i] = np.nan
            # elif ADAT2[i] == -99:  #in case emergence does not happen
            #     ADAT2_array2[i] = np.nan
            #     MDAT2_array2[i] = np.nan
            # elif MDAT2[i] == -99: 
            #     MDAT2_array2[i] = np.nan
            # else:
            #     ADAT2_array2[i] = int(repr(ADAT2[i])[4:])
            #     MDAT2_array2[i] = int(repr(MDAT2[i])[4:])
                
        #make three arrays into a dataframe
        df2 = pd.DataFrame({'Year': year_array2,'DOY': DOY_array2, 'HWAM':HWAM2}, columns=['Year','DOY','HWAM'])
        #df2 = pd.DataFrame({'Year': year_array2,'DOY': DOY_array2, 'ADAT':ADAT2_array2, 'MDAT':MDAT2_array2, 'HWAM':HWAM2}, columns=['Year','DOY','ADAT','MDAT','HWAM'])

        # #make an empty array [nyears * n_plt_dates]
        n_year = sim_years
        # For Boxplot
        yield2_n = np.empty([n_year,len(pdate_list)])*np.nan
        # ADAT2_n = np.empty([n_year,len(pdate_list)])*np.nan
        # MDAT2_n = np.empty([n_year,len(pdate_list)])*np.nan

        for count in range(len(pdate_list)):
            Pdate = pdate_list[count]
            yield2_n[:,count] = df2.HWAM[(df2["DOY"] == Pdate)].values  
            # ADAT2_n[:,count] = df2.ADAT[(df2["DOY"] == Pdate)].values 
            # MDAT2_n[:,count] = df2.MDAT[(df2["DOY"] == Pdate)].values  

        if obs_flag == 1:
            obs_yield = []
            # obs_ADAT2 = []
            # obs_MDAT2 = []
            for count in range(len(pdate_list)):
                Pdate = pdate_list[count]
                if count > 0 and pdate_list[count] < pdate_list[count-1]:  #when two consecutive years
                    start_year = repr(int(start_year)+1)
                #collect simulated results from observed weather
                obs_yield.append(df2.HWAM[(df2["DOY"] == Pdate) & (df2["Year"] == int(start_year))].values[0])      
                # obs_ADAT2.append(df2.ADAT[(df2["DOY"] == Pdate) & (df2["Year"] == int(start_year))].values[0])
                # obs_MDAT2.append(df2.MDAT[(df2["DOY"] == Pdate) & (df2["Year"] == int(start_year))].values[0])
        #To remove 'nan' from data array
        #https://stackoverflow.com/questions/44305873/how-to-deal-with-nan-value-when-plot-boxplot-using-python
        mask = ~np.isnan(yield2_n)
        filtered_yield2 = [d[m] for d, m in zip(yield2_n.T, mask.T)]
        # mask2 = ~np.isnan(ADAT2_n)
        # filtered_data2 = [d[m] for d, m in zip(ADAT2_n.T, mask2.T)]
        # mask3 = ~np.isnan(MDAT2_n)
        # filtered_data3 = [d[m] for d, m in zip(MDAT2_n.T, mask3.T)]
        
        # if obs_flag == 1: 
        #     #replace ADAT2 = 0 with nan
        #     obs_ADAT2 = [np.nan if x==1 else x for x in obs_ADAT2]
        #     obs_MDAT2 = [np.nan if x==1 else x for x in obs_MDAT2]
    ##End of reading Yields from historical weather observations
        #plot============================================
        #regroup matrix for different planting date
        i_pos = 1
        fig = plt.figure()
        fig.suptitle('Estimated Yields with differnt planting dates', fontsize=14, fontweight='bold')
        ax = plt.axes()
        #plt.hold(True)

        for count in range(len(pdate_list)):
            a = filtered_yield2[count].tolist() #.reshape((1,obs_years,1))
            b = filtered_yield[count].tolist() #.reshape((nrealiz,1))
            new_list = []
            new_list.append(a)
            new_list.append(b) #=> nested list
            # first boxplot pair
            bp = plt.boxplot(new_list, positions = [i_pos, i_pos+1], widths = 0.6)
            setBoxColors(bp)
            i_pos = i_pos + 3

        #X data for plot
        #myXList=[i+3 for i in range(len(pdate_list))]
        myXList=[i+0.5 for i in range(1, 3*len(pdate_list), 3)]
        if obs_flag == 1:
            # Plot a line between yields with observed weather
            plt.plot(myXList, obs_yield, 'go-')
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            temp_str = repr(year_list[count]) + " " + repr(pdate_list[count]) 
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)

        # set axes limits and labels
        plt.xlim(0,3*len(pdate_list))
    # ax.set_xticks([1.5, 4.5, 7.5, 10.5, 13.5, 16.5])
        ax.set_xticklabels(scename)
        ax.set_xticks(myXList)
        ax.set_xlabel('Planting Date',fontsize=14)
        ax.set_ylabel('Yield [kg/ha]',fontsize=14)
        # draw temporary red and blue lines and use them to create a legend
        hB, = plt.plot([1,1],'b-')
        hR, = plt.plot([1,1],'r-')
        plt.legend((hB, hR),('observed WTH', 'SCF'), loc='best')
        hB.set_visible(False)
        hR.set_visible(False)
        fig.set_size_inches(12, 8)
        fig_name = Wdir_path + "\\"+sname+"_output\\"+sname+"_Yield_boxplot2.png"
        plt.savefig(fig_name,dpi=100)    
        #plt.show()  
    ##############################################################
    ##############################################################
    def Yield_tercile_forecast(self,sname, SNX_list, start_year, sim_years, obs_flag):
        fname1 = Wdir_path + "\\"+ sname+"_output\\SUMMARY.OUT"
        if obs_flag == 1:
            nrealiz = 101
        else:
            nrealiz = 100
        pdate_list = []
        year_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(int(SNX_list[i][-11:-8]))
            if i == 0:
                year_list.append(int(start_year))
            else:
                if pdate_list[i] > pdate_list[i-1]:
                    year_list.append(int(start_year))
                else:
                    year_list.append(int(start_year) + 1)
        #check if output  SUMMARY.OUt exists
        if not os.path.isfile(fname1):
            print( '**Error!!- SUMMARY.OUT file does not exist under ####_output folder')
            os.system('pause')

        df_OUT=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
        PDAT = df_OUT.ix[:,14].values  #read 14th column only => Planting date 
        HWAM = df_OUT.ix[:,21].values  #read 21th column only

    #    year_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize
        DOY_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize

        for i in range(len(HWAM)):
            # year_array[i] = int(repr(PDAT[i])[:4])
            DOY_array[i] = int(repr(PDAT[i])[4:])
                
        #make three arrays into a dataframe
        df = pd.DataFrame({'DOY': DOY_array, 'HWAM':HWAM}, columns=['DOY','HWAM'])
    ##====================================================
    ##READ Yields from historical weather observations
        fname1 = Wdir_path + "\\"+ sname+"_output_hist\\SUMMARY.OUT"

        #check if output  SUMMARY.OUt exists
        if not os.path.isfile(fname1):
            print( '**Error!!- SUMMARY.OUT file does not exist under ####_output_hist folder')
            os.system('pause')

        df_OUT2=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
        PDAT2 = df_OUT2.ix[:,14].values  #read 14th column only => Planting date 
        HWAM2 = df_OUT2.ix[:,21].values  #read 21th column only
        year_array2 = np.zeros(len(HWAM2)) #np.empty((ndays,1))*np.nan #initialize
        DOY_array2 = np.zeros(len(HWAM2)) #np.empty((ndays,1))*np.nan #initialize

        BN_list = []
        NN_list = []
        AN_list = []
        for i in range(len(HWAM2)):
            year_array2[i] = int(repr(PDAT2[i])[:4])
            DOY_array2[i] = int(repr(PDAT2[i])[4:])
        #make three arrays into a dataframe
        df2 = pd.DataFrame({'Year': year_array2,'DOY': DOY_array2, 'HWAM':HWAM2}, columns=['Year','DOY','HWAM'])
    #===========================
    # extract target values corresponding to 33% and 67%
        for Pdate in pdate_list:
            yield_frst= df.HWAM[(df["DOY"] == Pdate)].values 
            yield_hist= df2.HWAM[(df2["DOY"] == Pdate)].values

            #a) compute CDF curve from the simulated yields-frst
            sorted_yield_frst = np.sort(yield_frst) #sort monthly rain from smallest to largest
            index_yield_frst = np.argsort(yield_frst) #** argsort - returns the original indexes of the sorted array
            temp = np.zeros(len(yield_frst))+1
            pdf = np.divide(temp,len(yield_frst))  #1/100years 
            cdf = np.cumsum(pdf)  #compute CDF

            #b) compute CDF curve from the simulated yields-historical
            sorted_yield_hist = np.sort(yield_hist) #sort monthly rain from smallest to largest
            index_yield_hist = np.argsort(yield_hist) #** argsort - returns the original indexes of the sorted array
            temp = np.zeros(len(yield_hist))+1
            pdf2 = np.divide(temp,len(yield_hist))  #1/30 years 
            cdf2 = np.cumsum(pdf2)  #compute CDF

            #C) compute threshold yield vaues (33% & 67%) from the yield-historical distribution
            #numpy.interp(x, xp, fp, left=None, right=None, period=None)
            yield_thres = np.interp([0.33,0.67],cdf2,sorted_yield_hist,left=sorted_yield_hist[0])# 100 WGEN outputs to theoretical CDF SCF curve

            #d) compute threshold yield vaues (33% & 67%) from the yield-historical distribution
            tercile2 = np.interp(yield_thres, sorted_yield_frst, cdf, left=0)      

            #e) add to the list
            BN_list.append(tercile2[0]*100)  #0.1167
            AN_list.append(100 - tercile2[1]*100)  # 1- 0.4297
            NN_list.append((tercile2[1]- tercile2[0])*100)  #NN= 100-BN - AN, e.g.,( 0.4297-0.1167) *100
            # #=======================================================
            # # Test figures
            # # Plotting yield exceedance curve
            # fig = plt.figure()
            # fig.suptitle('Predicted Yields: planted on DoY = '+repr(Pdate), fontsize=13, fontweight='bold')
            # ax = fig.add_subplot(111)
            # ax.set_xlabel('Yield [kg/ha]',fontsize=14)
            # ax.set_ylabel('Probability[-]',fontsize=14)
            # plt.ylim(0, 1) 
            # ax.plot(sorted_yield_hist,cdf2,'o-', label='historical')
            # ax.plot(sorted_yield_frst,cdf,'*-', label='forecast')
            # #horizontal line for 33th percentile
            # y_data=[0.333, 0.3333] #only two points to draw a line
            # x_data=[0,sorted_yield_hist[-1]]
            # ax.plot(x_data,y_data,'k-.', label='33%')
            # #horizontal line for 67th percentile
            # y_data2=[0.667, 0.667] #only two points to draw a line
            # ax.plot(x_data,y_data2,'b-.', label='67%')
            # #vertical line for 33th percentile
            # y_data33=[0, 1] #only two points to draw a line
            # x_data33=[yield_thres[0],yield_thres[0]]
            # ax.plot(x_data33,y_data33,'k--', label='33% yield')
            # #vertical line for 67th percentile
            # x_data67=[yield_thres[1],yield_thres[1]]
            # ax.plot(x_data67,y_data33,'b--', label='67% yield')
            # plt.xlim(0, np.maximum(sorted_yield_hist[-1], sorted_yield_frst[-1]))
            # ax.legend(loc='best')         # Put a legend to the right of the current axis
            # plt.show()
    #===============================================================================================================
        #f) Write the extract BN, NN and AN into a csv file
        temp_csv=Wdir_path.replace("/","\\") + "\\"+ sname+"_output\\yield_tercile_forecast.csv" 
        with open(temp_csv, 'w', newline='') as csvfile:
            scfwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            scfwriter.writerow(['Pdate_DOY','BN','NN', 'AN'])
            for i in range(len(pdate_list)):
                scfwriter.writerow([repr(pdate_list[i])] + [repr(BN_list[i])]+ [repr(NN_list[i])] + [repr(AN_list[i])])

######################################################################
######################################################################
# main program
if __name__ == '__main__':
    root = tkinter.Tk()  #The main toplevel referred to as the "root" (Grayson p.32)
##    #Setting application-wide defaults for fonts by opening a file, called optionDB (Grayson p49)
##    root.option_readfile(r'C:\IRI\Uruguay_project\DSS_v2_D46_2017\Python_script\optionDB')
    
    Pmw.initialise(root)
    root.title(title)  #add title = 'CAMDT User-Interface' on the main window
 # Add a logo image as a button
    img = PhotoImage(file = os.path.join(os.getcwd(), 'camdt_dcc_logo.gif'))
    logo_button = Button(root, background = 'white', image = img).pack(side = TOP)
##    img = PhotoImage(file='C:\\IRI\\Uruguay_project\\DSS_v2_D46_2017\\Python_script\\logo_all6.gif') 
##    logo_button=Button(root, background='white', image=img).pack(side=TOP) #, padx=10, pady=10)
##    
    widget = CAMDT(root)  #Calling CAMDT class as a function

    #Add an Exit button to destroy the main window
    exitButton = tkinter.Button(root, text = 'Exit', command = root.destroy)
    exitButton.pack()
     
    #call tkinter mainloop to process events and keep the display activated
    root.mainloop()




    