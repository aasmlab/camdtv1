from __init__ import *


from Setup1Window import *
from Setup2Window import *
from DisplayWindow import *
from DisplayWindow2 import *
from DisplayWindow3 import *
from Create_WTH import create_WTH_main

class Scenario( BaseWidget):

    def __init__(self):
        super(Scenario,self).__init__('CAMDT for Dynamic Cropping Calendar-Maize')
        self.parent = None

        # The main menu
        self.mainmenu = self.mainmenu()

        self.cmp1a = '' 
        self.cmp2a = '' 
        self.cmp3a = ''
        self.cmp1b = '' 
        self.cmp2b = '' 
        self.cmp3b = ''

        self.lst1Cmp = []
        self.lst2Cmp = []
        self.lst3Cmp = []

        logoDir = os.path.abspath(os.path.dirname(sys.argv[0])) + "\\logos\\"


        logoIri = cv2.imread(logoDir+'iri.jpg', 1)
        width = int(logoIri.shape[1] * 0.7)
        height = int(logoIri.shape[0] * 0.7)
        dim = (width, height)
        logoIri1 = cv2.resize(logoIri, dim, interpolation = cv2.INTER_AREA  )
 

        logoMsu = cv2.imread(logoDir+'msu.png', 1)
        logoUpl = cv2.imread(logoDir+'upl.jpg', 1)
        logoFao = cv2.imread(logoDir+'fao.png', 1)
        logoAgri = cv2.imread(logoDir+'DA.png', 1)
        logoDssat = cv2.imread(logoDir+'dssat.png', 1)

        width = int(logoDssat.shape[1] * 0.8)
        height = logoIri.shape[0]
        dim = (width, height)
        logoDssat1 = cv2.resize(logoDssat, dim, interpolation = cv2.INTER_AREA  )

        self.yearsDoy = [1,2] # store start + end year on YYYDOY format

        self._logoIri = ControlImage('')
        self._logoIri.value = logoIri1

        self._logoMsu = ControlImage('')
        self._logoMsu.value = logoMsu

        self._logoUpl = ControlImage('')
        self._logoUpl.value = logoUpl

        self._logoFao = ControlImage('')
        self._logoFao.value = logoFao

        self._logoAgri = ControlImage('')
        self._logoAgri.value = logoAgri

        self._logoDssat = ControlImage('')
        self._logoDssat.value = logoDssat1

        self._panelWin1 = ControlEmptyWidget()
        self._setup1 = Setup1Window()
        self._setup1.parent = self
        self._panelWin1.value = self._setup1
        self.setup1Results = self._setup1.Setup1Results()

        self._panelWin2 = ControlEmptyWidget()
        self._setup2 = Setup2Window()
        self._setup2.parent = self
        self._panelWin2.value = self._setup2
        self.setup2Results = self._setup2.Setup2Results()

        self._panelWin4 = ControlEmptyWidget()
        self._display = DisplayWindow()
        self._display.parent = self
        self._panelWin4.value = self._display
        self._panelWin4.hide

        self._panelWin5 = ControlEmptyWidget()
        self._display2 = DisplayWindow2()
        self._display2.parent = self
        self._panelWin5.value = self._display2
        self._panelWin5.hide()

        self._panelWin6 = ControlEmptyWidget()
        self._display3 = DisplayWindow3()
        self._display3.parent = self
        self._panelWin6.value = self._display3
        self._panelWin6.hide()

        # Variables
        self._numScenarios = 0
        formatStr1 = 4*'\t' + 25*' ' + "Scenario 1"
        formatStr2 = 5*'\t'  
        
        self.errText = ControlText("Warning", default="Something went wrong...")

        self._scLabel1 = ControlLabel(formatStr1)
        self.waitLabel = ControlLabel("DSSAT is performing analysis. Please wait for the loading bar...")
        #self.waitLabel.hide()
        self._scName1 = ControlText("\t\ta - Scenario Name:\t\t") # limit the name to 4 char
        self._scSteps1 = ControlText("\t\tb - Steps (Days):\t\t")
        self._scSteps1.value = '10'
        self._scComments1 = ControlText("\t\tc - Provide any comments:\t")
        self._scButton1  		= ControlButton('Update SNX file') 
        self._scButton1.hide() 
        self._scButton1.value = self.__scButton1Action

        self._scLabel2 = ControlLabel("\t Scenario 2")
        self._scName2 = ControlText("")
        self._scSteps2 = ControlText("")
        self._scSteps2.value = '10'
        self._scComments2 = ControlText("")
        self._scButton2  		= ControlButton('Update SNX file')  
        self._scButton2.hide() 
        self._scButton2.value = self.__scButton2Action

        self._scLabel3 = ControlLabel("\t Scenario 3")
        self._scName3 = ControlText("")
        self._scSteps3 = ControlText("")
        self._scSteps3.value = '10'
        self._scComments3 = ControlText("")
        self._scButton3  		= ControlButton('Update SNX file') 
        self._scButton3.hide()  
        self._scButton3.value = self.__scButton3Action

        self._btnScn1Seas = ControlButton('Run Seasonal')
        self._btnScn1Seas.hide()
        self._btnScn1Seas.value = self.__btnScn1SeasAction
        #self._btnScn1Seas.value = self.__dummyEvent
        self._btnScn1Hist = ControlButton('Run Historical')
        self._btnScn1Hist.hide()
        self._btnScn1Hist.value = self.__btnScn1HistAction
        # self._btnScn1Hist.value = self.__dummyEvent

        self._btnScn2Seas = ControlButton('Run Seasonal')
        self._btnScn2Seas.hide()
        self._btnScn2Seas.value = self.__btnScn2SeasAction
        # self._btnScn2Seas.value = self.__dummyEvent
        self._btnScn2Hist = ControlButton('Run Historical')
        self._btnScn2Hist.hide()
        self._btnScn2Hist.value = self.__btnScn2HistAction
        # self._btnScn2Hist.value = self.__dummyEvent

        self._btnScn3Seas = ControlButton('Run Seasonal')
        self._btnScn3Seas.hide()
        self._btnScn3Seas.value = self.__btnScn3SeasAction
        # self._btnScn3Seas.value = self.__dummyEvent
        self._btnScn3Hist = ControlButton('Run Historical')
        self._btnScn3Hist.hide()
        self._btnScn3Hist.value = self.__btnScn3HistAction
        # self._btnScn3Hist.value = self.__dummyEvent
         
        self._summaryText = ControlTextArea('Quick Summary', readonly=True)
        #self._summaryText.value = self.updateInfo()
        self._summaryText.hide()


        self._workingDir 	= ControlDir('\t\t1 - Select the working directory')

        #Definition of the forms fields
        self._label1     = ControlLabel('\t1 - Working Directoy')

        self._label2     = ControlLabel('\t2 - What-If Scenarios (Max is 3)')
        self._labelNote  = ControlLabel('\t\u26A0 NOTE: Make sure all input files are in the chosen directory\n \
            \n\tOutput files will be created under the chosen directory with new scenario names')


        self._buttonNext  		= ControlButton('Next')
        self._buttonNext.hide() 
        self._buttonNext.value = self.__buttonNextAction

        self._labeli = ControlLabel('Move to next tab to display results')
        self._labeli.hide()

        self._progBar 	        = ControlProgress()
        self._progBar.hide() 
        self._progBar.value 	= 0
        
        self._numScenariosLabel = ControlText("How many scenarios to simulate?")

        self.Wdir_path = self.__getWorkingDir()

        self._buttonField  		= ControlButton('Start')
        self._buttonField.value = self.__buttonStartAction

        self._imgPathList = ['', '', '', '', '', '']


        self._labelBtn     = ControlButton('\tValidate!')
        self._labelBtn.value = self.__VerifyDir

        msg = """\n \n\
            Climate Agriculture Modeling and Decision Tool (CAMDT)\n The CAMDT is a computer desktop tool designed to guide decision-makers in adopting appropriate crop and water management practices that can improve crop yields given a climatic condition."""
        

        self._welcomeMsg = ControlTextArea("\nCAMDT", readonly=True)
        self._welcomeMsg.value = msg
        # layout and structure 
        self.formset = [
            {

            #'a:Welcome':['_welcomeMsg','||','Usage','||','Credits', '=', (' ','_buttonField', ' ')],
            'a:Welcome':[('_welcomeMsg') ,'||',
                ['h5:Credits (Software Development Team)', 
                    '\n\t - Eunjin Han, IRI Columbia',
                    '\n\t - Amor Ines, MSU',
                    '\n\t - Walter Baethgen, IRI Columbia',
                    '\n\t - Josue Kopdo, MSU',
                    '\n\t - Prakash Jha, MSU','\n',
                
                
                '=', "h5:Collaborators", '\n',

                    ( '_logoIri', '_logoMsu', '=', '_logoUpl', '_logoFao', '=', (' ', '_logoAgri',' '), '='

                    ),
                
                
                '=', "h5:\nAcknowledgement", '\n', (' ', '_logoDssat', ' ')
                
                ],
             
                
                
                ' '],

            'b:DSSAT Setup 1':['_panelWin1'] ,

            'c:DSSAT Setup 2':['_panelWin2'] ,

            'd:Scenario Setup':[  
                          ("_workingDir", "_labelBtn", ' '), ' ',


                        "_label2",

                        "=",

                        # What-If scenarios block
                        [
                           
                            ('_scLabel1', ' '), 
                            ('_scName1', '     '), 
                            ('_scSteps1', '    '),
                            ('_scComments1', '  '),
                            (formatStr2, '_btnScn1Seas', '_btnScn1Hist', '  '),

                            '||',
                            
                            ('_scLabel2', ' '), 
                            ('_scName2', '     '), 
                            ('_scSteps2', '    '),
                            ('_scComments2', '  '),
                            ( '_btnScn2Seas', '_btnScn2Hist', '  '),

                            '||',
                            
                            ('_scLabel3', ' '),                 
                            ('_scName3', '     '), 
                            ('_scSteps3', '    '),
                            ('_scComments3', '  '), 
                            ( '_btnScn3Seas', '_btnScn3Hist','  '),

                            '||',
                            (' ', '_summaryText', ' '),

                            # '||', '||', '='
                            
                        ],

                        "=",

                        ('waitLabel', '_progBar', '_labeli'), ' ',

                        # "=",

                        # (' ', '_labeli', ' '), ' '
                        ]
            ,

            'e:Scenario 1 - Results':['_panelWin4'], 
            'e:Scenario 2 - Results':['_panelWin5'], 
            'e:Scenario 3 - Results':['_panelWin6'] 
            
            }
  

        ]


        self.Wdir_path = self.__getWorkingDir()

    def getImagePath(self):
        return self._imgPathList


    def compare1(self, lst):
        if self.cmp1a == 'seasonal' and self.cmp1b == 'hist':
            self.Yield_boxplot_compare(lst[0], lst[1], lst[2], lst[3], lst[4], self._display, 1)
            self.Yield_tercile_forecast(lst[0], lst[1], lst[2], lst[3], lst[4], self._display)
        
    def compare2(self, lst):
        if self.cmp2a == 'seasonal' and self.cmp2b == 'hist':
            self.Yield_boxplot_compare(lst[0], lst[1], lst[2], lst[3], lst[4], self._display2, 2)
            self.Yield_tercile_forecast(lst[0], lst[1], lst[2], lst[3], lst[4], self._display2)
        
    def compare3(self, lst):
        if self.cmp3a == 'seasonal' and self.cmp3b == 'hist':
            self.Yield_boxplot_compare(lst[0], lst[1], lst[2], lst[3], lst[4], self._display3, 3)
            self.Yield_tercile_forecast(lst[0], lst[1], lst[2], lst[3], lst[4], self._display3)


    def __btnScn1SeasAction(self):
        self.cmp1a = 'seasonal'
        self.waitLabel.show()     
        self.writeSNX1_DSSAT() 
        self.compare1(self.lst1Cmp) 
        self._progBar.show() 
        self.__progLoading()
        self.waitLabel.hide()
        

    def __btnScn2SeasAction(self):
        """write to file"""
        self.cmp2a = 'seasonal'
        self.waitLabel.show()
        self.writeSNX2_DSSAT()
        self.compare2(self.lst2Cmp) 
        self._progBar.show() 
        self.__progLoading()
        self.waitLabel.hide()
        

    def __btnScn3SeasAction(self):
        """write to file"""
        self.cmp3a = 'seasonal'
        self.waitLabel.show()
        self.writeSNX3_DSSAT()
        self.compare3(self.lst3Cmp) 
        self._progBar.show() 
        self.__progLoading()
        self.waitLabel.hide()
        

    def __btnScn1HistAction(self):
        
        self.cmp1b = 'hist'
        self.waitLabel.show()
        self.writeSNX1_DSSAT_hist()
        self.compare1(self.lst1Cmp) 
        self._progBar.show() 
        self.__progLoading()
        self.waitLabel.hide()
        

    def __btnScn2HistAction(self):
        
        self.cmp2b = 'hist'
        self.waitLabel.show()
        self.writeSNX2_DSSAT_hist()
        self.compare2(self.lst2Cmp) 
        self._progBar.show() 
        self.__progLoading()
        self.waitLabel.hide()
        

    def __btnScn3HistAction(self):
        
        self.cmp3b = 'hist'
        self.waitLabel.show()
        self.writeSNX3_DSSAT_hist()
        self.compare3(self.lst3Cmp) 
        self._progBar.show() 
        self.__progLoading()
        self.waitLabel.hide()
        

    def __buttonStartAction(self):
        pass

    # This functions define actions for the main menu
    def mainmenu(self):
        menu_list = [
            { 'File': [
                    # {'Open': self.__openEvent},
                    # '-',
                    # {'Save': self.__saveEvent},
                    # '-',
                    {'Quit': self.__closeEvent}
                ]
            # },
            # { 'Edit': [
            #         {'Copy': self.__editEvent},
            #         {'Past': self.__pastEvent}
            #     ]
            # },
            # { 'About': [
                    
            #     ]
            }
        ]
        return menu_list

    def __openEvent(self):
        pass

    def __dummyEvent(self):
        pass

    def __saveEvent(self):
        pass
        
    def __editEvent(self):
        pass

    def __pastEvent(self):
        pass

    def __closeEvent(self):
        exit()


    def __summary(self):
        print("__SUMMARY___")
        print("Working Dir:", self.__getWorkingDir()) 

    def __progLoading(self):
        try:
            for t in range(0, 125, 25):
                self._progBar.value = t
                sleep(1)

            #self.__buttonNextAction()
            self._labeli.show()
            self._panelWin4.show()
            self._panelWin5.show()
            self._panelWin6.show()
        except:
            pass

    def __buttonNextAction(self):
        
        if self._progBar.value == 100:
            self._buttonNext.show()


    def __getWorkingDir(self):
        """returns the working directory selected"""
        #global Wdir_path
        self.Wdir_path = self._workingDir.value
        return self._workingDir.value

    def __VerifyDir(self):
        self.Wdir_path = self._workingDir.value
        if len(self.Wdir_path) == 0 :
            self._btnScn1Seas.hide()
            self._btnScn2Seas.hide()
            self._btnScn3Seas.hide()
            self._btnScn1Hist.hide()
            self._btnScn2Hist.hide()
            self._btnScn3Hist.hide()
        else:
            self.copyInputFiles(self.Wdir_path)
            self._btnScn1Seas.show()
            self._btnScn2Seas.show()
            self._btnScn3Seas.show()
            self._btnScn1Hist.show()
            self._btnScn2Hist.show()
            self._btnScn3Hist.show()
        

            

    def copyInputFiles(self, dest):
        dest = dest.replace('/', "\\") + '\\'
        pathdir = os.path.abspath(os.path.dirname(sys.argv[0]))
        input_path = pathdir[:(len(pathdir)-8)] + "\\Input_Files\\"
        #print(input_path)

        src_files = os.listdir(input_path)
        for file in src_files:
            fname = input_path + file
            try:
                shutil.copy(fname, dest)
            except:
                continue

    def __scButton1Action(self):
        """write to file"""
        self.waitLabel.show()
        self.writeSNX1_DSSAT()
        self._summaryText.value = self.updateInfo()
        self._summaryText.show()
        self.waitLabel.hide()
        

    def __scButton2Action(self):
        """write to file"""
        self.waitLabel.show()
        self.writeSNX2_DSSAT()
        self._summaryText.value = self.updateInfo()
        self._summaryText.show()
        self.waitLabel.hide()

    def __scButton3Action(self):
        """write to file"""
        self.writeSNX3_DSSAT()
        self._summaryText.value = self.updateInfo()
        self._summaryText.show()

    def updateInfo(self):
        val1, val2 = self.yearsDoy[0], self.yearsDoy[1]
        info0 = "Are the information below correct? - They will be used for the setup!\n\n"
        info1 = "Crop: MAIZE\n"
        info2 = "First planting (DOY): " + val1 + "\n" # Still giving 1 and 90: TO FIX
        info3 = "Last planting (DOY): " + val2 + "\n"
        infoF = info0 + info1 + info2 + info3

        return infoF

    def writeSNX1_DSSAT(self):
        
        self.Wdir_path = self.__getWorkingDir()
        if self._scName1.value == "":
            sname = "TST1" 
        elif self._scName1.value in [' ', "TST1"]:
            sname = "TST1" 
        else:
            sname = self._scName1.value[:4]

        setup1Values = self._setup1.Setup1Results() # All inputs from Setup1 are stored here
        #print("Setup1 Results:", setup1Values)
        WSTA = setup1Values[0][:4] #'SANJ'
        WTD_fname = self.Wdir_path.replace("/","\\") + "\\"+ WSTA + ".WTD"
        WTD_last_date = self.find_obs_date(WTD_fname)

        start_year = setup1Values[1] #'2002'
        obs_flag = 0  #by default, assumes no obs weather data is available
        #=============================================
        #Determine pdate_1[YYYYDOY] and pdate_2[YYYYDOY]
        lst = setup1Values[13]  #Year 1 for planting
        lst2 = setup1Values[14]   #Year 2 for planting
        lst_frst = setup1Values[11]  #Year 1 for SCF
        lst_frst2 = setup1Values[12]  #Year 2 for SCF
        pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m = self.find_plt_date(start_year, lst, lst2, lst_frst, lst_frst2)
        self.yearsDoy[0], self.yearsDoy[1] = pdate_1, pdate_2
        if WTD_last_date >= hv_date: #180 is arbitrary number to fully cover weather data until late maturity
            obs_flag = 1

        step = int(self._scSteps1.value)
        cr_type='MZ'
        SNX_list = []
        if pdate_2%1000 > pdate_1%1000:
            for iday in range(pdate_1,pdate_2+1,step):   
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\M"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main(SNX_fname,cr_type,iday,obs_flag)  
                SNX_list.append(SNX_fname)

        else: #if planting window covers two consecutive years
                #go through Year 1
            end_day = int(start_year)*1000 + 365
            if calendar.isleap(int(start_year)):  end_day = int(start_year)*1000 + 366
            for iday in range(pdate_1,end_day + 1,step):   
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\M"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main(SNX_fname,cr_type,iday,obs_flag)    
                SNX_list.append(SNX_fname) 
                temp_date = iday
            #next, go throught Year 2
            start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 365  #e.g, 364 + 5 - 365 = DOY 4
            if calendar.isleap(int(start_year)):  start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 366
            for iday in range(start_day,pdate_2 + 1,step):   #e.g, 2018004 to 2018030
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\M"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main(SNX_fname,cr_type,iday,obs_flag)     
                SNX_list.append(SNX_fname)       

        #=============================================
        # Write scenario summary into a text file 
        # => this can be also used for input for Create_WTH subroutine
        #=============================================
        LAT, LONG, ELEV, TAV, AMP = self.find_station_info(WSTA)  #Call a function to find more info about station to write header in *.WTH
        summary_fname=self.Wdir_path.replace("/","\\") + "\\" + sname+"_summary.txt"
        f = open(summary_fname,"w") 
        working_dir=self.Wdir_path.replace("/","\\")
        f.write("Directory:  " + working_dir + "\n")
        f.write("Station_name: " + WSTA + " \n")
        f.write("LAT: " + repr(LAT) + " \n")
        f.write("LONG: " + repr(LONG) + " \n")
        f.write("ELEV: " + repr(ELEV) + " \n")
        f.write("TAV: " + repr(TAV) + " \n")
        f.write("AMP: " + repr(AMP) + " \n")
        f.write("First_Plt_Date: " + repr(pdate_1) + " \n")
        f.write("Last_Plt_Date: " + repr(pdate_2) + " \n")
        f.write("Harvest_Date: " + repr(hv_date) + " \n")
        f.write("frst_start_Date: " + repr(frst_date1) + " \n")
        f.write("Planting_window: " + repr(pwindow_m) + " \n")
        f.write("SCF_window: " + repr(fwindow_m) + " \n")
        f.write("Cultivar: " + str(setup1Values[5]) + " \n")  #INGENO=self.cul_type.getvalue()[0][0:18] #cultivar type
        f.write("Planting_density: " + str(setup1Values[6]) + " \n")  # PPOP=self.plt_density.getvalue()[0:3] #planting density
        f.write("Soil_type: " + str(setup1Values[7])  + " \n")  #ID_SOIL=self.soil_type.getvalue()[0][0:10]
        f.write("Irrigation_A(1)_N(0): " + repr(self._setup2.getApplyIrr()) + " \n")    #automatic or no-irrigation
        
        if self._setup2.getApplyIrr() == 1:
            IMETH = self._setup2.getApplyIrrMeth() #'IR001' #irrigation method
            f.write("IRR_method: " + IMETH + " \n")
        f.write("Fertilizer_Y(1)_N(0): " + repr(self._setup2.getApplyFert()) + " \n")
        
        FInputs1 = self._setup2.getFertInpList1()
        FInputs2 = self._setup2.getFertInpList2()
        FInputs3 = self._setup2.getFertInpList3()

        FDATE1  =   FInputs1[0] #'0'
        FDATE2  =   FInputs2[0]  #'60'
        FDATE3  =   FInputs3[0]  #'60'

        FAMN1   =   FInputs1[1] #'50'  #1st fertilizer application amount
        FAMN2   =   FInputs2[1] #'90'  #2nd fertilizer application amount
        FAMN3   =   FInputs3[1] #'90'  #3rd fertilizer application amount

        FMCD1   =   FInputs1[2] #'50'  #1st fertilizer Material
        FMCD2   =   FInputs2[2] #'50'  #2nd fertilizer Material
        FMCD3   =   FInputs3[2] #'50'  #3rd fertilizer Material 

        
        if self._setup2.getApplyFert == 1:
            f.write("FDATE1: " + str(FDATE1) + " \n")  
            f.write("FDATE2: " + str(FDATE2) + " \n")
            f.write("FDATE3: " + str(FDATE3) + " \n")
            f.write("F_amount1: " + str(FAMN1)  + " \n")
            f.write("F_amount2: " + str(FAMN2) + " \n")
            f.write("F_amount3: " + str(FAMN3) + " \n")
            f.write("F_method1: " + str(FMCD1) + " \n")
            f.write("F_method2: " + str(FMCD2) + " \n")
            f.write("F_method3: " + str(FMCD3) + " \n")
    
        f.close()

        list_BN = setup1Values[15]
        list_NN = setup1Values[10]
        list_AN = setup1Values[16]

        temp_csv=self.Wdir_path.replace("/","\\") + "\\SCF_input_monthly1.csv"
        with open(temp_csv, 'w', newline='') as csvfile:
            scfwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            scfwriter.writerow(['Month','1','2', '3', '4','5','6','7','8','9','10','11','12'])
            scfwriter.writerow(['BN'] + list_BN[:12])
            scfwriter.writerow(['NN'] + list_NN[:12])
            scfwriter.writerow(['AN'] + list_AN[:12])

        temp_csv=self.Wdir_path.replace("/","\\") + "\\SCF_input_monthly2.csv" 
        with open(temp_csv, 'w', newline='') as csvfile:
            scfwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            scfwriter.writerow(['Month','1','2', '3', '4','5','6','7','8','9','10','11','12'])
            scfwriter.writerow(['BN'] + list_BN[12:])
            scfwriter.writerow(['NN'] + list_NN[12:])
            scfwriter.writerow(['AN'] + list_AN[12:])

        temp_snx=self.Wdir_path.replace("/","\\") + "\\button_write.csv" 
        with open(temp_snx, 'w', newline='') as csvfile:
            scfwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            scfwriter.writerow(['Month','J1','F1', 'M1', 'A1','M1','J1','J1','A1','S1','O1',
                    'N1','D1', 'J2','F2', 'M2', 'A2','M2','J2','J2','A2','S2','O2','N2','D2'])
            scfwriter.writerow(['SCF'] + setup1Values[11] + setup1Values[12])
            scfwriter.writerow(['plt'] + setup1Values[13] + setup1Values[14])
            # scfwriter.writerow(self.scf_button1.state())
        #=============================================
        # Generate 100 weather realizations
        #=============================================
        # Get a list of all files in directory
        for rootDir, subdirs, filenames in os.walk(self.Wdir_path):
            # Find the files that matches the given patterm
            for filename in fnmatch.filter(filenames, '0*.WTH'):
                try:
                    os.remove(os.path.join(rootDir, filename))
                except OSError:
                    print("Error while deleting file")

        create_WTH_main(self.Wdir_path,WTD_fname, WSTA, LAT, LONG, ELEV, TAV, AMP, pdate_1, pdate_2, step, hv_date, frst_date1)
        #=============================================
        # Create DSSBatch.v47 file and run DSSAT executable 
        #=============================================
        entries = ("PlantGro.OUT","Evaluate.OUT","SCF_input_monthly1.csv", "SCF_input_monthly2.csv", 
                     "ET.OUT","OVERVIEW.OUT","PlantN.OUT","SoilNi.OUT","Weather.OUT", 
                    "SolNBalSum.OUT","SoilNoBal.OUT","SoilTemp.OUT","SoilWat.OUT","SoilWatBal.OUT","Summary.OUT") 
        self.writeV47_main(SNX_list, obs_flag)   ##write *.V47 batch file

        #==RUN DSSAT with ARGUMENT 
        start_time = time.perf_counter() # => not compatible with Python 2.7
        os.chdir(self.Wdir_path)  #change directory
        args = "DSCSM047.EXE N DSSBatch.v47 "
        subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
        end_time0 = time.perf_counter() # => not compatible with Python 2.7
        print('It took {0:5.1f} sec to finish 100 weather realizations x n planting dates'.format(end_time0-start_time))
        start_time1 = time.perf_counter()  # => not compatible with Python 2.7

        #create a new folder to save outputs of the target scenario
        new_folder=self._scName1.value+"_output"
        if os.path.exists(new_folder):
            shutil.rmtree(new_folder)   #remove existing folder
        os.makedirs(new_folder)
        #copy outputs to the new folder
        dest_dir=self.Wdir_path + "\\"+new_folder
        # print 'dest_dir is', dest_dir

        for entry in entries:
            source_file=self.Wdir_path + "\\" + entry
            if os.path.isfile(source_file):
                shutil.move(source_file, dest_dir)
            else:
                print( '**Error!!- No DSSAT output files => Check if DSSAT simuation was successful')

        #move *.SNX file to the new folder with output files
        for i in range(len(SNX_list)):
            shutil.move(SNX_list[i], dest_dir)
        shutil.move(summary_fname, dest_dir) 

        #  temp = "WGEN_out_" + WSTA + repr(pdate_1//1000)+".txt"
        shutil.move("WGEN_input_param_month_" + WSTA + ".txt", dest_dir)
        shutil.move("WGEN_out_" + WSTA + repr(pdate_1//1000)+".txt", dest_dir)
        #=============================================
        # Make a boxplot and exceedance curve of yield and other output
        #=============================================
        self.Yield_boxplot(sname, SNX_list, start_year, obs_flag, self._display)
        #=============================================
        # Make plots of water/nitrogen stresss
        #=============================================
        self.WSGD_plot(sname, SNX_list, start_year, obs_flag, self._display)
        #=============================================
        # Make plots of weather time series (Tmin, Tmax, Srad)
        #=============================================
        self.weather_plot(sname, SNX_list, start_year, obs_flag, self._display)
        #=============================================
        # Make plots of cumulative rainfall time series
        #=============================================
        self.plot_cum_rain(sname, SNX_list, start_year, obs_flag, self._display)
        print('It took {0:5.1f} sec to finish 100 simulations & to post-process outputs'.format(end_time0-start_time))
        print ("** Yeah!! DSSAT Simulation has been finished successfully!")
        print ("** Please check all output and figues in your working directory.")



    def writeSNX2_DSSAT(self):
        
        self.Wdir_path = self.__getWorkingDir()
        if self._scName2.value == "":
            sname = "TST2" 
        elif self._scName1.value in [' ', "TST1"]:
            sname = "TST2" 
        else:
            sname = self._scName2.value[:4]

        setup1Values = self._setup1.Setup1Results() # All inputs from Setup1 are stored here
        #print("Setup1 Results:", setup1Values)
        WSTA = setup1Values[0][:4] #'SANJ'
        WTD_fname = self.Wdir_path.replace("/","\\") + "\\"+ WSTA + ".WTD"
        WTD_last_date = self.find_obs_date(WTD_fname)

        start_year = setup1Values[1] #'2002'
        obs_flag = 0  #by default, assumes no obs weather data is available
        #=============================================
        #Determine pdate_1[YYYYDOY] and pdate_2[YYYYDOY]
        lst = setup1Values[13]  #Year 1 for planting
        lst2 = setup1Values[14]   #Year 2 for planting
        lst_frst = setup1Values[11]  #Year 1 for SCF
        lst_frst2 = setup1Values[12]  #Year 2 for SCF
        pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m = self.find_plt_date(start_year, lst, lst2, lst_frst, lst_frst2)

        if WTD_last_date >= hv_date: #180 is arbitrary number to fully cover weather data until late maturity
            obs_flag = 1

        step = int(self._scSteps2.value)
        cr_type='MZ'
        SNX_list = []
        if pdate_2%1000 > pdate_1%1000:
            for iday in range(pdate_1,pdate_2+1,step):   
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\M"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main(SNX_fname,cr_type,iday,obs_flag)  
                SNX_list.append(SNX_fname)

        else: #if planting window covers two consecutive years
                #go through Year 1
            end_day = int(start_year)*1000 + 365
            if calendar.isleap(int(start_year)):  end_day = int(start_year)*1000 + 366
            for iday in range(pdate_1,end_day + 1,step):   
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\M"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main(SNX_fname,cr_type,iday,obs_flag)    
                SNX_list.append(SNX_fname) 
                temp_date = iday
            #next, go throught Year 2
            start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 365  #e.g, 364 + 5 - 365 = DOY 4
            if calendar.isleap(int(start_year)):  start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 366
            for iday in range(start_day,pdate_2 + 1,step):   #e.g, 2018004 to 2018030
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\M"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main(SNX_fname,cr_type,iday,obs_flag)     
                SNX_list.append(SNX_fname)       

        #=============================================
        # Write scenario summary into a text file 
        # => this can be also used for input for Create_WTH subroutine
        #=============================================
        LAT, LONG, ELEV, TAV, AMP = self.find_station_info(WSTA)  #Call a function to find more info about station to write header in *.WTH
        summary_fname=self.Wdir_path.replace("/","\\") + "\\" + sname+"_summary.txt"
        f = open(summary_fname,"w") 
        working_dir=self.Wdir_path.replace("/","\\")
        f.write("Directory:  " + working_dir + "\n")
        f.write("Station_name: " + WSTA + " \n")
        f.write("LAT: " + repr(LAT) + " \n")
        f.write("LONG: " + repr(LONG) + " \n")
        f.write("ELEV: " + repr(ELEV) + " \n")
        f.write("TAV: " + repr(TAV) + " \n")
        f.write("AMP: " + repr(AMP) + " \n")
        f.write("First_Plt_Date: " + repr(pdate_1) + " \n")
        f.write("Last_Plt_Date: " + repr(pdate_2) + " \n")
        f.write("Harvest_Date: " + repr(hv_date) + " \n")
        f.write("frst_start_Date: " + repr(frst_date1) + " \n")
        f.write("Planting_window: " + repr(pwindow_m) + " \n")
        f.write("SCF_window: " + repr(fwindow_m) + " \n")
        f.write("Cultivar: " + str(setup1Values[5]) + " \n")  #INGENO=self.cul_type.getvalue()[0][0:18] #cultivar type
        f.write("Planting_density: " + str(setup1Values[6]) + " \n")  # PPOP=self.plt_density.getvalue()[0:3] #planting density
        f.write("Soil_type: " + str(setup1Values[7])  + " \n")  #ID_SOIL=self.soil_type.getvalue()[0][0:10]
        f.write("Irrigation_A(1)_N(0): " + repr(self._setup2.getApplyIrr()) + " \n")    #automatic or no-irrigation
        
        if self._setup2.getApplyIrr() == 1:
            IMETH = self._setup2.getApplyIrrMeth() #'IR001' #irrigation method
            f.write("IRR_method: " + IMETH + " \n")
        f.write("Fertilizer_Y(1)_N(0): " + repr(self._setup2.getApplyFert()) + " \n")
        
        FInputs1 = self._setup2.getFertInpList1()
        FInputs2 = self._setup2.getFertInpList2()
        FInputs3 = self._setup2.getFertInpList3()

        FDATE1  =   FInputs1[0] #'0'
        FDATE2  =   FInputs2[0]  #'60'
        FDATE3  =   FInputs3[0]  #'60'

        FAMN1   =   FInputs1[1] #'50'  #1st fertilizer application amount
        FAMN2   =   FInputs2[1] #'90'  #2nd fertilizer application amount
        FAMN3   =   FInputs3[1] #'90'  #3rd fertilizer application amount

        FMCD1   =   FInputs1[2] #'50'  #1st fertilizer Material
        FMCD2   =   FInputs2[2] #'50'  #2nd fertilizer Material
        FMCD3   =   FInputs3[2] #'50'  #3rd fertilizer Material 

        
        if self._setup2.getApplyFert == 1:
            f.write("FDATE1: " + str(FDATE1) + " \n")  
            f.write("FDATE2: " + str(FDATE2) + " \n")
            f.write("FDATE3: " + str(FDATE3) + " \n")
            f.write("F_amount1: " + str(FAMN1)  + " \n")
            f.write("F_amount2: " + str(FAMN2) + " \n")
            f.write("F_amount3: " + str(FAMN3) + " \n")
            f.write("F_method1: " + str(FMCD1) + " \n")
            f.write("F_method2: " + str(FMCD2) + " \n")
            f.write("F_method3: " + str(FMCD3) + " \n")
    
        f.close()

        list_BN = setup1Values[15]
        list_NN = setup1Values[10]
        list_AN = setup1Values[16]

        temp_csv=self.Wdir_path.replace("/","\\") + "\\SCF_input_monthly1.csv"
        with open(temp_csv, 'w', newline='') as csvfile:
            scfwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            scfwriter.writerow(['Month','1','2', '3', '4','5','6','7','8','9','10','11','12'])
            scfwriter.writerow(['BN'] + list_BN[:12])
            scfwriter.writerow(['NN'] + list_NN[:12])
            scfwriter.writerow(['AN'] + list_AN[:12])

        temp_csv=self.Wdir_path.replace("/","\\") + "\\SCF_input_monthly2.csv" 
        with open(temp_csv, 'w', newline='') as csvfile:
            scfwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            scfwriter.writerow(['Month','1','2', '3', '4','5','6','7','8','9','10','11','12'])
            scfwriter.writerow(['BN'] + list_BN[12:])
            scfwriter.writerow(['NN'] + list_NN[12:])
            scfwriter.writerow(['AN'] + list_AN[12:])

        temp_snx=self.Wdir_path.replace("/","\\") + "\\button_write.csv" 
        with open(temp_snx, 'w', newline='') as csvfile:
            scfwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            scfwriter.writerow(['Month','J1','F1', 'M1', 'A1','M1','J1','J1','A1','S1','O1',
                    'N1','D1', 'J2','F2', 'M2', 'A2','M2','J2','J2','A2','S2','O2','N2','D2'])
            scfwriter.writerow(['SCF'] + setup1Values[11] + setup1Values[12])
            scfwriter.writerow(['plt'] + setup1Values[13] + setup1Values[14])
            # scfwriter.writerow(self.scf_button1.state())
        #=============================================
        # Generate 100 weather realizations
        #=============================================
            # Get a list of all files in directory
            for rootDir, subdirs, filenames in os.walk(self.Wdir_path):
                # Find the files that matches the given patterm
                for filename in fnmatch.filter(filenames, '0*.WTH'):
                    try:
                        os.remove(os.path.join(rootDir, filename))
                    except OSError:
                        print("Error while deleting file")

        create_WTH_main(self.Wdir_path,WTD_fname, WSTA, LAT, LONG, ELEV, TAV, AMP, pdate_1, pdate_2, step, hv_date, frst_date1)
        #=============================================
        # Create DSSBatch.v47 file and run DSSAT executable 
        #=============================================
        entries = ("PlantGro.OUT","Evaluate.OUT","SCF_input_monthly1.csv", "SCF_input_monthly2.csv", 
                     "ET.OUT","OVERVIEW.OUT","PlantN.OUT","SoilNi.OUT","Weather.OUT", 
                    "SolNBalSum.OUT","SoilNoBal.OUT","SoilTemp.OUT","SoilWat.OUT","SoilWatBal.OUT","Summary.OUT") 
        self.writeV47_main(SNX_list, obs_flag)   ##write *.V47 batch file

        #==RUN DSSAT with ARGUMENT 
        start_time = time.perf_counter() # => not compatible with Python 2.7
        os.chdir(self.Wdir_path)  #change directory
        args = "DSCSM047.EXE N DSSBatch.v47 "
        subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
        end_time0 = time.perf_counter() # => not compatible with Python 2.7
        print('It took {0:5.1f} sec to finish n years of obs weather x n planting dates'.format(end_time0-start_time))
        start_time1 = time.perf_counter()  # => not compatible with Python 2.7

        #create a new folder to save outputs of the target scenario
        new_folder=self._scName2.value+"_output"
        if os.path.exists(new_folder):
            shutil.rmtree(new_folder)   #remove existing folder
        os.makedirs(new_folder)
        #copy outputs to the new folder
        dest_dir=self.Wdir_path + "\\"+new_folder
        # print 'dest_dir is', dest_dir

        for entry in entries:
            source_file=self.Wdir_path + "\\" + entry
            if os.path.isfile(source_file):
                shutil.move(source_file, dest_dir)
            else:
                print( '**Error!!- No DSSAT output files => Check if DSSAT simuation was successful')

        #move *.SNX file to the new folder with output files
        for i in range(len(SNX_list)):
            shutil.move(SNX_list[i], dest_dir)
        shutil.move(summary_fname, dest_dir) 

        #  temp = "WGEN_out_" + WSTA + repr(pdate_1//1000)+".txt"
        shutil.move("WGEN_input_param_month_" + WSTA + ".txt", dest_dir)
        shutil.move("WGEN_out_" + WSTA + repr(pdate_1//1000)+".txt", dest_dir)
        #=============================================
        # Make a boxplot and exceedance curve of yield and other output
        #=============================================
        self.Yield_boxplot(sname, SNX_list, start_year, obs_flag, self._display2)
        #=============================================
        # Make plots of water/nitrogen stresss
        #=============================================
        self.WSGD_plot(sname, SNX_list, start_year, obs_flag, self._display2)
        #=============================================
        # Make plots of weather time series (Tmin, Tmax, Srad)
        #=============================================
        self.weather_plot(sname, SNX_list, start_year, obs_flag, self._display2)
        #=============================================
        # Make plots of cumulative rainfall time series
        #=============================================
        self.plot_cum_rain(sname, SNX_list, start_year, obs_flag, self._display2)
        print('It took {0:5.1f} sec to finish 100 simulations & to post-process outputs'.format(end_time0-start_time))
        print ("** Yeah!! DSSAT Simulation has been finished successfully!")
        print ("** Please check all output and figues in your working directory.")



    def writeSNX3_DSSAT(self):
        
        self.Wdir_path = self.__getWorkingDir()
        if self._scName3.value == "":
            sname = "TST2" 
        elif self._scName3.value in [' ', "TST1"]:
            sname = "TST2" 
        else:
            sname = self._scName3.value[:4]

        setup1Values = self._setup1.Setup1Results() # All inputs from Setup1 are stored here
        print("Setup1 Results:", setup1Values)
        WSTA = setup1Values[0][:4] #'SANJ'
        WTD_fname = self.Wdir_path.replace("/","\\") + "\\"+ WSTA + ".WTD"
        WTD_last_date = self.find_obs_date(WTD_fname)

        start_year = setup1Values[1] #'2002'
        obs_flag = 0  #by default, assumes no obs weather data is available
        #=============================================
        #Determine pdate_1[YYYYDOY] and pdate_2[YYYYDOY]
        lst = setup1Values[13]  #Year 1 for planting
        lst2 = setup1Values[14]   #Year 2 for planting
        lst_frst = setup1Values[11]  #Year 1 for SCF
        lst_frst2 = setup1Values[12]  #Year 2 for SCF
        pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m = self.find_plt_date(start_year, lst, lst2, lst_frst, lst_frst2)

        if WTD_last_date >= hv_date: #180 is arbitrary number to fully cover weather data until late maturity
            obs_flag = 1

        step = int(self._scSteps3.value)
        cr_type='MZ'
        SNX_list = []
        if pdate_2%1000 > pdate_1%1000:
            for iday in range(pdate_1,pdate_2+1,step):   
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\M"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main(SNX_fname,cr_type,iday,obs_flag)  
                SNX_list.append(SNX_fname)

        else: #if planting window covers two consecutive years
                #go through Year 1
            end_day = int(start_year)*1000 + 365
            if calendar.isleap(int(start_year)):  end_day = int(start_year)*1000 + 366
            for iday in range(pdate_1,end_day + 1,step):   
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\M"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main(SNX_fname,cr_type,iday,obs_flag)    
                SNX_list.append(SNX_fname) 
                temp_date = iday
            #next, go throught Year 2
            start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 365  #e.g, 364 + 5 - 365 = DOY 4
            if calendar.isleap(int(start_year)):  start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 366
            for iday in range(start_day,pdate_2 + 1,step):   #e.g, 2018004 to 2018030
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\M"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main(SNX_fname,cr_type,iday,obs_flag)     
                SNX_list.append(SNX_fname)       

        #=============================================
        # Write scenario summary into a text file 
        # => this can be also used for input for Create_WTH subroutine
        #=============================================
        LAT, LONG, ELEV, TAV, AMP = self.find_station_info(WSTA)  #Call a function to find more info about station to write header in *.WTH
        summary_fname=self.Wdir_path.replace("/","\\") + "\\" + sname+"_summary.txt"
        f = open(summary_fname,"w") 
        working_dir=self.Wdir_path.replace("/","\\")
        f.write("Directory:  " + working_dir + "\n")
        f.write("Station_name: " + WSTA + " \n")
        f.write("LAT: " + repr(LAT) + " \n")
        f.write("LONG: " + repr(LONG) + " \n")
        f.write("ELEV: " + repr(ELEV) + " \n")
        f.write("TAV: " + repr(TAV) + " \n")
        f.write("AMP: " + repr(AMP) + " \n")
        f.write("First_Plt_Date: " + repr(pdate_1) + " \n")
        f.write("Last_Plt_Date: " + repr(pdate_2) + " \n")
        f.write("Harvest_Date: " + repr(hv_date) + " \n")
        f.write("frst_start_Date: " + repr(frst_date1) + " \n")
        f.write("Planting_window: " + repr(pwindow_m) + " \n")
        f.write("SCF_window: " + repr(fwindow_m) + " \n")
        f.write("Cultivar: " + str(setup1Values[5]) + " \n")  #INGENO=self.cul_type.getvalue()[0][0:18] #cultivar type
        f.write("Planting_density: " + str(setup1Values[6]) + " \n")  # PPOP=self.plt_density.getvalue()[0:3] #planting density
        f.write("Soil_type: " + str(setup1Values[7])  + " \n")  #ID_SOIL=self.soil_type.getvalue()[0][0:10]
        f.write("Irrigation_A(1)_N(0): " + repr(self._setup2.getApplyIrr()) + " \n")    #automatic or no-irrigation
        
        if self._setup2.getApplyIrr() == 1:
            IMETH = self._setup2.getApplyIrrMeth() #'IR001' #irrigation method
            f.write("IRR_method: " + IMETH + " \n")
        f.write("Fertilizer_Y(1)_N(0): " + repr(self._setup2.getApplyFert()) + " \n")
        
        FInputs1 = self._setup2.getFertInpList1()
        FInputs2 = self._setup2.getFertInpList2()
        FInputs3 = self._setup2.getFertInpList3()

        FDATE1  =   FInputs1[0] #'0'
        FDATE2  =   FInputs2[0]  #'60'
        FDATE3  =   FInputs3[0]  #'60'

        FAMN1   =   FInputs1[1] #'50'  #1st fertilizer application amount
        FAMN2   =   FInputs2[1] #'90'  #2nd fertilizer application amount
        FAMN3   =   FInputs3[1] #'90'  #3rd fertilizer application amount

        FMCD1   =   FInputs1[2] #'50'  #1st fertilizer Material
        FMCD2   =   FInputs2[2] #'50'  #2nd fertilizer Material
        FMCD3   =   FInputs3[2] #'50'  #3rd fertilizer Material 

        
        if self._setup2.getApplyFert == 1:
            f.write("FDATE1: " + str(FDATE1) + " \n")  
            f.write("FDATE2: " + str(FDATE2) + " \n")
            f.write("FDATE3: " + str(FDATE3) + " \n")
            f.write("F_amount1: " + str(FAMN1)  + " \n")
            f.write("F_amount2: " + str(FAMN2) + " \n")
            f.write("F_amount3: " + str(FAMN3) + " \n")
            f.write("F_method1: " + str(FMCD1) + " \n")
            f.write("F_method2: " + str(FMCD2) + " \n")
            f.write("F_method3: " + str(FMCD3) + " \n")
    
        f.close()

        list_BN = setup1Values[15]
        list_NN = setup1Values[10]
        list_AN = setup1Values[16]

        temp_csv=self.Wdir_path.replace("/","\\") + "\\SCF_input_monthly1.csv"
        with open(temp_csv, 'w', newline='') as csvfile:
            scfwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            scfwriter.writerow(['Month','1','2', '3', '4','5','6','7','8','9','10','11','12'])
            scfwriter.writerow(['BN'] + list_BN[:12])
            scfwriter.writerow(['NN'] + list_NN[:12])
            scfwriter.writerow(['AN'] + list_AN[:12])

        temp_csv=self.Wdir_path.replace("/","\\") + "\\SCF_input_monthly2.csv" 
        with open(temp_csv, 'w', newline='') as csvfile:
            scfwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            scfwriter.writerow(['Month','1','2', '3', '4','5','6','7','8','9','10','11','12'])
            scfwriter.writerow(['BN'] + list_BN[12:])
            scfwriter.writerow(['NN'] + list_NN[12:])
            scfwriter.writerow(['AN'] + list_AN[12:])

        temp_snx=self.Wdir_path.replace("/","\\") + "\\button_write.csv" 
        with open(temp_snx, 'w', newline='') as csvfile:
            scfwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            scfwriter.writerow(['Month','J1','F1', 'M1', 'A1','M1','J1','J1','A1','S1','O1',
                    'N1','D1', 'J2','F2', 'M2', 'A2','M2','J2','J2','A2','S2','O2','N2','D2'])
            scfwriter.writerow(['SCF'] + setup1Values[11] + setup1Values[12])
            scfwriter.writerow(['plt'] + setup1Values[13] + setup1Values[14])
            # scfwriter.writerow(self.scf_button1.state())
        #=============================================
        # Generate 100 weather realizations
        #=============================================
            # Get a list of all files in directory
            for rootDir, subdirs, filenames in os.walk(self.Wdir_path):
                # Find the files that matches the given patterm
                for filename in fnmatch.filter(filenames, '0*.WTH'):
                    try:
                        os.remove(os.path.join(rootDir, filename))
                    except OSError:
                        print("Error while deleting file")

        create_WTH_main(self.Wdir_path,WTD_fname, WSTA, LAT, LONG, ELEV, TAV, AMP, pdate_1, pdate_2, step, hv_date, frst_date1)
        #=============================================
        # Create DSSBatch.v47 file and run DSSAT executable 
        #=============================================
        entries = ("PlantGro.OUT","Evaluate.OUT","SCF_input_monthly1.csv", "SCF_input_monthly2.csv", 
                     "ET.OUT","OVERVIEW.OUT","PlantN.OUT","SoilNi.OUT","Weather.OUT", 
                    "SolNBalSum.OUT","SoilNoBal.OUT","SoilTemp.OUT","SoilWat.OUT","SoilWatBal.OUT","Summary.OUT") 
        self.writeV47_main(SNX_list, obs_flag)   ##write *.V47 batch file

        #==RUN DSSAT with ARGUMENT 
        os.chdir(self.Wdir_path)  #change directory
        args = "DSCSM047.EXE N DSSBatch.v47 "
        subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)

        #create a new folder to save outputs of the target scenario
        new_folder=self._scName3.value+"_output"
        if os.path.exists(new_folder):
            shutil.rmtree(new_folder)   #remove existing folder
        os.makedirs(new_folder)
        #copy outputs to the new folder
        dest_dir=self.Wdir_path + "\\"+new_folder
        # print 'dest_dir is', dest_dir

        for entry in entries:
            source_file=self.Wdir_path + "\\" + entry
            if os.path.isfile(source_file):
                shutil.move(source_file, dest_dir)
            else:
                print( '**Error!!- No DSSAT output files => Check if DSSAT simuation was successful')

        #move *.SNX file to the new folder with output files
        for i in range(len(SNX_list)):
            shutil.move(SNX_list[i], dest_dir)
        shutil.move(summary_fname, dest_dir) 

        #  temp = "WGEN_out_" + WSTA + repr(pdate_1//1000)+".txt"
        shutil.move("WGEN_input_param_month_" + WSTA + ".txt", dest_dir)
        shutil.move("WGEN_out_" + WSTA + repr(pdate_1//1000)+".txt", dest_dir)
        #=============================================
        # Make a boxplot and exceedance curve of yield and other output
        #=============================================
        self.Yield_boxplot(sname, SNX_list, start_year, obs_flag, self._display3)
        #=============================================
        # Make plots of water/nitrogen stresss
        #=============================================
        self.WSGD_plot(sname, SNX_list, start_year, obs_flag, self._display3)
        #=============================================
        # Make plots of weather time series (Tmin, Tmax, Srad)
        #=============================================
        self.weather_plot(sname, SNX_list, start_year, obs_flag, self._display3)
        #=============================================
        # Make plots of cumulative rainfall time series
        #=============================================
        self.plot_cum_rain(sname, SNX_list, start_year, obs_flag, self._display3)
        # print('It took {0:5.1f} sec to finish 100 simulations & to post-process outputs'.format(end_time0-start_time))
        print ("** Yeah!! DSSAT Simulation has been finished successfully!")
        print ("** Please check all output and figues in your working directory.")


    def writeV47_main(self, snx_list, obs_flag):
        self.Wdir_path = self.__getWorkingDir()

        temp_dv4    =   self.Wdir_path.replace("/","\\") + "\\DSSBatch_TEMP_MZ.v47"
        # pdate_1, pdate_2 = self._setup1.setPlantingWindow()

        dv4_fname   =   self.Wdir_path.replace("/","\\") + "\\DSSBatch.v47" 
        fr = open(temp_dv4,"r") #opens temp DV4 file to read
        fw = open(dv4_fname,"w") 
        #read template and write lines
        for line in range(0,10):
            temp_str=fr.readline()
            fw.write(temp_str)
            
        temp_str=fr.readline()
        #write SNX file with each different planting date
        for i in range(len(snx_list)):   #EJ(3/12/2019) for finding optimal planting dates
            #new_str=Wdir_path.replace("/","\\")+ "\\" + snx_list[i]
            #new_str2='{:<95}'.format(new_str)+ temp_str[95:]
            for j in range(100):
                #new_str2='{:<95}'.format(snx_list[i])+ temp_str[95:]
                new_str2='{0:<95}{1:4s}'.format(snx_list[i], repr(j+1).rjust(4))+ temp_str[99:]
                fw.write(new_str2)
            if obs_flag ==1:
                new_str2='{0:<95}{1:4s}'.format(snx_list[i], repr(101).rjust(4))+ temp_str[99:]
                fw.write(new_str2)                
        fr.close()
        fw.close()

    def writeSNX_main(self, SNX_fname ,crop, plt_date, obs_flag):
        self.Wdir_path = self.__getWorkingDir()
        temp_snx =  self.Wdir_path.replace("/","\\") + "\\PHMZTEMP.SNX" 
        
        fr = open(temp_snx,"r") #opens temp SNX file to read
        fw = open(SNX_fname,"w") #opens SNX file to write

        #===set up parameters
        MI= '0'    #'1'  #str(self._setup2.getApplyIrr())  #should be one character 
        MF= str(self._setup2.getApplyFert())    #'1'

        #print("Results1: ", self.setup1Results)
        INGENO = self.setup1Results[5] #cultivar type
        WSTA = self.setup1Results[0][0:4]  #'SANJ'
        ID_SOIL = self.setup1Results[7]
        start_year = self.setup1Results[1] #'2002'
        NYERS='1'  #repr(int(end_year)-int(start_year)+1) #'12'
        temp=int(plt_date%1000)-30  #initial condition: 30 day before planting  #EJ(3/13/2019)

        if temp <= 0 :   #EJ(3/13/2019)
            if calendar.isleap(int(start_year)-1):  
                temp = 366 + temp
            else:
                temp = 365 + temp
            ICDAT = repr((plt_date//1000 -1)*1000+temp)
        else:
            ICDAT = repr((plt_date//1000)*1000+temp)

        i_NO3 = self.setup1Results[9] #'H' #or 'L'
        SNH4 = 1.5  #**EJ(5/27/2015) followed by Walter's ETMZDSS6.SNX
        #PDATE=start_year[2:]+repr(plt_date).zfill(3)
        PDATE=repr(plt_date)[2:]      #YYDOY format

        # if plt_date > 244: #244 (Sep 1) is an approximate number to prevent growing period goes beyond the available late year data
        #     NYERS = str(int(NYERS)-1)   #adjust total simulation years  

        FInputs1 = self._setup2.getFertInpList1()
        FInputs2 = self._setup2.getFertInpList2()
        FInputs3 = self._setup2.getFertInpList3()

        FDATE1  =   FInputs1[0] #'0'
        FAMN1   =   FInputs1[1] #'50'  #1st fertilizer application amount
        FMCD1   =   FInputs1[2] #'50'  #1st fertilizer Material   
        if FInputs1[3] != "None":      #1st fertilizer application
            FACD1   = FInputs1[3]
        else:
             FACD1  = '-99' 

        FDATE2  =   FInputs2[0]  #'60'
        FAMN2   =   FInputs2[1] #'90'  #2nd fertilizer application amount
        FMCD2   =   FInputs2[2] #'50'  #2nd fertilizer Material  
        if FInputs2[3] != "None":      #2nd fertilizer application
            FACD2   = FInputs2[3]
        else:
             FACD2  = '-99'     

        FDATE3  =   FInputs3[0]  #'60'
        FAMN3   =   FInputs3[1] #'90'  #3rd fertilizer application amount
        FMCD3   =   FInputs3[2] #'50'  #3rd fertilizer Material    
        if FInputs3[3] != "None":      #3rd fertilizer application
            FACD3   = FInputs3[3]
        else:
             FACD3  = '-99' 

        # temp=int(plt_date)-3 #EJ(6/9/2015) #simuation starting date
       
        SDATE=ICDAT    #EJ(3/13/2019)

        if self._setup2.getApplyIrr() == 1:
            IRRIG='A'  #automatic, or 'N' (no irrigation)
        else:
            IRRIG='N'  #automatic, or 'N' (no irrigation)

        if self._setup2.getApplyFert == 1:
            FERTI='D' # 'D'= Days after planting, 'R'=on report date, or 'N' (no fertilizer)
        else:
            FERTI='N'

        IC_w_ratio = float(self.setup1Results[8]) #e.g., 0.7 
        IMETH = self._setup2.getApplyIrrMeth() #'IR001' #irrigation method
        #===end of setting up paramters

        
         #read lines 1-9 from temp file
        for line in range(0,14):
            temp_str=fr.readline()
            fw.write(temp_str)

        #write *TREATMENTS 
        CU='1'
        SA='0'
        IC='1'
        MP='1'
        MR='0'
        MC='0'
        MT='0'
        ME='0'
        MH='0'
        SM='1'
        temp_str=fr.readline()
        for i in range(0,100):
            FL = str(i+1)
            fw.write('{0:3s}{1:31s}{2:3s}{3:3s}{4:3s}{5:3s}{6:3s}{7:3s}{8:3s}{9:3s}{10:3s}{11:3s}{12:3s}{13:3s}'.format(FL.rjust(3),'1 0 0 ERiMA DCC1                 1',
                        FL.rjust(3),SA.rjust(3), IC.rjust(3), MP.rjust(3), MI.rjust(3), MF.rjust(3), MR.rjust(3), MC.rjust(3), 
                        MT.rjust(3), ME.rjust(3), MH.rjust(3), SM.rjust(3)))  
            fw.write(" \n")  
        if obs_flag == 1:
            FL = str(101)  #if observed weather is available, run #101 treatment with observed weather
            fw.write('{0:3s}{1:31s}{2:3s}{3:3s}{4:3s}{5:3s}{6:3s}{7:3s}{8:3s}{9:3s}{10:3s}{11:3s}{12:3s}{13:3s}'.format(FL.rjust(3),'1 0 0 ERiMA DCC1                 1',
                        FL.rjust(3),SA.rjust(3), IC.rjust(3), MP.rjust(3), MI.rjust(3), MF.rjust(3), MR.rjust(3), MC.rjust(3), 
                        MT.rjust(3), ME.rjust(3), MH.rjust(3), SM.rjust(3)))       
            fw.write(" \n")

        #read lines from temp file
        for line in range(0,3):
            temp_str=fr.readline()
            fw.write(temp_str)

        #write *CULTIVARS
        temp_str=fr.readline()
        new_str=temp_str[0:6] + INGENO + temp_str[26:]
        fw.write(new_str)
        fw.write(" \n")

        #read lines from temp file
        for line in range(0,3):
            temp_str=fr.readline()
            fw.write(temp_str)

        #================write *FIELDS   
        #Get soil info from *.SOL
        #soil_depth, wp, fc, nlayer = get_soil_IC(ID_SOIL)  
        soil_depth, wp, fc, nlayer, SLTX = self.get_soil_IC(ID_SOIL)  # <=========== ***********************
        temp_str=fr.readline()
        SLDP = repr(soil_depth[-1])
        for i in range(0,100):
            FL = str(i+1)
            ID_FIELD = WSTA + str(i+1).zfill(4)  #WSTA = 'SANJ'
            WSTA_ID = str(i+1).zfill(4)
            fw.write('{0:3s}{1:8s}{2:5s}{3:3s}{4:6s}{5:4s}  {6:10s}{7:4s}'.format(FL.rjust(3), ID_FIELD, WSTA_ID.rjust(5), '       -99   -99   -99   -99   -99   -99 ',
                                                SLTX.ljust(6), SLDP.rjust(4), ID_SOIL,' -99'))  
            fw.write(" \n")  
        if obs_flag == 1:
            FL = str(101)  #if observed weather is available, run #101 treatment with observed weather
            ID_FIELD = WSTA + str(101).zfill(4)  #WSTA = 'SANJ'
            WSTA_ID = WSTA
            fw.write('{0:3s}{1:8s}{2:5s}{3:3s}{4:6s}{5:4s}  {6:10s}{7:4s}'.format(FL.rjust(3), ID_FIELD, WSTA_ID.rjust(5), '       -99   -99   -99   -99   -99   -99 ',
                                                SLTX.ljust(6), SLDP.rjust(4), ID_SOIL,' -99'))  
            fw.write(" \n")  

        temp_str=fr.readline()  #@L ...........XCRD ...........YCRD .....ELEV
        fw.write(temp_str)
        temp_str=fr.readline()  # 1             -99             -99       -99   ==> skip
        #================write *FIELDS - second section
        for i in range(0,100):
            FL = str(i+1)
            fw.write('{0:3s}{1:89s}'.format(FL.rjust(3), '            -99             -99       -99               -99   -99   -99   -99   -99   -99'))
            fw.write(" \n")  
        if obs_flag == 1:
            FL = str(101)  #if observed weather is available, run #101 treatment with observed weather
            fw.write('{0:3s}{1:89s}'.format(FL.rjust(3), '            -99             -99       -99               -99   -99   -99   -99   -99   -99'))
            fw.write(" \n")  
        
        #read lines from temp file
        for line in range(0,3):
            temp_str=fr.readline()
            fw.write(temp_str)
        #write *INITIAL CONDITIONS   
        temp_str=fr.readline()
        new_str=temp_str[0:9] + ICDAT[2:] + temp_str[14:]
        fw.write(new_str)
        temp_str=fr.readline() #@C  ICBL  SH2O  SNH4  SNO3 
        fw.write(temp_str)

        #Get soil info from *.SOL
        #soil_depth, wp, fc, nlayer = get_soil_IC(ID_SOIL)  
        temp_str=fr.readline()
        for nline in range(0,nlayer):
            if nline == 0:  #first layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                # SH2O=0.7*(float(fc[nline])- float(wp[nline]))+ float(wp[nline])#EJ(6/25/2015): initial AWC=70% of maximum AWC
                if i_NO3 == 'H':
                    SNO3='14'  # arbitrary... following to Uruguay DSS
                elif i_NO3 == 'L':
                    SNO3='5'  # arbitrary... following to Uruguay DSS
                else:
                    # self.ini_NO3_err.activate() 
                    print("Error of NO3 value")                  
            elif nline == 1:  #second layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                if i_NO3 == 'H':
                    SNO3='14'  # arbitrary... following to Uruguay DSS
                elif i_NO3 == 'L':
                    SNO3='5'  # arbitrary... following to Uruguay DSS
                else:
                    # self.ini_NO3_err.activate() 
                    print("Error of NO3 value") 
            elif nline == 2:  #third layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
            elif nline == 3:  #forth layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
            elif nline == 4:  #fifth layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
            elif nline == 5:  #sixth layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
            else:
                temp_SH2O=fc[nline] #float
                SNO3='0'  #**EJ(5/27/2015)
            
            SH2O=repr(temp_SH2O)[0:5]  #convert float to string
            new_str=temp_str[0:5] + repr(soil_depth[nline]).rjust(3) + ' ' + SH2O.rjust(5) + temp_str[14:22]+ SNO3.rjust(4)+ "\n"
            fw.write(new_str)
        fw.write("  \n")

        for nline in range(0,10):
            temp_str=fr.readline()
            #print temp_str
            if temp_str[0:9] == '*PLANTING':
                break

        fw.write(temp_str) #*PLANTING DETAILS  
        temp_str=fr.readline() #@P PDATE EDATE
        fw.write(temp_str)


        #write *PLANTING DETAILS
        temp_str=fr.readline()
        PPOP    = self.setup1Results[6] #planting density
        PPOE    = self.setup1Results[6] #planting density       
        #new_str=temp_str[0:3] + PDATE + temp_str[8:]
        new_str=temp_str[0:3] + PDATE + temp_str[8:14] + PPOP.rjust(6) + PPOE.rjust(6) + temp_str[26:]
        fw.write(new_str)


        #read lines from temp file
        for line in range(0,3):
            temp_str=fr.readline()
            fw.write(temp_str)

        #write *FERTILIZERS (INORGANIC)
        if MF == '1':
            temp_str=fr.readline()
            new_str=temp_str[0:5] + str(FDATE1).rjust(3) + ' '+ str(FMCD1).rjust(5)+' '+str(FACD1).rjust(5)+temp_str[20:30]+ str(FAMN1).rjust(2) + temp_str[32:]
            fw.write(new_str)
            temp_str=fr.readline()
            if self._setup2.getApplyNumFert() == '2':
                new_str=temp_str[0:5] + str(FDATE2).rjust(3) + ' '+ str(FMCD2).rjust(5)+' '+str(FACD2).rjust(5)+temp_str[20:30]+ str(FAMN2).rjust(2) + temp_str[32:]
                fw.write(new_str)
            if self._setup2.getApplyNumFert() == '3':
                new_str=temp_str[0:5] + str(FDATE2).rjust(3) + ' '+ str(FMCD2).rjust(5)+' '+str(FACD2).rjust(5)+temp_str[20:30]+ str(FAMN2).rjust(2) + temp_str[32:]
                fw.write(new_str)
                new_str=temp_str[0:5] + str(FDATE3).rjust(3) + ' '+ str(FMCD3).rjust(5)+' '+str(FACD3).rjust(5)+temp_str[20:30]+ str(FAMN3).rjust(2) + temp_str[32:]
                fw.write(new_str)
            #check errors in User's input
            # if self.nfertilizer.getvalue() == '':
            #     self.fert_err.activate()
            # if self.label006.cget("text") == '': #'N/A':
            #     self.fert_err.activate()
            # if self.label007.cget("text") == '': #'N/A':
            #     self.fert_err.activate()
            if self._setup2.getApplyNumFert() == '0':
                print("Error: NUmber of fertlization app is 0.")

        else:  #
            temp_str=fr.readline()
            fw.write(temp_str)
            temp_str=fr.readline()
            fw.write(temp_str)

        #read lines from temp file
        for line in range(0,3):
            temp_str=fr.readline()
            fw.write(temp_str)
        
        #write *SIMULATION CONTROLS
        temp_str=fr.readline()
        new_str=temp_str[0:33]+ SDATE[2:] + temp_str[38:]
        fw.write(new_str)
        temp_str=fr.readline() #@N OPTIONS
        fw.write(temp_str)
        temp_str=fr.readline()  # 1 OP     
        fw.write(temp_str)
        temp_str=fr.readline()  #@N METHODS 
        fw.write(temp_str)
        temp_str=fr.readline()  # 1 ME      
        fw.write(temp_str)
        temp_str=fr.readline()  #@N MANAGEMENT 
        fw.write(temp_str)
        temp_str=fr.readline()  # 1 MA   
        new_str=temp_str[0:25] + IRRIG + temp_str[26:31]+ FERTI + temp_str[32:]
        fw.write(new_str)
        temp_str=fr.readline()  #@N OUTPUTS
        fw.write(temp_str)
        temp_str=fr.readline()  # 1 OU   
        fw.write(temp_str)
            
        #read lines from temp file
        for line in range(0,5):
            temp_str=fr.readline()
            fw.write(temp_str)
        #irrigation method
        temp_str=fr.readline()  #  1 IR 
        new_str=temp_str[0:39] + IMETH + temp_str[44:]
        fw.write(new_str)

        #read lines from temp file
        for line in range(0,7):
            temp_str=fr.readline()
            fw.write(temp_str)

        irr_meth = self._setup2.getApplyIrrMeth()

        #read lines from temp file
        for line in range(0,7):
            temp_str=fr.readline()
            fw.write(temp_str) 

        fr.close()
        fw.close()
    #====End of WRITE *.SNX

    def Yield_boxplot(self,sname, SNX_list, start_year, obs_flag, display):
        
        self.Wdir_path = self.__getWorkingDir()
        
        fname1 = self.Wdir_path + "\\"+ sname+"_output\\SUMMARY.OUT"
        pdate_list = []
        year_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(int(SNX_list[i][-11:-8]))
            if i == 0:
                year_list.append(int(start_year))
            else:
                if pdate_list[i] > pdate_list[i-1]:
                    year_list.append(int(start_year))
                else:
                    year_list.append(int(start_year) + 1)

        df_OUT=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
        PDAT = df_OUT.ix[:,14].values  #read 14th column only => Planting date 
        HWAM = df_OUT.ix[:,21].values  #read 21th column only
        ADAT = df_OUT.ix[:,16].values  #read 21th column only
        MDAT = df_OUT.ix[:,17].values  #read 21th column only

        DOY_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize
        ADAT_array = np.zeros(len(ADAT)) #np.empty((ndays,1))*np.nan #initialize
        MDAT_array = np.zeros(len(MDAT)) #np.empty((ndays,1))*np.nan #initialize
        
        for i in range(len(HWAM)):
           # year_array[i] = int(repr(PDAT[i])[:4])
            DOY_array[i] = int(repr(PDAT[i])[4:])
            #MDAT_array[i] = int(repr(MDAT[i])[4:])
            if ADAT[i] == 0:  #in case emergence does not happen
                ADAT_array[i] = np.nan
                MDAT_array[i] = np.nan
            elif ADAT[i] == -99:  #in case emergence does not happen
                ADAT_array[i] = np.nan
                MDAT_array[i] = np.nan
            elif MDAT[i] == -99: 
                MDAT_array[i] = np.nan
            else:
                ADAT_array[i] = int(repr(ADAT[i])[4:])
                MDAT_array[i] = int(repr(MDAT[i])[4:])

        #make three arrays into a dataframe
        df = pd.DataFrame({'DOY': DOY_array, 'ADAT':ADAT_array, 'MDAT':MDAT_array, 'HWAM':HWAM}, columns=['DOY','ADAT','MDAT','HWAM'])

        # #make an empty array [nyears * n_plt_dates]
        n_year = 100
        # For Boxplot
        yield_n = np.empty([n_year,len(pdate_list)])*np.nan
        ADAT_n = np.empty([n_year,len(pdate_list)])*np.nan
        MDAT_n = np.empty([n_year,len(pdate_list)])*np.nan
        # For exceedance curve
        sorted_yield_n = np.empty([n_year,len(pdate_list)])*np.nan
        Fx_scf = np.empty([n_year,len(pdate_list)])*np.nan       
        sorted_ADAT_n = np.empty([n_year,len(pdate_list)])*np.nan   
        Fx_scf_ADAT = np.empty([n_year,len(pdate_list)])*np.nan
        sorted_MDAT_n = np.empty([n_year,len(pdate_list)])*np.nan
        # Fx_scf_MDAT = np.empty([n_year,len(pdate_list)])*np.nan

        if obs_flag == 0:  #if there is no observed weather available 
            for count in range(len(pdate_list)):
                Pdate = pdate_list[count]
                yield_n[:,count] = df.HWAM[(df["DOY"] == Pdate)].values  
                ADAT_n[:,count] = df.ADAT[(df["DOY"] == Pdate)].values 
                MDAT_n[:,count] = df.MDAT[(df["DOY"] == Pdate)].values  
                sorted_yield_n[:,count] = np.sort(df.HWAM[(df["DOY"] == Pdate)].values)
                fx_scf = [1.0/len(sorted_yield_n)] * len(sorted_yield_n) #pdf
                #Fx_scf = np.cumsum(fx_scf)
                Fx_scf[:,count] = 1.0-np.cumsum(fx_scf)  #for exceedance curve
                sorted_ADAT_n[:,count] = np.sort(df.ADAT[(df["DOY"] == Pdate)].values)
                sorted_MDAT_n[:,count] = np.sort(df.MDAT[(df["DOY"] == Pdate)].values)
                temp = sorted_ADAT_n[:,count]
                temp2 = temp[~np.isnan(temp)]
                fx_scf_ADAT = [1.0/len(temp2)] * len(temp2) #pdf
                Fx_scf_ADAT[:len(fx_scf_ADAT),count] = 1.0-np.cumsum(fx_scf_ADAT)  #for exceedance curve         
        else:   
            obs_yield = []
            obs_ADAT = []
            obs_MDAT = []
            for count in range(len(pdate_list)):
                Pdate = pdate_list[count]
                yield_n[:,count] = df.HWAM[(df["DOY"] == Pdate)].values[:-1]  
                ADAT_n[:,count] = df.ADAT[(df["DOY"] == Pdate)].values[:-1] 
                MDAT_n[:,count] = df.MDAT[(df["DOY"] == Pdate)].values[:-1] 
                sorted_yield_n[:,count] = np.sort(df.HWAM[(df["DOY"] == Pdate)].values[:-1])
                fx_scf = [1.0/len(sorted_yield_n)] * len(sorted_yield_n) #pdf
                #Fx_scf = np.cumsum(fx_scf)
                Fx_scf[:,count] = 1.0-np.cumsum(fx_scf)  #for exceedance curve
                sorted_ADAT_n[:,count] = np.sort(df.ADAT[(df["DOY"] == Pdate)].values[:-1])
                sorted_MDAT_n[:,count] = np.sort(df.MDAT[(df["DOY"] == Pdate)].values[:-1])
                temp = sorted_ADAT_n[:,count]
                temp2 = temp[~np.isnan(temp)]
                fx_scf_ADAT = [1.0/len(temp2)] * len(temp2) #pdf
                Fx_scf_ADAT[:len(fx_scf_ADAT),count] = 1.0-np.cumsum(fx_scf_ADAT)  #for exceedance curve    
                #collect simulated results from observed weather
                obs_yield.append(df.HWAM[(df["DOY"] == Pdate)].values[-1])      
                obs_ADAT.append(df.ADAT[(df["DOY"] == Pdate)].values[-1])  
                obs_MDAT.append(df.MDAT[(df["DOY"] == Pdate)].values[-1])  
       # Fx_scf_ADAT = Fx_scf
        #To remove 'nan' from data array
        #https://stackoverflow.com/questions/44305873/how-to-deal-with-nan-value-when-plot-boxplot-using-python
        mask = ~np.isnan(yield_n)
        filtered_yield = [d[m] for d, m in zip(yield_n.T, mask.T)]
        mask2 = ~np.isnan(ADAT_n)
        filtered_data2 = [d[m] for d, m in zip(ADAT_n.T, mask2.T)]
        mask3 = ~np.isnan(MDAT_n)
        filtered_data3 = [d[m] for d, m in zip(MDAT_n.T, mask3.T)]
        
        if obs_flag == 1: 
            #replace ADAT = 0 with nan
            obs_ADAT = [np.nan if x==1 else x for x in obs_ADAT]
            obs_MDAT = [np.nan if x==1 else x for x in obs_MDAT]

        #X data for plot
        myXList=[i+1 for i in range(len(pdate_list))]
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            temp_str = repr(year_list[count]) + " " + repr(pdate_list[count]) 
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)
        # 1) Plotting Yield 
        fig = plt.figure()
        fig.suptitle('Estimated Yields with differnt planting dates', fontsize=14, fontweight='bold')
        ax = fig.add_subplot(111)
        ax.set_xlabel('Planting Date[MM/DD]',fontsize=14)
        ax.set_ylabel('Yield [kg/ha]',fontsize=14)
        if obs_flag == 1:
            # Plot a line between yields with observed weather
            plt.plot(myXList, obs_yield, 'go-')
        ax.boxplot(filtered_yield,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
        fig_name = self.Wdir_path + "\\"+ sname +"_output\\"+ sname +"_Yield_boxplot.png"
        plt.savefig(fig_name) 
        display.updateImgPath(0,0,fig_name) # Add the image pathname to the main CAMDT
    

        # 2) Plotting ADAT
        fig2 = plt.figure()
        fig2.suptitle('Estimated Anthesis Date with differnt planting dates', fontsize=14, fontweight='bold')
        ax2 = fig2.add_subplot(111)
        ax2.set_xlabel('Planting Date[DOY]',fontsize=14)
        ax2.set_ylabel('Anthesis Date [DOY]',fontsize=14)
        if obs_flag == 1:
            # Plot a line between yields with observed weather
            plt.plot(myXList, obs_ADAT, 'go-')
        ax2.boxplot(filtered_data2,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
        fig_name2 = self.Wdir_path + "\\"+ sname +"_output\\"+ sname +"_ADAT_boxplot.png"
        plt.savefig(fig_name2)   
        display.updateImgPath(0,1,fig_name2) # Add the image pathname to the main CAMDT        
            

        # 3) Plotting MDAT
        fig3 = plt.figure()
        fig3.suptitle('Estimated Maturity Date with differnt planting dates', fontsize=14, fontweight='bold')
        ax3 = fig3.add_subplot(111)
        ax3.set_xlabel('Planting Date[DOY]',fontsize=14)
        ax3.set_ylabel('Maturity Date [DOY]',fontsize=14)
        if obs_flag == 1:
            # Plot a line between yields with observed weather
            plt.plot(myXList, obs_MDAT, 'go-')
        ax3.boxplot(filtered_data3,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
        fig_name = self.Wdir_path + "\\"+ sname+"_output\\"+sname+"_MDAT_boxplot.png"
        plt.savefig(fig_name)
        display.updateImgPath(0,2,fig_name) # Add the image pathname to the main CAMDT      


        #4) Plotting yield exceedance curve
        fig4 = plt.figure()
        fig4.suptitle('Yield Exceedance Curve', fontsize=14, fontweight='bold')
        ax4 = fig4.add_subplot(111)
        ax4.set_xlabel('Yield [kg/ha]',fontsize=14)
        ax4.set_ylabel('Probability of Exceedance [-]',fontsize=14)
        plt.ylim(0, 1) 
        for x in range(len(pdate_list)):
            ax4.plot(sorted_yield_n[:,x],Fx_scf[:,x],'o-', label=scename[x])
            #vertical line for the yield with observed weather
            if obs_flag == 1:
                x_data=[obs_yield[x],obs_yield[x]] #only two points to draw a line
                y_data=[0,1]
                temp='w/ obs wth (' + scename[x] + ')'
                ax4.plot(x_data,y_data,'-.', label=temp)
                plt.ylim(0, 1)
        box = ax4.get_position()  # Shrink current axis by 15%
        ax4.set_position([box.x0, box.y0, box.width * 0.70, box.height])
        plt.grid(True)
        ax4.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
        fig_name = self.Wdir_path + "\\"+ sname +"_output\\" + sname+"_Yield_exceedance.png"
        plt.savefig(fig_name) 
        display.updateImgPath(0,3,fig_name) # Add the image pathname to the main CAMDT    
        

        #5) Plotting ADAT exceedance curve
        fig5 = plt.figure()
        fig5.suptitle('Anthesis Date Exceedance Curve', fontsize=14, fontweight='bold')
        ax5 = fig5.add_subplot(111)
        ax5.set_xlabel('Anthesis Date [DOY]',fontsize=14)
        ax5.set_ylabel('Probability of Exceedance [-]',fontsize=14)
        plt.ylim(0, 1) 
        for x in range(len(pdate_list)):
            ax5.plot(sorted_ADAT_n[:,x],Fx_scf_ADAT[:,x],'o-', label=scename[x])
            #vertical line for the yield with observed weather
            if obs_flag == 1:
                x_data=[obs_ADAT[x],obs_ADAT[x]] #only two points to draw a line
                y_data=[0,1]
                temp='w/ obs wth (' + scename[x] + ')'
                ax5.plot(x_data,y_data,'-.', label=temp)
                plt.ylim(0, 1)
        box = ax5.get_position()
        ax5.set_position([box.x0, box.y0, box.width * 0.70, box.height])
        plt.grid(True)
        ax5.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
        fig_name = self.Wdir_path + "\\"+ sname +"_output\\"+ sname +"_ADAT_exceedance.png"
        plt.savefig(fig_name)
        display.updateImgPath(0,4,fig_name) # Add the image pathname to the main CAMDT    
        

        #6) Plotting MDAT exceedance curve
        fig6 = plt.figure()
        fig6.suptitle('Maturity Date Exceedance Curve', fontsize=14, fontweight='bold')
        ax6 = fig6.add_subplot(111)
        ax6.set_xlabel('Maturity Date [DOY]',fontsize=14)
        ax6.set_ylabel('Probability of Exceedance [-]',fontsize=14)
        plt.ylim(0, 1) 
        for x in range(len(pdate_list)):
            ax6.plot(sorted_MDAT_n[:,x],Fx_scf_ADAT[:,x],'o-', label=scename[x])
            #vertical line for the yield with observed weather
            if obs_flag == 1:
                x_data=[obs_MDAT[x],obs_MDAT[x]] #only two points to draw a line
                y_data=[0,1]
                temp='w/ obs wth (' + scename[x] + ')'
                ax6.plot(x_data,y_data,'-.', label=temp)
                plt.ylim(0, 1)
        box = ax6.get_position()
        ax6.set_position([box.x0, box.y0, box.width * 0.70, box.height])
        plt.grid(True)
        ax6.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
        fig_name = self.Wdir_path + "\\"+ sname +"_output\\"+ sname +"_MDAT_exceedance.png"
        plt.savefig(fig_name)
        display.updateImgPath(0,5,fig_name) # Add the image pathname to the main CAMDT


    ##############################################################
    def WSGD_plot(self,sname, SNX_list, start_year, obs_flag, display):

        self.Wdir_path = self.__getWorkingDir()

        fname1 = self.Wdir_path + "\\"+ sname+"_output\\PlantGro.OUT"
        pdate_list = []
        # year_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(SNX_list[i][-11:-8])     
        if obs_flag == 1:
            nrealiz = 101
        else:
            nrealiz = 100
        n_days = 180 #approximate growing days
        WSGD_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
        NSTD_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN 
        read_flag = 0  #off
        n_count = 0  #count of n weahter realizations
        d_count = 0  #day counter during a growing season
        p_index_old = 0
        p_count = 0  #count for different planting date scenario
        with open(fname1) as fp:
            for line in fp:
                if line[18:21] == '  0':   #line[18:21] => DAP
                    p_index = pdate_list.index(line[6:9])   #line[6:9] => DOY
                    read_flag = 1  #on
                    if p_index > p_index_old:  #move to a new planting date scenario
                        p_count = p_count + 1
                        n_count = 0
                #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
                if read_flag ==1 and line == '\n':  #after reading last growing day
                    read_flag = 0 #off
                    p_index_old = p_index
                    d_count = 0
                    n_count = n_count + 1
                if read_flag == 1:
                    WSGD_3D[d_count, n_count, p_count] = float(line[127:133])
                    NSTD_3D[d_count, n_count, p_count] = float(line[134:140])
                    d_count = d_count + 1

        # WSGD_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
        # NSTD_avg = np.empty((n_days,len(pdate_list),))*np.NAN  
        WSGD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
        WSGD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        WSGD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        NSTD_50 = np.empty((n_days,len(pdate_list),))*np.NAN  
        NSTD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        NSTD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        if obs_flag == 1:
            WSGD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
            NSTD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
        
        #plot
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            if count > 0:
                if int(pdate_list[count]) < int(pdate_list[count-1]):
                    start_year = repr(int(start_year) + 1)
            temp_str = str(start_year) + " " + pdate_list[count]
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)

        for i in range(len(pdate_list)):
            # WSGD_avg[:, i] = np.nanmean(WSGD_3D[:, 0:100, i], axis = 1)
            # NSTD_avg[:, i] = np.nanmean(WSGD_3D[:, 0:100, i], axis = 1)
            WSGD_50[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 50, axis = 1)
            WSGD_25[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 25, axis = 1)
            WSGD_75[:, i] = np.nanpercentile(WSGD_3D[:, 0:100, i], 75, axis = 1)
            NSTD_50[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 50, axis = 1)
            NSTD_25[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 25, axis = 1)
            NSTD_75[:, i] = np.nanpercentile(NSTD_3D[:, 0:100, i], 75, axis = 1)
            if obs_flag == 1:
                WSGD_obs[:, i] = WSGD_3D[:, -1, i]
                NSTD_obs[:, i] = NSTD_3D[:, -1, i]
        #================================== 
        #1) Plot WSGD Water stress
        nrow = math.ceil(len(pdate_list)/2.0)
        ncol = 2
        #fig, axs = plt.subplots(nrow, ncol)
        fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
        fig.suptitle('WSGD (Water Stress Index)')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    xdata = range(len(WSGD_50[:,0]))
                    axs[j, i].plot(xdata, WSGD_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(WSGD_50[:,p_count]))
                    yerr_low = WSGD_50[:, p_count] -WSGD_25[:, p_count]
                    yerr_up = WSGD_75[:, p_count] - WSGD_50[:, p_count]
                    axs[j, i].errorbar(xdata, WSGD_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, WSGD_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(WSGD_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='Water Stress Index [-]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(13, 8)
        fig_name = self.Wdir_path + "\\"+ sname +"_output\\" + sname +"_WStress.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        display.updateImgPath(3,0,fig_name) # Add the image pathname to the main CAMDT

        #==================================

        #2) Plot NSTD Nitrogen stress
        nrow = math.ceil(len(pdate_list)/2.0)
        ncol = 2
        #fig, axs = plt.subplots(nrow, ncol)
        fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
        fig.suptitle('NSTD (Nitrogen Stress Index)')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    xdata = range(len(NSTD_50[:,0]))
                    axs[j, i].plot(xdata, NSTD_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(NSTD_50[:,p_count]))
                    yerr_low = NSTD_50[:, p_count] -NSTD_25[:, p_count]
                    yerr_up = NSTD_75[:, p_count] - NSTD_50[:, p_count]
                    axs[j, i].errorbar(xdata, NSTD_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, NSTD_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(NSTD_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='Nitrogen Stress Index [-]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = self.Wdir_path + "\\"+ sname +"_output\\" + sname +"_NStress.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        display.updateImgPath(3,1,fig_name) # Add the image pathname to the main CAMDT


    def weather_plot(self,sname, SNX_list, start_year, obs_flag, display):

        self.Wdir_path = self.__getWorkingDir()
        # TMND   MIN TEMP °C     Minimum daily temperature (°C)                           .
        # TMXD   MAX TEMP °C     Maximum daily temperature (°C) 
        # PRED   PRECIP mm/d     Precipitation depth (mm/d) ==>> ??????                        .
        # SRAD   SRAD MJ/m2.d    Solar radiation (MJ/(m2.d))                              .
        # TAVD   AVG TEMP °C     Average daily temperature (°C)
        fname1 = self.Wdir_path + "\\"+ sname+"_output\\Weather.OUT"
        pdate_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(SNX_list[i][-11:-8])

        if obs_flag == 1:
            nrealiz = 101
        else:
            nrealiz = 100
        n_days = 180 #approximate growing days
        TMXD_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
        TMND_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN 
        SRAD_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN 
        read_flag = 0  #off
        n_count = 0  #count of n weahter realizations
        d_count = 0  #day counter during a growing season
        p_count = 0  #count for different planting date scenario
        with open(fname1) as fp:
            for line in fp:
                if line[6:9] == pdate_list[p_count]:   #line[12:15] => DAS
                    read_flag = 1  #on
                if read_flag ==1 and line == '\n':  #after reading last growing day
                    read_flag = 0 #off
                    d_count = 0
                    n_count = n_count + 1
                    if n_count == nrealiz:  #move to next planting date scenario
                        p_count = p_count + 1
                        n_count = 0
                if read_flag == 1:
                    TMXD_3D[d_count, n_count, p_count] = float(line[60:64])
                    TMND_3D[d_count, n_count, p_count] = float(line[67:71])
                    SRAD_3D[d_count, n_count, p_count] = float(line[39:43])
                    d_count = d_count + 1

        # TMXD_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
        # TMND_avg = np.empty((n_days,len(pdate_list),))*np.NAN  
        TMXD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMXD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMXD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMND_50 = np.empty((n_days,len(pdate_list),))*np.NAN  
        TMND_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMND_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        SRAD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
        SRAD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        SRAD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        if obs_flag == 1:
            TMXD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
            TMND_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
            SRAD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
    
        #plot
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            if count > 0:
                if int(pdate_list[count]) < int(pdate_list[count-1]):
                    start_year = repr(int(start_year) + 1)
            temp_str = str(start_year) + " " + pdate_list[count]
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)

        for i in range(len(pdate_list)):
            TMXD_50[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 50, axis = 1)
            TMXD_25[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 25, axis = 1)
            TMXD_75[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 75, axis = 1)
            TMND_50[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 50, axis = 1)
            TMND_25[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 25, axis = 1)
            TMND_75[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 75, axis = 1)
            SRAD_50[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 50, axis = 1)
            SRAD_25[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 25, axis = 1)
            SRAD_75[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 75, axis = 1)
            if obs_flag == 1:
                TMXD_obs[:, i] = TMXD_3D[:, -1, i]
                TMND_obs[:, i] = TMND_3D[:, -1, i]
                SRAD_obs[:, i] = SRAD_3D[:, -1, i]
        #================================== 
        #1) Plot TMXD
        nrow = math.ceil(len(pdate_list)/2.0)
        ncol = 2
        # fig, axs = plt.subplots(nrow, ncol)
        fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
        fig.suptitle('TMXD (Maximum daily temperature)')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    xdata = range(len(TMXD_50[:,0]))
                    axs[j, i].plot(xdata, TMXD_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(TMXD_50[:,p_count]))
                    yerr_low = TMXD_50[:, p_count] -TMXD_25[:, p_count]
                    yerr_up = TMXD_75[:, p_count] - TMXD_50[:, p_count]
                    axs[j, i].errorbar(xdata, TMXD_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, TMXD_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(TMXD_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='Temperature[C]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = self.Wdir_path + "\\"+ sname +"_output\\" + sname +"_Tmax.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        display.updateImgPath(4,0,fig_name) # Add the image pathname to the main CAMDT

        #==================================
        #2) Plot TMND
        # fig, axs = plt.subplots(nrow, ncol)
        fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
        fig.suptitle('TMND (Minimum daily temperature)')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    axs[j, i].plot(xdata, TMND_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(TMND_50[:,p_count]))
                    yerr_low = TMND_50[:, p_count] -TMND_25[:, p_count]
                    yerr_up = TMND_75[:, p_count] - TMND_50[:, p_count]
                    axs[j, i].errorbar(xdata, TMND_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, TMND_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(TMND_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='Temperature[C]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = self.Wdir_path + "\\"+ sname +"_output\\" + sname +"_Tmin.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        display.updateImgPath(4,1,fig_name) # Add the image pathname to the main CAMDT
        
        #==================================
        #3) Plot SRAD   SRAD MJ/m2.d    Solar radiation (MJ/(m2.d)) 
        # fig, axs = plt.subplots(nrow, ncol)
        fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
        fig.suptitle('SARD (Daily solar radiation (MJ/(m2.d))')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    axs[j, i].plot(xdata, SRAD_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(SRAD_50[:,p_count]))
                    yerr_low = SRAD_50[:, p_count] -SRAD_25[:, p_count]
                    yerr_up = SRAD_75[:, p_count] - SRAD_50[:, p_count]
                    axs[j, i].errorbar(xdata, SRAD_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, SRAD_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(SRAD_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='SRAD[MJ/(m2.d)]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = self.Wdir_path + "\\"+ sname +"_output\\" + sname +"_SRAD.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        display.updateImgPath(4,2,fig_name) # Add the image pathname to the main CAMDT

    ##############################################################
    def plot_cum_rain(self,sname, SNX_list, start_year, obs_flag, display):

        self.Wdir_path = self.__getWorkingDir()
        fname1 = self.Wdir_path + "\\"+ sname+"_output\\SoilWat.OUT"
        pdate_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(SNX_list[i][-11:-8])

        if obs_flag == 1:
            nrealiz = 101
        else:
            nrealiz = 100
        n_days = 180 + 30 #approximate growing days + 30 days from IC to plt date
        PREC_3D = np.empty((n_days,nrealiz,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]

        read_flag = 0  #off
        n_count = 0  #count of n weahter realizations
        d_count = 0  #day counter during a growing season
        p_count = 0  #count for different planting date scenario
        with open(fname1) as fp:
            for line in fp:
                if line[12:15] == '  0':   #line[12:15] => DAS
                    read_flag = 1  #on
                if read_flag ==1 and line == '\n':  #after reading last growing day
                    read_flag = 0 #off
                    d_count = 0
                    n_count = n_count + 1
                    if n_count == nrealiz:  #move to next planting date scenario
                        p_count = p_count + 1
                        n_count = 0
                if read_flag == 1:
                    PREC_3D[d_count, n_count, p_count] = float(line[44:48])
                    d_count = d_count + 1

        # PREC_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
        PREC_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
        PREC_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        PREC_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        if obs_flag == 1:
            PREC_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
    
        #plot
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            if count > 0:
                if int(pdate_list[count]) < int(pdate_list[count-1]):
                    start_year = repr(int(start_year) + 1)
            temp_str = str(start_year) + " " + pdate_list[count]
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)

        for i in range(len(pdate_list)):
            PREC_50[:, i] = np.nanpercentile(PREC_3D[:, 0:100, i], 50, axis = 1)
            PREC_25[:, i] = np.nanpercentile(PREC_3D[:, 0:100, i], 25, axis = 1)
            PREC_75[:, i] = np.nanpercentile(PREC_3D[:, 0:100, i], 75, axis = 1)
            if obs_flag == 1:
                PREC_obs[:, i] = PREC_3D[:, -1, i]
        #================================== 
        #1) Plot PREC
        nrow = math.ceil(len(pdate_list)/2.0)
        ncol = 2
        # fig, axs = plt.subplots(nrow, ncol)
        fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
        fig.suptitle('Cumulative Precipitation')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    xdata = range(len(PREC_50[:,0]))
                    axs[j, i].plot(xdata, PREC_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(PREC_50[:,p_count]))
                    yerr_low = PREC_50[:, p_count] -PREC_25[:, p_count]
                    yerr_up = PREC_75[:, p_count] - PREC_50[:, p_count]
                    axs[j, i].errorbar(xdata, PREC_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, PREC_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(PREC_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After SIMULATION [DAS]', ylabel='Cum. Rainfall[mm]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = self.Wdir_path + "\\"+ sname +"_output\\" + sname +"_CumRain.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        display.updateImgPath(4,3,fig_name) # Add the image pathname to the main CAMDT


    ######################################################################
    # For historical runs
    ######################################################################

    ######################################################################
    def writeSNX1_DSSAT_hist(self):
        
        self.Wdir_path = self.__getWorkingDir()
        if self._scName1.value == "":
            sname = "TST1" 
        elif self._scName1.value in [' ', "TST1"]:
            sname = "TST1" 
        else:
            sname = self._scName1.value[:4]

        setup1Values = self._setup1.Setup1Results() # All inputs from Setup1 are stored here 
        

        WSTA = setup1Values[0][:4] #'SANJ'
        WTD_fname=self.Wdir_path.replace("/","\\") + "\\"+ WSTA + ".WTD"
        WTD_last_date = self.find_obs_date(WTD_fname)

        start_year = setup1Values[1] #'2002'
        obs_flag = 0  #by default, assumes no obs weather data is available
        #=============================================
        #Determine pdate_1[YYYYDOY] and pdate_2[YYYYDOY]
        lst = self.setup1Results[13] #setup1Values[13]  #Year 1 for planting
        lst2 = self.setup1Results[14] #setup1Values[14]   #Year 2 for planting
        lst_frst = self.setup1Results[11] #setup1Values[11]  #Year 1 for SCF
        lst_frst2 = self.setup1Results[12] #setup1Values[12]  #Year 2 for SCF
        pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m = self.find_plt_date (start_year, lst, lst2, lst_frst, lst_frst2)

        #find simulation start date
        temp=int(pdate_1%1000)-30  #initial condition: 30 day before planting  #EJ(3/13/2019)
        if temp <= 0 :   #EJ(3/13/2019)
            if calendar.isleap(int(start_year)-1):  
                temp = 366 + temp
            else:
                temp = 365 + temp
            IC_date = (pdate_1//1000 -1)*1000+temp
        else:
            IC_date = (pdate_1//1000)*1000+temp
        #Determines how many crop growing seasons are available from the historical observed.
        sim_years, obs_year1 = self.find_obs_years(WTD_fname, IC_date, hv_date) # <=======********

        if WTD_last_date >= hv_date: #180 is arbitrary number to fully cover weather data until late maturity
            obs_flag = 1
        step = int(self._scSteps1.value) #5 days distance for visualizing boxplots
        SNX_list = []
        if pdate_2%1000 > pdate_1%1000:
            for iday in range(pdate_1,pdate_2+1,step):   
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main_hist(SNX_fname,sim_years,obs_year1, iday)  
                SNX_list.append(SNX_fname)
        else: #if planting window covers two consecutive years
            #go through Year 1
            end_day = int(start_year)*1000 + 365
            if calendar.isleap(int(start_year)):  end_day = int(start_year)*1000 + 366
            for iday in range(pdate_1,end_day + 1,step):   
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main_hist(SNX_fname,sim_years,obs_year1,iday)    
                SNX_list.append(SNX_fname) 
                temp_date = iday
            #next, go throught Year 2
            start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 365  #e.g, 364 + 5 - 365 = DOY 4
            if calendar.isleap(int(start_year)):  start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 366
            for iday in range(start_day,pdate_2 + 1,step):   #e.g, 2018004 to 2018030
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main_hist(SNX_fname,sim_years,obs_year1,iday)     
                SNX_list.append(SNX_fname)   
        #=============================================
        # Create DSSBatch.v47 file and run DSSAT executable 
        #=============================================
        entries = ("PlantGro.OUT","Evaluate.OUT", "ET.OUT","OVERVIEW.OUT","PlantN.OUT","SoilNi.OUT","Weather.OUT", 
                "SolNBalSum.OUT","SoilNoBal.OUT","SoilTemp.OUT","SoilWat.OUT","SoilWatBal.OUT","Summary.OUT") 
        self.writeV47_main_hist(SNX_list, obs_flag)   ##write *.V47 batch file
        #==RUN DSSAT with ARGUMENT 
        start_time = time.perf_counter() # => not compatible with Python 2.7
        os.chdir(self.Wdir_path)  #change directory
        args = "DSCSM047.EXE N DSSBatch.v47 "
        subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
        end_time0 = time.perf_counter() # => not compatible with Python 2.7
        print('It took {0:5.1f} sec to finish n years of obs weather x n planting dates'.format(end_time0-start_time))
        start_time1 = time.perf_counter()  # => not compatible with Python 2.7

        #create a new folder to save outputs of the target scenario
        new_folder=self._scName1.value+"_output_hist"
        if os.path.exists(new_folder):
            shutil.rmtree(new_folder)   #remove existing folder
        os.makedirs(new_folder)
        #copy outputs to the new folder
        dest_dir=self.Wdir_path + "\\"+new_folder
        # print 'dest_dir is', dest_dir
        for entry in entries:
            source_file=self.Wdir_path + "\\" + entry
            if os.path.isfile(source_file):
                shutil.move(source_file, dest_dir)
            else:
                print( '**Error!!- No DSSAT output files => Check if DSSAT simuation was successful')

        #move *.SNX file to the new folder with output files
        for i in range(len(SNX_list)):
            shutil.move(SNX_list[i], dest_dir)
        #=============================================
        # Make a boxplot and exceedance curve of yield and other output
        #=============================================
        self.Yield_boxplot_hist(sname, SNX_list, start_year, sim_years, obs_flag, self._display)
        #=============================================
        # Make plots of two yield boxplots(forecast-based vs. historical)
        #=============================================
        
        self.lst1Cmp.append(sname)
        self.lst1Cmp.append(SNX_list)
        self.lst1Cmp.append(start_year)
        self.lst1Cmp.append(sim_years)
        self.lst1Cmp.append(obs_flag)

        #self.Yield_boxplot_compare(sname, SNX_list, start_year, sim_years,obs_flag, self._display, 1)
        #=============================================
        # Write tercile yield forecast
        #=============================================
        #self.Yield_tercile_forecast(sname, SNX_list, start_year, sim_years,obs_flag, self._display)
        #=============================================
            
        # Make plots of water/nitrogen stresss
        #=============================================
        self.WSGD_plot_hist(sname, SNX_list, start_year, sim_years,obs_flag, self._display)
        #=============================================
        # Make plots of weather time series (Tmin, Tmax, Srad)
        #=============================================
        self.weather_plot_hist(sname, SNX_list, start_year, sim_years,obs_flag, self._display)
        #=============================================
        # Make plots of cumulative rainfall time series
        #=============================================
        self.plot_cum_rain_hist(sname, SNX_list, start_year, sim_years,obs_flag, self._display)
        print('========================================================================')
        print('It took {0:5.1f} sec to finish 100 simulations & to post-process outputs'.format(end_time0-start_time))
        print ("** Yeah!! DSSAT Simulation has been finished successfully!")
        print ("** Please check all output and figues in your working directory.")
        print('========================================================================')
    ######################################################################

    ######################################################################
    def writeSNX2_DSSAT_hist(self):
        
        self.Wdir_path = self.__getWorkingDir()
        
        if self._scName2.value == "":
            sname = "TST1" 
        elif self._scName2.value in [' ', "TST1"]:
            sname = "TST1" 
        else:
            sname = self._scName2.value[:4]

        setup1Values = self._setup1.Setup1Results() # All inputs from Setup1 are stored here
        

        WSTA = setup1Values[0][:4] #'SANJ'
        WTD_fname=self.Wdir_path.replace("/","\\") + "\\"+ WSTA + ".WTD"
        WTD_last_date = self.find_obs_date (WTD_fname)

        start_year=setup1Values[1] #'2002'
        obs_flag = 0  #by default, assumes no obs weather data is available
        #=============================================
        #Determine pdate_1[YYYYDOY] and pdate_2[YYYYDOY]
        lst = setup1Values[13]  #Year 1 for planting
        lst2 = setup1Values[14]   #Year 2 for planting
        lst_frst = setup1Values[11]  #Year 1 for SCF
        lst_frst2 = setup1Values[12]  #Year 2 for SCF
        pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m = self.find_plt_date (start_year, lst, lst2, lst_frst, lst_frst2)

        #find simulation start date
        temp=int(pdate_1%1000)-30  #initial condition: 30 day before planting  #EJ(3/13/2019)
        if temp <= 0 :   #EJ(3/13/2019)
            if calendar.isleap(int(start_year)-1):  
                temp = 366 + temp
            else:
                temp = 365 + temp
            IC_date = (pdate_1//1000 -1)*1000+temp
        else:
            IC_date = (pdate_1//1000)*1000+temp
        #Determines how many crop growing seasons are available from the historical observed.
        sim_years, obs_year1 = self.find_obs_years(WTD_fname, IC_date, hv_date) # <=======********

        if WTD_last_date >= hv_date: #180 is arbitrary number to fully cover weather data until late maturity
            obs_flag = 1
        step = int(self._scSteps2.value) #5 days distance for visualizing boxplots
        SNX_list = []
        if pdate_2%1000 > pdate_1%1000:
            for iday in range(pdate_1,pdate_2+1,step):   
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main_hist(SNX_fname,sim_years,obs_year1, iday)  
                SNX_list.append(SNX_fname)
        else: #if planting window covers two consecutive years
            #go through Year 1
            end_day = int(start_year)*1000 + 365
            if calendar.isleap(int(start_year)):  end_day = int(start_year)*1000 + 366
            for iday in range(pdate_1,end_day + 1,step):   
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main_hist(SNX_fname,sim_years,obs_year1,iday)    
                SNX_list.append(SNX_fname) 
                temp_date = iday
            #next, go throught Year 2
            start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 365  #e.g, 364 + 5 - 365 = DOY 4
            if calendar.isleap(int(start_year)):  start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 366
            for iday in range(start_day,pdate_2 + 1,step):   #e.g, 2018004 to 2018030
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main_hist(SNX_fname,sim_years,obs_year1,iday)     
                SNX_list.append(SNX_fname)   
        #=============================================
        # Create DSSBatch.v47 file and run DSSAT executable 
        #=============================================
        entries = ("PlantGro.OUT","Evaluate.OUT", "ET.OUT","OVERVIEW.OUT","PlantN.OUT","SoilNi.OUT","Weather.OUT", 
                "SolNBalSum.OUT","SoilNoBal.OUT","SoilTemp.OUT","SoilWat.OUT","SoilWatBal.OUT","Summary.OUT") 
        self.writeV47_main_hist(SNX_list, obs_flag)   ##write *.V47 batch file
        #==RUN DSSAT with ARGUMENT 
        start_time = time.perf_counter() # => not compatible with Python 2.7
        os.chdir(self.Wdir_path)  #change directory
        args = "DSCSM047.EXE N DSSBatch.v47 "
        subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
        end_time0 = time.perf_counter() # => not compatible with Python 2.7
        print('It took {0:5.1f} sec to finish n years of obs weather x n planting dates'.format(end_time0-start_time))
        start_time1 = time.perf_counter()  # => not compatible with Python 2.7

        #create a new folder to save outputs of the target scenario
        new_folder=self._scName2.value+"_output_hist"
        if os.path.exists(new_folder):
            shutil.rmtree(new_folder)   #remove existing folder
        os.makedirs(new_folder)
        #copy outputs to the new folder
        dest_dir=self.Wdir_path + "\\"+new_folder
        # print 'dest_dir is', dest_dir
        for entry in entries:
            source_file=self.Wdir_path + "\\" + entry
            if os.path.isfile(source_file):
                shutil.move(source_file, dest_dir)
            else:
                print( '**Error!!- No DSSAT output files => Check if DSSAT simuation was successful')

        #move *.SNX file to the new folder with output files
        for i in range(len(SNX_list)):
            shutil.move(SNX_list[i], dest_dir)
        #=============================================
        # Make a boxplot and exceedance curve of yield and other output
        #=============================================
        self.Yield_boxplot_hist(sname, SNX_list, start_year, sim_years, obs_flag, self._display2)
        #=============================================
        # Make plots of two yield boxplots(forecast-based vs. historical)
        #=============================================
        self.lst2Cmp.append(sname)
        self.lst2Cmp.append(SNX_list)
        self.lst2Cmp.append(start_year)
        self.lst2Cmp.append(sim_years)
        self.lst2Cmp.append(obs_flag)

        # self.Yield_boxplot_compare(sname, SNX_list, start_year, sim_years,obs_flag, self._display2, 2)
        #=============================================
        # Write tercile yield forecast
        #=============================================
        # self.Yield_tercile_forecast(sname, SNX_list, start_year, sim_years,obs_flag, self._display2)
        #=============================================
       
        # Make plots of water/nitrogen stresss
        #=============================================
        self.WSGD_plot_hist(sname, SNX_list, start_year, sim_years,obs_flag, self._display2)
        #=============================================
        # Make plots of weather time series (Tmin, Tmax, Srad)
        #=============================================
        self.weather_plot_hist(sname, SNX_list, start_year, sim_years,obs_flag, self._display2)
        #=============================================
        # Make plots of cumulative rainfall time series
        #=============================================
        self.plot_cum_rain_hist(sname, SNX_list, start_year, sim_years,obs_flag, self._display2)
        print('========================================================================')
        print('It took {0:5.1f} sec to finish 100 simulations & to post-process outputs'.format(end_time0-start_time))
        print ("** Yeah!! DSSAT Simulation has been finished successfully!")
        print ("** Please check all output and figues in your working directory.")
        print('========================================================================')
    ######################################################################


    ######################################################################
    def writeSNX3_DSSAT_hist(self):
        
        self.Wdir_path = self.__getWorkingDir()
        if self._scName3.value == "":
            sname = "TST1" 
        elif self._scName3.value in [' ', "TST1"]:
            sname = "TST1" 
        else:
            sname = self._scName3.value[:4]

        setup1Values = self._setup1.Setup1Results() # All inputs from Setup1 are stored here
        

        WSTA = setup1Values[0][:4] #'SANJ'
        WTD_fname=self.Wdir_path.replace("/","\\") + "\\"+ WSTA + ".WTD"
        WTD_last_date = self.find_obs_date(WTD_fname)

        start_year=setup1Values[1] #'2002'
        obs_flag = 0  #by default, assumes no obs weather data is available
        #=============================================
        #Determine pdate_1[YYYYDOY] and pdate_2[YYYYDOY]
        lst = setup1Values[13]  #Year 1 for planting
        lst2 = setup1Values[14]   #Year 2 for planting
        lst_frst = setup1Values[11]  #Year 1 for SCF
        lst_frst2 = setup1Values[12]  #Year 2 for SCF
        pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m = self.find_plt_date (start_year, lst, lst2, lst_frst, lst_frst2)

        #find simulation start date
        temp=int(pdate_1%1000)-30  #initial condition: 30 day before planting  #EJ(3/13/2019)
        if temp <= 0 :   #EJ(3/13/2019)
            if calendar.isleap(int(start_year)-1):  
                temp = 366 + temp
            else:
                temp = 365 + temp
            IC_date = (pdate_1//1000 -1)*1000+temp
        else:
            IC_date = (pdate_1//1000)*1000+temp
        #Determines how many crop growing seasons are available from the historical observed.
        sim_years, obs_year1 = self.find_obs_years(WTD_fname, IC_date, hv_date) # <=======********

        if WTD_last_date >= hv_date: #180 is arbitrary number to fully cover weather data until late maturity
            obs_flag = 1
        step = int(self._scSteps3.value) #5 days distance for visualizing boxplots
        SNX_list = []
        if pdate_2%1000 > pdate_1%1000:
            for iday in range(pdate_1,pdate_2+1,step):   
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main_hist(SNX_fname,sim_years,obs_year1, iday)  
                SNX_list.append(SNX_fname)
        else: #if planting window covers two consecutive years
            #go through Year 1
            end_day = int(start_year)*1000 + 365
            if calendar.isleap(int(start_year)):  end_day = int(start_year)*1000 + 366
            for iday in range(pdate_1,end_day + 1,step):   
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main_hist(SNX_fname,sim_years,obs_year1,iday)    
                SNX_list.append(SNX_fname) 
                temp_date = iday
            #next, go throught Year 2
            start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 365  #e.g, 364 + 5 - 365 = DOY 4
            if calendar.isleap(int(start_year)):  start_day = (int(start_year)+1)*1000 + temp_date%1000 + step - 366
            for iday in range(start_day,pdate_2 + 1,step):   #e.g, 2018004 to 2018030
                SNX_fname=self.Wdir_path.replace("/","\\") + "\\H"+repr(iday%1000).zfill(3)+sname+".SNX"
                self.writeSNX_main_hist(SNX_fname,sim_years,obs_year1,iday)     
                SNX_list.append(SNX_fname)   
        #=============================================
        # Create DSSBatch.v47 file and run DSSAT executable 
        #=============================================
        entries = ("PlantGro.OUT","Evaluate.OUT", "ET.OUT","OVERVIEW.OUT","PlantN.OUT","SoilNi.OUT","Weather.OUT", 
                "SolNBalSum.OUT","SoilNoBal.OUT","SoilTemp.OUT","SoilWat.OUT","SoilWatBal.OUT","Summary.OUT") 
        self.writeV47_main_hist(SNX_list, obs_flag)   ##write *.V47 batch file
        #==RUN DSSAT with ARGUMENT 
        start_time = time.perf_counter() # => not compatible with Python 2.7
        os.chdir(self.Wdir_path)  #change directory
        args = "DSCSM047.EXE N DSSBatch.v47 "
        subprocess.call(args) ##Run executable with argument  , stdout=FNULL, stderr=FNULL, shell=False)
        end_time0 = time.perf_counter() # => not compatible with Python 2.7
        print('It took {0:5.1f} sec to finish n years of obs weather x n planting dates'.format(end_time0-start_time))
        start_time1 = time.perf_counter()  # => not compatible with Python 2.7

        #create a new folder to save outputs of the target scenario
        new_folder=self._scName3.value+"_output_hist"
        if os.path.exists(new_folder):
            shutil.rmtree(new_folder)   #remove existing folder
        os.makedirs(new_folder)
        #copy outputs to the new folder
        dest_dir=self.Wdir_path + "\\"+new_folder
        # print 'dest_dir is', dest_dir
        for entry in entries:
            source_file=self.Wdir_path + "\\" + entry
            if os.path.isfile(source_file):
                shutil.move(source_file, dest_dir)
            else:
                print( '**Error!!- No DSSAT output files => Check if DSSAT simuation was successful')

        #move *.SNX file to the new folder with output files
        for i in range(len(SNX_list)):
            shutil.move(SNX_list[i], dest_dir)
        #=============================================
        # Make a boxplot and exceedance curve of yield and other output
        #=============================================
        self.Yield_boxplot_hist(sname, SNX_list, start_year, sim_years, obs_flag, self._display3)
        #=============================================
        # Make plots of two yield boxplots(forecast-based vs. historical)
        #=============================================
        self.lst3Cmp.append(sname)
        self.lst3Cmp.append(SNX_list)
        self.lst3Cmp.append(start_year)
        self.lst3Cmp.append(sim_years)
        self.lst3Cmp.append(obs_flag)

        # self.Yield_boxplot_compare(sname, SNX_list, start_year, sim_years,obs_flag, self._display3, 3)
        #=============================================
        # Write tercile yield forecast
        #=============================================
        # self.Yield_tercile_forecast(sname, SNX_list, start_year, sim_years,obs_flag, self._display3)
        #=============================================
       
        # Make plots of water/nitrogen stresss
        #=============================================
        self.WSGD_plot_hist(sname, SNX_list, start_year, sim_years,obs_flag, self._display3)
        #=============================================
        # Make plots of weather time series (Tmin, Tmax, Srad)
        #=============================================
        self.weather_plot_hist(sname, SNX_list, start_year, sim_years,obs_flag, self._display3)
        #=============================================
        # Make plots of cumulative rainfall time series
        #=============================================
        self.plot_cum_rain_hist(sname, SNX_list, start_year, sim_years,obs_flag, self._display3)
        print('========================================================================')
        print('It took {0:5.1f} sec to finish 100 simulations & to post-process outputs'.format(end_time0-start_time))
        print ("** Yeah!! DSSAT Simulation has been finished successfully!")
        print ("** Please check all output and figues in your working directory.")
        print('========================================================================')
    ######################################################################

    ##############################################################
    def Yield_boxplot_hist(self,sname, SNX_list, start_year, sim_years, obs_flag, display):
        
        self.Wdir_path = self.__getWorkingDir()

        fname1 = self.Wdir_path + "\\"+ sname+"_output_hist\\SUMMARY.OUT"
        pdate_list = []
        year_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(int(SNX_list[i][-11:-8]))
            if i == 0:
                year_list.append(int(start_year))
            else:
                if pdate_list[i] > pdate_list[i-1]:
                    year_list.append(int(start_year))
                else:
                    year_list.append(int(start_year) + 1)

        df_OUT=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
        PDAT = df_OUT.ix[:,14].values  #read 14th column only => Planting date 
        HWAM = df_OUT.ix[:,21].values  #read 21th column only
        ADAT = df_OUT.ix[:,16].values  #read 21th column only
        MDAT = df_OUT.ix[:,17].values  #read 21th column only

        year_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize
        DOY_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize
        ADAT_array = np.zeros(len(ADAT)) #np.empty((ndays,1))*np.nan #initialize
        MDAT_array = np.zeros(len(MDAT)) #np.empty((ndays,1))*np.nan #initialize
        for i in range(len(HWAM)):
            year_array[i] = int(repr(PDAT[i])[:4])
            DOY_array[i] = int(repr(PDAT[i])[4:])
            #MDAT_array[i] = int(repr(MDAT[i])[4:])
            if ADAT[i] == 0:  #in case emergence does not happen
                ADAT_array[i] = np.nan
                MDAT_array[i] = np.nan
            elif ADAT[i] == -99:  #in case emergence does not happen
                ADAT_array[i] = np.nan
                MDAT_array[i] = np.nan
            elif MDAT[i] == -99: 
                MDAT_array[i] = np.nan
            else:
                ADAT_array[i] = int(repr(ADAT[i])[4:])
                MDAT_array[i] = int(repr(MDAT[i])[4:])
                
        #make three arrays into a dataframe
        df = pd.DataFrame({'Year': year_array,'DOY': DOY_array, 'ADAT':ADAT_array, 'MDAT':MDAT_array, 'HWAM':HWAM}, columns=['Year','DOY','ADAT','MDAT','HWAM'])

        # #make an empty array [nyears * n_plt_dates]
        n_year = sim_years
        # For Boxplot
        yield_n = np.empty([n_year,len(pdate_list)])*np.nan
        ADAT_n = np.empty([n_year,len(pdate_list)])*np.nan
        MDAT_n = np.empty([n_year,len(pdate_list)])*np.nan
        # For exceedance curve
        sorted_yield_n = np.empty([n_year,len(pdate_list)])*np.nan
        Fx_scf = np.empty([n_year,len(pdate_list)])*np.nan       
        sorted_ADAT_n = np.empty([n_year,len(pdate_list)])*np.nan   
        Fx_scf_ADAT = np.empty([n_year,len(pdate_list)])*np.nan
        sorted_MDAT_n = np.empty([n_year,len(pdate_list)])*np.nan
        # Fx_scf_MDAT = np.empty([n_year,len(pdate_list)])*np.nan
        for count in range(len(pdate_list)):
            Pdate = pdate_list[count]
            yield_n[:,count] = df.HWAM[(df["DOY"] == Pdate)].values  
            ADAT_n[:,count] = df.ADAT[(df["DOY"] == Pdate)].values 
            MDAT_n[:,count] = df.MDAT[(df["DOY"] == Pdate)].values  
            sorted_yield_n[:,count] = np.sort(df.HWAM[(df["DOY"] == Pdate)].values)
            fx_scf = [1.0/len(sorted_yield_n)] * len(sorted_yield_n) #pdf
            #Fx_scf = np.cumsum(fx_scf)
            Fx_scf[:,count] = 1.0-np.cumsum(fx_scf)  #for exceedance curve
            sorted_ADAT_n[:,count] = np.sort(df.ADAT[(df["DOY"] == Pdate)].values)
            sorted_MDAT_n[:,count] = np.sort(df.MDAT[(df["DOY"] == Pdate)].values)
            temp = sorted_ADAT_n[:,count]
            temp2 = temp[~np.isnan(temp)]
            fx_scf_ADAT = [1.0/len(temp2)] * len(temp2) #pdf
            Fx_scf_ADAT[:len(fx_scf_ADAT),count] = 1.0-np.cumsum(fx_scf_ADAT)  #for exceedance curve      

        if obs_flag == 1:
            obs_yield = []
            obs_ADAT = []
            obs_MDAT = []
            for count in range(len(pdate_list)):
                Pdate = pdate_list[count]
                if count > 0 and pdate_list[count] < pdate_list[count-1]:  #when two consecutive years
                    start_year = repr(int(start_year)+1)
                #collect simulated results from observed weather
                obs_yield.append(df.HWAM[(df["DOY"] == Pdate) & (df["Year"] == int(start_year))].values[0])      
                obs_ADAT.append(df.ADAT[(df["DOY"] == Pdate) & (df["Year"] == int(start_year))].values[0])
                obs_MDAT.append(df.MDAT[(df["DOY"] == Pdate) & (df["Year"] == int(start_year))].values[0])
       # Fx_scf_ADAT = Fx_scf
        #To remove 'nan' from data array
        #https://stackoverflow.com/questions/44305873/how-to-deal-with-nan-value-when-plot-boxplot-using-python
        mask = ~np.isnan(yield_n)
        filtered_yield = [d[m] for d, m in zip(yield_n.T, mask.T)]
        mask2 = ~np.isnan(ADAT_n)
        filtered_data2 = [d[m] for d, m in zip(ADAT_n.T, mask2.T)]
        mask3 = ~np.isnan(MDAT_n)
        filtered_data3 = [d[m] for d, m in zip(MDAT_n.T, mask3.T)]
        
        if obs_flag == 1: 
            #replace ADAT = 0 with nan
            obs_ADAT = [np.nan if x==1 else x for x in obs_ADAT]
            obs_MDAT = [np.nan if x==1 else x for x in obs_MDAT]

        #X data for plot
        myXList=[i+1 for i in range(len(pdate_list))]
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            temp_str = repr(year_list[count]) + " " + repr(pdate_list[count]) 
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)
        # 1) Plotting Yield 
        fig = plt.figure()
        fig.suptitle('Estimated Yields using historical weather', fontsize=12, fontweight='bold')
        ax = fig.add_subplot(111)
        ax.set_xlabel('Planting Date[MM/DD]',fontsize=14)
        ax.set_ylabel('Yield [kg/ha]',fontsize=14)
        if obs_flag == 1:
            # Plot a line between yields with observed weather
            plt.plot(myXList, obs_yield, 'go-')
        ax.boxplot(filtered_yield,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
        fig_name = self.Wdir_path + "\\"+sname+"_output_hist\\"+sname+"_Yield_boxplot.png"
        plt.savefig(fig_name)      
        display.updateImgPath(1,0,fig_name) # Add the image pathname to the main CAMDT

        # 2) Plotting ADAT
        fig2 = plt.figure()
        fig2.suptitle('Estimated Anthesis Dates using historical weather', fontsize=12, fontweight='bold')
        ax2 = fig2.add_subplot(111)
        ax2.set_xlabel('Planting Date[DOY]',fontsize=14)
        ax2.set_ylabel('Anthesis Date [DOY]',fontsize=14)
        if obs_flag == 1:
            # Plot a line between yields with observed weather
            plt.plot(myXList, obs_ADAT, 'go-')
        ax2.boxplot(filtered_data2,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
        fig_name2 = self.Wdir_path + "\\"+ sname+"_output_hist\\"+sname+"_ADAT_boxplot.png"
        plt.savefig(fig_name2)       
        display.updateImgPath(1,1,fig_name2) # Add the image pathname to the main CAMDT        

        # 3) Plotting MDAT
        fig3 = plt.figure()
        fig3.suptitle('Estimated Maturity Dates using historical weather', fontsize=12, fontweight='bold')
        ax3 = fig3.add_subplot(111)
        ax3.set_xlabel('Planting Date[DOY]',fontsize=14)
        ax3.set_ylabel('Maturity Date [DOY]',fontsize=14)
        if obs_flag == 1:
            # Plot a line between yields with observed weather
            plt.plot(myXList, obs_MDAT, 'go-')
        ax3.boxplot(filtered_data3,labels=scename) #, showmeans=True) #, meanline=True, notch=True) #, bootstrap=10000)
        fig_name = self.Wdir_path + "\\"+ sname+"_output_hist\\"+sname+"_MDAT_boxplot.png"
        plt.savefig(fig_name)    
        display.updateImgPath(1,2,fig_name) # Add the image pathname to the main CAMDT      

        #4) Plotting yield exceedance curve
        fig4 = plt.figure()
        fig4.suptitle('Yield Exceedance Curve using historical weather', fontsize=12, fontweight='bold')
        ax4 = fig4.add_subplot(111)
        ax4.set_xlabel('Yield [kg/ha]',fontsize=14)
        ax4.set_ylabel('Probability of Exceedance [-]',fontsize=14)
        plt.ylim(0, 1) 
        for x in range(len(pdate_list)):
            ax4.plot(sorted_yield_n[:,x],Fx_scf[:,x],'o-', label=scename[x])
            #vertical line for the yield with observed weather
            if obs_flag == 1:
                x_data=[obs_yield[x],obs_yield[x]] #only two points to draw a line
                y_data=[0,1]
                temp='w/ obs wth (' + scename[x] + ')'
                ax4.plot(x_data,y_data,'-.', label=temp)
                plt.ylim(0, 1)
        box = ax4.get_position()  # Shrink current axis by 15%
        ax4.set_position([box.x0, box.y0, box.width * 0.70, box.height])
        plt.grid(True)
        ax4.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
        fig4.set_size_inches(10, 8)
        fig_name = self.Wdir_path + "\\" + sname + "_output_hist\\" + sname +"_Yield_exceedance.png"
        plt.savefig(fig_name)
        display.updateImgPath(1,3,fig_name) # Add the image pathname to the main CAMDT    


        #5) Plotting ADAT exceedance curve
        fig5 = plt.figure()
        fig5.suptitle('Anthesis Date Exceedance Curve using historical weather', fontsize=12, fontweight='bold')
        ax5 = fig5.add_subplot(111)
        ax5.set_xlabel('Anthesis Date [DOY]',fontsize=14)
        ax5.set_ylabel('Probability of Exceedance [-]',fontsize=14)
        plt.ylim(0, 1) 
        for x in range(len(pdate_list)):
            ax5.plot(sorted_ADAT_n[:,x],Fx_scf_ADAT[:,x],'o-', label=scename[x])
            #vertical line for the yield with observed weather
            if obs_flag == 1:
                x_data=[obs_ADAT[x],obs_ADAT[x]] #only two points to draw a line
                y_data=[0,1]
                temp='w/ obs wth (' + scename[x] + ')'
                ax5.plot(x_data,y_data,'-.', label=temp)
                plt.ylim(0, 1)
        box = ax5.get_position()
        ax5.set_position([box.x0, box.y0, box.width * 0.70, box.height])
        plt.grid(True)
        ax5.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
        fig5.set_size_inches(10, 8)
        fig_name = self.Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_ADAT_exceedance.png"
        plt.savefig(fig_name)    
        display.updateImgPath(1,4,fig_name) # Add the image pathname to the main CAMDT    

        #6) Plotting MDAT exceedance curve
        fig6 = plt.figure()
        fig6.suptitle('Maturity Date Exceedance Curve using historical weather', fontsize=12, fontweight='bold')
        ax6 = fig6.add_subplot(111)
        ax6.set_xlabel('Maturity Date [DOY]',fontsize=14)
        ax6.set_ylabel('Probability of Exceedance [-]',fontsize=14)
        plt.ylim(0, 1) 
        for x in range(len(pdate_list)):
            ax6.plot(sorted_MDAT_n[:,x],Fx_scf_ADAT[:,x],'o-', label=scename[x])
            #vertical line for the yield with observed weather
            if obs_flag == 1:
                x_data=[obs_MDAT[x],obs_MDAT[x]] #only two points to draw a line
                y_data=[0,1]
                temp='w/ obs wth (' + scename[x] + ')'
                ax6.plot(x_data,y_data,'-.', label=temp)
                plt.ylim(0, 1)
        box = ax6.get_position()
        ax6.set_position([box.x0, box.y0, box.width * 0.70, box.height])
        plt.grid(True)
        ax6.legend(loc='center left', bbox_to_anchor=(1, 0.5))         # Put a legend to the right of the current axis
        fig6.set_size_inches(10, 8)
        fig_name = self.Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_MDAT_exceedance.png"
        plt.savefig(fig_name)    
        display.updateImgPath(1,5,fig_name) # Add the image pathname to the main CAMDT    

    ##############################################################
    def WSGD_plot_hist(self,sname, SNX_list, start_year, sim_years,obs_flag, display):

        self.Wdir_path = self.__getWorkingDir()
        fname1 = self.Wdir_path + "\\"+ sname+"_output_hist\\PlantGro.OUT"
        pdate_list = []
        # year_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(SNX_list[i][-11:-8])
                    
        n_days = 180 #approximate growing days
        WSGD_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
        NSTD_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN 
        if obs_flag == 1:
            WSGD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
            NSTD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
        read_flag = 0  #off
        n_count = 0  #count of n weahter realizations
        d_count = 0  #day counter during a growing season
        p_index_old = 0
        p_count = 0  #count for different planting date scenario
        obs_read_flag = 0
        with open(fname1) as fp:
            for line in fp:
                if line[18:21] == '  0':   #line[18:21] => DAP
                    p_index = pdate_list.index(line[6:9])   #line[6:9] => DOY
                    read_flag = 1  #on
                    if line[1:5] == start_year:  #if this is the observed year
                        obs_read_flag = 1  #on
                    if p_index > p_index_old:  #move to a new planting date scenario
                        p_count = p_count + 1
                        n_count = 0
                #if read_flag ==1 and line[20:21] == " ":  #after reading last growing day
                if read_flag ==1 and line == '\n':  #after reading last growing day
                    read_flag = 0 #off
                    obs_read_flag = 0
                    p_index_old = p_index
                    d_count = 0
                    n_count = n_count + 1
                if read_flag == 1:
                    WSGD_3D[d_count, n_count, p_count] = float(line[127:133])
                    NSTD_3D[d_count, n_count, p_count] = float(line[134:140])
                    if obs_flag == 1 and obs_read_flag == 1:
                        WSGD_obs[d_count, p_count] = float(line[127:133])
                        NSTD_obs[d_count, p_count] = float(line[134:140])
                    d_count = d_count + 1     

        # WSGD_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
        # NSTD_avg = np.empty((n_days,len(pdate_list),))*np.NAN  
        WSGD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
        WSGD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        WSGD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        NSTD_50 = np.empty((n_days,len(pdate_list),))*np.NAN  
        NSTD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        NSTD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 

        #plot
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            if count > 0:
                if int(pdate_list[count]) < int(pdate_list[count-1]):
                    start_year = repr(int(start_year) + 1)
            temp_str = str(start_year) + " " + pdate_list[count]
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)

        for i in range(len(pdate_list)):
            # WSGD_avg[:, i] = np.nanmean(WSGD_3D[:, 0:100, i], axis = 1)
            # NSTD_avg[:, i] = np.nanmean(WSGD_3D[:, 0:100, i], axis = 1)
            WSGD_50[:, i] = np.nanpercentile(WSGD_3D[:, 0:sim_years, i], 50, axis = 1)
            WSGD_25[:, i] = np.nanpercentile(WSGD_3D[:, 0:sim_years, i], 25, axis = 1)
            WSGD_75[:, i] = np.nanpercentile(WSGD_3D[:, 0:sim_years, i], 75, axis = 1)
            NSTD_50[:, i] = np.nanpercentile(NSTD_3D[:, 0:sim_years, i], 50, axis = 1)
            NSTD_25[:, i] = np.nanpercentile(NSTD_3D[:, 0:sim_years, i], 25, axis = 1)
            NSTD_75[:, i] = np.nanpercentile(NSTD_3D[:, 0:sim_years, i], 75, axis = 1)
        #================================== 
        #1) Plot WSGD Water stress
        nrow = math.ceil(len(pdate_list)/2.0)
        ncol = 2
        fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
        fig.suptitle('WSGD (Water Stress Index)')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    xdata = range(len(WSGD_50[:,0]))
                    axs[j, i].plot(xdata, WSGD_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(WSGD_50[:,p_count]))
                    yerr_low = WSGD_50[:, p_count] -WSGD_25[:, p_count]
                    yerr_up = WSGD_75[:, p_count] - WSGD_50[:, p_count]
                    axs[j, i].errorbar(xdata, WSGD_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, WSGD_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(WSGD_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='Water Stress Index [-]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(13, 8)
        fig_name = self.Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_WStress.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        display.updateImgPath(3,2,fig_name) # Add the image pathname to the main CAMDT


        #==================================
        #2) Plot NSTD Nitrogen stress
        nrow = math.ceil(len(pdate_list)/2.0)
        ncol = 2
        fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
        fig.suptitle('NSTD (Nitrogen Stress Index)')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    xdata = range(len(NSTD_50[:,0]))
                    axs[j, i].plot(xdata, NSTD_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(NSTD_50[:,p_count]))
                    yerr_low = NSTD_50[:, p_count] -NSTD_25[:, p_count]
                    yerr_up = NSTD_75[:, p_count] - NSTD_50[:, p_count]
                    axs[j, i].errorbar(xdata, NSTD_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, NSTD_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(NSTD_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='Nitrogen Stress Index [-]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = self.Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_NStress.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        display.updateImgPath(3,3,fig_name) # Add the image pathname to the main CAMDT

    ##############################################################  
    def weather_plot_hist(self,sname, SNX_list, start_year, sim_years, obs_flag, display):

        self.Wdir_path = self.__getWorkingDir()
        fname1 = self.Wdir_path + "\\"+ sname+"_output_hist\\Weather.OUT"
        # TMND   MIN TEMP °C     Minimum daily temperature (°C)                           .
        # TMXD   MAX TEMP °C     Maximum daily temperature (°C) 
        # PRED   PRECIP mm/d     Precipitation depth (mm/d) ==>> ??????                        .
        # SRAD   SRAD MJ/m2.d    Solar radiation (MJ/(m2.d))                              .
        # TAVD   AVG TEMP °C     Average daily temperature (°C)
        pdate_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(SNX_list[i][-11:-8])

        n_days = 180 #approximate growing days
        TMXD_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
        TMND_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN 
        SRAD_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN 
        if obs_flag == 1:
            TMXD_obs = np.empty((n_days,len(pdate_list),))*np.NAN 
            TMND_obs = np.empty((n_days,len(pdate_list),))*np.NAN
            SRAD_obs = np.empty((n_days,len(pdate_list),))*np.NAN

        read_flag = 0  #off
        n_count = 0  #count of n weahter realizations
        d_count = 0  #day counter during a growing season
        p_count = 0  #count for different planting date scenario
        obs_read_flag = 0
        with open(fname1) as fp:
            for line in fp:
                if line[6:9] == pdate_list[p_count]:   #line[12:15] => DAS
                    read_flag = 1  #on
                    if line[1:5] == start_year:  #if this is the observed year
                        obs_read_flag = 1  #on
                if read_flag ==1 and line == '\n':  #after reading last growing day
                    read_flag = 0 #off
                    obs_read_flag = 0  #off
                    d_count = 0
                    n_count = n_count + 1
                    if n_count == sim_years:  #move to next planting date scenario
                        p_count = p_count + 1
                        n_count = 0
                if read_flag == 1:
                    TMXD_3D[d_count, n_count, p_count] = float(line[60:64])
                    TMND_3D[d_count, n_count, p_count] = float(line[67:71])
                    SRAD_3D[d_count, n_count, p_count] = float(line[39:43])
                    if obs_flag == 1 and obs_read_flag == 1:
                        TMXD_obs[d_count, p_count] = float(line[60:64])
                        TMND_obs[d_count, p_count] = float(line[67:71])
                        SRAD_obs[d_count, p_count] = float(line[39:43])
                    d_count = d_count + 1

        # TMXD_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
        # TMND_avg = np.empty((n_days,len(pdate_list),))*np.NAN  
        TMXD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMXD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMXD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMND_50 = np.empty((n_days,len(pdate_list),))*np.NAN  
        TMND_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        TMND_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
        SRAD_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
        SRAD_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        SRAD_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
    
        #plot
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            if count > 0:
                if int(pdate_list[count]) < int(pdate_list[count-1]):
                    start_year = repr(int(start_year) + 1)
            temp_str = str(start_year) + " " + pdate_list[count]
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)

        for i in range(len(pdate_list)):
            TMXD_50[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 50, axis = 1)
            TMXD_25[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 25, axis = 1)
            TMXD_75[:, i] = np.nanpercentile(TMXD_3D[:, 0:100, i], 75, axis = 1)
            TMND_50[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 50, axis = 1)
            TMND_25[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 25, axis = 1)
            TMND_75[:, i] = np.nanpercentile(TMND_3D[:, 0:100, i], 75, axis = 1)
            SRAD_50[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 50, axis = 1)
            SRAD_25[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 25, axis = 1)
            SRAD_75[:, i] = np.nanpercentile(SRAD_3D[:, 0:100, i], 75, axis = 1)
        #================================== 
        #1) Plot TMXD
        nrow = math.ceil(len(pdate_list)/2.0)
        ncol = 2
        fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
        fig.suptitle('TMXD (Maximum daily temperature)')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    xdata = range(len(TMXD_50[:,0]))
                    axs[j, i].plot(xdata, TMXD_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(TMXD_50[:,p_count]))
                    yerr_low = TMXD_50[:, p_count] -TMXD_25[:, p_count]
                    yerr_up = TMXD_75[:, p_count] - TMXD_50[:, p_count]
                    axs[j, i].errorbar(xdata, TMXD_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, TMXD_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(TMXD_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='Temperature[C]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = self.Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_Tmax.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        display.updateImgPath(5,0,fig_name) # Add the image pathname to the main CAMDT

        #==================================
        #2) Plot TMND
        fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
        fig.suptitle('TMND (Minimum daily temperature)')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    axs[j, i].plot(xdata, TMND_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(TMND_50[:,p_count]))
                    yerr_low = TMND_50[:, p_count] -TMND_25[:, p_count]
                    yerr_up = TMND_75[:, p_count] - TMND_50[:, p_count]
                    axs[j, i].errorbar(xdata, TMND_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, TMND_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(TMND_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='Temperature[C]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = self.Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_Tmin.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        display.updateImgPath(5,1,fig_name) # Add the image pathname to the main CAMDT

        #==================================
        #3) Plot SRAD   SRAD MJ/m2.d    Solar radiation (MJ/(m2.d)) 
        fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
        fig.suptitle('SARD (Daily solar radiation (MJ/(m2.d))')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    axs[j, i].plot(xdata, SRAD_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(SRAD_50[:,p_count]))
                    yerr_low = SRAD_50[:, p_count] -SRAD_25[:, p_count]
                    yerr_up = SRAD_75[:, p_count] - SRAD_50[:, p_count]
                    axs[j, i].errorbar(xdata, SRAD_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, SRAD_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(SRAD_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After Planting [DAP]', ylabel='SRAD[MJ/(m2.d)]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = self.Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_SRAD.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        display.updateImgPath(5,2,fig_name) # Add the image pathname to the main CAMDT

    ##############################################################
    def plot_cum_rain_hist(self,sname, SNX_list, start_year, sim_years, obs_flag, display):

        self.Wdir_path = self.__getWorkingDir()
        fname1 = self.Wdir_path + "\\"+ sname+"_output_hist\\SoilWat.OUT"
        pdate_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(SNX_list[i][-11:-8])

        n_days = 180 + 30 #approximate growing days + 30 days from IC to plt date
        PREC_3D = np.empty((n_days,sim_years,len(pdate_list),))*np.NAN # n growing dats * [100 realizations * n plt scenarios]
        if obs_flag == 1:
            PREC_obs= np.empty((n_days,len(pdate_list),))*np.NAN 

        read_flag = 0  #off
        n_count = 0  #count of n weahter realizations
        d_count = 0  #day counter during a growing season
        p_count = 0  #count for different planting date scenario
        obs_read_flag = 0
        with open(fname1) as fp:
            for line in fp:
                if line[12:15] == '  0':   #line[12:15] => DAS
                    read_flag = 1  #on
                    if line[1:5] == start_year:  #if this is the observed year
                        obs_read_flag = 1  #on
                if read_flag ==1 and line == '\n':  #after reading last growing day
                    read_flag = 0 #off
                    obs_read_flag = 0
                    d_count = 0
                    n_count = n_count + 1
                    if n_count == sim_years:  #move to next planting date scenario
                        p_count = p_count + 1
                        n_count = 0
                if read_flag == 1:
                    PREC_3D[d_count, n_count, p_count] = float(line[44:48])
                    if obs_flag == 1 and obs_read_flag == 1:
                        PREC_obs[d_count, p_count] = float(line[44:48])
                    d_count = d_count + 1

        # PREC_avg = np.empty((n_days,len(pdate_list),))*np.NAN   
        PREC_50 = np.empty((n_days,len(pdate_list),))*np.NAN 
        PREC_25 = np.empty((n_days,len(pdate_list),))*np.NAN 
        PREC_75 = np.empty((n_days,len(pdate_list),))*np.NAN 
    
        #plot
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            if count > 0:
                if int(pdate_list[count]) < int(pdate_list[count-1]):
                    start_year = repr(int(start_year) + 1)
            temp_str = str(start_year) + " " + pdate_list[count]
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)

        for i in range(len(pdate_list)):
            PREC_50[:, i] = np.nanpercentile(PREC_3D[:, 0:sim_years, i], 50, axis = 1)
            PREC_25[:, i] = np.nanpercentile(PREC_3D[:, 0:sim_years, i], 25, axis = 1)
            PREC_75[:, i] = np.nanpercentile(PREC_3D[:, 0:sim_years, i], 75, axis = 1)

        #================================== 
        #1) Plot PREC
        nrow = math.ceil(len(pdate_list)/2.0)
        ncol = 2
        fig, axs = plt.subplots(nrow, ncol, sharex=True, sharey=True)
        fig.suptitle('Cumulative Precipitation')
        p_count = 0
        for i in range(ncol):
            for j in range(nrow):
                if p_count > (len(pdate_list)-1):  #for a remaining empty plot
                    xdata = range(len(PREC_50[:,0]))
                    axs[j, i].plot(xdata, PREC_50[:, p_count-1], 'w')
                else:
                    xdata = range(len(PREC_50[:,p_count]))
                    yerr_low = PREC_50[:, p_count] -PREC_25[:, p_count]
                    yerr_up = PREC_75[:, p_count] - PREC_50[:, p_count]
                    axs[j, i].errorbar(xdata, PREC_50[:, p_count], yerr=yerr_low, uplims=True)
                    axs[j, i].errorbar(xdata, PREC_50[:, p_count], yerr=yerr_up, lolims=True)
                    if obs_flag == 1:
                        axs[j, i].plot(PREC_obs[:, p_count],'x-.', label='w/ obs. weather')
                    axs[j, i].set_title(scename[p_count])
                p_count = p_count + 1
        for ax in axs.flat:
            ax.set(xlabel='Days After SIMULATION [DAS]', ylabel='Cum. Rainfall[mm]')
        # Hide x labels and tick labels for top plots and y ticks for right plots.
        for ax in axs.flat:
            ax.label_outer()
        fig.set_size_inches(12, 8)
        fig_name = self.Wdir_path + "\\"+ sname +"_output_hist\\" + sname +"_CumRain.png"
        #plt.show()
        plt.savefig(fig_name,dpi=100)
        display.updateImgPath(5,3,fig_name) # Add the image pathname to the main CAMDT

    ###################################################################### 

    def writeV47_main_hist(self, snx_list, obs_flag):
        self.Wdir_path = self.__getWorkingDir()

        temp_dv4    =   self.Wdir_path.replace("/","\\") + "\\DSSBatch_TEMP_MZ.v47"
        # pdate_1, pdate_2 = self._setup1.setPlantingWindow()

        dv4_fname   =   self.Wdir_path.replace("/","\\") + "\\DSSBatch.v47" 
        fr = open(temp_dv4,"r") #opens temp DV4 file to read
        fw = open(dv4_fname,"w") 
        #read template and write lines
        for line in range(0,10):
            temp_str=fr.readline()
            fw.write(temp_str)
            
        temp_str=fr.readline()
        #write SNX file with each different planting date
        for i in range(len(snx_list)):   #EJ(3/12/2019) for finding optimal planting dates
            new_str2='{:<95}'.format(snx_list[i])+ temp_str[95:]
           # new_str2='{0:<95}{1:4s}'.format(snx_list[i], repr(j+1).rjust(4))+ temp_str[99:]
            fw.write(new_str2)               
        fr.close()
        fw.close()

    def writeSNX_main_hist(self,SNX_fname,sim_years,obs_year1, plt_date):   #Note: plt_date is in YYYYDOY format             
        self.Wdir_path = self.__getWorkingDir()
        temp_snx=self.Wdir_path.replace("/","\\") + "\\PHMZTEMP.SNX" 
        fr = open(temp_snx,"r") #opens temp SNX file to read
        fw = open(SNX_fname,"w") #opens SNX file to write

        #===set up parameters
        MI= '0' #str(IRbutton.get())  #EJ(5/30/2019)
        # if IRbutton.get() == 1:  #automatic irrigation
        #     IRRIG='A'  #automatic, or 'N' (no irrigation)
        # else:
        #     IRRIG='N'  #automatic, or 'N' (no irrigation)
        MF= str(self._setup2.getApplyFert())    #'1'
        # INGENO=self.label_01.cget("text")[0:6] #self.cul_type.getvalue()[0][0:6] #'UY0371' #cultivar type
        INGENO = self.setup1Results[5] #cultivar type
        WSTA = self.setup1Results[0][0:4]  #'SANJ'
        ID_SOIL = self.setup1Results[7]
        start_year = self.setup1Results[1] #'2002'
        NYERS= sim_years  # ***** <<<<=====================================
        
        temp=int(plt_date%1000)-30  #initial condition: 30 day before planting  #EJ(3/13/2019)
        if temp <= 0 :   #EJ(3/13/2019)
            if calendar.isleap(int(start_year)-1):  
                temp = 366 + temp
            else:
                temp = 365 + temp
            ICDAT = repr((plt_date//1000 -1)*1000+temp)
        else:
            ICDAT = repr((plt_date//1000)*1000+temp)

        ICDAT = repr(obs_year1) + ICDAT[4:] #ICDAT => str in YYYYDOY, obs_year is integer in YYYY   <<<<=====================================
        # i_NO3=self.label_04.cget("text")[0:1]  #self.NO3_soil.getvalue()[0][0:1] #'H' #or 'L'
        i_NO3 = self.setup1Results[9] #'H' #or 'L'
        SNH4=1.5  #**EJ(5/27/2015) followed by Walter's ETMZDSS6.SNX
        #PDATE=start_year[2:]+repr(plt_date).zfill(3)
        #PDATE=repr(plt_date)[2:]      
        #    
        PDATE = repr(obs_year1)[2:] + repr(plt_date)[4:]     #YYDOY format   
        if int(ICDAT) > (obs_year1*1000 + plt_date%1000):  #if ICDAT > PDATE
            PDATE = repr(obs_year1+1)[2:] + repr(plt_date)[4:]
            
        FInputs1 = self._setup2.getFertInpList1()
        FInputs2 = self._setup2.getFertInpList2()
        FInputs3 = self._setup2.getFertInpList3()

        FDATE1  =   FInputs1[0] #'0'
        FAMN1   =   FInputs1[1] #'50'  #1st fertilizer application amount
        FMCD1   =   FInputs1[2] #'50'  #1st fertilizer Material   
        if FInputs1[3] != "None":      #1st fertilizer application
            FACD1   = FInputs1[3]
        else:
             FACD1  = '-99' 

        FDATE2  =   FInputs2[0]  #'60'
        FAMN2   =   FInputs2[1] #'90'  #2nd fertilizer application amount
        FMCD2   =   FInputs2[2] #'50'  #2nd fertilizer Material  
        if FInputs2[3] != "None":      #2nd fertilizer application
            FACD2   = FInputs2[3]
        else:
             FACD2  = '-99'     

        FDATE3  =   FInputs3[0]  #'60'
        FAMN3   =   FInputs3[1] #'90'  #3rd fertilizer application amount
        FMCD3   =   FInputs3[2] #'50'  #3rd fertilizer Material    
        if FInputs3[3] != "None":      #3rd fertilizer application
            FACD3   = FInputs3[3]
        else:
             FACD3  = '-99' 

        # temp=int(plt_date)-3 #EJ(6/9/2015) #simuation starting date
       
        SDATE=ICDAT    #EJ(3/13/2019)

        if self._setup2.getApplyIrr() == 1:
            IRRIG='A'  #automatic, or 'N' (no irrigation)
        else:
            IRRIG='N'  #automatic, or 'N' (no irrigation)

        if self._setup2.getApplyFert == 1:
            FERTI='D' # 'D'= Days after planting, 'R'=on report date, or 'N' (no fertilizer)
        else:
            FERTI='N'

        IC_w_ratio = float(self.setup1Results[8]) #e.g., 0.7 
        IMETH = self._setup2.getApplyIrrMeth() #'IR001' #irrigation method
        #===end of setting up paramters

        #read lines 1-9 from temp file
        for line in range(0,14):
            temp_str=fr.readline()
            fw.write(temp_str)

        #write *TREATMENTS 
        CU='1'
        SA='0'
        IC='1'
        MP='1'
        MR='0'
        MC='0'
        MT='0'
        ME='0'
        MH='0'
        SM='1'
        temp_str=fr.readline()
        # *TREATMENTS   
        FL = str(1)
        fw.write('{0:3s}{1:31s}{2:3s}{3:3s}{4:3s}{5:3s}{6:3s}{7:3s}{8:3s}{9:3s}{10:3s}{11:3s}{12:3s}{13:3s}'.format(FL.rjust(3),'1 0 0 ERiMA DCC1                 1',
                    FL.rjust(3),SA.rjust(3), IC.rjust(3), MP.rjust(3), MI.rjust(3), MF.rjust(3), MR.rjust(3), MC.rjust(3), 
                    MT.rjust(3), ME.rjust(3), MH.rjust(3), SM.rjust(3)))  
        fw.write(" \n")  
        #read lines from temp file
        for line in range(0,3):
            temp_str=fr.readline()
            #print temp_str
            fw.write(temp_str)
        #write *CULTIVARS
        temp_str=fr.readline()
        new_str=temp_str[0:6] + INGENO + temp_str[26:]
        fw.write(new_str)
        fw.write(" \n")   

        #read lines from temp file
        for line in range(0,3):
            temp_str=fr.readline()
            fw.write(temp_str)
        #================write *FIELDS   
        #Get soil info from *.SOL
       # soil_depth, wp, fc, nlayer = get_soil_IC(ID_SOIL)  
        soil_depth, wp, fc, nlayer, SLTX = self.get_soil_IC(ID_SOIL)  # <=========== ***********************
        temp_str=fr.readline()
        SLDP = repr(soil_depth[-1])
        FL = str(1)
        ID_FIELD = WSTA + str(1).zfill(4)  #WSTA = 'SANJ'
       # WSTA_ID = WSTA + '8830'  # <=========== *********************** '8830' shoudl be added to read SANJ8830.WTH
        WSTA_ID = self.setup1Results[0][0:8]
        fw.write('{0:3s}{1:8s} {2:8s}{3:37s}{4:6s}{5:4s}  {6:10s}{7:4s}'.format(FL.rjust(3), ID_FIELD, WSTA_ID, '   -99   -99   -99   -99   -99   -99 ',
                                            SLTX.ljust(6), SLDP.rjust(4), ID_SOIL,' -99'))  
        fw.write(" \n")  
        temp_str=fr.readline()  #@L ...........XCRD ...........YCRD .....ELEV
        fw.write(temp_str)
        temp_str=fr.readline()  # 1             -99             -99       -99   ==> skip
        #================write *FIELDS - second section
        FL = str(1)
        fw.write('{0:3s}{1:89s}'.format(FL.rjust(3), '            -99             -99       -99               -99   -99   -99   -99   -99   -99'))
        fw.write(" \n")  
        
        #read lines from temp file
        for line in range(0,3):
            temp_str=fr.readline()
            fw.write(temp_str)
        #write *INITIAL CONDITIONS   
        temp_str=fr.readline()
        new_str=temp_str[0:9] + ICDAT[2:] + temp_str[14:]
        fw.write(new_str)
        temp_str=fr.readline() #@C  ICBL  SH2O  SNH4  SNO3 
        fw.write(temp_str)

        #Get soil info from *.SOL
       # soil_depth, wp, fc, nlayer = get_soil_IC(ID_SOIL)  
        temp_str=fr.readline()
        for nline in range(0,nlayer):
            if nline == 0:  #first layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                # SH2O=0.7*(float(fc[nline])- float(wp[nline]))+ float(wp[nline])#EJ(6/25/2015): initial AWC=70% of maximum AWC
                if i_NO3 == 'H':
                    SNO3='14'  # arbitrary... following to Uruguay DSS
                elif i_NO3 == 'L':
                    SNO3='5'  # arbitrary... following to Uruguay DSS
                else:
                    # self.ini_NO3_err.activate() 
                    print("Error of NO3 value")                  
            elif nline == 1:  #second layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                if i_NO3 == 'H':
                    SNO3='14'  # arbitrary... following to Uruguay DSS
                elif i_NO3 == 'L':
                    SNO3='5'  # arbitrary... following to Uruguay DSS
                else:
                    # self.ini_NO3_err.activate() 
                    print("Error of NO3 value")  
            elif nline == 2:  #third layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
            elif nline == 3:  #forth layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
            elif nline == 4:  #fifth layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
            elif nline == 5:  #sixth layer
                temp_SH2O=IC_w_ratio*(fc[nline]- wp[nline])+ wp[nline]#EJ(6/25/2015): initial AWC=70% of maximum AWC
                SNO3 =  '5' # arbitrary... following to Uruguay DSS, regardless of H or L
            else:
                temp_SH2O=fc[nline] #float
                SNO3='0'  #**EJ(5/27/2015)
            
            SH2O=repr(temp_SH2O)[0:5]  #convert float to string
            new_str=temp_str[0:5] + repr(soil_depth[nline]).rjust(3) + ' ' + SH2O.rjust(5) + temp_str[14:22]+ SNO3.rjust(4)+ "\n"
            fw.write(new_str)
        fw.write("  \n")

        for nline in range(0,10):
            temp_str=fr.readline()
            #print temp_str
            if temp_str[0:9] == '*PLANTING':
                break

        fw.write(temp_str) #*PLANTING DETAILS  
        temp_str=fr.readline() #@P PDATE EDATE
        fw.write(temp_str)
            
        #write *PLANTING DETAILS
        temp_str=fr.readline()
        PPOP    = self.setup1Results[6] #planting density
        PPOE    = self.setup1Results[6] #planting density        
        new_str=temp_str[0:3] + PDATE + temp_str[8:14] + PPOP.rjust(6) + PPOE.rjust(6) + temp_str[26:]
        fw.write(new_str)

        #read lines from temp file
        for line in range(0,3):
            temp_str=fr.readline()
            fw.write(temp_str)
        #write *FERTILIZERS (INORGANIC)
        if MF == '1':
            temp_str=fr.readline()
            new_str=temp_str[0:5] + str(FDATE1).rjust(3) + ' '+ str(FMCD1).rjust(5)+' '+str(FACD1).rjust(5)+temp_str[20:30]+ str(FAMN1).rjust(2) + temp_str[32:]
            fw.write(new_str)
            temp_str=fr.readline()
            if self._setup2.getApplyNumFert() == '2':
                new_str=temp_str[0:5] + str(FDATE2).rjust(3) + ' '+ str(FMCD2).rjust(5)+' '+ str(FACD2).rjust(5)+temp_str[20:30]+ str(FAMN2).rjust(2) + temp_str[32:]
                fw.write(new_str)
            if self._setup2.getApplyNumFert() == '3':
                new_str=temp_str[0:5] + str(FDATE2).rjust(3) + ' '+ str(FMCD2).rjust(5)+' '+ str(FACD2).rjust(5)+temp_str[20:30]+ str(FAMN2).rjust(2) + temp_str[32:]
                fw.write(new_str)
                new_str=temp_str[0:5] + str(FDATE3).rjust(3) + ' '+ str(FMCD3).rjust(5)+' '+ str(FACD3).rjust(5)+temp_str[20:30]+ str(FAMN3).rjust(2) + temp_str[32:]
                fw.write(new_str)
            #check errors in User's input
            # if self.nfertilizer.getvalue() == '':
            #     self.fert_err.activate()
            # if self.label006.cget("text") == '': #'N/A':
            #     self.fert_err.activate()
            # if self.label007.cget("text") == '': #'N/A':
            #     self.fert_err.activate()
            if self._setup2.getApplyNumFert() == '0':
                print("Error: NUmber of fertlization app is 0.")
        else:  #
            temp_str=fr.readline()
            fw.write(temp_str)
            temp_str=fr.readline()
            fw.write(temp_str)

        #read lines from temp file
        for line in range(0,3):
            temp_str=fr.readline()
            fw.write(temp_str)
        #write *SIMULATION CONTROLS
        temp_str=fr.readline()
        #new_str=temp_str[0:33]+ SDATE[2:] + temp_str[38:]
        new_str=temp_str[0:18] + repr(NYERS).rjust(2) + temp_str[20:33]+ SDATE[2:] + temp_str[38:]
        fw.write(new_str)
        temp_str=fr.readline() #@N OPTIONS
        fw.write(temp_str)
        temp_str=fr.readline()  # 1 OP     
        fw.write(temp_str)
        temp_str=fr.readline()  #@N METHODS 
        fw.write(temp_str)
        temp_str=fr.readline()  # 1 ME      
        fw.write(temp_str)
        temp_str=fr.readline()  #@N MANAGEMENT 
        fw.write(temp_str)
        temp_str=fr.readline()  # 1 MA   
        new_str=temp_str[0:25] + IRRIG + temp_str[26:31]+ FERTI + temp_str[32:]
        fw.write(new_str)
        temp_str=fr.readline()  #@N OUTPUTS
        fw.write(temp_str)
        temp_str=fr.readline()  # 1 OU   
        fw.write(temp_str)

        #read lines from temp file
        for line in range(0,5):
            temp_str=fr.readline()
            fw.write(temp_str)
        #irrigation method
        temp_str=fr.readline()  #  1 IR 
        new_str=temp_str[0:39] + IMETH + temp_str[44:]
        fw.write(new_str)

        #read lines from temp file
        for line in range(0,7):
            temp_str=fr.readline()
            fw.write(temp_str)

        # irr_meth = self._setup2.getApplyIrrMeth()
        # #read lines from temp file
        # for line in range(0,7):
        #     temp_str=fr.readline()
        #     fw.write(temp_str) 

        fr.close()
        fw.close()
    #====End of WRITE *.SNX
    ##############################################################


    def get_soil_IC(self, ID_SOIL):
        SOL_file=self.Wdir_path.replace("/","\\") + "\\PH.SOL" 
        #initialize
        depth_layer=[]
        ll_layer=[]
        ul_layer=[]
        n_layer=0
        soil_flag=0
        count=0
        fname = open(SOL_file,"r") #opens *.SOL
        for line in fname:
            if ID_SOIL in line:
                #soil_depth=line[33:36]
                soil_depth=line[34:38]  #PH.SOL
                sltx=line[25:29]  #PH.SOL
                soil_flag=1
            if soil_flag == 1:
                count=count+1
                if count >= 7:
                    depth_layer.append(int(line[0:6]))
                    ll_layer.append(float(line[13:18]))
                    ul_layer.append(float(line[19:24]))
                    n_layer=n_layer+1
                    if int(line[3:6]) == int(soil_depth):
                        yield depth_layer  #list 
                        yield ll_layer
                        yield ul_layer
                        yield n_layer
                        yield sltx
                        fname.close()
                        break

    #=============================================
    # check last observed data from *.WTD
    def find_obs_date(self, WTD_fname):
        data1 = np.loadtxt(WTD_fname,skiprows=1)
        #convert numpy array to dataframe
        WTD_df = pd.DataFrame({'YEAR':data1[:,0].astype(int)//1000,    #python 3.6: / --> //
            'DOY':data1[:,0].astype(int)%1000,
            'SRAD':data1[:,1],
            'TMAX':data1[:,2],
            'TMIN':data1[:,3],
            'RAIN':data1[:,4]})
        WTD_last_date = int(repr(WTD_df.YEAR.values[-1]) + repr(WTD_df.DOY.values[-1]).zfill(3))
        del WTD_df
        return WTD_last_date

    def find_station_info(self, station_name):
        if station_name == 'SANJ':
            LAT = 12.35    
            LONG = 121.033
            ELEV = 3
            TAV = 28.3
            AMP = 2.1
        elif station_name == 'APAR':
            LAT = 18.359    
            LONG = 121.630
            ELEV = 3
            TAV = 27.3
            AMP = 5.0
        elif station_name == 'MALA':
            LAT = 8.151    
            LONG = 125.134
            ELEV = 627
            TAV = 24.2
            AMP = 1.6
        elif station_name == 'PALA':
            LAT = 9.740    
            LONG = 118.759
            ELEV = 16
            TAV = 27.9 
            AMP = 1.8
        elif station_name == 'TUGE':
            LAT = 17.637    
            LONG = 121.752
            ELEV = 62
            TAV = 26.9
            AMP = 5.8       
        elif station_name == 'CLAR':
            LAT = 15.183   
            LONG = 120.533
            ELEV = 151
            TAV = 27.1
            AMP = 3.6
        else:
            LAT = -99    
            LONG = -99
            ELEV = -99
            TAV = -99
            AMP = -99
        return LAT, LONG, ELEV, TAV, AMP 


    # check last observed data from *.WTD
    def find_obs_years(self, WTD_fname, IC_date, hv_date): #note: IC_date and hv_date is integer in [YYYYDOY] 
        data1 = np.loadtxt(WTD_fname,skiprows=1)
        WTD_df = pd.DataFrame({'YEAR':data1[:,0].astype(int)//1000,    #python 3.6: / --> //
            'DOY':data1[:,0].astype(int)%1000,
            'SRAD':data1[:,1],
            'TMAX':data1[:,2],
            'TMIN':data1[:,3],
            'RAIN':data1[:,4]})
        year1 = WTD_df.YEAR[(WTD_df["DOY"] == IC_date%1000)].values[0]
        year2 = WTD_df.YEAR[(WTD_df["DOY"] == hv_date%1000)].values[-1]
        if IC_date%1000 < hv_date%1000:  #when IC_date and hv_date are in a same year
            sim_years = year2 - year1 + 1
        else:
            sim_years = year2 - year1  #when a growing period go beyond next year
        del WTD_df
        return sim_years, year1


        #=============================================
    
    #Determine pdate_1[YYYYDOY] and pdate_2[YYYYDOY]
    def find_plt_date(self, start_year, lst, lst2, lst_frst, lst_frst2):
        #=============================================
        m_doys_list = [1,32,60,91,121,152,182,213,244,274,305,335]  #starting date of each month for regular years
        m_doye_list = [31,59,90,120,151,181,212,243,273,304,334,365] #ending date of each month for regular years
        m_doys_list2 = [1,32,61,92,122,153,183,214,245,275,306,336]  #starting date of each month for leap years
        m_doye_list2 = [31,60,91,121,152,182,213,244,274,305,335,366] #ending date of each month for leap years
        numday_list = [31,28,31,30,31,30,31,31,30,31,30,31]
        #======================================================================
        #==> (1)find the first planting month
        #check year 1 first
        p_index=[i for i in range(len(lst)) if lst[i]== 1]  #range (python 3):   #xrange (python 2):
        s_index=[i for i in range(len(lst_frst)) if lst_frst[i]== 1] 
        p_index2=[i for i in range(len(lst2)) if lst2[i]== 1]
        s_index2=[i for i in range(len(lst_frst2)) if lst_frst2[i]== 1] 
        # plt1_month = p_index[0] + 1
        if len(p_index) > 0:
            pdate_1 = int(start_year)*1000 + m_doys_list[p_index[0]]
            if calendar.isleap(int(start_year)):  pdate_1 = int(start_year)*1000 + m_doys_list2[p_index[0]]
            if len(s_index) > 0:
                frst_date1 = int(start_year)*1000 + m_doys_list[s_index[0]]
                if calendar.isleap(int(start_year)):  frst_date1 = int(start_year)*1000 + m_doys_list2[s_index[0]]
            else: #check year 2
                s_index2=[i for i in range(len(lst_frst2)) if lst_frst2[i]== 1]  #range (python 3):
                frst_date1 = (int(start_year)+1)*1000 + m_doys_list[s_index2[0]]
                if calendar.isleap(int(start_year)+1):  frst_date1 = (int(start_year)+1)*1000 + m_doys_list2[s_index2[0]]
        else: # len(p_index) == 0:  #If cannot find planting month in the year 1
            #plt1_month = p_index2[0] + 1
            pdate_1 = int(start_year)*1000 + m_doys_list[p_index2[0]]
            if calendar.isleap(int(start_year)):  pdate_1 = int(start_year)*1000 + m_doys_list2[p_index2[0]]
            if len(s_index) > 0:  #check year 1 first  => case 2
                frst_date1 = (int(start_year)-1)*1000 + m_doys_list[s_index[0]]
                if calendar.isleap(int(start_year)-1):  frst_date1 = (int(start_year)-1)*1000 + m_doys_list2[s_index[0]]
            else: #check year 2
                frst_date1 = int(start_year)*1000 + m_doys_list[s_index2[0]]
                if calendar.isleap(int(start_year)):  frst_date1 = int(start_year)*1000 + m_doys_list2[s_index2[0]]

        #==> (2)find the last planting month 
        p2_index=[i for i in range(len(lst2)) if lst2[i]== 1]   #check year 2 first
        # plt2_month = p2_index[-1]  #last element
        if len(p2_index) == 0:  #If cannot find planting month in the year 2, search year 1
            p2_index2=[i for i in range(len(lst)) if lst[i]== 1]
            #plt2_month = p2_index2[-1]
            pdate_2 = int(start_year)*1000 + m_doye_list[p2_index2[-1]]
            if calendar.isleap(int(start_year)):  pdate_2 = int(start_year)*1000 + m_doye_list2[p2_index2[-1]]
            end_date = 365  #int(start_year)*1000 + 365
            if calendar.isleap(int(start_year)):  end_date = 366 #int(start_year)*1000 + 366
            if (pdate_2%1000 + 180) > end_date:   #180 is arbitrary number to fully cover weather data until late maturity
                hv_date = (pdate_2//1000 + 1)*1000 + (pdate_2%1000 + 180) - end_date  
            else:
                hv_date = pdate_2 + 180
        else: #If can find planting month in the year 2
            if len(p_index) == 0:  #If cannot find planting month in the year 1, the first planting date is in year 2
                #case 2
                pdate_2 = int(start_year)*1000 + m_doye_list[p2_index[-1]]
                if calendar.isleap(int(start_year)):  pdate_2 = int(start_year)*1000 + m_doye_list2[p2_index[-1]]
                hv_date = pdate_2 + 180
            else: #case 3 => plating window covers both year 1 and year 2
                pdate_2 = (int(start_year) + 1)*1000 + m_doye_list[p2_index[-1]]
                if calendar.isleap(int(start_year) + 1):  pdate_2 = (int(start_year) + 1)*1000 + m_doye_list2[p2_index[-1]]
                hv_date = pdate_2 + 180
        #make a list of planting window months
        pwindow_m= []
        if len(p_index) > 0:
            for i in range(len(p_index)):
                pwindow_m.append(p_index[i] + 1)
            if len(p_index2) > 0:
                for i in range(len(p_index2)):
                    pwindow_m.append(p_index2[i] + 1)
        else:
            if len(p_index2) > 0:
                for i in range(len(p_index2)):
                    pwindow_m.append(p_index2[i] + 1)
            else:
                print ("error! - no planting window is found!")
                os.system('pause')
        #make a list of SCF window months
        fwindow_m= []
        if len(s_index) > 0:
            for i in range(len(s_index)):
                fwindow_m.append(s_index[i] + 1)
            if len(s_index2) > 0:
                for i in range(len(s_index2)):
                    fwindow_m.append(s_index2[i] + 1)
        else:
            if len(s_index2) > 0:
                for i in range(len(s_index2)):
                    fwindow_m.append(s_index2[i]+ 1)
            else:
                print ("error! - no SCF window is found!")
                os.system('pause')
        return pdate_1, pdate_2, frst_date1, hv_date, pwindow_m, fwindow_m


    ##############################################################
    def Yield_boxplot_compare(self, sname, SNX_list, start_year, sim_years, obs_flag, display, index):
        self.Wdir_path = self.__getWorkingDir()

        fname1 = self.Wdir_path + "\\"+ sname+"_output\\SUMMARY.OUT"
        if obs_flag == 1:
            nrealiz = 101
        else:
            nrealiz = 100
        pdate_list = []
        year_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(int(SNX_list[i][-11:-8]))
            if i == 0:
                year_list.append(int(start_year))
            else:
                if pdate_list[i] > pdate_list[i-1]:
                    year_list.append(int(start_year))
                else:
                    year_list.append(int(start_year) + 1)
        #check if output  SUMMARY.OUt exists
        if not os.path.isfile(fname1):
            print( '**Error!!- SUMMARY.OUT file does not exist under ####_output folder')
            os.system('pause')

        df_OUT=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
        PDAT = df_OUT.ix[:,14].values  #read 14th column only => Planting date 
        HWAM = df_OUT.ix[:,21].values  #read 21th column only

        DOY_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize
        for i in range(len(HWAM)):
            # year_array[i] = int(repr(PDAT[i])[:4])
            DOY_array[i] = int(repr(PDAT[i])[4:])
                
        #make three arrays into a dataframe
        df = pd.DataFrame({'DOY': DOY_array, 'HWAM':HWAM}, columns=['DOY','HWAM'])
        #df = pd.DataFrame({'DOY': DOY_array, 'ADAT':ADAT_array, 'MDAT':MDAT_array, 'HWAM':HWAM}, columns=['DOY','ADAT','MDAT','HWAM'])

        # #make an empty array [nyears * n_plt_dates]
        n_year = 100
        # For Boxplot
        yield_n = np.empty([n_year,len(pdate_list)])*np.nan

        if obs_flag == 0:  #if there is no observed weather available 
            for count in range(len(pdate_list)):
                Pdate = pdate_list[count]
                yield_n[:,count] = df.HWAM[(df["DOY"] == Pdate)].values           
        else:   
            obs_yield = []
            # obs_ADAT = []
            # obs_MDAT = []
            for count in range(len(pdate_list)):
                Pdate = pdate_list[count]
                yield_n[:,count] = df.HWAM[(df["DOY"] == Pdate)].values[:-1]  
                obs_yield.append(df.HWAM[(df["DOY"] == Pdate)].values[-1])      

        #To remove 'nan' from data array
        #https://stackoverflow.com/questions/44305873/how-to-deal-with-nan-value-when-plot-boxplot-using-python
        mask = ~np.isnan(yield_n)
        filtered_yield = [d[m] for d, m in zip(yield_n.T, mask.T)]
        ##====================================================
        ##READ Yields from historical weather observations
        fname1 = self.Wdir_path + "\\"+ sname+"_output_hist\\SUMMARY.OUT"

        #check if output  SUMMARY.OUt exists
        if not os.path.isfile(fname1):
            print( '**Error!!- SUMMARY.OUT file does not exist under ####_output_hist folder')
            os.system('pause')

        df_OUT2=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
        PDAT2 = df_OUT2.ix[:,14].values  #read 14th column only => Planting date 
        HWAM2 = df_OUT2.ix[:,21].values  #read 21th column only

        year_array2 = np.zeros(len(HWAM2)) #np.empty((ndays,1))*np.nan #initialize
        DOY_array2 = np.zeros(len(HWAM2)) #np.empty((ndays,1))*np.nan #initialize

        for i in range(len(HWAM2)):
            year_array2[i] = int(repr(PDAT2[i])[:4])
            DOY_array2[i] = int(repr(PDAT2[i])[4:])
                
        #make three arrays into a dataframe
        df2 = pd.DataFrame({'Year': year_array2,'DOY': DOY_array2, 'HWAM':HWAM2}, columns=['Year','DOY','HWAM'])
        #df2 = pd.DataFrame({'Year': year_array2,'DOY': DOY_array2, 'ADAT':ADAT2_array2, 'MDAT':MDAT2_array2, 'HWAM':HWAM2}, columns=['Year','DOY','ADAT','MDAT','HWAM'])

        # #make an empty array [nyears * n_plt_dates]
        n_year = sim_years
        # For Boxplot
        yield2_n = np.empty([n_year,len(pdate_list)])*np.nan

        for count in range(len(pdate_list)):
            Pdate = pdate_list[count]
            yield2_n[:,count] = df2.HWAM[(df2["DOY"] == Pdate)].values  

        if obs_flag == 1:
            obs_yield = []
            for count in range(len(pdate_list)):
                Pdate = pdate_list[count]
                if count > 0 and pdate_list[count] < pdate_list[count-1]:  #when two consecutive years
                    start_year = repr(int(start_year)+1)
                #collect simulated results from observed weather
                obs_yield.append(df2.HWAM[(df2["DOY"] == Pdate) & (df2["Year"] == int(start_year))].values[0])      
        #To remove 'nan' from data array
        #https://stackoverflow.com/questions/44305873/how-to-deal-with-nan-value-when-plot-boxplot-using-python
        mask = ~np.isnan(yield2_n)
        filtered_yield2 = [d[m] for d, m in zip(yield2_n.T, mask.T)]
        ##End of reading Yields from historical weather observations
        #plot============================================
        #regroup matrix for different planting date
        i_pos = 1
        fig = plt.figure()
        fig.suptitle('Estimated Yields with differnt planting dates', fontsize=14, fontweight='bold')
        ax = plt.axes()
        #plt.hold(True)

        for count in range(len(pdate_list)):
            a = filtered_yield2[count].tolist() #.reshape((1,obs_years,1))
            b = filtered_yield[count].tolist() #.reshape((nrealiz,1))
            new_list = []
            new_list.append(a)
            new_list.append(b) #=> nested list
            # first boxplot pair
            bp = plt.boxplot(new_list, positions = [i_pos, i_pos+1], widths = 0.6)
            setBoxColors(bp)
            i_pos = i_pos + 3

        #X data for plot
        #myXList=[i+3 for i in range(len(pdate_list))]
        myXList=[i+0.5 for i in range(1, 3*len(pdate_list), 3)]
        if obs_flag == 1:
            # Plot a line between yields with observed weather
            plt.plot(myXList, obs_yield, 'go-')
        scename = [] #'6/1','6/11','6/21','7/1','7/11']  #['7','17','27','37','47','57']
        for count in range(len(pdate_list)):
            temp_str = repr(year_list[count]) + " " + repr(pdate_list[count]) 
            temp_name = datetime.datetime.strptime(temp_str, '%Y %j').strftime('%m/%d')
            scename.append(temp_name)

        # set axes limits and labels
        plt.xlim(0,3*len(pdate_list))
        # ax.set_xticks([1.5, 4.5, 7.5, 10.5, 13.5, 16.5])
        ax.set_xticklabels(scename)
        ax.set_xticks(myXList)
        ax.set_xlabel('Planting Date',fontsize=14)
        ax.set_ylabel('Yield [kg/ha]',fontsize=14)
        # draw temporary red and blue lines and use them to create a legend
        hB, = plt.plot([1,1],'b-')
        hR, = plt.plot([1,1],'r-')
        plt.legend((hB, hR),('observed WTH', 'SCF'), loc='best')
        hB.set_visible(False)
        hR.set_visible(False)
        fig.set_size_inches(12, 8)
        fig_name = self.Wdir_path + "\\"+sname+"_output\\"+sname+"_Yield_boxplot2.png"
        plt.savefig(fig_name,dpi=100)
        
        display.updateImgPath(6, index, fig_name)   
       
    ##############################################################

    ##############################################################
    def Yield_tercile_forecast(self,sname, SNX_list, start_year, sim_years, obs_flag, display):
        self.Wdir_path = self.__getWorkingDir()

    
        fname1 = self.Wdir_path + "\\"+ sname+"_output\\SUMMARY.OUT"
        if obs_flag == 1:
            nrealiz = 101
        else:
            nrealiz = 100
        pdate_list = []
        year_list = []
        for i in range(len(SNX_list)):
            pdate_list.append(int(SNX_list[i][-11:-8]))
            if i == 0:
                year_list.append(int(start_year))
            else:
                if pdate_list[i] > pdate_list[i-1]:
                    year_list.append(int(start_year))
                else:
                    year_list.append(int(start_year) + 1)
        #check if output  SUMMARY.OUt exists
        if not os.path.isfile(fname1):
            print( '**Error!!- SUMMARY.OUT file does not exist under ####_output folder')
            os.system('pause')

        df_OUT=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
        PDAT = df_OUT.ix[:,14].values  #read 14th column only => Planting date 
        HWAM = df_OUT.ix[:,21].values  #read 21th column only

        DOY_array = np.zeros(len(HWAM)) #np.empty((ndays,1))*np.nan #initialize

        for i in range(len(HWAM)):
            # year_array[i] = int(repr(PDAT[i])[:4])
            DOY_array[i] = int(repr(PDAT[i])[4:])
                
        #make three arrays into a dataframe
        df = pd.DataFrame({'DOY': DOY_array, 'HWAM':HWAM}, columns=['DOY','HWAM'])
        ##====================================================
        ##READ Yields from historical weather observations
        self.Wdir_path = self.__getWorkingDir()

        fname1 = self.Wdir_path + "\\"+ sname+"_output_hist\\SUMMARY.OUT"

        #check if output  SUMMARY.OUt exists
        if not os.path.isfile(fname1):
            print( '**Error!!- SUMMARY.OUT file does not exist under ####_output_hist folder')
            os.system('pause')

        df_OUT2=pd.read_csv(fname1,delim_whitespace=True ,skiprows=3)
        PDAT2 = df_OUT2.ix[:,14].values  #read 14th column only => Planting date 
        HWAM2 = df_OUT2.ix[:,21].values  #read 21th column only
        year_array2 = np.zeros(len(HWAM2)) #np.empty((ndays,1))*np.nan #initialize
        DOY_array2 = np.zeros(len(HWAM2)) #np.empty((ndays,1))*np.nan #initialize

        BN_list = []
        NN_list = []
        AN_list = []
        for i in range(len(HWAM2)):
            year_array2[i] = int(repr(PDAT2[i])[:4])
            DOY_array2[i] = int(repr(PDAT2[i])[4:])
        #make three arrays into a dataframe
        df2 = pd.DataFrame({'Year': year_array2,'DOY': DOY_array2, 'HWAM':HWAM2}, columns=['Year','DOY','HWAM'])
        #===========================
        # extract target values corresponding to 33% and 67%
        for Pdate in pdate_list:
            yield_frst= df.HWAM[(df["DOY"] == Pdate)].values 
            yield_hist= df2.HWAM[(df2["DOY"] == Pdate)].values

            #a) compute CDF curve from the simulated yields-frst
            sorted_yield_frst = np.sort(yield_frst) #sort monthly rain from smallest to largest
            index_yield_frst = np.argsort(yield_frst) #** argsort - returns the original indexes of the sorted array
            temp = np.zeros(len(yield_frst))+1
            pdf = np.divide(temp,len(yield_frst))  #1/100years 
            cdf = np.cumsum(pdf)  #compute CDF

            #b) compute CDF curve from the simulated yields-historical
            sorted_yield_hist = np.sort(yield_hist) #sort monthly rain from smallest to largest
            index_yield_hist = np.argsort(yield_hist) #** argsort - returns the original indexes of the sorted array
            temp = np.zeros(len(yield_hist))+1
            pdf2 = np.divide(temp,len(yield_hist))  #1/30 years 
            cdf2 = np.cumsum(pdf2)  #compute CDF

            #C) compute threshold yield vaues (33% & 67%) from the yield-historical distribution
            #numpy.interp(x, xp, fp, left=None, right=None, period=None)
            yield_thres = np.interp([0.33,0.67],cdf2,sorted_yield_hist,left=sorted_yield_hist[0])# 100 WGEN outputs to theoretical CDF SCF curve

            #d) compute threshold yield vaues (33% & 67%) from the yield-historical distribution
            tercile2 = np.interp(yield_thres, sorted_yield_frst, cdf, left=0)      

            #e) add to the list
            BN_list.append(tercile2[0]*100)  #0.1167
            AN_list.append(100 - tercile2[1]*100)  # 1- 0.4297
            NN_list.append((tercile2[1]- tercile2[0])*100)  #NN= 100-BN - AN, e.g.,( 0.4297-0.1167) *100
            
        #f) Write the extract BN, NN and AN into a csv file
        temp_csv=self.Wdir_path.replace("/","\\") + "\\"+ sname+"_output\\yield_tercile_forecast.csv" 
        with open(temp_csv, 'w', newline='') as csvfile:
            scfwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            scfwriter.writerow(['Pdate_DOY','BN','NN', 'AN'])
            for i in range(len(pdate_list)):
                scfwriter.writerow([repr(pdate_list[i])] + [repr(BN_list[i])]+ [repr(NN_list[i])] + [repr(AN_list[i])])



# function for setting the colors of the box plots pairs
def setBoxColors(bp):
    plt.setp(bp['boxes'][0], color='blue')
    plt.setp(bp['caps'][0], color='blue')
    plt.setp(bp['caps'][1], color='blue')
    plt.setp(bp['whiskers'][0], color='blue')
    plt.setp(bp['whiskers'][1], color='blue')
    plt.setp(bp['fliers'][0], color='blue')
    plt.setp(bp['fliers'][1], color='blue')
    plt.setp(bp['medians'][0], color='blue')

    plt.setp(bp['boxes'][1], color='red')
    plt.setp(bp['caps'][2], color='red')
    plt.setp(bp['caps'][3], color='red')
    plt.setp(bp['whiskers'][2], color='red')
    plt.setp(bp['whiskers'][3], color='red')
    plt.setp(bp['fliers'][1], color='red')
    plt.setp(bp['fliers'][1], color='red')
    plt.setp(bp['medians'][1], color='red')



#Execute the application
if __name__ == "__main__":
    pyforms.start_app( CAMDT, geometry=(300, 100, 500, 500) )
    # pyforms.start_app( Warning, geometry=(300, 100, 500, 500) )
    